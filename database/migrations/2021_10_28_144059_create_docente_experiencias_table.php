<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocenteExperienciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('docente_experiencia', function (Blueprint $table) {
            $table->id();
            $table->foreignId('docente_id')->constrained('docente')->onUpdate('no action')->onDelete('no action');
            $table->string('entidad',255);
            $table->string('cargo',30);
            $table->string('area');
            $table->text('funciones');
            $table->date('ingreso');
            $table->date('egreso');
            $table->string('sustento');
            $table->Integer('cod_usu');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('docente_experiencia');
    }
}
