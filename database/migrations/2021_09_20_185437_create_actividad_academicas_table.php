<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActividadAcademicasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actividad_academica', function (Blueprint $table) {
            $table->bigInteger('id')->primary()->unsigned();
            $table->string('codigo',15)->unique();
            $table->string('titulo',255)->index();
            $table->boolean('programado')->default(0);
            $table->text('descripcion')->nullable();
            $table->foreignId('sub_gerencia_id')->constrained('sub_gerencia')->onUpdate('no action')->onDelete('no action');
            $table->foreignId('tipo_actividad_id')->constrained('tipo_actividad')->onUpdate('no action')->onDelete('no action');
            $table->foreignId('modalidad_id')->constrained('modalidad')->onUpdate('no action')->onDelete('no action');
            $table->string('sede',4)->nullable();
            $table->foreignId('sistema_id')->constrained('sistema')->onUpdate('no action')->onDelete('no action');
            $table->foreignId('tematica_id')->constrained('tematica')->onUpdate('no action')->onDelete('no action');
            $table->foreignId('bloque_academico_id')->constrained('bloque_academico')->onUpdate('no action')->onDelete('no action');
            $table->foreignId('tipo_certificado_id')->constrained('tipo_certificado')->onUpdate('no action')->onDelete('no action');
            $table->integer('coordinacion_nacional_id')->nullable();
            $table->string('origen',15);
            $table->string('organizador_id',4)->nullable();
            $table->boolean('convenio')->default(0);
            $table->integer('convenio_id')->nullable();
            $table->date('fecha_inicio')->nullable();
            $table->date('fecha_final')->nullable();
            $table->smallInteger('duracion')->nullable();
            $table->string('horario_detallado',255)->nullable();
            $table->json('horarioj_detallado')->nullable();
            $table->smallInteger('nro_aulas')->nullable();
            $table->smallInteger('nro_vacantes')->nullable();
            $table->smallInteger('nro_docentes')->nullable();
            $table->smallInteger('nro_discentes')->nullable();
            $table->date('fecha_inicioe')->nullable();
            $table->date('fecha_finale')->nullable();
            $table->smallInteger('nro_aulase')->nullable();
            $table->smallInteger('nro_vacantese')->nullable();
            $table->smallInteger('nro_docentese')->nullable();
            $table->smallInteger('duracione')->nullable();
            $table->tinyInteger('convocatoria')->default(0);
            $table->tinyInteger('publico')->default(0);
            $table->tinyInteger('compromiso')->default(0);
            $table->tinyInteger('autoformativo')->default(0);
            $table->text('observacion')->nullable();
            $table->string('coordinador',8)->nullable();
            $table->integer('tutor')->nullable();
            $table->integer('eva')->default(0);
            $table->tinyInteger('estado')->default(1);
            $table->integer('tipo_modulo')->nullable();
            $table->Integer('cod_usuc');
            $table->Integer('cod_usum');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actividad_academica');
    }
}
