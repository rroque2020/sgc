<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActividadDocentesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actividad_docente', function (Blueprint $table) {
            $table->id();
            $table->foreignId('actividad_academica_id')->constrained('actividad_academica')->onUpdate('no action')->onDelete('no action');
            $table->foreignId('docente_id')->constrained('docente')->onUpdate('no action')->onDelete('no action');
            $table->tinyInteger('estado')->default(1);
            $table->timestamp('fecha_hora')->nullable();
            $table->string('certificado',255)->nullable();
            $table->boolean('certificado_entrega')->default(0);
            $table->date('fecha_entrega')->nullable();
            $table->string('observacion_entrega',255)->nullable();
            $table->Integer('cod_usuc');
            $table->Integer('cod_usum');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actividad_docente');
    }
}
