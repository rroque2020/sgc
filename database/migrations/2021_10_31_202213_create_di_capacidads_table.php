<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiCapacidadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('di_capacidad', function (Blueprint $table) {
            $table->id();

            $table->foreignId('di_competencia_id')->constrained('di_competencia')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('orden');
            $table->text('descripcion');
            $table->unique(['di_competencia_id', 'orden'],'uk_capacidad');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('di_capacidad');
    }
}
