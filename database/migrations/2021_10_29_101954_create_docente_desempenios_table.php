<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocenteDesempeniosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('docente_desempenio', function (Blueprint $table) {
            $table->id();
            $table->foreignId('actividad_docente_id')->constrained('actividad_docente')->onUpdate('no action')->onDelete('no action');
            $table->Integer('nota');
            $table->Integer('rating');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('docente_desempenio');
    }
}
