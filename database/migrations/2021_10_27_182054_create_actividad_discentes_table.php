<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActividadDiscentesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actividad_discente', function (Blueprint $table) {
            $table->id();
            $table->foreignId('actividad_academica_id')->constrained('actividad_academica')->onUpdate('no action')->onDelete('no action');
            $table->foreignId('discente_id')->constrained('discente')->onUpdate('no action')->onDelete('no action');
            $table->string('certificado',255)->default(0);
            $table->boolean('certificado_entrega')->default(0);
            $table->date('fecha_entrega')->nullable();
            $table->string('observacion_entrega',255)->nullable();
            $table->tinyInteger('nota')->default(0);
            $table->tinyInteger('estado')->default(1);
            $table->Integer('cod_usuc');
            $table->Integer('cod_usum');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actividad_discente');
    }
}
