<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class SpGenerarCodigoModulo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS sp_generar_codigo_modulo;');
        DB::unprepared("CREATE PROCEDURE sp_generar_codigo_modulo(in actividad int, out codigo int)
        BEGIN
            DECLARE done INT DEFAULT FALSE;
            DECLARE cont INT DEFAULT 0;
            DECLARE total INT DEFAULT 0;
            DECLARE it INT;
            DECLARE bandera INT DEFAULT 0;
            DECLARE curModulo CURSOR FOR SELECT item FROM di_modulo WHERE actividad_academica_id = actividad ORDER BY item;
            DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

            SELECT COUNT(*) INTO total FROM di_modulo WHERE actividad_academica_id = actividad;

            IF total = 0 THEN
                SET codigo = 1;
            ELSE
                OPEN curModulo;
                getItem: LOOP
                    FETCH curModulo INTO it;
                    IF done THEN LEAVE getItem; END IF;

                    SET cont = cont + 1;
                    IF cont <> it THEN
                        SET bandera = 1;
                        LEAVE getItem;
                    END IF;

                END LOOP getItem;

                CLOSE curModulo;

                IF bandera = 1 THEN
                    SET codigo = cont;
                ELSE
                    SET codigo = cont + 1;
                END IF;

            END IF;
        END");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS sp_generar_codigo_modulo;');
    }
}
