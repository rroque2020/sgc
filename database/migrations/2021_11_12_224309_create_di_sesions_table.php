<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiSesionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('di_sesion', function (Blueprint $table) {
            $table->id();
            $table->foreignId('di_capacidad_id')->constrained('di_capacidad')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('orden');
            $table->text('descripcion');
            $table->integer('duracion');
            $table->foreignId('tipo_actividad_estrategica_id')->constrained('tipo_actividad_estrategica')->onUpdate('no action')->onDelete('no action');
            $table->text('des_actividad_estrategica_id');
            $table->text('recursos_obligatorios');
            $table->text('recursos_complementarios')->nullable();
            $table->timestamp('fecha_hora_inicio');
            $table->timestamp('fecha_hora_cierre');
            $table->unique(['di_capacidad_id', 'orden'],'uk_sesion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('di_sesion');
    }
}
