<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiModulosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('di_modulo', function (Blueprint $table) {
            $table->id();
            $table->foreignId('actividad_academica_id')->constrained('actividad_academica')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('tipo_capacitacion_id')->constrained('tipo_capacitacion')->onUpdate('cascade')->onDelete('cascade');
            $table->smallInteger('item');
            $table->string('descripcion',255);
            $table->string('abreviatura',15)->nullable();
            $table->string('nombre',255);
            $table->integer('duracion')->nullable();
            $table->date('fecha_inicio')->nullable();
            $table->date('fecha_final')->nullable();
            $table->unique(['actividad_academica_id', 'item'],'uk_modulo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('di_modulo');
    }
}
