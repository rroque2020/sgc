<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLineaEstrategicasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('linea_estrategica', function (Blueprint $table) {
            $table->id();
            $table->string('descripcion',255);
            $table->string('nombre_corto',15)->nullable();
            $table->string('abreviatura',15)->nullable();
            $table->boolean('estado')->default(1);
            $table->Integer('cod_usuc');
            $table->Integer('cod_usum');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('linea_estrategica');
    }
}
