<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocenteFormacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('docente_formacion', function (Blueprint $table) {
            $table->id();
            $table->foreignId('docente_id')->constrained('docente')->onUpdate('no action')->onDelete('no action');
            $table->foreignId('grado_academico_id')->constrained('grado_academico')->onUpdate('no action')->onDelete('no action');
            $table->foreignId('institucion_id')->constrained('institucion')->onUpdate('no action')->onDelete('no action');
            $table->foreignId('profesion_id')->constrained('profesion')->onUpdate('no action')->onDelete('no action');
            $table->integer('ingreso');
            $table->integer('egreso');
            $table->string('sustento');
            $table->Integer('cod_usu');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('docente_formacion');
    }
}
