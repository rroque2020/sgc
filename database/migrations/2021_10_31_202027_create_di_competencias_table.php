<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiCompetenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('di_competencia', function (Blueprint $table) {
            $table->id();
            $table->integer('orden');
            $table->foreignId('di_modulo_id')->constrained('di_modulo')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('competencia_perfil_personal_id')->constrained('competencia_perfil_personal')->onUpdate('cascade')->onDelete('cascade');
            $table->unique(['di_modulo_id', 'competencia_perfil_personal_id'],'uk_competencia');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('di_competencia');
    }
}
