<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCriterioDiscenteCargosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('criterio_discente_cargo', function (Blueprint $table) {
            $table->id();
            $table->foreignId('criterio_discente_id')->constrained('criterio_discente')->onUpdate('cascade')->onDelete('cascade');
            $table->string('cargo_id',255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('criterio_discente_cargo');
    }
}
