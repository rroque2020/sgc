<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocenteCapacitacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('docente_capacitacion', function (Blueprint $table) {
            $table->id();
            $table->foreignId('docente_id')->constrained('docente')->onUpdate('no action')->onDelete('no action');
            $table->string('tipo',255);
            $table->string('institucion',255);
            $table->string('especialidad',255);
            $table->date('ingreso');
            $table->date('egreso');
            $table->tinyInteger('horas');
            $table->string('sustento');
            $table->Integer('cod_usu');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('docente_capacitacion');
    }
}
