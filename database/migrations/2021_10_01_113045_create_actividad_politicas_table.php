<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActividadPoliticasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actividad_politica', function (Blueprint $table) {
            $table->id();
            $table->foreignId('actividad_academica_id')->constrained('actividad_academica')->onUpdate('no action')->onDelete('no action');
            $table->foreignId('politica_id')->constrained('politica')->onUpdate('no action')->onDelete('no action');
            $table->Integer('cod_usu');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actividad_politica');
    }
}
