<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiActividadEvaluativasTable extends Migration
{
    public function up()
    {
        Schema::create('di_actividad_evaluativa', function (Blueprint $table) {
            $table->id();
            $table->foreignId('di_modulo_id')->constrained('di_modulo')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('tipo_actividad_evaluativa_id')->constrained('tipo_actividad_evaluativa')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('orden');
            $table->text('descripcion');
            $table->tinyInteger('porcentaje');
            $table->foreignId('instrumento_evaluativo_id')->constrained('instrumento_evaluativo')->onUpdate('no action')->onDelete('no action');
            $table->tinyInteger('nota_aprobacion');
            $table->timestamp('fecha_hora_inicio');
            $table->timestamp('fecha_hora_cierre');
            $table->unique(['di_modulo_id', 'orden'],'uk_actividad_evaluativa');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('di_actividad_evaluativa');
    }
}
