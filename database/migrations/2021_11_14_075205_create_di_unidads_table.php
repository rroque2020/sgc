<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiUnidadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('di_unidad', function (Blueprint $table) {
            $table->id();
            $table->foreignId('di_sesion_id')->constrained('di_sesion')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('orden');
            $table->text('nombre');
            $table->text('descripcion');
            $table->text('bibliografia');
            $table->unique(['di_sesion_id', 'orden'],'uk_unidad');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('di_unidad');
    }
}
