<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreInscritosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_inscrito', function (Blueprint $table) {
            $table->id();
            $table->foreignId('actividad_academica_id')->constrained('actividad_academica')->onUpdate('no action')->onDelete('no action');
            $table->boolean('tipo')->default(1);
            $table->string('dni',8);
            $table->string('apellidos',255);
            $table->string('nombres',255);
            $table->string('celular',9)->nullable();
            $table->string('email',255)->nullable();
            $table->tinyInteger('estado_correo')->default(0);
            $table->timestamp('fecha_hora')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_inscrito');
    }
}
