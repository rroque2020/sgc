<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('persona', function (Blueprint $table) {
            $table->id();
            $table->boolean('tipo')->default(1);
            $table->string('dni',8);
            $table->string('apellidos',255);
            $table->string('nombres',255);
            $table->string('celular',9)->nullable();
            $table->date('fecha')->nullable();
            $table->string('email',255)->nullable();
            $table->string('genero',1)->nullable();
            $table->string('idcargo',255)->nullable();
            $table->string('cargo',255)->nullable();
            $table->string('iddependencia',255)->nullable();
            $table->string('institucion',255)->nullable();
            $table->string('foto',255)->nullable();
            $table->Integer('cod_usuc')->default(0);
            $table->Integer('cod_usum')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('persona');
    }
}
