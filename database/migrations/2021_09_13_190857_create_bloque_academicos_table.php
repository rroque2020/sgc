<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBloqueAcademicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bloque_academico', function (Blueprint $table) {
            $table->id();
            $table->foreignId('linea_estrategica_id')->constrained('linea_estrategica')->onUpdate('no action')->onDelete('no action');
            $table->string('descripcion',255);
            $table->string('abreviatura',15)->nullable();
            $table->boolean('estado')->default(1);
            $table->Integer('cod_usuc');
            $table->Integer('cod_usum');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bloque_academico');
    }
}
