<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDistritoFiscalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distrito_fiscal', function (Blueprint $table) {
            $table->string('id',4)->primary();
            $table->string('descripcion',255);
            $table->string('abreviatura',30)->nullable();
            $table->double('latitud')->nullable();
            $table->double('longitud')->nullable();
            $table->boolean('estado')->default(1);
            $table->Integer('cod_usuc');
            $table->Integer('cod_usum');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('distrito_fiscal');
    }
}
