<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCriterioDiscentePerfilsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('criterio_discente_perfil', function (Blueprint $table) {
            $table->id();
            $table->foreignId('criterio_discente_id')->constrained('criterio_discente')->onUpdate('cascade')->onDelete('no action');
            $table->foreignId('perfil_personal_id')->constrained('perfil_personal')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('criterio_discente_perfil');
    }
}
