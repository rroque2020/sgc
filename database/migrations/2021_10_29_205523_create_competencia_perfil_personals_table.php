<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompetenciaPerfilPersonalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('competencia_perfil_personal', function (Blueprint $table) {
            $table->id();
            $table->string('codigo',15)->unique();
            $table->foreignId('perfil_personal_id')->constrained('perfil_personal')->onUpdate('no action')->onDelete('no action');
            $table->string('tipo',255);
            $table->text('descripcion');
            $table->Integer('cod_usuc');
            $table->Integer('cod_usum');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('competencia_perfil_personal');
    }
}
