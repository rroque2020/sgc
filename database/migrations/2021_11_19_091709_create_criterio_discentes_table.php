<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCriterioDiscentesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('criterio_discente', function (Blueprint $table) {
            $table->id();
            $table->foreignId('actividad_academica_id')->constrained('actividad_academica')->onUpdate('cascade')->onDelete('cascade');
            $table->boolean('dni')->default(0);
            $table->boolean('aprobado')->default(0);
            $table->boolean('temporalidad')->default(0);
            $table->boolean('sancion')->default(0);
            $table->string('especialidad',255)->nullable();
            $table->boolean('invitados')->default(0);
            $table->boolean('brecha')->default(0);
            $table->Integer('cod_usu');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('criterio_discente');
    }
}
