<?php
namespace App\Helpers;
use App\Models\ActividadAcademica;
use App\Models\LineaEstrategica;
use Carbon\Carbon;

class Helper
{
    public static function mestoCadena($mes){
        $cadena = '';
        switch ($mes) {
            case 1:
                $cadena = 'Enero';
                break;
            case 2:
                $cadena = 'Febrero';
                break;
            case 3:
                $cadena = 'Marzo';
                break;
            case 4:
                $cadena = 'Abril';
                break;
            case 5:
                $cadena = 'Mayo';
                break;
            case 6:
                $cadena = 'Junio';
                break;
            case 7:
                $cadena = 'Julio';
                break;
            case 8:
                $cadena = 'Agosto';
                break;
            case 9:
                $cadena = 'Setiembre';
                break;
            case 10:
                $cadena = 'Octubre';
                break;
            case 11:
                $cadena = 'Noviembre';
                break;
            case 12:
                $cadena = 'Diciembre';
                break;
        }

        return $cadena;
    }

    public static function extraerAnio($fecha){
        return Carbon::createFromFormat('d/m/Y', $fecha)->format('Y');
    }

    public static function estadoAA($estado, $span){
        $cadena = "";
        switch ($estado) {
            case 1:
                $cadena = ($span) ? 'POR INICIAR' : "<span class='badge-est badge-estado1'>POR INICIAR</span>";
                break;
            case 2:
                $cadena = ($span) ? 'EN DISEÑO' : "<span class='badge-est badge-estado2'>EN DISEÑO</span>";
                break;
            case 3:
                $cadena = ($span) ? 'DISEÑO APROBADO' : "<span class='badge-est badge-estado3'>DISEÑO APROBADO</span>";
                break;
            case 4:
                $cadena = ($span) ? 'EN CONVOCATORIA' : "<span class='badge-est badge-estado4'>EN CONVOCATORIA</span>";
                break;
            case 5:
                $cadena = ($span) ? 'REGISTRO EVA' : "<span class='badge-est badge-estado4'>REGISTRO EVA</span>";
                break;
            case 6:
                $cadena = ($span) ? 'EN PROCESO' : "<span class='badge-est badge-estado5'>EN PROCESO</span>";
                break;
            case 7:
                $cadena = ($span) ? 'CULMINADO' : "<span class='badge-est badge-estado6'>CULMINADO</span>";
                break;
            case 8:
                $cadena = ($span) ? 'PARA CERTIFICAR' : "<span class='badge-est badge-estado7'>PARA CERTIFICAR</span>";
                break;
            case 9:
                $cadena = ($span) ? 'CERRADO' : "<span class='badge-est badge-estado8'>CERRADO</span>";
                break;
            case 10:
                $cadena = ($span) ? 'BAJA' : "<span class='badge-est badge-estado9'>BAJA</span>";
                break;
        }

        return $cadena;
    }

    public static function estado(){
        $cadena = "<option value='1'>POR INICIAR</option>";
        $cadena .= "<option value='2'>EN DISEÑO</option>";
        $cadena .= "<option value='3'>DISEÑO APROBADO</option>";
        $cadena .= "<option value='4'>EN CONVOCATORIA</option>";
        $cadena .= "<option value='5'>REGISTRO EVA</option>";
        $cadena .= "<option value='6'>EN PROCESO</option>";
        $cadena .= "<option value='7'>CULMINADO</option>";
        $cadena .= "<option value='8'>PARA CERTIFICAR</option>";
        $cadena .= "<option value='9'>CERRADO</option>";
        $cadena .= "<option value='10'>BAJA</option>";

        return $cadena;
    }

    public static function limpiaEspacios($cadena){
        $cadena = str_replace(' ', '', $cadena);
        return $cadena;
    }

    public static function fechaActual(){
        return Carbon::now();
    }

    public static function codigoAA($anio, $linea){
        $abrev = LineaEstrategica::listarDatos($linea)->abreviatura;
        $maximo = ActividadAcademica::maximo($anio.$abrev)->maximo;
        if((int)$maximo != 0){
            $cont = (int) filter_var($maximo, FILTER_SANITIZE_NUMBER_INT);
            $codigo = $anio.$abrev.str_pad($cont + 1, 3, '0', STR_PAD_LEFT);
        }else{
            $codigo = $anio.$abrev.str_pad(1, 3, '0', STR_PAD_LEFT);
        }
        return $codigo;
    }

    public static function usuarioActual(){
     return 1;
//        return auth()->user();
    }

    public static function convertirARomano($integer, $upcase = true)
    {
        $table = array('M'=>1000, 'CM'=>900, 'D'=>500, 'CD'=>400, 'C'=>100,
            'XC'=>90, 'L'=>50, 'XL'=>40, 'X'=>10, 'IX'=>9,
            'V'=>5, 'IV'=>4, 'I'=>1);
        $return = '';
        while($integer > 0)
        {
            foreach($table as $rom=>$arb)
            {
                if($integer >= $arb)
                {
                    $integer -= $arb;
                    $return .= $rom;
                    break;
                }
            }
        }
        return $return;
    }
}
