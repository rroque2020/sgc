<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CheckSelect implements Rule
{
    protected $alias = 'checkselect';

    public function __toString()
    {
        return $this->alias;
    }

    public function __construct()
    {
    }

    public function passes($attribute, $value)
    {
        if ($value <> -1) {
            return true;
        }
    }

    public function message()
    {
        return 'El :attribute debe seleccionar.';
    }
}
