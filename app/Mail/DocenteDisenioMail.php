<?php

namespace App\Mail;

use App\Exports\DisenioInstruccionalExport;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Maatwebsite\Excel\Facades\Excel;

class DocenteDisenioMail extends Mailable
{
    use Queueable, SerializesModels;

    public $detalles;

    public function __construct($detalles)
    {
        $this->detalles = $detalles;
    }

    public function build()
    {
        return $this->to($this->detalles['destinatario'])
            ->subject('Escuela del Ministerio Público - Remito Diseño Instruccional')
            ->markdown('docente.correodisenio')
            ->attach(
                 Excel::download(new DisenioInstruccionalExport($this->detalles['codactividad']), $this->detalles['codigo'].'.xlsx')
                                ->getFile(),['as' => $this->detalles['codigo'].'.xlsx'])
            ->with('detalles', $this->detalles);
    }
}
