<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PreInscritoMail extends Mailable
{
    use Queueable, SerializesModels;

    public $detalles;

    public function __construct($detalles)
    {
        $this->detalles = $detalles;
    }

    public function build()
    {
        return $this->to($this->detalles['destinatario'])
            ->subject('Escuela del Ministerio Público - Confirmación de Pre-Inscripción')
            ->markdown('discente.email.preinscrito')
            ->with('detalles', $this->detalles);
    }
}
