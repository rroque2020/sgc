<?php

namespace App\Http\Controllers;

use App\Models\BloqueAcademico;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class BloqueAcademicoController extends Controller
{
    public function listarByLinea($linea)
    {
        if (request()->ajax()) {
            $bloques = BloqueAcademico::listarByLinea($linea);

            return response()->json(['bloques' => $bloques]);
        }
    }
}
