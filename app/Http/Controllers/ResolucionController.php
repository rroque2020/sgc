<?php

namespace App\Http\Controllers;

use App\Models\Resolucion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class ResolucionController extends Controller
{
    public function listarDatos($codigo){
        if (request()->ajax()) {
            $resolucion = Resolucion::listarByCodigo($codigo);

            return Response::json(['resolucion' => $resolucion]);
        }
    }
}
