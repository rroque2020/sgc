<?php

namespace App\Http\Controllers;

use App\Models\TDependencia;
use Illuminate\Http\Request;

class TDependenciaController extends Controller
{
    public function listarByDistrito($distrito)
    {
        if (request()->ajax()) {
            $dependencias = TDependencia::listarByDistrito($distrito);
            return response()->json(['dependencias' => $dependencias]);
        }
    }
}
