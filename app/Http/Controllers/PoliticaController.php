<?php

namespace App\Http\Controllers;

use App\Models\Politica;
use Illuminate\Http\Request;

class PoliticaController extends Controller
{
    public function listarDatos($codigo){
        if(request()->ajax()){
            $politica = Politica::listarByCodigo($codigo);

            return response()->json(['politica' => $politica]);
        }
    }
}
