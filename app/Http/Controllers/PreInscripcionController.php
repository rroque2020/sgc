<?php

namespace App\Http\Controllers;

use App\Http\Requests\ActividadDiscenteRequest;
use App\Http\Requests\PreInscripcionWebRequest;
use App\Mail\PreInscritoMail;
use App\Models\ActividadAcademica;
use App\Models\ActividadDiscente;
use App\Models\CriterioDiscente;
use App\Models\CriterioDiscenteCargo;
use App\Models\CriterioDiscenteDistritoFiscal;
use App\Models\CriterioDiscentePerfil;
use App\Models\PreInscrito;
use App\Models\TMaestroPersonal;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class PreInscripcionController extends Controller
{
    public function registroweb($actividad)
    {
        if(request()->ajax()){
            $invitado = CriterioDiscente::listarDatos($actividad)->invitados;

            return view("discente.preinscripcion.registroweb")
                ->with(compact('invitado'))
                ->with(compact('actividad'));
        }
    }

    public function listardatosdniweb($invitado, $dni)
    {
        if(request()->ajax()) {
            $psinvitado = 0;
            $data = TMaestroPersonal::listarDatosbyDNI($dni);
            if (isset($data)) {
                $apellidos = $data->APE_PAT_PER . ' ' . $data->APE_MAT_PER ;
                $nombres = $data->NOM_EMP_PER;
            } else {
                if ($invitado == 1) {
                    $data = (new ApiDNIController())->getDatos($dni);
                    if (isset($data)) {
                        $apellidos = $data->original['apellidoPaterno'] . ' ' . $data->original['apellidoMaterno'];
                        $nombres = $data->original['nombres'];
                        $psinvitado = 1;
                    } else {
                        $apellidos = '';
                        $nombres = '';
                    }
                } else {
                    $apellidos = '';
                    $nombres = '';
                }
            }
            return response()->json(['apellidos' => $apellidos, 'nombres' => $nombres, 'invitado' => $psinvitado]);
        }
    }

    public function preinscribirweb(PreInscripcionWebRequest $request)
    {
        if(request()->ajax()) {
            $data = $request->all();
            $actividad = $data['codactividad'];
            $invitado = $data['invitado'];

            $valor = false;
            $criterio = CriterioDiscente::listarDatos($actividad);

//            if ($criterio->aprobado == 1) {
//                $notas = CriterioDiscente::verificarAprobadoPreRequisito($actividad,$data['dni']);
//
//                if (!count($notas) > 0) {
//                    return response()->json(['valor' => $valor, 'mensaje' => 'Ud. ha desaprobado en Actividades Académicas anteriores, no puede pre-inscribirse']);
//                }
//            }
//
//            if ($criterio->temporalidad == 1) {
//                $paralelos = CriterioDiscente::verificaCruceHorarios($data['dni']);
//
//                if (count($paralelos) > 0) {
//                    return response()->json(['valor' => $valor, 'mensaje' => 'Ud. esta inscrito en otras Actividades Académicas, no puede pre-inscribirse']);
//                }
//            }
//
//            if ($criterio->brecha == 1) {
//                $paralelos = CriterioDiscente::verificarParticipacionActividad($criterio->especialidad, $data['dni']);
//
//                if (count($paralelos) > 0) {
//                    return response()->json(['valor' => $valor, 'mensaje' => 'Ud. esta inscrito en otras Actividades Académicas, no puede pre-inscribirse']);
//                }
//            }
//
//            if (isset($criterio->especialidad) && $invitado != 1) {
//                $dependencias = CriterioDiscente::verificarEspecialidad($criterio->especialidad, $data['dni']);
//                if (count($dependencias) == 0) {
//                    return response()->json(['valor' => $valor, 'mensaje' => 'Ud. no se encuentra afin a la Especialidad de la Actividad Académica, no puede pre-inscribirse']);
//                }
//            }
//
//            $perfiles = CriterioDiscentePerfil::listarPerfilesSeleccionados($actividad)->pluck('id');
//            if (count($perfiles) > 0 && $invitado != 1) {
//                $pasa = 0;
//                for($i=0; $i<count($perfiles); $i++){
//                    $perf = CriterioDiscente::listarPertenenciaPerfil($perfiles[$i], $data['dni']);
//
//                    if(count($perf) > 0){
//                        $pasa = 1;
//                        break;
//                    }
//                }
//
//                if ($pasa == 0) {
//                    return response()->json(['valor' => $valor, 'mensaje' => 'Ud. no tiene el Perfil de Personal al que está dirigido la Actividad Académica, no puede pre-inscribirse']);
//                }
//            }
//
//            $distritos = CriterioDiscenteDistritoFiscal::listarDistritosSeleccionados($actividad)->pluck('id');
//            if (count($distritos) > 0 && $invitado != 1) {
//                $distr = CriterioDiscente::listarPertenenciaDistritoFiscal($distritos, $data['dni']);
//
//                if (count($distr) == 0) {
//                    return response()->json(['valor' => $valor, 'mensaje' => 'Ud. no se encuentra en el Distrito al que está dirigido la Actividad Académica, no puede pre-inscribirse']);
//                }
//            }
//
//            $cargos = CriterioDiscenteCargo::listarCargosSeleccionados($actividad)->pluck('id');
//            if (count($cargos) > 0 && $invitado != 1) {
//                $carg = CriterioDiscente::listarPertenenciaCargo($cargos, $data['dni']);
//                if (count($carg) == 0) {
//                    return response()->json(['valor' => $valor, 'mensaje' => 'Ud. no tiene el Cargo al que está dirigido la Actividad Académica, no puede pre-inscribirse']);
//                }
//            }

            $preins = PreInscrito::listarDatosByDNI($data['dni']);

            if($preins){
                $item = PreInscrito::findOrFail($preins->id);
                $item->celular = $data['celular'];
                $item->email = $data['correo'];
            }else{
                $item = new PreInscrito();
                $item->actividad_academica_id = $actividad;
                if($invitado == 1)
                    $item->tipo = 0;
                else
                    $item->tipo = 1;
                $item->dni = $data['dni'];
                $item->apellidos = strtoupper($data['apellidos']);
                $item->nombres = strtoupper($data['nombres']);
                $item->celular = $data['celular'];
                $item->email = $data['correo'];
            }

            /////////////////////////
            $act = ActividadAcademica::findOrFail($data['codactividad']);

            $detalles = [
                'destinatario' => $item->email,
                'nombres' => $item->nombres,
                'titulo' => $act->titulo,
            ];

            Mail::send(new PreInscritoMail($detalles));

            if( count(Mail::failures()) > 0 ) {
            } else {
                $item->estado_correo = 2;
                $item->fecha_hora = Carbon::now();

            }
            /////////////////////////
            $item->save();

            return response()->json(['valor' => 'true',
                'id' => $item->id,
                'actividad' => $item->actividad_academica_id,
                'mensaje' => 'Pre-Inscrito a la Actividad Académica correctamente, revise la bandeja de su Correo Electrónico ...']);

        }
    }

    public function index($id){
        $actividad = ActividadAcademica::findOrFail($id);
        $preinscritos = PreInscrito::listarForCoordinador($id);

        return view('discente.preinscripcion.index')
            ->with(compact('actividad'))
            ->with(compact('preinscritos'));
    }

    public function listar($id){
        $preinscritos = PreInscrito::listarForCoordinador($id);

        return view('discente.preinscripcion.listarpreinscritos')
            ->with(compact('preinscritos'));
    }

    public function mailmasivo(Request $request){
        if (request()->ajax()) {
            $data = $request->all();
            $actividad = ActividadAcademica::findOrFail($data['codactividad']);
            $ids = explode(",", $data['ids'][0]);

            $cont = 0;
            while ($cont < count($ids)) {
                $item = PreInscrito::findOrFail($ids[$cont]);

                $detalles = [
                    'destinatario' => $item->email,
                    'nombres' => $item->nombres,
                    'titulo' => $actividad->titulo,
                ];

                try{
                    Mail::send(new PreInscritoMail($detalles));
                }catch (\Exception $e) {
                }


                if( !count(Mail::failures()) > 0 ) {
                    $item->estado_correo = 2;
                    $item->fecha_hora = Carbon::now();
                    $item->save();
                }

                $cont = $cont + 1;
            }

            return response()->json('exito');
        }
    }

    public function registrocoordinador($actividad)
    {
        if(request()->ajax()){
            $invitado = CriterioDiscente::listarDatos($actividad)->invitados;

            return view("discente.preinscripcion.registrocoordinador")
                ->with(compact('invitado'))
                ->with(compact('actividad'));
        }
    }

    public function preinscribircoordinador(PreInscripcionWebRequest $request)
    {
        if(request()->ajax()) {
            $data = $request->all();
            $actividad = $data['codactividad'];
            $invitado = $data['invitado'];

            $valor = false;
            $criterio = CriterioDiscente::listarDatos($actividad);

//            if ($criterio->aprobado == 1) {
//                $notas = CriterioDiscente::verificarAprobadoPreRequisito($actividad,$data['dni']);
//
//                if (!count($notas) > 0) {
//                    return response()->json(['valor' => $valor, 'mensaje' => 'Ud. ha desaprobado en Actividades Académicas anteriores, no puede pre-inscribirse']);
//                }
//            }
//
//            if ($criterio->temporalidad == 1) {
//                $paralelos = CriterioDiscente::verificaCruceHorarios($data['dni']);
//
//                if (count($paralelos) > 0) {
//                    return response()->json(['valor' => $valor, 'mensaje' => 'Ud. esta inscrito en otras Actividades Académicas, no puede pre-inscribirse']);
//                }
//            }
//
//            if ($criterio->brecha == 1) {
//                $paralelos = CriterioDiscente::verificarParticipacionActividad($criterio->especialidad, $data['dni']);
//
//                if (count($paralelos) > 0) {
//                    return response()->json(['valor' => $valor, 'mensaje' => 'Ud. esta inscrito en otras Actividades Académicas, no puede pre-inscribirse']);
//                }
//            }
//
//            if (isset($criterio->especialidad) && $invitado != 1) {
//                $dependencias = CriterioDiscente::verificarEspecialidad($criterio->especialidad, $data['dni']);
//                if (count($dependencias) == 0) {
//                    return response()->json(['valor' => $valor, 'mensaje' => 'Ud. no se encuentra afin a la Especialidad de la Actividad Académica, no puede pre-inscribirse']);
//                }
//            }
//
//            $perfiles = CriterioDiscentePerfil::listarPerfilesSeleccionados($actividad)->pluck('id');
//            if (count($perfiles) > 0 && $invitado != 1) {
//                $pasa = 0;
//                for($i=0; $i<count($perfiles); $i++){
//                    $perf = CriterioDiscente::listarPertenenciaPerfil($perfiles[$i], $data['dni']);
//
//                    if(count($perf) > 0){
//                        $pasa = 1;
//                        break;
//                    }
//                }
//
//                if ($pasa == 0) {
//                    return response()->json(['valor' => $valor, 'mensaje' => 'Ud. no tiene el Perfil de Personal al que está dirigido la Actividad Académica, no puede pre-inscribirse']);
//                }
//            }
//
//            $distritos = CriterioDiscenteDistritoFiscal::listarDistritosSeleccionados($actividad)->pluck('id');
//            if (count($distritos) > 0 && $invitado != 1) {
//                $distr = CriterioDiscente::listarPertenenciaDistritoFiscal($distritos, $data['dni']);
//
//                if (count($distr) == 0) {
//                    return response()->json(['valor' => $valor, 'mensaje' => 'Ud. no se encuentra en el Distrito al que está dirigido la Actividad Académica, no puede pre-inscribirse']);
//                }
//            }
//
//            $cargos = CriterioDiscenteCargo::listarCargosSeleccionados($actividad)->pluck('id');
//            if (count($cargos) > 0 && $invitado != 1) {
//                $carg = CriterioDiscente::listarPertenenciaCargo($cargos, $data['dni']);
//                if (count($carg) == 0) {
//                    return response()->json(['valor' => $valor, 'mensaje' => 'Ud. no tiene el Cargo al que está dirigido la Actividad Académica, no puede pre-inscribirse']);
//                }
//            }

            $preins = PreInscrito::listarDatosByDNI($data['dni']);
            if($preins){
                $item = PreInscrito::findOrFail($preins->id);
                $item->celular = $data['celular'];
                $item->email = $data['correo'];
            }else{
                $item = new PreInscrito();
                $item->actividad_academica_id = $actividad;
                if($invitado == 1)
                    $item->tipo = 0;
                else
                    $item->tipo = 1;
                $item->dni = $data['dni'];
                $item->apellidos = strtoupper($data['apellidos']);
                $item->nombres = strtoupper($data['nombres']);
                $item->celular = $data['celular'];
                $item->email = $data['correo'];
            }

            /////////////////////////
            $act = ActividadAcademica::findOrFail($actividad);

            $detalles = [
                'destinatario' => $item->email,
                'nombres' => $item->nombres,
                'titulo' => $act->titulo,
            ];

            Mail::send(new PreInscritoMail($detalles));

            if( count(Mail::failures()) > 0 ) {
            } else {
                $item->estado_correo = 2;
                $item->fecha_hora = Carbon::now();

            }
            /////////////////////////
            $item->save();

            return response()->json(['valor' => 'true',
                'id' => $item->id,
                'actividad' => $item->actividad_academica_id,
                'mensaje' => 'Pre-Inscrito a la Actividad Académica correctamente, revise la bandeja de su Correo Electrónico ...']);
        }
    }
}
