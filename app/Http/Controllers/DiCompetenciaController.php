<?php

namespace App\Http\Controllers;

use App\Http\Requests\DiCompetenciaRequest;
use App\Models\ActividadAcademica;
use App\Models\CompetenciaPerfilPersonal;
use App\Models\DiCompetencia;
use App\Models\DiModulo;
use Illuminate\Http\Request;

class DiCompetenciaController extends Controller
{
    public function create($modulo)
    {
        if (request()->ajax()) {
            $tipos = CompetenciaPerfilPersonal::listarTipos();
            $competencias = CompetenciaPerfilPersonal::listar();

            return view('competencia.nuevo_editar')
                        ->with(compact('modulo'))
                        ->with(compact('tipos'))
                        ->with(compact('competencias'));
        }
    }

    public function store(DiCompetenciaRequest $request)
    {
        if (request()->ajax()) {
            $data = $request->all();

            $item = new DiCompetencia();
            $item->orden = $data['orden'];
            $item->di_modulo_id = $data['codmodulo'];
            $item->competencia_perfil_personal_id = $data['competencia'];
            $item->save();

            return response()->json(['modulo' => $item->di_modulo_id]);
        }
    }

    public function show($id)
    {
//        $estructuras = DiModulo::listarEstructuraComps($id);
        $competencias = DiModulo::listarCompetencias($id);
//        $capacidades = DiModulo::listarEstructuraCaps($id);
//        $sesiones = DiModulo::listarEstructuraSes($id);
//        $unidades = DiModulo::listarEstructuraUnd($id);
//        $pruebas = DiModulo::listarEstructuraPrueba($id);
        $modulo = $id;
        return view('disenioinstruccional.tablacompetencia')
                    ->with(compact('modulo'))
                    ->with(compact('competencias'));
    }

    public function edit($id)
    {
        if (request()->ajax()) {
            $tipos = CompetenciaPerfilPersonal::listarTipos();
            $competencias = CompetenciaPerfilPersonal::listar();
            $item = DiCompetencia::findOrFail($id);
            return view('competencia.nuevo_editar')
                ->with(compact('tipos'))
                ->with(compact('competencias'))
                ->with(compact('item'));
        }
    }

    public function ver($id)
    {
        if (request()->ajax()) {
            $item = DiCompetencia::listarDatos($id);
            return view('competencia.ver')
                        ->with(compact('item'));
        }
    }

    public function update(DiCompetenciaRequest $request, $id)
    {
        if (request()->ajax()) {
            $data = $request->all();

            $item = DiCompetencia::findOrFail($id);
            $item->orden = $data['orden'];

            $item->di_modulo_id = $data['codmodulo'];
            $item->competencia_perfil_personal_id = $data['competencia'];
            $item->save();

            return response()->json(['modulo' => $item->di_modulo_id]);
        }
    }

    public function destroy($id)
    {
        if (request()->ajax()) {
            $competencia = DiCompetencia::findOrFail($id);
            $competencia->delete();

            return response()->json('exito');
        }
    }
}
