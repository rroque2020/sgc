<?php

namespace App\Http\Controllers;

use App\Models\Plan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class PlanController extends Controller
{
    public function listarDatos($codigo){
        if(request()->ajax()){
            $plan = Plan::listarByCodigo($codigo);

            return Response::json(['plan' => $plan]);
        }
    }
}
