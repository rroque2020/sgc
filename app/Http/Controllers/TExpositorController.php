<?php

namespace App\Http\Controllers;


use App\Models\TExpositor;

class TExpositorController extends Controller
{
    public function listarByDNI($dni){
        if(request()->ajax()) {
            $datos = TExpositor::listar($dni);

            return response()->json($datos);
        }
    }
}
