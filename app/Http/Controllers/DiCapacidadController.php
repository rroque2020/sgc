<?php

namespace App\Http\Controllers;

use App\Http\Requests\DiCapacidadRequest;
use App\Models\DiCapacidad;
use App\Models\DiCompetencia;
use Illuminate\Http\Request;

class DiCapacidadController extends Controller
{
    public function create($competencia)
    {
        if (request()->ajax()) {
            $modulo = DiCompetencia::findOrFail($competencia)->di_modulo_id;
            return view('capacidad.nuevo_editar')
                ->with(compact('competencia'))
                ->with(compact('modulo'));
        }
    }

    public function store(DiCapacidadRequest $request)
    {
        if (request()->ajax()) {
            $data = $request->all();

            $item = new DiCapacidad();
            $item->di_competencia_id = $data['codcompetencia'];
            $item->orden = $data['orden'];
            $item->descripcion = $data['descripcion'];
            $item->save();

            return response()->json(['modulo' => $data['codmodulo'], 'competencia' => $data['codcompetencia']]);
        }
    }

    public function show($id)
    {
        $capacidades = DiCompetencia::listarCapacidades($id);
        $competencia = $id;

        return view('capacidad.index')
            ->with(compact('competencia'))
            ->with(compact('capacidades'));
    }

    public function edit($id)
    {
        if (request()->ajax()) {
            $item = DiCapacidad::findOrFail($id);
            $modulo = DiCompetencia::findOrFail($item->di_competencia_id)->di_modulo_id;
            return view('capacidad.nuevo_editar')
                ->with(compact('item'))
                ->with(compact('modulo'));
        }
    }

    public function update(DiCapacidadRequest $request, $id)
    {
        if (request()->ajax()) {
            $data = $request->all();

            $item = DiCapacidad::findOrFail($id);
            $item->di_competencia_id = $data['codcompetencia'];
            $item->orden = $data['orden'];
            $item->descripcion = $data['descripcion'];
            $item->save();

            return response()->json(['modulo' => $data['codmodulo'], 'competencia' => $data['codcompetencia']]);
        }
    }

    public function destroy($id)
    {
        if (request()->ajax()) {
            $capacidad = DiCapacidad::findOrFail($id);
            $modulo = DiCompetencia::findOrFail($capacidad->di_competencia_id)->di_modulo_id;
            $capacidad->delete();

            return response()->json(['modulo' => $data['codmodulo'], 'competencia' => $capacidad->di_competencia_id]);
        }
    }
}
