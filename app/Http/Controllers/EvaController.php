<?php

namespace App\Http\Controllers;

use App\Http\Requests\CursoEvaRequest;
use App\Models\ActividadAcademica;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class EvaController extends Controller
{
    private $url;
    private $token;

    private function getcategorias()
    {
        if (request()->ajax()) {
            $url = env('MOODLE_ENDPOINT');
            $token = env('MOODLE_TOKEN');

            $response = Http::get($url, [
                'wstoken' => $token,
                'moodlewsrestformat' => 'json',
                'wsfunction' => 'core_course_get_categories',
            ]);

            return response()->json($response->json());
        }
    }

    public function mostrarformulario($codactividad)
    {
        if (request()->ajax()) {

            $categorias = $this->getcategorias()->getData();

            return view('eva.registro')
                ->with(compact('codactividad'))
                ->with(compact('categorias'));
        }
    }

    public function storecurso(CursoEvaRequest $request){
        if(request()->ajax()){
            $data = $request->all();
            $actividad = ActividadAcademica::findOrFail($data['codactividad']);

            $url = env('MOODLE_ENDPOINT');
            $token = env('MOODLE_TOKEN');

            $response = Http::get($url, [
                'wstoken' => $token,
                'moodlewsrestformat' => 'json',
                'wsfunction' => 'core_course_create_courses',
                'courses[0][fullname]' => $actividad->titulo,
                'courses[0][shortname]' => $actividad->codigo.'a',
                'courses[0][categoryid]' => $data['categoria'],
            ]);

            $resultado = json_decode($response->getBody()->getContents());

            $actividad->eva = $resultado[0]->id;
            $actividad->estado = 5;
            $actividad->save();

            return response()->json('exito');
        }
    }

    public function deshacerRegistro(Request $request){
        if(request()->ajax()){
            $data = $request->all();
            $actividad = ActividadAcademica::findOrFail($data['codactividad']);
            $actividad->eva = 0;
            $actividad->estado = 3;
            $actividad->save();

            return response()->json('exito');
        }
    }
}
