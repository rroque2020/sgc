<?php

namespace App\Http\Controllers;

use App\Http\Requests\DiSesionRequest;
use App\Models\DiCapacidad;
use App\Models\DiSesion;
use App\Models\TipoActividadEstrategica;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class DiSesionController extends Controller
{
    public function create($capacidad)
    {
        if (request()->ajax()) {
            $estrategias = TipoActividadEstrategica::listarByEstado(1);

            return view('sesion.nuevo_editar')
                ->with(compact('capacidad'))
                ->with(compact('estrategias'));
        }
    }

    public function store(DiSesionRequest $request)
    {
        if (request()->ajax()) {
            $data = $request->all();
            $sumaduracion = DiSesion::sumaDuracion(DiCapacidad::leerModulo($data['codcapacidad'])->modulo_id);
            if($sumaduracion->suma + (int)$data['duracion'] > $sumaduracion->duracion){
                throw ValidationException::withMessages(['duracion' => "La Duración debe ser menor o igual a: ".($sumaduracion->duracion - $sumaduracion->suma)]);
            }

            $item = new DiSesion();
            $item->di_capacidad_id = $data['codcapacidad'];
            $item->orden = $data['orden'];
            $item->descripcion = $data['descripcion'];
            $item->duracion = $data['duracion'];
            $item->tipo_actividad_estrategica_id = $data['idestrategia'];
            $item->des_actividad_estrategica_id = $data['desestrategia'];
            $item->recursos_obligatorios = $data['recursosobligatorios'];
            $item->recursos_complementarios = $data['recursoscomplementarios'];
            $item->fecha_hora_inicio = Carbon::createFromFormat('d/m/Y H:i',$data["fechahorainicio"])->format('Y-m-d H:i:s');
            $item->fecha_hora_cierre = Carbon::createFromFormat('d/m/Y H:i',$data["fechahoracierre"])->format('Y-m-d H:i:s');
            $item->save();

            return response()->json(['capacidad' => $data['codcapacidad']]);
        }
    }

    public function show($id)
    {
        $sesiones = DiCapacidad::listarSesiones($id);
        $capacidad = $id;
        return view('sesion.index')
            ->with(compact('capacidad'))
            ->with(compact('sesiones'));
    }

    public function edit($id)
    {
        if (request()->ajax()) {
            $estrategias = TipoActividadEstrategica::listarByEstado(1);

            $item = DiSesion::findOrFail($id);
            return view('sesion.nuevo_editar')
                ->with(compact('item'))
                ->with(compact('estrategias'));
        }
    }

    public function update(DiSesionRequest $request, $id)
    {
        if (request()->ajax()) {
            $data = $request->all();

            $item = DiSesion::findOrFail($id);

            $sumaduracion = DiSesion::sumaDuracion(DiCapacidad::leerModulo($data['codcapacidad'])->modulo_id);
            if (($sumaduracion->suma - $item->duracion) + (int)$data['duracion'] > $sumaduracion->duracion) {
                throw ValidationException::withMessages(['duracion' => "La Duración debe ser menor o igual a: " . ($sumaduracion->duracion - ($sumaduracion->suma - $item->duracion))]);
            }

            $item->di_capacidad_id = $data['codcapacidad'];
            $item->orden = $data['orden'];
            $item->descripcion = $data['descripcion'];
            $item->duracion = $data['duracion'];
            $item->tipo_actividad_estrategica_id = $data['idestrategia'];
            $item->des_actividad_estrategica_id = $data['desestrategia'];
            $item->recursos_obligatorios = $data['recursosobligatorios'];
            $item->recursos_complementarios = $data['recursoscomplementarios'];
            $item->fecha_hora_inicio = Carbon::createFromFormat('d/m/Y H:i',$data["fechahorainicio"])->format('Y-m-d H:i:s');
            $item->fecha_hora_cierre = Carbon::createFromFormat('d/m/Y H:i',$data["fechahoracierre"])->format('Y-m-d H:i:s');
            $item->save();

            return response()->json(['capacidad' => $data['codcapacidad']]);
        }
    }

    public function destroy($id)
    {
        if (request()->ajax()) {
            $sesion = DiSesion::findOrFail($id);
            $sesion->delete();

            return response()->json(['capacidad' => $sesion->di_capacidad_id]);
        }
    }
}
