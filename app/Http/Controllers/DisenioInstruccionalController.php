<?php

namespace App\Http\Controllers;

use App\Exports\DisenioInstruccionalExport;
use App\Helpers\Helper;
use App\Http\Requests\ActividadAcademicaEjecucionRequest;
use App\Models\ActividadAcademica;
use App\Models\CriterioDiscente;
use App\Models\CriterioDiscenteCargo;
use App\Models\CriterioDiscenteDistritoFiscal;
use App\Models\CriterioDiscentePerfil;
use App\Models\DiActividadEvaluativa;
use App\Models\DiCapacidad;
use App\Models\DiModulo;
use App\Models\DiSesion;
use App\Models\DistritoFiscal;
use App\Models\DiUnidad;
use App\Models\PerfilPersonal;
use App\Models\TCargo;
use App\Models\TipoActividadEstrategica;
use App\Models\TipoModulo;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpWord\TemplateProcessor;

class DisenioInstruccionalController extends Controller
{
    public function index($id) {
        $actividad = ActividadAcademica::listarDatos($id);
        $modulos = ActividadAcademica::listarModulos($id);
        $item = CriterioDiscente::listarDatos($id);
        $perfilesseleccionados = implode(',', CriterioDiscentePerfil::listarPerfilesSeleccionados($id)->pluck('descripcion')->toArray());
        $cargosseleccionados = implode(',', CriterioDiscenteCargo::listarCargosSeleccionados($id)->pluck('desc_carg_tca')->toArray());
        $distritosseleccionados = implode(',', CriterioDiscenteDistritoFiscal::listarDistritosSeleccionados($id)->pluck('descripcion')->toArray());
        return view('disenioinstruccional.index')
            ->with(compact('actividad'))
            ->with(compact('item'))
            ->with(compact('perfilesseleccionados'))
            ->with(compact('cargosseleccionados'))
            ->with(compact('distritosseleccionados'))
            ->with(compact('modulos'));
    }

    public function registroejecucion($id)
    {
        if (request()->ajax()) {
            $actividad = ActividadAcademica::listarDatos($id);
            $tipos = TipoModulo::listarByEstado(1);
            return view('disenioinstruccional.ejecucion')
                        ->with(compact('actividad'))
                        ->with(compact('tipos'));
        }
    }

    public function actualizarejecucion(ActividadAcademicaEjecucionRequest $request){
        if(request()->ajax()){
            $data = $request->all();

            $actividad = ActividadAcademica::findOrFail($data['codactividad']);
            $usuario = Helper::usuarioActual();
            $actividad->fecha_inicioe = Carbon::createFromFormat('d/m/Y',$data["fechainicioe"])->format('Y-m-d');
            $actividad->fecha_finale = Carbon::createFromFormat('d/m/Y',$data["fechafine"])->format('Y-m-d');
            $actividad->nro_aulase = $data['nroaulase'];
            $actividad->nro_vacantese = $data['nrovacantese'];
            $actividad->nro_docentese = $data['nrodocentese'];
            $actividad->duracione = $data['duracione'];
            $actividad->tipo_modulo = $data['tipomodulo'];
            $actividad->cod_usum = $usuario;
            $actividad->save();

            $actividad = ActividadAcademica::listarDatos($data['codactividad']);

            return view('disenioinstruccional.ejecuciondet')
                        ->with(compact('actividad'));
        }
    }

    public function createcriteriodiscente($actividad)
    {
        if (request()->ajax()) {
            $perfilespersonales = PerfilPersonal::listarByEstado(1);
            $cargos = TCargo::listar();
            $distritos = DistritoFiscal::listarByEstado(1);

            return view('disenioinstruccional.criteriodiscente')
                ->with(compact('cargos'))
                ->with(compact('perfilespersonales'))
                ->with(compact('distritos'))
                ->with(compact('actividad'));
        }
    }

    public function storecriteriodiscente(Request $request){
        if(request()->ajax()){
            DB::beginTransaction();
            try {
                $data = $request->all();
                $usuario = Helper::usuarioActual();

                $criterio = new CriterioDiscente();
                $criterio->actividad_academica_id = $data['codactividad'];
                if(isset($data['dni'])){
                    $criterio->dni = 1;
                }
                if(isset($data['aprobado'])){
                    $criterio->aprobado = 1;
                }
                if(isset($data['temporalidad'])){
                    $criterio->temporalidad = 1;
                }
                if(isset($data['sancion'])){
                    $criterio->sancion = 1;
                }

                $criterio->especialidad = $data['especialidad'];
                if(isset($data['invitados'])){
                    $criterio->invitados = 1;
                }
                if(isset($data['brecha'])){
                    $criterio->brecha = 1;
                }
                $criterio->cod_usu = $usuario;
                $criterio->save();

                if(isset($data['perfilpersonal'])){
                    $cont = 0;
                    while($cont < count($data['perfilpersonal'])){
                        $criterioperfil = new CriterioDiscentePerfil();
                        $criterioperfil->criterio_discente_id = $criterio->id;
                        $criterioperfil->perfil_personal_id = $data['perfilpersonal'][$cont];
                        $criterioperfil->save();
                        $cont = $cont + 1;
                    }
                }

                if(isset($data['cargo'])) {
                    $cont = 0;
                    while ($cont < count($data['cargo'])) {
                        $criteriocargo = new CriterioDiscenteCargo();
                        $criteriocargo->criterio_discente_id = $criterio->id;
                        $criteriocargo->cargo_id = $data['cargo'][$cont];
                        $criteriocargo->save();
                        $cont = $cont + 1;
                    }
                }

                if(isset($data['distritofiscal'])) {
                    $cont = 0;
                    while ($cont < count($data['distritofiscal'])) {
                        $criteriodistrito = new CriterioDiscenteDistritoFiscal();
                        $criteriodistrito->criterio_discente_id = $criterio->id;
                        $criteriodistrito->distrito_fiscal_id = $data['distritofiscal'][$cont];
                        $criteriodistrito->save();
                        $cont = $cont + 1;
                    }
                }
            }catch (\Exception $e) {
                DB::rollback();
                throw ValidationException::withMessages([
                    'dni' => $e->getMessage(). "Hay un error desconocido al guardar, vuelva a intentarlo y si persiste consulte con el Administrador del Sistema...",
                ]);
            }
            DB::commit();

            $perfilesseleccionados = implode(',', CriterioDiscentePerfil::listarPerfilesSeleccionados($data['codactividad'])->pluck('descripcion')->toArray());
            $cargosseleccionados = implode(',', CriterioDiscenteCargo::listarCargosSeleccionados($data['codactividad'])->pluck('desc_carg_tca')->toArray());
            $distritosseleccionados = implode(',', CriterioDiscenteDistritoFiscal::listarDistritosSeleccionados($data['codactividad'])->pluck('descripcion')->toArray());

            return view('disenioinstruccional.criteriodiscentedet')
                ->with(['item' => $criterio])
                ->with(compact('perfilesseleccionados'))
                ->with(compact('cargosseleccionados'))
                ->with(compact('distritosseleccionados'));
        }
    }

    public function editcriteriodiscente($actividad)
    {
        if (request()->ajax()) {
            $perfilespersonales = PerfilPersonal::listarByEstado(1);
            $perfilesseleccionados = CriterioDiscentePerfil::listarPerfilesSeleccionados($actividad)->pluck('id');
            $cargos = TCargo::listar();
            $cargosseleccionados = CriterioDiscenteCargo::listarCargosSeleccionados($actividad)->pluck('codi_carg_tca');
            $distritos = DistritoFiscal::listarByEstado(1);
            $distritosseleccionados = CriterioDiscenteDistritoFiscal::listarDistritosSeleccionados($actividad)->pluck('id');
            $item = CriterioDiscente::where('actividad_academica_id', $actividad)->firstOrFail();
//            return response()->json($item);
            return view('disenioinstruccional.criteriodiscente')
                ->with(compact('item'))
                ->with(compact('perfilespersonales'))
                ->with(compact('perfilesseleccionados'))
                ->with(compact('cargos'))
                ->with(compact('cargosseleccionados'))
                ->with(compact('distritos'))
                ->with(compact('distritosseleccionados'))
                ->with(compact('actividad'));
        }
    }

    public function updatecriteriodiscente(Request $request, $id){
        if(request()->ajax()){
            $data = $request->all();
            $usuario = Helper::usuarioActual();

            $criterio = CriterioDiscente::findOrFail($id);
            if(isset($data['dni'])){
                $criterio->dni = 1;
            }else{
                $criterio->dni = 0;
            }
            if(isset($data['aprobado'])){
                $criterio->aprobado = 1;
            }else{
                $criterio->aprobado = 0;
            }
            if(isset($data['temporalidad'])){
                $criterio->temporalidad = 1;
            }else{
                $criterio->temporalidad = 0;
            }
            if(isset($data['sancion'])){
                $criterio->sancion = 1;
            }else{
                $criterio->sancion = 0;
            }
            $criterio->especialidad = $data['especialidad'];
            if(isset($data['invitados'])){
                $criterio->invitados = 1;
            }else{
                $criterio->invitados = 0;
            }
            if(isset($data['brecha'])){
                $criterio->brecha = 1;
            }else{
                $criterio->brecha = 0;
            }
            $criterio->cod_usu = $usuario;
            $criterio->save();

            if(isset($data['perfilpersonal'])){
                CriterioDiscentePerfil::where('criterio_discente_id', $id)->delete();
                $cont = 0;
                while($cont < count($data['perfilpersonal'])){
                    $criterioperfil = new CriterioDiscentePerfil();
                    $criterioperfil->criterio_discente_id = $criterio->id;
                    $criterioperfil->perfil_personal_id = $data['perfilpersonal'][$cont];
                    $criterioperfil->save();
                    $cont = $cont + 1;
                }
            }

            if(isset($data['cargo'])) {
                CriterioDiscenteCargo::where('criterio_discente_id',$id)->delete();
                $cont = 0;
                while ($cont < count($data['cargo'])) {
                    $criteriocargo = new CriterioDiscenteCargo();
                    $criteriocargo->criterio_discente_id = $criterio->id;
                    $criteriocargo->cargo_id = $data['cargo'][$cont];
                    $criteriocargo->save();
                    $cont = $cont + 1;
                }
            }

            if(isset($data['distritofiscal'])) {
                CriterioDiscenteDistritoFiscal::where('criterio_discente_id',$id)->delete();
                $cont = 0;
                while ($cont < count($data['distritofiscal'])) {
                    $criteriodistrito = new CriterioDiscenteDistritoFiscal();
                    $criteriodistrito->criterio_discente_id = $criterio->id;
                    $criteriodistrito->distrito_fiscal_id = $data['distritofiscal'][$cont];
                    $criteriodistrito->save();
                    $cont = $cont + 1;
                }
            }

            $perfilesseleccionados = implode(',', CriterioDiscentePerfil::listarPerfilesSeleccionados($data['codactividad'])->pluck('descripcion')->toArray());
            $cargosseleccionados = implode(',', CriterioDiscenteCargo::listarCargosSeleccionados($data['codactividad'])->pluck('desc_carg_tca')->toArray());
            $distritosseleccionados = implode(',', CriterioDiscenteDistritoFiscal::listarDistritosSeleccionados($data['codactividad'])->pluck('descripcion')->toArray());

            return view('disenioinstruccional.criteriodiscentedet')
                ->with(['item' => $criterio])
                ->with(compact('perfilesseleccionados'))
                ->with(compact('cargosseleccionados'))
                ->with(compact('distritosseleccionados'));
        }
    }

    public function detalle($id) {
        $modulos = ActividadAcademica::listarModulos($id);

        return view('disenioinstruccional.detalle')
            ->with(compact('modulos'));
    }

    public function export($id) {
        $actividad = ActividadAcademica::findOrFail($id);

        return Excel::download(new DisenioInstruccionalExport($id), 'DI-'.$actividad->codigo.'.xlsx');
    }

    public function exportword($id) {
        $actividad = ActividadAcademica::listarDatosWord($id);
        $modulosgenerales = DiModulo::listarByCapacitacionGeneral($id);
        $modulogeneralcad = '';
        foreach($modulosgenerales as $modulogeneral){
            $modulogeneralcad .= $modulogeneral->descripcion.': '.$modulogeneral->nombre.'<w:br/>';
        }
        $modulosespecificos = DiModulo::listarByCapacitacionEspecifica($id);
        $moduloespecificocad = '';
        foreach($modulosespecificos as $moduloespecifico){
            $moduloespecificocad .= $moduloespecifico->descripcion.': '.$moduloespecifico->nombre.'<w:br/>' ;
        }
        $templateProcessor = new TemplateProcessor('word-template/plantilla-silabo.docx');
        $templateProcessor->setValue('tipo_actividad', $actividad->tipo);
        $templateProcessor->setValue('titulo_actividad', $actividad->titulo);
        $templateProcessor->setValue('codigo_actividad', $actividad->codigo);
        $templateProcessor->setValue('personal_actividad', $actividad->perfil);
        $templateProcessor->setValue('duracion_actividad', $actividad->duracione);
        $templateProcessor->setValue('finicio_actividad', Carbon::createFromFormat('Y-m-d',$actividad->fecha_inicioe)->format('d/m/Y'));
        $templateProcessor->setValue('ffinal_actividad', Carbon::createFromFormat('Y-m-d',$actividad->fecha_finale)->format('d/m/Y'));
        $templateProcessor->setValue('modulo_general', $modulogeneralcad);
        $templateProcessor->setValue('modulo_especifico', $moduloespecificocad);
        $templateProcessor->setValue('modalidad_actividad', $actividad->modalidad);
        $templateProcessor->setValue('certificado_actividad', $actividad->certificado);
        $templateProcessor->setValue('sumilla_actividad', $actividad->descripcion);

        $capacidades = DiCapacidad::listarDatosWord($id)->toArray();
        $arrayCapacidades = array();
        foreach($capacidades as $capacidad){
            array_push($arrayCapacidades,
                        ['mod_descripcion' => $capacidad->mod_descripcion,
                            'comp_orden' => $capacidad->comp_orden,
                            'comp_descripcion' => $capacidad->comp_descripcion,
                            'cap_orden' => $capacidad->cap_orden,
                            'cap_descripcion' => $capacidad->cap_descripcion]);
        }
        $templateProcessor->cloneRowAndSetValues('mod_descripcion', $arrayCapacidades);

        $arrayModulosGenerales = array();
        $i=0;
        foreach($modulosgenerales->toArray() as $modulo){
            array_push($arrayModulosGenerales,
                ['org_modulogeneral' => $modulo->descripcion,
                    'org_modulogeneral_desde' => Carbon::createFromFormat('Y-m-d',$modulo->fecha_inicio)->format('d/m/Y'),
                    'org_modulogeneral_hasta' => Carbon::createFromFormat('Y-m-d',$modulo->fecha_final)->format('d/m/Y'),
                    'org_modulogeneral_duracion' => $modulo->duracion,
                    'omg_sorden' => '${omg_sorden_'.$i.'}',
                    'omg_sdes' => '${omg_sdes_'.$i.'}',
                    'omg_stipo' => '${omg_stipo_'.$i.'}',
                    'omg_stipodes' => '${omg_stipodes_'.$i.'}',
                    'omg_sduracion' => '${omg_sduracion_'.$i.'}',
                    'omg_sunidad' => '${omg_sunidad_'.$i.'}',
                    'omg_sfhi' => '${omg_sfhi_'.$i.'}',
                    'omg_sfhc' => '${omg_sfhc_'.$i.'}',
                ]);

            $i++;
        }

        $templateProcessor->cloneBlock('block_org_modulo_general', 0, true, false, $arrayModulosGenerales);


        $i=0;
        foreach($modulosgenerales as $modulo){
            $sesiones = DiSesion::listarDatosWord($modulo->id)->toArray();
            $arraySesiones = array();
            foreach($sesiones as $sesion){
                $unidades = DiUnidad::listarDatosWord($sesion->ses_id);
                $mgunid = '';
                foreach($unidades as $unidad){
                    $mgunid .= $unidad->ut_orden.': '.$unidad->ut_nombre.'<w:br/>'.$unidad->ut_descripcion.'<w:br/>';
                }
               // array_push($arraySesioness,
                $arraySesiones[] = array(
                        "omg_sorden_{$i}" => $sesion->ses_orden,
                        "omg_sdes_{$i}" => $sesion->ses_descripcion,
                        "omg_stipo_{$i}" => $sesion->ses_tipo,
                        "omg_stipodes_{$i}" => $sesion->ses_tipodescripcion,
                        "omg_sduracion_{$i}" => $sesion->ses_duracion,
                        "omg_sunidad_{$i}" => $mgunid,
                        "omg_sfhi_{$i}" => Carbon::createFromFormat('Y-m-d H:i:s',$sesion->ses_fhi)->format('d/m/Y H:i'),
                        "omg_sfhc_{$i}" => Carbon::createFromFormat('Y-m-d H:i:s',$sesion->ses_fhc)->format('d/m/Y H:i')
                    );
            }
            $templateProcessor->cloneRowAndSetValues("omg_sorden_{$i}", $arraySesiones);

            $i++;
        }

        $arrayModulosEspecificas = array();
        $i=0;
        foreach($modulosespecificos->toArray() as $modulo){
            array_push($arrayModulosEspecificas,
                ['org_moduloespecifica' => $modulo->descripcion,
                    'org_moduloespecifica_desde' => Carbon::createFromFormat('Y-m-d',$modulo->fecha_inicio)->format('d/m/Y'),
                    'org_moduloespecifica_hasta' => Carbon::createFromFormat('Y-m-d',$modulo->fecha_final)->format('d/m/Y'),
                    'org_moduloespecifica_duracion' => $modulo->duracion,
                    'ome_sorden' => '${ome_sorden_'.$i.'}',
                    'ome_sdes' => '${ome_sdes_'.$i.'}',
                    'ome_stipo' => '${ome_stipo_'.$i.'}',
                    'ome_stipodes' => '${ome_stipodes_'.$i.'}',
                    'ome_sduracion' => '${ome_sduracion_'.$i.'}',
                    'ome_sunidad' => '${ome_sunidad_'.$i.'}',
                    'ome_sfhi' => '${ome_sfhi_'.$i.'}',
                    'ome_sfhc' => '${ome_sfhc_'.$i.'}']);

            $i++;
        }
        $templateProcessor->cloneBlock('block_org_modulo_especifica', 0, true, false, $arrayModulosEspecificas);

        $i=0;
        foreach($modulosespecificos as $modulo){
            $sesiones = DiSesion::listarDatosWord($modulo->id)->toArray();
            $arraySesiones = array();
            foreach($sesiones as $sesion){
                $unidades = DiUnidad::listarDatosWord($sesion->ses_id);
                $mgunid = '';
                foreach($unidades as $unidad){
                    $mgunid .= $unidad->ut_orden.': '.$unidad->ut_nombre.'<w:br/>'.$unidad->ut_descripcion.'<w:br/>';
                }
                $arraySesiones[] = array(
                    "ome_sorden_{$i}" => $sesion->ses_orden,
                    "ome_sdes_{$i}" => $sesion->ses_descripcion,
                    "ome_stipo_{$i}" => $sesion->ses_tipo,
                    "ome_stipodes_{$i}" => $sesion->ses_tipodescripcion,
                    "ome_sduracion_{$i}" => $sesion->ses_duracion,
                    "ome_sunidad_{$i}" => $mgunid,
                    "ome_sfhi_{$i}" => Carbon::createFromFormat('Y-m-d H:i:s',$sesion->ses_fhi)->format('d/m/Y H:i'),
                    "ome_sfhc_{$i}" => Carbon::createFromFormat('Y-m-d H:i:s',$sesion->ses_fhc)->format('d/m/Y H:i')
                );
            }

            $templateProcessor->cloneRowAndSetValues("ome_sorden_{$i}", $arraySesiones);

            $i++;
        }

        $taes = TipoActividadEstrategica::listarDatosWord($id);
        $arrayTae = array();
        foreach($taes->toArray() as $tae){
            array_push($arrayTae,
                ['tae_descripcion' => $tae->tae_descripcion]);
        }
        $templateProcessor->cloneBlock('block_tae', 0, true, false, $arrayTae);

        $daes = DiActividadEvaluativa::listarDatosWord($id);
        $arrayDae = array();
        foreach($daes->toArray() as $dae){
            array_push($arrayDae,
                [
                    'dae_mod' => $dae->mod_descripcion,
                    'dae_orden' => $dae->dae_orden,
                    'dae_tipo' => $dae->dae_tipo,
                    'dae_descripcion' => $dae->dae_descripcion,
                    'dae_porcentaje' => $dae->dae_porcentaje,
                    'dae_instrumento' => $dae->dae_instrumento,
                    'dae_fhi' => Carbon::createFromFormat('Y-m-d H:i:s',$dae->dae_fhi)->format('d/m/Y H:i'),
                    'dae_fhc' => Carbon::createFromFormat('Y-m-d H:i:s',$dae->dae_fhc)->format('d/m/Y H:i')
                ]);
        }
        $templateProcessor->cloneRowAndSetValues("dae_mod", $arrayDae);

        $ros = DiSesion::listarRecursosObligatoriosWord($id);
        $arrayRo = array();
        foreach($ros->toArray() as $ro){
            array_push($arrayRo,
                ['ro_descripcion' => $ro->ses_recobl]);
        }
        $templateProcessor->cloneBlock('block_ro', 0, true, false, $arrayRo);

        $rcs = DiSesion::listarRecursosComplementariosWord($id);
        $arrayRc = array();
        foreach($rcs->toArray() as $rc){
            array_push($arrayRc,
                ['rc_descripcion' => $rc->ses_recomp]);
        }
        $templateProcessor->cloneBlock('block_rc', 0, true, false, $arrayRc);

        $biblios = DiUnidad::listarBibliografiasWord($id);
        $arrayBiblio = array();
        foreach($biblios->toArray() as $biblio){
            array_push($arrayBiblio,
                ['und_bibliografia' => $biblio->und_bibliografia]);
        }
        $templateProcessor->cloneBlock('block_biblio', 0, true, false, $arrayBiblio);

        $fileName = 'SILABO-'.$actividad->codigo;
        $templateProcessor->saveAs($fileName . '.docx');

        return response()->download($fileName . '.docx')->deleteFileAfterSend(true);

//        $word = new PhpWord();
//        $section = $word->addSection();
//        $html = view('disenioinstruccional.word')->render();
//
//        \PhpOffice\PhpWord\Shared\Html::addHtml($section, $html, false, false);
//        $file = 'prueba.docx';
//        header('Content-Disposition: attachment; filename="'.$file.'"');
//        $objWriter = IOFactory::createWriter($word, 'Word2007');
//        $objWriter->save('php://output');
    }
}
