<?php

namespace App\Http\Controllers;

use App\Http\Requests\PreInscripcionWebRequest;
use App\Mail\PreInscritoMail;
use App\Models\ActividadAcademica;
use App\Models\ActividadDiscente;
use App\Models\CriterioDiscente;
use App\Models\CriterioDiscenteCargo;
use App\Models\CriterioDiscenteDistritoFiscal;
use App\Models\CriterioDiscentePerfil;
use App\Models\PreInscrito;
use App\Models\TMaestroPersonal;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use phpDocumentor\Reflection\Types\Collection;

class SeleccionDiscenteController extends Controller
{
    public function index($id){
        $actividad = ActividadAcademica::findOrFail($id);
        $item = CriterioDiscente::listarDatos($id);
        $perfilesseleccionados = implode(',', CriterioDiscentePerfil::listarPerfilesSeleccionados($id)->pluck('descripcion')->toArray());
        $cargosseleccionados = implode(',', CriterioDiscenteCargo::listarCargosSeleccionados($id)->pluck('desc_carg_tca')->toArray());
        $distritosseleccionados = implode(',', CriterioDiscenteDistritoFiscal::listarDistritosSeleccionados($id)->pluck('descripcion')->toArray());


        return view('discente.seleccion.index')
            ->with(compact('actividad'))
            ->with(compact('item'))
            ->with(compact('perfilesseleccionados'))
            ->with(compact('cargosseleccionados'))
            ->with(compact('distritosseleccionados'));
    }

    public function listar($id){
        $item = CriterioDiscente::listarDatos($id);

        $perfilesseleccionados = implode(',', CriterioDiscentePerfil::listarPerfilesSeleccionados($id)->pluck('id')->toArray());
        $cargosseleccionados = implode(',', CriterioDiscenteCargo::listarCargosSeleccionados($id)->pluck('codi_carg_tca')->toArray());
        $distritosseleccionados = implode(',', CriterioDiscenteDistritoFiscal::listarDistritosSeleccionados($id)->pluck('id')->toArray());

        $dnis = TMaestroPersonal::listarBusqueda($item->especialidad, $cargosseleccionados, $perfilesseleccionados,$distritosseleccionados);

        if($item->aprobado == 1){
            $postulantes = collect();
            foreach($dnis as $dni){
                if(CriterioDiscente::verificarAprobadoPreRequisito($id, $dni->dni)->cont > 0){
                    $postulantes->push($dni);
                }
            }
        }else{
            $postulantes = $dnis;
        }

        if($item->temporalidad == 1){
            $horarios = collect();
            foreach($postulantes as $postulante){
                if(CriterioDiscente::verificaCruceHorarios($postulante->dni)->cont > 0){
                    $horarios->push($postulante);
                }
            }
        }else{
            $horarios = $postulantes;
        }

        if($item->brecha == 1){
            $participo = collect();
            foreach($horarios as $horario){
                if(CriterioDiscente::verificarParticipacionActividad($item->sugerencia, $horario->dni)->cont == 0){
                    $participo->push($horario);
                }
            }
        }else{
            $participo = $horarios;
        }

        return view('discente.seleccion.listarseleccion')
            ->with(['postulantes'=> $participo]);
    }

    public function convocar(Request $request){
        if (request()->ajax()) {
            $data = $request->all();
            $ids = explode(",", $data['ids'][0]);

            $cont = 0;
            while ($cont < count($ids)) {
                $item = PreInscrito::where('dni',$ids[$cont])->first();

                if($item){
                    $item->estado_correo = 0;
                    $item->fecha_hora = Carbon::now();
                    $item->save();
                }else{
                    $item = new PreInscrito();
                    $buscar = TMaestroPersonal::listarDatosbyDNI($ids[$cont]);
                    $item->actividad_academica_id = $data['codactividad'];
                    $item->tipo = 1;
                    $item->dni = $buscar->CODI_EMPL_PER;
                    $item->apellidos = $buscar->APE_PAT_PER.' '.$buscar->APE_MAT_PER;
                    $item->nombres = $buscar->NOM_EMP_PER;
                    $item->celular = $buscar->TELF_CELU_PER;
                    $item->email = $buscar->DIRE_EMAI_PER;
                    $item->estado_correo = 0;
                    $item->fecha_hora = Carbon::now();
                    $item->save();
                }
                $cont = $cont + 1;
            }

            return response()->json('exito');
        }
    }

    public function registroconvocatoria($actividad)
    {
        if(request()->ajax()){

            return view("discente.convocatoria.registro")
                ->with(compact('actividad'));
        }
    }

    public function listardatosconvocatoria($actividad, $dni)
    {
        if(request()->ajax()) {
            $item = PreInscrito::listarDatosByActividadAndDNI($actividad, $dni);

            if ($item) {
                $apellidos = $item->apellidos ;
                $nombres = $item->nombres;
            } else {
                $apellidos = '';
                $nombres = '';
            }
            return response()->json(['apellidos' => $apellidos, 'nombres' => $nombres]);
        }
    }

    public function preinscribirconvocatoria(PreInscripcionWebRequest $request)
    {
        if(request()->ajax()) {
            $data = $request->all();
            $actividad = $data['codactividad'];

            $preins = PreInscrito::listarDatosByDNI($data['dni']);

            if($preins){
                $item = PreInscrito::findOrFail($preins->id);
                $item->celular = $data['celular'];
                $item->email = $data['correo'];
            }else{
                $item = new PreInscrito();
                $item->actividad_academica_id = $actividad;
                $item->tipo = 1;
                $item->dni = $data['dni'];
                $item->apellidos = strtoupper($data['apellidos']);
                $item->nombres = strtoupper($data['nombres']);
                $item->celular = $data['celular'];
                $item->email = $data['correo'];
            }

            /////////////////////////
            $act = ActividadAcademica::findOrFail($data['codactividad']);

            $detalles = [
                'destinatario' => $item->email,
                'nombres' => $item->nombres,
                'titulo' => $act->titulo,
            ];

            Mail::send(new PreInscritoMail($detalles));

            if( count(Mail::failures()) > 0 ) {
            } else {
                $item->estado_correo = 2;
                $item->fecha_hora = Carbon::now();

            }
            /////////////////////////
            $item->save();

            return response()->json(['valor' => 'true',
                'id' => $item->id,
                'actividad' => $item->actividad_academica_id,
                'mensaje' => 'Pre-Inscrito a la Actividad Académica correctamente, revise la bandeja de su Correo Electrónico ...']);

        }
    }
}
