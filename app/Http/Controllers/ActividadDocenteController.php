<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Mail\DocenteDisenioMail;
use App\Mail\DocenteMail;
use App\Models\ActividadAcademica;
use App\Models\ActividadDocente;
use App\Models\Docente;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;

class ActividadDocenteController extends Controller
{
    public function index($id){
        $actividad = ActividadAcademica::findOrFail($id);
        $docentes = ActividadDocente::listardocentes($id);

        return view('actividad.selecciondocente')
                    ->with(compact('actividad'))
                    ->with(compact('docentes'));
    }

    public function store(Request $request){
        if (request()->ajax()) {
            $data = $request->all();
            $usuario = Helper::usuarioActual();

            $conta = 0;
            while($conta < count($data['coddocente'])){
                $cont = count(ActividadDocente::existe($data['codactividad'], $data['coddocente'][$conta]));

                if($cont > 0){
                    return response()->json('nada');
                }
                else{
                    $item = new ActividadDocente();
                    $item->actividad_academica_id = $data['codactividad'];
                    $item->docente_id = $data['coddocente'][$conta];
                    $item->estado = 1;
                    $item->cod_usuc = $usuario;
                    $item->cod_usum = $usuario;
                    $item->save();
                }

                $conta = $conta + 1;
            }
            $docentes = ActividadDocente::listardocentes($data['codactividad']);

            return view('docente.listarseleccionados')
                        ->with(compact('docentes'));
        }
    }

    public function destroy(Request $request)
    {
        if (request()->ajax()) {
            $data = $request->all();
            $docente = ActividadDocente::findOrFail($data['id']);
            $docente->delete();

            $docentes = ActividadDocente::listardocentes($data['codactividad']);

            return view('docente.listarseleccionados')
                ->with(compact('docentes'));
        }
    }

    public function mail(Request $request)
    {
        if (request()->ajax()) {
            $data = $request->all();
            $actividad = ActividadAcademica::findOrFail($data['codactividad']);
            $docente = Docente::listarDatos($data['coddocente']);

            $hash = $data['id'].' '.$data['codactividad'].' '.$data['coddocente'];
            $fecha_inicio = Carbon::createFromFormat('Y-m-d',$actividad->fecha_inicioe)->format('d/m/Y');
            $fecha_final = Carbon::createFromFormat('Y-m-d',$actividad->fecha_finale)->format('d/m/Y');

            $detalles = [
                'destinatario' => $docente->email,
                'titulo' => $actividad->titulo,
                'fecha_inicio' => $fecha_inicio,
                'fecha_final' => $fecha_final,
                'horario' => $actividad->horario_detallado,
                'url' =>  env('APP_URL').'/recepcionarconfirmacion/'.Crypt::encryptString($hash),
            ];

            Mail::send(new DocenteMail($detalles));

            if( count(Mail::failures()) > 0 ) {
                return response()->json('error');
            } else {
                $usuario = Helper::usuarioActual();
                $item = ActividadDocente::findOrFail($data['id']);
                $item->estado = 2;
                $item->fecha_hora = Carbon::now();
                $item->cod_usum = $usuario;
                $item->save();
            }

            $docentes = ActividadDocente::listardocentes($data['codactividad']);
            return view('docente.listarseleccionados')
                ->with(compact('docentes'));
        }
    }

    public function confirmar($hash)
    {
        $url = Crypt::decryptString($hash);
        $data = explode(" ", $url);

        $cont = count(ActividadDocente::confirmo($data[1], $data[2]));

        if($cont > 0){
            $texto = 'Ya ha sido confirmado ...';
        }else{
            $actividad = ActividadAcademica::findOrFail($data[1]);
            $docente = Docente::listarDatos($data[2]);
            $fecha_inicio = Carbon::createFromFormat('Y-m-d',$actividad->fecha_inicioe)->format('d/m/Y');
            $fecha_final = Carbon::createFromFormat('Y-m-d',$actividad->fecha_finale)->format('d/m/Y');

            $detalles = [
                'destinatario' => $docente->email,
                'titulo' => $actividad->titulo,
                'fecha_inicio' => $fecha_inicio,
                'fecha_final' => $fecha_final,
                'horario' => $actividad->horario_detallado,
                'codactividad' =>  $actividad->id,
                'codigo' =>  $actividad->codigo,
            ];

            Mail::send(new DocenteDisenioMail($detalles));

            if( count(Mail::failures()) > 0 ) {
                $texto = 'Ocurrió un error inesperado, consulte con el Administrador del Sistema ...';
            } else {
                $item = ActividadDocente::findOrFail($data[0]);
                $item->estado = 3;
                $item->fecha_hora = Carbon::now();
                $item->save();
                $texto = 'Confirmación realizada exitosamente, se le ha enviado el Diseño Instruccional, revise su Correo Electrónico ...';
            }

        }

        dd($texto);
    }
}
