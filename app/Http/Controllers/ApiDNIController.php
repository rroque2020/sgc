<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ApiDNIController extends Controller
{
    public function getDatos($dni){
        if(request()->ajax()) {
            $url = env('APIDNI_ENDPOINT');
            $token = env('APIDNI_TOKEN');

            $response = Http::get($url . $dni, [
                'token' => $token,
            ]);

            return response()->json($response->json());
        }
    }
}
