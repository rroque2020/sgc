<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Requests\DocenteRequest;
use App\Models\DistritoFiscal;
use App\Models\Docente;
use App\Models\DocenteCapacitacion;
use App\Models\DocenteExperiencia;
use App\Models\DocenteFormacion;
use App\Models\Persona;
use App\Models\TCargo;
use App\Models\TDependencia;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class DocenteController extends Controller
{
    public function index()
    {
        $items = Docente::listar();
        return view('docente.index')
                    ->with(compact('items'));
    }

    public function create()
    {
        $cargos = TCargo::listar();
        $distritos = DistritoFiscal::listarByEstado(1);

        return view('docente.nuevo_editar')
                    ->with(compact('cargos'))
                    ->with(compact('distritos'));
    }

    public function store(DocenteRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = $request->all();
            $usuario = Helper::usuarioActual();

            $item = new Persona();
            if (isset($data['tipo'])) {
                $item->tipo = 1;
                $item->idcargo = $data['cargoi'];
                $item->iddependencia = $data['dependenciai'];
                $item->cargo = $data['cargodet'];
                $item->institucion = $data['dependenciadet'];
            } else {
                $item->tipo = 0;
                $item->idcargo = null;
                $item->iddependencia = null;
                $item->cargo = strtoupper($data['cargoe']);
                $item->institucion = strtoupper($data['institucione']);
            }
            $item->dni = $data['dni'];
            $item->apellidos = strtoupper($data['apellidos']);
            $item->nombres = strtoupper($data['nombres']);
            $item->celular = $data['celular'];
            if (!is_null($data["fecha"]))
                $item->fecha = Carbon::createFromFormat('d/m/Y', $data["fecha"])->format('Y-m-d');
            $item->email = $data['correo'];
            $item->genero = $data['genero'];
            $item->cod_usuc = $usuario;
            $item->cod_usum = $usuario;
            $item->save();

            $docente = new Docente();
            $docente->persona_id = $item->id;
            $docente->estado = 1;
            $docente->cod_usuc = $usuario;
            $docente->cod_usum = $usuario;
            $docente->save();
        }catch (\Exception $e) {
            DB::rollback();
            throw ValidationException::withMessages([
                'unknown' => $e->getMessage(). "Hay un error desconocido al guardar, vuelva a intentarlo y si persiste consulte con el Administrador del Sistema...",
            ]);
        }
        DB::commit();

        return redirect()
            ->route('docentes.index')
            ->withSuccess('Docente registrado correctamente...');
    }

    public function listarDocente()
    {
        if(request()->ajax()){
            $items = Docente::listar();
            return view('docente.lista')
                ->with(compact('items'));
        }
    }

    public function show($id)
    {
        $item = Docente::listarDatos($id);
        $totalesdoc = Docente::totales($id);
        $formaciones = DocenteFormacion::listarByDocente($id);
        $capacitaciones = DocenteCapacitacion::listarByDocente($id);
        $experiencias = DocenteExperiencia::listarByDocente($id);

        return view('docente.ver')
                    ->with(compact('item'))
                    ->with(compact('totalesdoc'))
                    ->with(compact('formaciones'))
                    ->with(compact('capacitaciones'))
                    ->with(compact('experiencias'));
    }

    public function edit($id)
    {
        $item = Docente::listarDatos($id);
        $cargos = TCargo::listar();
        $distritos = DistritoFiscal::listarByEstado(1);

        if(TDependencia::listarDatos($item->iddependencia) == null){
            $distritosel = null;
        }else{
            $distritosel = TDependencia::listarDatos($item->iddependencia)->CODI_DIJU_DJU;
        }

        return view('docente.nuevo_editar')
            ->with(compact('cargos'))
            ->with(compact('distritos'))
            ->with(compact('distritosel'))
            ->with(compact('item'));
    }

    public function update(DocenteRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $data = $request->all();
            $usuario = Helper::usuarioActual();

            $docente = Docente::findOrFail($id);

            $item = Persona::findOrFail($docente->persona_id);
            if(isset($data['tipo'])){
                $item->tipo = 1;
                $item->idcargo = $data['cargoi'];
                $item->iddependencia = $data['dependenciai'];
                $item->cargo = $data['cargodet'];
                $item->institucion = $data['dependenciadet'];
            }else{
                $item->tipo = 0;
                $item->idcargo = null;
                $item->iddependencia = null;
                $item->cargo = $data['cargoe'];
                $item->institucion = $data['institucione'];
            }

//            $item->dni = $data['dni'];
//            $item->apellidos = $data['apellidos'];
//            $item->nombres = $data['nombres'];
            $item->celular = $data['celular'];
            if(!is_null($data["fecha"]))
                $item->fecha = Carbon::createFromFormat('d/m/Y',$data["fecha"])->format('Y-m-d');
            $item->email = $data['correo'];
            $item->genero = $data['genero'];
            if(isset($data['estado'])){
                $docente->estado = 1;
            }else{
                $docente->estado = 0;

            }
            $item->cod_usum = $usuario;

            $item->save();
            $docente->save();
        }catch (\Exception $e) {
            DB::rollback();
            throw ValidationException::withMessages([
                'unknown' => $e->getMessage(). "Hay un error desconocido al guardar, vuelva a intentarlo y si persiste consulte con el Administrador del Sistema...",
            ]);
        }
        DB::commit();

        return redirect()
            ->route('docentes.index')
            ->withSuccess('Docente actualizado correctamente...');
    }

    public function buscar(Request $request)
    {
        $data = $request->all();
        $items = Docente::filtro($data["filtro"]);
        return view('docente.listarbusqueda')
                    ->with(compact('items'));
    }
}
