<?php

namespace App\Http\Controllers;

use App\Http\Requests\DiActividadEvaluativaRequest;
use App\Models\DiActividadEvaluativa;
use App\Models\DiModulo;
use App\Models\InstrumentoEvaluativo;
use App\Models\TipoActividadEvaluativa;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class DiActividadEvaluativaController extends Controller
{
    public function create($modulo)
    {
        if (request()->ajax()) {
            $tiposs = TipoActividadEvaluativa::listarTipos();
            $tipos = TipoActividadEvaluativa::listarByEstado(1);
            $instrumentos = InstrumentoEvaluativo::listarByEstado(1);
            return view('actividadevaluativa.nuevo_editar')
                ->with(compact('modulo'))
                ->with(compact('tiposs'))
                ->with(compact('tipos'))
                ->with(compact('instrumentos'));
        }
    }

    public function store(DiActividadEvaluativaRequest $request)
    {
        if (request()->ajax()) {
            $data = $request->all();
            $sumaduracion = DiActividadEvaluativa::sumaDuracion($data['codmodulo']);
            if($sumaduracion->suma + (int)$data['porcentaje'] > 100){
                throw ValidationException::withMessages(['porcentaje' => "El Porcentaje debe ser menor o igual a: ".(100 - $sumaduracion->suma)]);
            }

            $item = new DiActividadEvaluativa();
            $item->di_modulo_id = $data['codmodulo'];
            $item->tipo_actividad_evaluativa_id = $data['tipo'];
            $item->orden = $data['orden'];
            $item->descripcion = $data['descripcion'];
            $item->porcentaje = $data['porcentaje'];
            $item->instrumento_evaluativo_id = $data['instrumento'];
            $item->nota_aprobacion = $data['nota'];
            $item->fecha_hora_inicio = Carbon::createFromFormat('d/m/Y H:i',$data["fechahorainicio"])->format('Y-m-d H:i:s');
            $item->fecha_hora_cierre = Carbon::createFromFormat('d/m/Y H:i',$data["fechahoracierre"])->format('Y-m-d H:i:s');
            $item->save();

            return response()->json(['modulo' => $data['codmodulo']]);
        }
    }

    public function show($id)
    {
        $actividadesevaluativas = DiModulo::listarActividadesEvaluativas($id);
        $modulo = $id;
        return view('actividadevaluativa.index')
            ->with(compact('modulo'))
            ->with(compact('actividadesevaluativas'));
    }

    public function edit($id)
    {
        if (request()->ajax()) {
            $item = DiActividadEvaluativa::findOrFail($id);
            $tiposs = TipoActividadEvaluativa::listarTipos();
            $tipos = TipoActividadEvaluativa::listarByEstado(1);
            $instrumentos = InstrumentoEvaluativo::listarByEstado(1);

            return view('actividadevaluativa.nuevo_editar')
                ->with(compact('tiposs'))
                ->with(compact('tipos'))
                ->with(compact('instrumentos'))
                ->with(compact('item'));
        }
    }

    public function update(DiActividadEvaluativaRequest $request, $id)
    {
        if (request()->ajax()) {
            $data = $request->all();

            $item = DiActividadEvaluativa::findOrFail($id);
            $sumaduracion = DiActividadEvaluativa::sumaDuracion($data['codmodulo']);
            if(($sumaduracion->suma - $item->porcentaje) + (int)$data['porcentaje'] > 100){
                throw ValidationException::withMessages(['porcentaje' => "La Duración debe ser menor o igual a: ".(100 - ($sumaduracion->suma - $item->porcentaje))]);
            }

            $item->di_modulo_id = $data['codmodulo'];
            $item->tipo_actividad_evaluativa_id = $data['tipo'];
            $item->orden = $data['orden'];
            $item->descripcion = $data['descripcion'];
            $item->porcentaje = $data['porcentaje'];
            $item->instrumento_evaluativo_id = $data['instrumento'];
            $item->nota_aprobacion = $data['nota'];
            $item->fecha_hora_inicio = Carbon::createFromFormat('d/m/Y H:i',$data["fechahorainicio"])->format('Y-m-d H:i:s');
            $item->fecha_hora_cierre = Carbon::createFromFormat('d/m/Y H:i',$data["fechahoracierre"])->format('Y-m-d H:i:s');
            $item->save();

            return response()->json(['modulo' => $data['codmodulo']]);
        }
    }

    public function destroy($id)
    {
        if (request()->ajax()) {
            $ae = DiActividadEvaluativa::findOrFail($id);
            $ae->delete();

            return response()->json(['modulo' => $ae->di_modulo_id]);
        }
    }
}
