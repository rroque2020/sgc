<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Requests\ActividadAcademicaRequest;
use App\Models\ActividadAcademica;
use App\Models\ActividadPlan;
use App\Models\ActividadPolitica;
use App\Models\ActividadRequisito;
use App\Models\ActividadResolucion;
use App\Models\BloqueAcademico;
use App\Models\Convenio;
use App\Models\CoordinacionNacional;
use App\Models\Coordinador;
use App\Models\DistritoFiscal;
use App\Models\Duracion;
use App\Models\EntidadExterna;
use App\Models\PerfilPersonal;
use App\Models\Politica;
use App\Models\Resolucion;
use App\Models\Tematica;
use App\Models\LineaEstrategica;
use App\Models\Modalidad;
use App\Models\Plan;
use App\Models\Sistema;
use App\Models\SubGerencia;
use App\Models\TipoCertificado;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\TipoActividad;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class ActividadAcademicaController extends Controller
{

    public function index()
    {
        $subgerencias = SubGerencia::listarByEstado(1);
        $tipos = TipoActividad::listarByEstado(1);
        $lineas = LineaEstrategica::listarByEstado(1);
        $tematicas = Tematica::listarByEstado(1);
        $modalidades = Modalidad::listarByEstado(1);
        $sistemas = Sistema::listarByEstado(1);
        $items  = ActividadAcademica::listar(Carbon::now()->format('Y'));

        return view('actividad.index')
            ->with(compact('subgerencias'))
            ->with(compact('tipos'))
            ->with(compact('lineas'))
            ->with(compact('tematicas'))
            ->with(compact('modalidades'))
            ->with(compact('sistemas'))
            ->with(compact('items'));
    }

    public function buscar(Request $request)
    {
        if($request->ajax()) {
            $data = $request->all();
            $consulta =  "actividad_academica.fecha_inicio between '".$data['inicio']."' and '".$data['final']."'";

            if($data["programado"] != -1){
                $consulta .= ' and actividad_academica.programado = '.$data['programado'];
            }

            if($data["subgerencia"] != -1){
                $consulta .= ' and actividad_academica.sub_gerencia_id = '.$data['subgerencia'];
            }

            if($data["modalidad"] != -1){
                $consulta .= ' and actividad_academica.modalidad_id = '.$data['modalidad'];
            }

            if($data["tipo"] != -1){
                $consulta .= ' and actividad_academica.tipo_actividad_id = '.$data['tipo'];
            }

            if($data["origen"] != -1){
                $consulta .= " and actividad_academica.origen = '".$data['origen']."'";
            }

            if($data["sistema"] != -1){
                $consulta .= ' and actividad_academica.sistema_id = '.$data['sistema'];
            }

            if($data["linea"] != -1){
                if($data["bloque"] != -1){
                    $consulta .= ' and  actividad_academica.bloque_academico_id = '.$data['bloque'];
                }else{
                    $consulta .= ' and linea_estrategica.id = '.$data['linea'];
                }
            }

            if($data["tematica"] != -1){
                $consulta .= ' and actividad_academica.tematica_id = '.$data['tematica'];
            }

            if($data["estado"] != -1){
                $consulta .= ' and actividad_academica.estado = '.$data['estado'];
            }

            $actividad = new ActividadAcademica();
            $items = $actividad->buscar($consulta);
            return view('actividad.lista')->with(compact('items'));
        }
    }

    public function create()
    {
        $tipos = TipoActividad::listarByEstado(1);
        $lineas = LineaEstrategica::listarByEstado(1);
        $tematicas = Tematica::listarByEstado(1);
        $modalidades = Modalidad::listarByEstado(1);
        $sistemas = Sistema::listarByEstado(1);
        $coordinaciones = CoordinacionNacional::listarByEstado(1);
        $convenios = Convenio::listarByEstado(1);
        $entidades = EntidadExterna::listarByEstado(1);
        $distritos = DistritoFiscal::listarByEstado(1);
        $actividades = ActividadAcademica::listarByAnio(Carbon::now()->format('Y'));
        $planes = Plan::listarByEstado(1);
        $politicas = Politica::listarByEstado(1);
        $tiposcertificado = TipoCertificado::listarByEstado(1);
        $subgerencias = SubGerencia::listarByEstado(1);
        $perfilespersonales = PerfilPersonal::listarByEstado(1);
        $resoluciones = Resolucion::listar();

        return view('actividad.nuevo')
            ->with(compact('tipos'))
            ->with(compact('lineas'))
            ->with(compact('tematicas'))
            ->with(compact('modalidades'))
            ->with(compact('sistemas'))
            ->with(compact('coordinaciones'))
            ->with(compact('convenios'))
            ->with(compact('entidades'))
            ->with(compact('distritos'))
            ->with(compact('actividades'))
            ->with(compact('planes'))
            ->with(compact('tiposcertificado'))
            ->with(compact('subgerencias'))
            ->with(compact('perfilespersonales'))
            ->with(compact('resoluciones'))
            ->with(compact('politicas'));
    }

    public function store(ActividadAcademicaRequest $request)
    {
        DB::beginTransaction();
        try{
            $data = $request->all();
            $usuario = Helper::usuarioActual();
            $item = new ActividadAcademica();

            $item->id = $item::siguiente();
            $item->codigo = Helper::codigoAA(Helper::extraerAnio($data['fechainicio']),$data['linea']);
            $item->titulo = $data['titulo'];
            if(isset($data['programado'])){
                $item->programado = 1;
            }
            $item->descripcion = $data['descripcion'];
            $item->sub_gerencia_id = $data['subgerencia'];
            $item->tipo_actividad_id = $data['tipo'];
            $item->modalidad_id = $data['modalidad'];
            if($data['modalidad'] != 3){
                $item->sede = $data['distritosede'];
            }
            $item->sistema_id = $data['sistema'];
            $item->tematica_id = $data['tematica'];
            $item->bloque_academico_id = $data['bloque'];
            $item->tipo_certificado_id = $data['tipocertificado'];
            if($data['coordinacion'] <> '-1'){
                $item->coordinacion_nacional_id = $data['coordinacion'];
            }
            $item->origen = $data['origen'];
            if($data['origen'] =='DISTRITO FISCAL'){
                $item->organizador_id = $data['distritofiscal'];
            }
            if($data['origen'] =='COOPERANTE'){
                $item->organizador_id = $data['cooperante'];
            }
            if(isset($data['convenio'])){
                $item->convenio = 1;
                $item->convenio_id = $data['codconvenio'];
            }
            $item->fecha_inicio = Carbon::createFromFormat('d/m/Y',$data["fechainicio"])->format('Y-m-d');
            $item->fecha_final = Carbon::createFromFormat('d/m/Y',$data["fechafin"])->format('Y-m-d');
            $item->duracion = $data['duracion'];
            $item->horario_detallado = $data['horariodetallado'];
            $item->horarioj_detallado = $data['horariojdetallado'];
            $item->nro_aulas = $data['nroaulas'];
            $item->nro_vacantes = $data['nrovacantes'];
            $item->nro_docentes = $data['nrodocentes'];
            if(isset($data['convocatoria'])){
                $item->convocatoria = 1;
            }
            if(isset($data['compromiso'])){
                $item->compromiso = 1;
            }
            if(isset($data['publico'])){
                $item->publico = 1;
            }
            if(isset($data['autoformativo'])){
                $item->autoformativo = 1;
            }
            $item->cod_usuc = $usuario;
            $item->cod_usum = $usuario;
            $item->save();

            if(isset($data['convenio'])){
                $item->entidadesconvenio()->syncWithPivotValues($data['entconvenio'], ['cod_usu' => $usuario]);
            }

            if(isset($data['codaa'])) {
                $detalles = $request->get('codaa');

                for ($i = 0; $i < count($detalles); $i++)
                {
                    $detalle = new ActividadRequisito();
                    $detalle->actividad_academica_id = $item->id;
                    $detalle->requisito = $detalles[$i];
                    $detalle->cod_usu = $usuario;
                    $detalle->save();
                }
            }
            $item->perfiles()->syncWithPivotValues($data['perfilpersonal'], ['cod_usu' => $usuario]);
            $item->resoluciones()->syncWithPivotValues($data['codresolucion'], ['cod_usu' => $usuario]);

            if(isset($data['codplan'])){
                $item->planes()->syncWithPivotValues($data['codplan'], ['cod_usu' => $usuario]);
            }
            if(isset($data['codpolitica'])){
                $item->politicas()->syncWithPivotValues($data['codpolitica'], ['cod_usu' => $usuario]);
            }
        }catch (\Exception $e) {
            DB::rollback();
            throw ValidationException::withMessages([
                'unknown' => $e->getMessage(). "Hay un error desconocido al guardar, vuelva a intentarlo y si persiste consulte con el Administrador del Sistema...",
            ]);
        }
        DB::commit();

        return redirect()
            ->route('actividad.index')
            ->withSuccess('Actividad Académica registrada correctamente...');
    }

    public function option($id)
    {
        $actividad = ActividadAcademica::findOrFail($id);
        $coordinadores = Coordinador::listar();

        return view('actividad.opciones')
                ->with(compact('actividad'))
                ->with(compact('coordinadores'));
    }

    public function show($id)
    {
        $actividad = ActividadAcademica::listarDatos($id);
        $act = ActividadAcademica::findOrFail($id);
        $requisitos = ActividadAcademica::listarRequisitos($id);
        $resoluciones = $act->resoluciones;
        $planes = $act->planes;
        $politicas = $act->politicas;

        return view('actividad.ver')
            ->with(compact('actividad'))
            ->with(compact('requisitos'))
            ->with(compact('resoluciones'))
            ->with(compact('planes'))
            ->with(compact('politicas'));
    }

    public function edit($id)
    {
        $actividad = ActividadAcademica::findOrFail($id);
        $subgerencias = SubGerencia::listarByEstado(1);
        $tipos = TipoActividad::listarByEstado(1);
        $modalidades = Modalidad::listarByEstado(1);
        $distritos = DistritoFiscal::listarByEstado(1);
        $sistemas = Sistema::listarByEstado(1);
        $tematicas = Tematica::listarByEstado(1);
        $lineas = LineaEstrategica::listarByEstado(1);
        $lineaid = BloqueAcademico::listarDatos($actividad->bloque_academico_id)->linea_estrategica_id;
        $bloques = BloqueAcademico::listarByLinea($lineaid);
        $tiposcertificado = TipoCertificado::listarByEstado(1);
        $coordinaciones = CoordinacionNacional::listarByEstado(1);
        $entidades = EntidadExterna::listarByEstado(1);
        $perfilespersonales = PerfilPersonal::listarByEstado(1);
        $perfilesseleccionados = $actividad->perfiles->pluck('id');
        $convenios = Convenio::listarByEstado(1);
        $entidadesconvenio = $actividad->entidadesconvenio->pluck('id');
        $actividades = ActividadAcademica::listarByAnio(Carbon::now()->format('Y'));
        $requisitos = ActividadAcademica::listarRequisitos($id);
        $resolucionesall = Resolucion::listar();
        $resoluciones = ActividadAcademica::listarResoluciones($id);
        $planesall = Plan::listarByEstado(1);
        $planes = ActividadAcademica::listarPlanes($id);
        $politicasall = Politica::listarByEstado(1);
        $politicas = ActividadAcademica::listarPoliticas($id);

        return view('actividad.editar')
            ->with(compact('actividad'))
            ->with(compact('subgerencias'))
            ->with(compact('tipos'))
            ->with(compact('modalidades'))
            ->with(compact('distritos'))
            ->with(compact('sistemas'))
            ->with(compact('tematicas'))
            ->with(compact('lineas'))
            ->with(compact('lineaid'))
            ->with(compact('bloques'))
            ->with(compact('tiposcertificado'))
            ->with(compact('coordinaciones'))
            ->with(compact('entidades'))
            ->with(compact('perfilespersonales'))
            ->with(compact('perfilesseleccionados'))
            ->with(compact('convenios'))
            ->with(compact('entidadesconvenio'))
            ->with(compact('actividades'))
            ->with(compact('requisitos'))
            ->with(compact('resolucionesall'))
            ->with(compact('resoluciones'))
            ->with(compact('planesall'))
            ->with(compact('planes'))
            ->with(compact('politicasall'))
            ->with(compact('politicas'));

    }

    public function update(ActividadAcademicaRequest $request, $id)
    {
        DB::beginTransaction();
        try{
            $data = $request->all();
            $usuario = Helper::usuarioActual();

            $item = ActividadAcademica::findOrFail($id);

            $item->titulo = $data['titulo'];
            if(isset($data['programado'])){
                $item->programado = 1;
            }else{
                $item->programado = 0;
            }
            $item->descripcion = $data['descripcion'];
            $item->sub_gerencia_id = $data['subgerencia'];
            $item->tipo_actividad_id = $data['tipo'];
            $item->modalidad_id = $data['modalidad'];
            if($data['modalidad'] != 3){
                $item->sede = $data['distritosede'];
            }
            $item->sistema_id = $data['sistema'];
            $item->tematica_id = $data['tematica'];
            $item->bloque_academico_id = $data['bloque'];
            $item->tipo_certificado_id = $data['tipocertificado'];
            if($data['coordinacion'] <> '-1'){
                $item->coordinacion_nacional_id = $data['coordinacion'];
            }
            $item->origen = $data['origen'];
            if($data['origen'] =='DISTRITO FISCAL'){
                $item->organizador_id = $data['distritofiscal'];
            }
            if($data['origen'] =='COOPERANTE'){
                $item->organizador_id = $data['cooperante'];
            }
            if(isset($data['convenio'])){
                $item->convenio = 1;
                $item->convenio_id = $data['codconvenio'];
                $item->entidadesconvenio()->syncWithPivotValues($data['entconvenio'], ['cod_usu' => $usuario]);
            }else{
                $item->convenio = 0;
                $item->convenio_id = null;
                $item->entidadesconvenio()->syncWithPivotValues(null, ['cod_usu' => $usuario]);
            }

            $item->fecha_inicio = Carbon::createFromFormat('d/m/Y',$data["fechainicio"])->format('Y-m-d');
            $item->fecha_final = Carbon::createFromFormat('d/m/Y',$data["fechafin"])->format('Y-m-d');
            $item->duracion = $data['duracion'];
            $item->horario_detallado = $data['horariodetallado'];
            $item->horarioj_detallado = $data['horariojdetallado'];
            $item->nro_aulas = $data['nroaulas'];
            $item->nro_vacantes = $data['nrovacantes'];
            $item->nro_docentes = $data['nrodocentes'];
            if(isset($data['convocatoria'])){
                $item->convocatoria = 1;
            }else{
                $item->convocatoria = 0;
            }

            if(isset($data['compromiso'])){
                $item->compromiso = 1;
            }else{
                $item->compromiso = 0;
            }

            if(isset($data['publico'])){
                $item->publico = 1;
            }else{
                $item->publico = 0;
            }

            if(isset($data['autoformativo'])){
                $item->autoformativo = 1;
            }else{
                $item->autoformativo = 0;
            }

            $item->cod_usum = $usuario;
            $item->save();


            $item->perfiles()->syncWithPivotValues($data['perfilpersonal'], ['cod_usu' => $usuario]);
        }catch (\Exception $e) {
            DB::rollback();
            throw ValidationException::withMessages([
                'unknown' => "Hay un error desconocido al guardar, vuelva a intentarlo y si persiste consulte con el Administrador del Sistema...",
            ]);
        }
        DB::commit();

        return redirect()
            ->route('actividad.option',['codigo' => $id])
            ->withSuccess('Actividad Académica actualizada correctamente ...');
    }

    public function storerequisito(Request $request){
        if (request()->ajax()) {
            $data = $request->all();
            $usuario = Helper::usuarioActual();

            $requisito = new ActividadRequisito();
            $requisito->actividad_academica_id = $data['codactividad'];
            $requisito->requisito = $data['actividad'];
            $requisito->cod_usu = $usuario;
            $requisito->save();

            $requisitos  = ActividadAcademica::listarRequisitos($data['codactividad']);
            return view('actividad.listarequisito')->with(compact('requisitos'));
        }
    }

    public function destroyrequisito(Request $request)
    {
        if (request()->ajax()) {
            $data = $request->all();
            $requisito = ActividadRequisito::findOrFail($data['actividad']);
            $requisito->delete();

            $requisitos = ActividadAcademica::listarRequisitos($data['codactividad']);
            return view('actividad.listarequisito')->with(compact('requisitos'));
        }
    }

    public function storeresolucion(Request $request){
        if (request()->ajax()) {
            $data = $request->all();
            $usuario = Helper::usuarioActual();

            $resolucion = new ActividadResolucion();
            $resolucion->actividad_academica_id = $data['codactividad'];
            $resolucion->resolucion_id = $data['resolucion'];
            $resolucion->cod_usu = $usuario;
            $resolucion->save();
            $resoluciones = ActividadAcademica::listarResoluciones($data['codactividad']);
            return view('actividad.listaresolucion')->with(compact('resoluciones'));
        }
    }

    public function destroyresolucion(Request $request)
    {
        if (request()->ajax()) {
            $data = $request->all();
            $resolucion = ActividadResolucion::findOrFail($data['resolucion']);
            $resolucion->delete();

            $resoluciones = ActividadAcademica::listarResoluciones($data['codactividad']);
            return view('actividad.listaresolucion')->with(compact('resoluciones'));
        }
    }

    public function storeplan(Request $request){
        if (request()->ajax()) {
            $data = $request->all();
            $usuario = Helper::usuarioActual();

            $plan = new ActividadPlan();
            $plan->actividad_academica_id = $data['codactividad'];
            $plan->plan_id = $data['plan'];
            $plan->cod_usu = $usuario;
            $plan->save();
            $planes = ActividadAcademica::listarPlanes($data['codactividad']);
            return view('actividad.listaplan')->with(compact('planes'));
        }
    }

    public function destroyplan(Request $request)
    {
        if (request()->ajax()) {
            $data = $request->all();
            $plan = ActividadPlan::findOrFail($data['plan']);
            $plan->delete();

            $planes = ActividadAcademica::listarPlanes($data['codactividad']);
            return view('actividad.listaplan')->with(compact('planes'));
        }
    }

    public function storepolitica(Request $request){
        if (request()->ajax()) {
            $data = $request->all();
            $usuario = Helper::usuarioActual();

            $politica = new ActividadPolitica();
            $politica->actividad_academica_id = $data['codactividad'];
            $politica->politica_id = $data['politica'];
            $politica->cod_usu = $usuario;
            $politica->save();
            $politicas = ActividadAcademica::listarPoliticas($data['codactividad']);
            return view('actividad.listapolitica')->with(compact('politicas'));
        }
    }

    public function destroypolitica(Request $request)
    {
        if (request()->ajax()) {
            $data = $request->all();
            $politica = ActividadPolitica::findOrFail($data['politica']);
            $politica->delete();

            $politicas = ActividadAcademica::listarPoliticas($data['codactividad']);
            return view('actividad.listapolitica')->with(compact('politicas'));
        }
    }

    public function assigncoordinator(Request $request){
        if (request()->ajax()) {
            $data = $request->all();
            $usuario = Helper::usuarioActual();

            $actividad = ActividadAcademica::findOrFail($data['codactividad']);
            $actividad->coordinador = $data['codcoordinador'];
            $actividad->cod_usum = $usuario;
            $actividad->estado = 2;
            $actividad->save();

            return response()->json(['estado' => $actividad->estado, 'htmlestado' => Helper::estadoAA($actividad->estado, false)]);
        }
    }

    public function schedule($inicial, $final){
        return view('actividad.horario');
    }

    public function aprobardisenio(Request $request){
        if(request()->ajax()){
            $data = $request->all();
            $actividad = ActividadAcademica::findOrFail($data['codactividad']);
            $actividad->estado = $data['estado'];
            $actividad->save();

            return response()->json('exito');
        }
    }
}
