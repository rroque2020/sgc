<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Requests\DiModuloRequest;
use App\Models\ActividadAcademica;
use App\Models\DiModulo;
use App\Models\TipoCapacitacion;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class DiModuloController extends Controller
{
    public function create($id)
    {
        if (request()->ajax()) {
            $cont = ActividadAcademica::validarEjecucion($id)->cont;

            if($cont == 0)
                return response()->json('nova');

            $tipos = TipoCapacitacion::listarByEstado(1);
            return view('disenioinstruccional.modulo')
                ->with(compact('id'))
                ->with(compact('tipos'));
        }
    }

    public function edit($actividad, $id)
    {
        if (request()->ajax()) {
            $modulo = DiModulo::findOrFail($id);
            $tipos = TipoCapacitacion::listarByEstado(1);
            return view('disenioinstruccional.modulo')
                ->with(compact('modulo'))
                ->with(compact('tipos'));
        }
    }

    public function store(DiModuloRequest $request)
    {
        if (request()->ajax()) {
            $data = $request->all();
            $sumaduracion = DiModulo::sumaDuracion($data['codactividad']);
            if($sumaduracion->suma + (int)$data['duracion'] > $sumaduracion->duracione){
                throw ValidationException::withMessages(['duracion' => "La Duración debe ser menor o igual a: ".($sumaduracion->duracione - $sumaduracion->suma)]);
            }
            $actividad = ActividadAcademica::listarDatos($data['codactividad']);
            $item = DiModulo::siguienteModulo($data['codactividad'])->codigo;
            $modulo = new DiModulo();
            $modulo->actividad_academica_id = $data['codactividad'];
            $modulo->item = $item;
            $modulo->tipo_capacitacion_id = $data['tipo'];
            $modulo->nombre = $data['nombre'];
            $modulo->descripcion = $actividad->destipo_modulo.' '.Helper::convertirARomano($item);
            $modulo->abreviatura = 'M'.Helper::convertirARomano($item);
            $modulo->fecha_inicio = Carbon::createFromFormat('d/m/Y',$data["fechainicio"])->format('Y-m-d');
            $modulo->fecha_final = Carbon::createFromFormat('d/m/Y',$data["fechafin"])->format('Y-m-d');
            $modulo->duracion = $data['duracion'];

            $modulo->save();

            $modulos = ActividadAcademica::listarModulos($data['codactividad']);
            return view('disenioinstruccional.detalle')->with(compact('modulos'));
        }
    }

    public function update(DiModuloRequest $request, $id)
    {
        if (request()->ajax()) {
            $data = $request->all();

            $modulo = DiModulo::findOrFail($id);
            $sumaduracion = DiModulo::sumaDuracion($data['codactividad']);
            if(($sumaduracion->suma - $modulo->duracion) + (int)$data['duracion'] > $sumaduracion->duracione){
                throw ValidationException::withMessages(['duracion' => "La Duración debe ser menor o igual a: ".($sumaduracion->duracione - ($sumaduracion->suma - $modulo->duracion))]);
            }
            $modulo->tipo_capacitacion_id = $data['tipo'];
            $modulo->nombre = $data['nombre'];
            $modulo->fecha_inicio = Carbon::createFromFormat('d/m/Y',$data["fechainicio"])->format('Y-m-d');
            $modulo->fecha_final = Carbon::createFromFormat('d/m/Y',$data["fechafin"])->format('Y-m-d');
            $modulo->duracion = $data['duracion'];

            $modulo->save();

            $modulos = ActividadAcademica::listarModulos($data['codactividad']);
            return view('disenioinstruccional.detalle')->with(compact('modulos'));
        }
    }

    public function destroy($id)
    {
        if (request()->ajax()) {
            $modulo = DiModulo::findOrFail($id);
            $modulo->delete();

            $modulos = ActividadAcademica::listarModulos($modulo->actividad_academica_id);
            return view('disenioinstruccional.detalle')->with(compact('modulos'));
        }
    }

    public function estructura($id)
    {
        $competencias = DiModulo::listarEstructuraCompetencias($id);
        $actividadesevaluativas = DiModulo::listarEstructuraActividadesEvaluativas($id);

        return view('disenioinstruccional.sheet')
            ->with(compact('competencias'))
            ->with(compact('actividadesevaluativas'));
    }
}
