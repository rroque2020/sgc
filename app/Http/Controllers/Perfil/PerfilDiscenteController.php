<?php

namespace App\Http\Controllers\Perfil;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;

class PerfilDiscenteController extends Controller
{
    public function resumen()
    {
        if (request()->ajax()) {
            $usuario = Helper::usuarioActual();

            return view("perfil.discente.resumen")
                ->with(compact('totalesdisc'));
        }
    }
}
