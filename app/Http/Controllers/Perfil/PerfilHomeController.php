<?php

namespace App\Http\Controllers\Perfil;

use App\Http\Controllers\Controller;
use App\Helpers\Helper;
use App\Models\ActividadDiscente;
use App\Models\ActividadDocente;
use App\Models\Discente;
use App\Models\DistritoFiscal;
use App\Models\Docente;
use App\Models\DocenteCapacitacion;
use App\Models\DocenteExperiencia;
use App\Models\DocenteFormacion;
use App\Models\GradoAcademico;
use App\Models\Institucion;
use App\Models\Persona;
use App\Models\Profesion;
use App\Models\TCargo;
use App\Models\TDependencia;

class PerfilHomeController extends Controller
{
    public function index()
    {
        $usuario = Helper::usuarioActual();
        $persona = Persona::listarDatos($usuario);
        $totalesdoc  = Docente::totales($usuario);
        $totalesdisc  = Discente::totales($usuario);
        $grados = GradoAcademico::listarByEstado(1);
        $instituciones = Institucion::listarByEstado(1);
        $profesiones = Profesion::listarByEstado(1);
        $formaciones = DocenteFormacion::listarByDocente($usuario);
        $capacitaciones = DocenteCapacitacion::listarByDocente($usuario);
        $experiencias = DocenteExperiencia::listarByDocente($usuario);
        $actividadesdoc = ActividadDocente::listarByDocente($usuario);
        $actividadesdisc = ActividadDiscente::listarByDiscente(1);
        $cargos = TCargo::listar();
        $distritos = DistritoFiscal::listarByEstado(1);
        if(TDependencia::listarDatos($persona->iddependencia) == null){
            $distritosel = null;
        }else{
            $distritosel = TDependencia::listarDatos($persona->iddependencia)->CODI_DIJU_DJU;
        }

        return view("perfil.home")
                    ->with(compact('persona'))
                    ->with(compact('totalesdoc'))
                    ->with(compact('totalesdisc'))
                    ->with(compact('grados'))
                    ->with(compact('instituciones'))
                    ->with(compact('profesiones'))
                    ->with(compact('formaciones'))
                    ->with(compact('capacitaciones'))
                    ->with(compact('experiencias'))
                    ->with(compact('actividadesdoc'))
                    ->with(compact('actividadesdisc'))
                    ->with(compact('cargos'))
                    ->with(compact('distritos'))
                    ->with(compact('distritosel'));
    }

    public function resumen()
    {
        if (request()->ajax()) {
            $usuario = Helper::usuarioActual();
            $totalesdoc = Docente::totales($usuario);
            $totalesdisc = Discente::totales($usuario);

            return view("perfil.resumen")
                ->with(compact('totalesdoc'))
                ->with(compact('totalesdisc'));
        }
    }
}
