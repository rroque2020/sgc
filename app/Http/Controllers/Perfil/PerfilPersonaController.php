<?php

namespace App\Http\Controllers\Perfil;

use App\Http\Controllers\Controller;
use App\Helpers\Helper;
use App\Http\Requests\Perfil\PersonaRequest;
use App\Models\Persona;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PerfilPersonaController extends Controller
{
    public function actualizarfoto(Request $request)
    {
        if (request()->ajax()) {
            $rules = [
                'foto' => ['required','mimes:jpeg','max:2048'],
            ];

            request()->validate($rules);

            $data = $request->all();

            $usuario = Helper::usuarioActual();
            $persona = Persona::findOrFail($data['id']);

            if ($request->hasFile('foto')) {
                if ($persona->foto != null) {
                    Storage::disk('fotos')->delete($persona->foto);
                    $persona->foto = null;
                }
                $persona->foto = $request->file('foto')->store( 'persona','fotos');
            }

            $persona->cod_usum = $usuario;
            $persona->save();
            return view("perfil.foto")->with(compact('persona'));
        }
    }

    public function actualizardatos(PersonaRequest $request)
    {
        if(request()->ajax()){
            $data = $request->all();
            $usuario = Helper::usuarioActual();

            $item = Persona::findOrFail($data['id']);
            $item->celular = $data['celular'];
            if(!is_null($data["fecha"]))
                $item->fecha = Carbon::createFromFormat('d/m/Y',$data["fecha"])->format('Y-m-d');
            $item->email = $data['correo'];
            $item->genero = $data['genero'];

            if($data['tipo'] == 1){
                $item->idcargo = $data['cargo'];
                $item->iddependencia = $data['dependencia'];
                $item->cargo = $data['cargodet'];
                $item->institucion = $data['dependenciadet'];
            }else{
				$item->cargo = $data['cargoe'];
                $item->institucion = $data['institucione'];
            }
            $item->cod_usum = $usuario;
            $item->save();
            $persona = Persona::listarDatos($item->id);

            return view("perfil.informacion")->with(compact('persona'));
        }
    }
}
