<?php

namespace App\Http\Controllers\Perfil;

use App\Http\Controllers\Controller;
use App\Helpers\Helper;

use App\Http\Requests\Perfil\DocenteExperienciaRequest;
use App\Http\Requests\Perfil\DocenteCapacitacionRequest;
use App\Http\Requests\Perfil\DocenteFormacionRequest;
use App\Models\DocenteCapacitacion;
use App\Models\DocenteExperiencia;
use App\Models\DocenteFormacion;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PerfilDocenteController extends Controller
{
    public function storeformacion(DocenteFormacionRequest $request)
    {
        if (request()->ajax()) {
            $data = $request->all();
            $usuario = Helper::usuarioActual();

            $item = new DocenteFormacion();
            $item->docente_id = $data['coddocente'];
            $item->grado_academico_id = $data['grado'];
            $item->institucion_id = $data['institucion'];
            $item->profesion_id = $data['profesion'];
            $item->ingreso = $data['ingreso'];
            $item->egreso = $data['egreso'];

            if ($request->hasFile('sustentoform')) {
                $item->sustento = $request->file('sustentoform')->store('docentes', 'archivos');
            }

            $item->cod_usu = $usuario;
            $item->save();

            $formaciones = DocenteFormacion::listarByDocente($usuario);
            return view("perfil.docente.listaformacion")->with(compact('formaciones'));
        }
    }

    public function destroyformacion(Request $request)
    {
        if (request()->ajax()) {
            $data = $request->all();
            $usuario = Helper::usuarioActual();

            $formacion = DocenteFormacion::findOrFail($data['codformacion']);
            if ($formacion->sustento != null) {
                Storage::disk('archivos')->delete($formacion->sustento);
                $formacion->sustento = null;
            }
            $formacion->delete();

            $formaciones = DocenteFormacion::listarByDocente($usuario);
            return view('perfil.docente.listaformacion')->with(compact('formaciones'));
        }
    }

    public function storecapacitacion(DocenteCapacitacionRequest $request)
    {
        if (request()->ajax()) {
            $data = $request->all();
            $usuario = Helper::usuarioActual();

            $item = new DocenteCapacitacion();
            $item->docente_id = $data['coddocente'];
            $item->tipo = strtoupper($data['tipocap']);
            $item->institucion = strtoupper($data['institucioncap']);
            $item->especialidad = strtoupper($data['especialidadcap']);
            $item->ingreso = Carbon::createFromFormat('d/m/Y',$data["ingresocap"])->format('Y-m-d');
            $item->egreso = Carbon::createFromFormat('d/m/Y',$data["egresocap"])->format('Y-m-d');
            $item->horas = $data['horascap'];

            if ($request->hasFile('sustentocap')) {
                $item->sustento = $request->file('sustentocap')->store('docentes', 'archivos');
            }

            $item->cod_usu = $usuario;
            $item->save();

            $capacitaciones = DocenteCapacitacion::listarByDocente($usuario);
            return view("perfil.docente.listacapacitacion")->with(compact('capacitaciones'));
        }
    }

    public function destroycapacitacion(Request $request)
    {
        if (request()->ajax()) {
            $data = $request->all();
            $usuario = Helper::usuarioActual();

            $capacitacion = DocenteCapacitacion::findOrFail($data['codcapacitacion']);
            if ($capacitacion->sustento != null) {
                Storage::disk('archivos')->delete($capacitacion->sustento);
                $capacitacion->sustento = null;
            }
            $capacitacion->delete();

            $capacitaciones = DocenteCapacitacion::listarByDocente($usuario);
            return view('perfil.docente.listacapacitacion')->with(compact('capacitaciones'));
        }
    }

    public function storeexperiencia(DocenteExperienciaRequest $request)
    {
        if (request()->ajax()) {
            $data = $request->all();
            $usuario = Helper::usuarioActual();

            $item = new DocenteExperiencia();
            $item->docente_id = $data['coddocente'];
            $item->entidad = strtoupper($data['entidadexp']);
            $item->cargo = strtoupper($data['cargoexp']);
            $item->area = strtoupper($data['areaexp']);
            $item->funciones = strtoupper($data['funcionesexp']);
            $item->ingreso = Carbon::createFromFormat('d/m/Y',$data["ingresoexp"])->format('Y-m-d');
            $item->egreso = Carbon::createFromFormat('d/m/Y',$data["egresoexp"])->format('Y-m-d');

            if ($request->hasFile('sustentoexp')) {
                $item->sustento = $request->file('sustentoexp')->store('docentes', 'archivos');
            }

            $item->cod_usu = $usuario;
            $item->save();

            $experiencias = DocenteExperiencia::listarByDocente($usuario);
            return view("perfil.docente.listaexperiencia")->with(compact('experiencias'));
        }
    }

    public function destroyexperiencia(Request $request)
    {
        if (request()->ajax()) {
            $data = $request->all();
            $usuario = Helper::usuarioActual();

            $experiencia = DocenteExperiencia::findOrFail($data['codexperiencia']);
            if ($experiencia->sustento != null) {
                Storage::disk('archivos')->delete($experiencia->sustento);
                $experiencia->sustento = null;
            }
            $experiencia->delete();

            $experiencias = DocenteExperiencia::listarByDocente($usuario);
            return view('perfil.docente.listaexperiencia')->with(compact('experiencias'));
        }
    }
}

