<?php

namespace App\Http\Controllers;

use App\Http\Requests\DiUnidadRequest;
use App\Models\DiSesion;
use App\Models\DiUnidad;

class DiUnidadController extends Controller
{
    public function create($sesion)
    {
        if (request()->ajax()) {
            return view('unidad.nuevo_editar')
                ->with(compact('sesion'));
        }
    }

    public function store(DiUnidadRequest $request)
    {
        if (request()->ajax()) {
            $data = $request->all();

            $item = new DiUnidad();
            $item->di_sesion_id = $data['codsesion'];
            $item->orden = $data['orden'];
            $item->nombre = $data['nombre'];
            $item->descripcion = $data['descripcion'];
            $item->bibliografia = $data['bibliografia'];
            $item->save();

            return response()->json(['sesion' => $data['codsesion']]);
        }
    }

    public function show($id)
    {
        $unidades = DiSesion::listarUnidades($id);
        $sesion = $id;
        return view('unidad.index')
            ->with(compact('sesion'))
            ->with(compact('unidades'));
    }

    public function edit($id)
    {
        if (request()->ajax()) {
            $item = DiUnidad::findOrFail($id);
            return view('unidad.nuevo_editar')
                ->with(compact('item'));
        }
    }

    public function update(DiUnidadRequest $request, $id)
    {
        if (request()->ajax()) {
            $data = $request->all();

            $item = DiUnidad::findOrFail($id);
            $item->di_sesion_id = $data['codsesion'];
            $item->orden = $data['orden'];
            $item->nombre = $data['nombre'];
            $item->descripcion = $data['descripcion'];
            $item->bibliografia = $data['bibliografia'];
            $item->save();

            return response()->json(['sesion' => $data['codsesion']]);
        }
    }

    public function destroy($id)
    {
        if (request()->ajax()) {
            $unidad = DiUnidad::findOrFail($id);
            $unidad->delete();

            return response()->json(['sesion' => $unidad->di_sesion_id]);
        }
    }
}
