<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DiCompetenciaRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'orden'=> 'required|checkselect',
            'competencia'=> 'required',
        ];
    }

    public function messages ()
    {
        return [
            'orden.required' => 'El Nro. de Competencia es obligatorio',
            'orden.checkselect' => 'El Nro. de Competencia es obligatorio',
            'competencia.required' => 'La Competencia es obligatoria',
        ];
    }
}
