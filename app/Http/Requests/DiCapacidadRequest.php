<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DiCapacidadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'orden'=> 'required|checkselect',
            'descripcion'=> 'required',
        ];
    }

    public function messages ()
    {
        return [
            'orden.required' => 'El Nro. de Capacidad es obligatorio',
            'orden.checkselect' => 'El Nro. de Capacidad es obligatorio',
            'descripcion.required' => 'La Capacidad es obligatoria',
        ];
    }
}
