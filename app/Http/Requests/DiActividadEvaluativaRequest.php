<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DiActividadEvaluativaRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'tipo' => 'required|checkselect',
            'orden' => 'required|checkselect',
            'descripcion' => 'required',
            'porcentaje' => 'required',
            'instrumento' => 'required|checkselect',
            'nota' => 'required',
            'fechahorainicio' => 'required',
            'fechahoracierre' => 'required',
        ];
    }

    public function messages ()
    {
        return [
            'tipo.required' => 'El Tipo es obligatorio',
            'tipo.checkselect' => 'El Tipo es obligatorio',
            'orden.required' => 'El Nro. de Actividad Evaluativa es obligatorio',
            'orden.checkselect' => 'El Nro. de Actividad Evaluativa es obligatorio',
            'descripcion.required' => 'La Descripción es obligatoria',
            'porcentaje.required' => 'El Porcentaje es obligatorio',
            'instrumento.required' => 'EL Instrumento es obligatorio',
            'instrumento.checkselect' => 'EL Instrumento es obligatorio',
            'nota.required' => 'La Notade Aprobación es obligatoria',
            'fechahorainicio.required' => 'La Fecha y Hora de Inicio es obligatoria',
            'fechahoracierre.required' => 'La Fecha y Hora de Cierre es obligatoria',
        ];
    }
}
