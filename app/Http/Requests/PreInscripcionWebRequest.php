<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PreInscripcionWebRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'dni'=> 'required|min:8|max:8',
            'celular'=> 'required|min:9|max:9',
            'correo' => 'required|email',
        ];
    }

    public function messages ()
    {
        return [
            'dni.required' => 'El DNI es obligatorio',
            'dni.min' => 'El DNI debe tener como mínimo 8 dígitos',
            'dni.max' => 'El DNI debe tener como máximo 8 dígitos',
            'celular.required' => 'El Celular es obligatorio',
            'celular.min' => 'El Celular debe tener como mínimo 9 dígitos',
            'celular.max' => 'El Celular debe tener como máximo 9 dígitos',
            'correo.required' => 'El Correo es obligatorio',
            'correo.email' => 'El Correo no es válido',
        ];
    }
}
