<?php

namespace App\Http\Requests;

use App\Rules\CheckSelect;
use Illuminate\Foundation\Http\FormRequest;

class ActividadAcademicaRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules1 = collect([
            'titulo' => 'required|max:255',
            'descripcion' => 'required|max:1000',
            'subgerencia' => 'required|checkselect',
            'tipo' => 'required|checkselect',
            'modalidad' => 'required|checkselect',
        ]);

        if($this["modalidad"] != "3"){
            $rules2 = collect([
                'distritosede' => 'required|checkselect',
                'sistema' => 'required|checkselect',
                'tematica' => 'required|checkselect',
                'linea' => 'required|checkselect',
                'bloque' => 'required|checkselect',
                'tipocertificado' => 'required|checkselect',
            ]);
        }else{
            $rules2 = collect([
                'sistema' => 'required|checkselect',
                'tematica' => 'required|checkselect',
                'linea' => 'required|checkselect',
                'bloque' => 'required|checkselect',
                'tipocertificado' => 'required|checkselect',
            ]);
        }

        $rules3 = $rules1->union($rules2);

        if(isset($this['convenio'])){
            $rules4 = collect([
                'codconvenio' => 'required|checkselect',
                'entconvenio' => 'required|array|min:1',
                'origen' => 'required|checkselect',
            ]);
        }else{
            $rules4 = collect([
                'origen' => 'required|checkselect',
            ]);
        }

        $rules5 = $rules3->union($rules4);

        if($this['origen'] =='DISTRITO FISCAL'){
            $rules6 = collect([
                'distritofiscal' => 'required|checkselect',
                'perfilpersonal' => 'required|array|min:1',
                'fechainicio' => 'required|date_format:d/m/Y',
                'fechafin' => 'required|date_format:d/m/Y',
                'duracion' => 'required|integer|min:0|not_in:0',
                'horariodetallado' => 'required',
                'nroaulas' => 'required|integer|min:0|not_in:0',
                'nrovacantes' => 'required|integer|min:0|not_in:0',
                'nrodocentes' => 'required|integer|min:0|not_in:0',
            ]);
        }else{
            if($this['origen'] =='COOPERANTE'){
                $rules6 = collect([
                    'cooperante' => 'required|checkselect',
                    'perfilpersonal' => 'required|array|min:1',
                    'fechainicio' => 'required|date_format:d/m/Y',
                    'fechafin' => 'required|date_format:d/m/Y',
                    'duracion' => 'required|integer|min:0|not_in:0',
                    'horariodetallado' => 'required',
                    'nroaulas' => 'required|integer|min:0|not_in:0',
                    'nrovacantes' => 'required|integer|min:0|not_in:0',
                    'nrodocentes' => 'required|integer|min:0|not_in:0',
                ]);
            }else {
                $rules6 = collect([
                    'perfilpersonal' => 'required|array|min:1',
                    'fechainicio' => 'required|date_format:d/m/Y',
                    'fechafin' => 'required|date_format:d/m/Y',
                    'duracion' => 'required|integer|min:0|not_in:0',
                    'horariodetallado' => 'required',
                    'nroaulas' => 'required|integer|min:0|not_in:0',
                    'nrovacantes' => 'required|integer|min:0|not_in:0',
                    'nrodocentes' => 'required|integer|min:0|not_in:0',
                ]);
            }
        }

        $rules = $rules5->union($rules6);

        return $rules->toArray();
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if (!isset($this['codresolucion']) && $this->isMethod('POST')) {
                $validator->errors()->add('codresol', 'Debe seleccionar y agregar al menos una resolución');
            }

            if ($this['fechafin'] < $this['fechainicio']) {
                $validator->errors()->add('fechafin', 'La Fecha de Fin, debe ser mayor');
            }
        });
    }

    public function messages ()
    {
        return [
            'titulo.required' => 'El Título es obligatorio',
            'titulo.max' => 'El Título debe tener como máximo 255 caracteres',
            'descripcion.required' => 'La Descripción es obligatoria',
            'descripcion.max' => 'La Descripción debe tener como máximo 1000 caracteres',
            'subgerencia.required' => 'La Sub Gerencia es obligatoria',
            'subgerencia.checkselect' => 'La Sub Gerencia es obligatoria',
            'tipo.required' => 'El Tipo es obligatorio',
            'tipo.checkselect' => 'El Tipo es obligatorio',
            'modalidad.required' => 'La Modalidad es obligatoria',
            'modalidad.checkselect' => 'La Modalidad es obligatoria',
            'distritosede.required' => 'La Sede es obligatoria',
            'distritosede.checkselect' => 'La Sede es obligatoria',
            'sistema.required' => 'El Sistema es obligatorio',
            'sistema.checkselect' => 'El Sistema es obligatorio',
            'tematica.required' => 'La Temática es obligatoria',
            'tematica.checkselect' => 'La Temática es obligatoria',
            'linea.required' => 'La Línea Estratégica es obligatoria',
            'linea.checkselect' => 'La Línea Estratégica es obligatoria',
            'bloque.required' => 'El Bloque Académico es obligatorio',
            'bloque.checkselect' => 'El Bloque Académico es obligatorio',
            'tipocertificado.required' => 'El Tipo de Certificado es obligatorio',
            'tipocertificado.checkselect' => 'El Tipo de Certificado es obligatorio',
            'entconvenio.required' => 'La Entidad del Convenio es obligatorio',
            'entconvenio.checkselect' => 'La Entidad del Convenio es obligatorio',
            'origen.required' => 'El Organizador es obligatorio',
            'origen.checkselect' => 'El Organizador es obligatorio',
            'distritofiscal.required' => 'El Distrito Fiscal es obligatorio',
            'distritofiscal.checkselect' => 'El Distrito Fiscal es obligatorio',
            'cooperante.required' => 'El Cooperante es obligatorio',
            'cooperante.checkselect' => 'El Cooperante es obligatorio',
            'perfilpersonal.required' => 'El Perfil de Personal Dirigido es obligatorio',
            'perfilpersonal.array' => 'El Perfil de Personal Dirigido es obligatorio',
            'perfilpersonal.min' => 'El Perfil de Personal Dirigido es obligatorio',
            'fechainicio.required' => 'La Fecha de Inicio es obligatorio',
            'fechainicio.date_format' => 'La Fecha de Inicio no es válida',
            'fechafin.required' => 'La Fecha de Fin es obligatorio',
            'fechafin.date_format' => 'La Fecha de Fin no es válida',
            'duracion.required' => 'La duración es obligatoria',
            'duracion.integer' => 'La duración debe ser un número entero',
            'duracion.min' => 'La duración debe ser mayor que cero',
            'duracion.not_in' => 'La duración debe ser mayor que cero',
            'duracionid.required' => 'La duración es obligatoria',
            'duracionid.checkselect' => 'La duración es obligatoria',
            'horariodetallado.required' => 'El Horario es obligatorio',
            'nroaulas.required' => 'El Nro. de Aulas es obligatorio',
            'nroaulas.integer' => 'El Nro. de Aulas debe ser un número entero',
            'nroaulas.min' => 'El Nro. de Aulas debe ser mayor que cero',
            'nroaulas.not_in' => 'El Nro. de Aulas debe ser mayor que cero',
            'nrovacantes.required' => 'El Nro. de Vacantes es obligatorio',
            'nrovacantes.integer' => 'El Nro. de Vacantes debe ser un número entero',
            'nrovacantes.min' => 'El Nro. de Vacantes debe ser mayor que cero',
            'nrovacantes.not_in' => 'El Nro. de Vacantes debe ser mayor que cero',
            'nrodocentes.required' => 'El Nro. de Docentes es obligatoria',
            'nrodocentes.integer' => 'El Nro. de Docentes debe ser un número entero',
            'nrodocentes.min' => 'El Nro. de Docentes debe ser mayor que cero',
            'nrodocentes.not_in' => 'El Nro. de Docentes debe ser mayor que cero',
        ];
    }

}
