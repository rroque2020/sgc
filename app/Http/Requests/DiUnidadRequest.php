<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DiUnidadRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'orden' => 'required|checkselect',
            'nombre' => 'required',
            'descripcion' => 'required',
            'bibliografia' => 'required',
        ];
    }

    public function messages ()
    {
        return [
            'orden.required' => 'El Nro. de Unidad Temática es obligatorio',
            'orden.checkselect' => 'El Nro. de Unidad Temática es obligatorio',
            'nombre.required' => 'El Título de la Unidad Temática es obligatoria',
            'descripcion.required' => 'El Contenido de la Unidad Temática es obligatoria',
            'bibliografia.required' => 'La Bibliografia es obligatoria',
        ];
    }
}
