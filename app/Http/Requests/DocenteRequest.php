<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DocenteRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        if($this->method() == 'POST'){
            $rules1 = collect([
                'dni'=> 'required|min:8|max:8|unique:persona,dni',
                'apellidos'=> 'required',
                'nombres'=> 'required',
                'celular'=> 'required|min:9|max:9',
                'genero' => 'required|checkselect',
                'correo' => 'required|email',
            ]);
        }

        if($this->method() == 'PUT' || $this->method() == 'PATCH'){
            $rules1 = collect([
                'apellidos'=> 'required',
                'nombres'=> 'required',
                'celular'=> 'required|min:9|max:9',
                'genero' => 'required|checkselect',
                'correo' => 'required|email',
            ]);
        }

        if(isset($this["tipo"])){
            $rules2 = collect([
                'distritoi' => 'required|checkselect',
                'dependenciai' => 'required|checkselect',
                'cargoi' => 'required|checkselect',
            ]);
        }else{
            $rules2 = collect([
                'cargoe' => 'required',
                'institucione' => 'required',
            ]);
        }

        $rules = $rules1->union($rules2);

        return $rules->toArray();
    }

    public function messages ()
    {
        return [
            'dni.required' => 'El DNI es obligatorio',
            'dni.min' => 'El DNI debe tener como mínimo 8 dígitos',
            'dni.max' => 'El DNI debe tener como máximo 8 dígitos',
            'dni.unique' => 'El DNI ya fué registrado, debe ser único',
            'apellidos.required' => 'Los Apellidos son obligatorios',
            'nombres.required' => 'Los Nombres son obligatorios',
            'celular.required' => 'El Celular es obligatorio',
            'celular.min' => 'El Celular debe tener como mínimo 9 dígitos',
            'celular.max' => 'El Celular debe tener como máximo 9 dígitos',
            'fecha.required' => 'La Fecha es obligatorio',
            'fecha.date_format' => 'La Fecha no es válida',
            'genero.required' => 'El Género es obligatorio',
            'genero.checkselect' => 'El Género es obligatorio',
            'correo.required' => 'El Correo es obligatorio',
            'correo.email' => 'El Correo no es válido',
            'distritoi.required' => 'El Distrito Fiscal es obligatorio',
            'distritoi.checkselect' => 'El Distrito Fiscal es obligatorio',
            'dependenciai.required' => 'La Dependencia es obligatoria',
            'dependenciai.checkselect' => 'La Dependencia es obligatoria',
            'cargoi.required' => 'El Cargo es obligatorio',
            'cargoi.checkselect' => 'El Cargo es obligatorio',
            'cargoe.required' => 'El Cargo es obligatorio',
            'cargoe.checkselect' => 'El Cargo es obligatorio',
            'institucione.required' => 'La Institución es obligatoria',
            'institucione.checkselect' => 'La Institución es obligatoria',
            'foto.required' => 'La Foto es Obligatoria',
            'foto.mime' => 'La Foto debe ser JPG',
            'foto.max' => 'La Foto debe ser máximo 2MB',
        ];
    }
}
