<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DiModuloRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'tipo' => 'required|checkselect',
            'nombre' => 'required',
            'fechainicio' => 'required|date_format:d/m/Y',
            'fechafin' => 'required|date_format:d/m/Y',
            'duracion' => 'required|integer|min:0|not_in:0',
        ];
    }

    public function messages ()
    {
        return [
            'tipo.required' => 'El Tipo de Capacitación es obligatorio',
            'tipo.checkselect' => 'El Tipo de Capacitación es obligatorio',
            'nombre.required' => 'El Nombre de Módulo es obligatorio',
            'fechainicio.required' => 'La Fecha de Inicio es obligatorio',
            'fechainicio.date_format' => 'La Fecha de Inicio no es válida',
            'fechafin.required' => 'La Fecha de Fin es obligatorio',
            'fechafin.date_format' => 'La Fecha de Fin no es válida',
            'duracion.required' => 'La Duración es obligatorio',
            'duracion.integer' => 'La Duración debe ser un número entero',
            'duracion.min' => 'La Duración debe ser mayor que cero',
            'duracion.not_in' => 'La Duración debe ser mayor que cero',
        ];
    }
}
