<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CursoEvaRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'codactividad'=> 'required',
            'categoria'=> 'required|checkselect',
        ];
    }

    public function messages ()
    {
        return [
            'codactividad.required' => 'La Actividad Académica es obligatoria',
            'categoria.required' => 'La Categoría es obligatoria',
            'categoria.checkselect' => 'La Categoría es obligatoria',
        ];
    }
}
