<?php

namespace App\Http\Requests\Perfil;

use Illuminate\Foundation\Http\FormRequest;

class DiscenteRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'celular'=> 'required|min:9|max:9',
            'fecha'=> 'required|date_format:d/m/Y',
            'genero' => 'required|checkselect',
            'correo' => 'required|email',
            'distrito' => 'required|checkselect',
            'dependencia' => 'required|checkselect',
            'cargo' => 'required|checkselect',
        ];
    }

    public function messages ()
    {
        return [
            'celular.required' => 'El Celular es obligatorio',
            'celular.min' => 'El Celular debe tener como mínimo 9 dígitos',
            'celular.max' => 'El Celular debe tener como máximo 9 dígitos',
            'fecha.required' => 'La Fecha de Nacimiento es obligatorio',
            'fecha.date_format' => 'La Fecha de Nacimiento no es válida',
            'genero.required' => 'El Género es obligatorio',
            'genero.checkselect' => 'El Género es obligatorio',
            'correo.required' => 'El Correo es obligatorio',
            'correo.email' => 'El Correo no es válido',
            'distrito.required' => 'El Distrito Fiscal es obligatorio',
            'distrito.checkselect' => 'El Distrito Fiscal es obligatorio',
            'dependencia.required' => 'La Dependencia es obligatoria',
            'dependencia.checkselect' => 'La Dependencia es obligatoria',
            'cargo.required' => 'El Cargo es obligatorio',
            'cargo.checkselect' => 'El Cargo es obligatorio',
        ];
    }
}
