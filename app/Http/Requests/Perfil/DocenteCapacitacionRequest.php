<?php

namespace App\Http\Requests\Perfil;

use Illuminate\Foundation\Http\FormRequest;

class DocenteCapacitacionRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'tipocap' => 'required|max:255',
            'institucioncap' => 'required|max:255',
            'especialidadcap' => 'required|max:255',
            'ingresocap' => 'required|date_format:d/m/Y',
            'egresocap' => 'required|date_format:d/m/Y',
            'horascap' => 'required|digits_between:1,24',
            'sustentocap'  => 'required|mimes:pdf|max:2048',
        ];
    }

    public function messages ()
    {
        return [
            'tipocap.required' => '* El Tipo de Estudio es obligatorio',
            'tipocap.max' => '* El Tipo de Estudio debe ser máximo de 255 caracteres',
            'institucioncap.required' => '* La Institución es obligatoria',
            'institucioncap.max' => '* La Institución debe ser máximo de 255 caracteres',
            'especialidadcap.required' => '* La Especialidad es obligatoria',
            'especialidadcap.max' => '* La Especialidad debe ser máximo de 255 caracteres',
            'ingresocap.required' => '* La Fecha de Ingreso es obligatoria',
            'ingresocap.date_format' => '* La Fecha de Ingreso no es válida',
            'egresocap.required' => '* La Fecha de Egreso es obligatoria',
            'egresocap.date_format' => '* La Fecha de Egreso no es válida',
            'horascap.required' => '* La Hora es obligatoria',
            'horascap.digits_between' => '* La Hora debe ser entre 1 y 24 es obligatoria',
            'sustentocap.required' => '* El Sustento es obligatorio',
            'sustentocap.mimes' => '* El Sustento debe ser PDF',
            'sustentocap.max' => '* El Sustento debe tener máximo 2MB',
        ];
    }
}
