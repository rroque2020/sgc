<?php

namespace App\Http\Requests\Perfil;

use Illuminate\Foundation\Http\FormRequest;

class DocenteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = collect([
            'celular'=> 'required|min:9|max:9',
            'fecha'=> 'required|date_format:d/m/Y',
            'genero' => 'required|checkselect',
            'correo' => 'required|email',
        ]);

        return $rules->toArray();
    }

    public function messages ()
    {
        return [
            'celular.required' => '* El Celular es obligatorio',
            'celular.min' => '* El Celular debe tener como mínimo 9 dígitos',
            'celular.max' => '* El Celular debe tener como máximo 9 dígitos',
            'fecha.required' => '* La Fecha de Nacimiento es obligatorio',
            'fecha.date_format' => '* La Fecha de Nacimiento no es válida',
            'genero.required' => '* El Género es obligatorio',
            'genero.checkselect' => '* El Género es obligatorio',
            'correo.required' => '* El Correo es obligatorio',
            'correo.email' => '* El Correo no es válido',
        ];
    }
}
