<?php

namespace App\Http\Requests\Perfil;

use Illuminate\Foundation\Http\FormRequest;

class DocenteFormacionRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'grado' => 'required|checkselect',
            'institucion' => 'required|checkselect',
            'profesion' => 'required|checkselect',
            'ingreso' => 'required',
            'egreso' => 'required',
            'sustentoform'  => 'required|mimes:pdf|max:2048',
        ];
    }

    public function messages ()
    {
        return [
            'grado.required' => '* El Grado Académico es obligatorio',
            'grado.checkselect' => '* El Grado Académico es obligatorio',
            'institucion.required' => '* La Institución es obligatoria',
            'institucion.checkselect' => '* La Institución es obligatoria',
            'profesion.required' => '* La Profesión es obligatoria',
            'profesion.checkselect' => '* La Profesión es obligatoria',
            'ingreso.required' => '* El Año de Ingreso es obligatorio',
            'egreso.required' => '* El Año de Egreso es obligatorio',
            'sustentoform.required' => '* El Sustento es obligatorio',
            'sustentoform.mimes' => '* El Sustento debe ser PDF',
            'sustentoform.max' => '* El Sustento debe tener máximo 2MB',
        ];
    }
}
