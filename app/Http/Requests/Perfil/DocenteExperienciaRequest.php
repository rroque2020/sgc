<?php

namespace App\Http\Requests\Perfil;

use Illuminate\Foundation\Http\FormRequest;

class DocenteExperienciaRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'entidadexp' => 'required|max:255',
            'cargoexp' => 'required|max:255',
            'areaexp' => 'required|max:255',
            'funcionesexp' => 'required',
            'ingresoexp' => 'required|date_format:d/m/Y',
            'egresoexp' => 'required|date_format:d/m/Y',
            'sustentoexp'  => 'required|mimes:pdf|max:2048',
        ];
    }

    public function messages ()
    {
        return [
            'entidadexp.required' => '* La Entidad es obligatoria',
            'entidadexp.max' => '* La Entidad debe ser máximo de 255 caracteres',
            'cargoexp.required' => '* El Cargo es obligatorio',
            'cargoexp.max' => '* El Cargo debe ser máximo de 255 caracteres',
            'areaexp.required' => '* El Área es obligatoria',
            'areaexp.max' => '* El Área debe ser máximo de 255 caracteres',
            'funcionesexp.required' => '* Las Funciones es obligatoria',
            'ingresoexp.required' => '* La Fecha de Ingreso es obligatoria',
            'ingresoexp.date_format' => '* La Fecha de Ingreso no es válida',
            'egresoexp.required' => '* La Fecha de Egreso es obligatoria',
            'egresoexp.date_format' => '* La Fecha de Egreso no es válida',
            'sustentoexp.required' => '* El Sustento es obligatorio',
            'sustentoexp.mimes' => '* El Sustento debe ser PDF',
            'sustentoexp.max' => '* El Sustento debe tener máximo 2MB',
        ];
    }
}
