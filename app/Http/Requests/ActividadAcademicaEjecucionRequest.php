<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ActividadAcademicaEjecucionRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'fechainicioe' => 'required|date_format:d/m/Y',
            'fechafine' => 'required|date_format:d/m/Y',
            'nroaulase' => 'required|integer|min:0|not_in:0',
            'nrovacantese' => 'required|integer|min:0|not_in:0',
            'nrodocentese' => 'required|integer|min:0|not_in:0',
            'duracione' => 'required|integer|min:0|not_in:0',
            'tipomodulo' => 'required|checkselect',
        ];
    }

    public function messages ()
    {
        return [
            'fechainicioe.required' => 'La Fecha de Inicio es obligatorio',
            'fechainicioe.date_format' => 'La Fecha de Inicio no es válida',
            'fechafine.required' => 'La Fecha de Fin es obligatorio',
            'fechafine.date_format' => 'La Fecha de Fin no es válida',
            'nroaulase.required' => 'El Nro. de Aulas es obligatorio',
            'nroaulase.integer' => 'El Nro. de Aulas debe ser un número entero',
            'nroaulase.min' => 'El Nro. de Aulas debe ser mayor que cero',
            'nroaulase.not_in' => 'El Nro. de Aulas debe ser mayor que cero',
            'nrovacantese.required' => 'El Nro. de Vacantes es obligatorio',
            'nrovacantese.integer' => 'El Nro. de Vacantes debe ser un número entero',
            'nrovacantese.min' => 'El Nro. de Vacantes debe ser mayor que cero',
            'nrovacantese.not_in' => 'El Nro. de Vacantes debe ser mayor que cero',
            'nrodocentese.required' => 'El Nro. de Docentes es obligatoria',
            'nrodocentese.integer' => 'El Nro. de Docentes debe ser un número entero',
            'nrodocentese.min' => 'El Nro. de Docentes debe ser mayor que cero',
            'nrodocentese.not_in' => 'El Nro. de Docentes debe ser mayor que cero',
            'duracione.required' => 'La Duración es obligatoria',
            'duracione.integer' => 'La Duración debe ser un número entero',
            'duracione.min' => 'La Duración debe ser mayor que cero',
            'duracione.not_in' => 'La Duración debe ser mayor que cero',
            'tipomodulo.required' => 'El Tipo de Estructura es obligatorio',
            'tipomodulo.checkselect' => 'El Tipo de Estructura es obligatorio',
        ];
    }
}
