<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DiSesionRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'orden' => 'required|checkselect',
            'descripcion' => 'required',
            'duracion' => 'required|integer|min:0|not_in:0',
            'idestrategia' => 'required|checkselect',
            'desestrategia' => 'required',
            'recursosobligatorios' => 'required',
            'fechahorainicio' => 'required',
            'fechahoracierre' => 'required',
        ];
    }

    public function messages ()
    {
        return [
            'orden.required' => 'El Nro. de Sesión es obligatorio',
            'orden.checkselect' => 'El Nro. de Sesión es obligatorio',
            'descripcion.required' => 'El Nombre de la Sesión es obligatoria',
            'duracion.required' => 'La Duración es obligatoria',
            'duracion.integer' => 'La Duración debe ser un número entero',
            'duracion.min' => 'La Duración debe ser mayor que cero',
            'duracion.not_in' => 'La Duración debe ser mayor que cero',
            'idestrategia.required' => 'El Tipo de Actividad Pedagógica es obligatorio',
            'idestrategia.checkselect' => 'El Tipo de Actividad Pedagógica es obligatorio',
            'desestrategia.required' => 'La Actividad Pedagógica es obligatoria',
            'recursosobligatorios.required' => 'Liste los Recursos Obligatorios',
            'fechahorainicio.required' => 'La Fecha y Hora de Inicio es obligatoria',
            'fechahoracierre.required' => 'La Fecha y Hora de Cierre es obligatoria',
        ];
    }
}
