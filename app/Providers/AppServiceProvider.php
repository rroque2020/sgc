<?php

namespace App\Providers;

use App\Rules\CheckSelect;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    protected $rules = [
        CheckSelect::class,
    ];

    public function register()
    {
        //
    }

    private function registerValidationRules()
    {
        foreach($this->rules as $class ) {
            $alias = (new $class)->__toString();
            if ($alias) {
                Validator::extend($alias, $class .'@passes');
            }
        }
    }

    public function boot()
    {
		Schema::defaultStringLength(191);
        Model::preventLazyLoading(!app()->isProduction());
        $this->registerValidationRules();
    }
}
