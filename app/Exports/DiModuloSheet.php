<?php

namespace App\Exports;

use App\Models\DiModulo;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class DiModuloSheet implements FromView, WithTitle, ShouldAutoSize, WithStrictNullComparison,
                                WithStyles, WithColumnWidths
{
    private $idmodulo;
    private $desmodulo;
    private $actividad;
    private $count;
    private $counteval;

    public function __construct($idmodulo, $desmodulo, $actividad)
    {
        $this->idmodulo = $idmodulo;
        $this->desmodulo = $desmodulo;
        $this->actividad = $actividad;
    }

    public function view(): View
    {
        $competencias = DiModulo::listarEstructuraCompetencias($this->idmodulo);
        $evaluaciones = DiModulo::listarEstructuraActividadesEvaluativas($this->idmodulo);
        $this->count = count($competencias);
        $this->counteval = count($evaluaciones);

        return view('disenioinstruccional.sheet', [
            'competencias' => $competencias,
            'evaluaciones' => $evaluaciones,
            'actividad' => $this->actividad
        ]);
    }

    public function title(): string
    {
        return $this->desmodulo;
    }

    public function styles(Worksheet $sheet)
    {
        return [
            'B3' => ['alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    'wrapText' => true
                ]
            ],
            'C1' => ['font' => ['bold' => true, 'size' => 12],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                        'wrapText' => true
                    ]
            ],
            'A5:I'.(5 + $this->count) => [
                'borders' => [
                    'allBorders' => [
                        'borderStyle' =>  \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                    ]
                ],
                'alignment' => [
                    'wrapText' => true
                ]
            ],
            'A'.(7 + $this->count).':I'.(7 + $this->count) => [
                'borders' => [
                    'allBorders' => [
                        'borderStyle' =>  \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_NONE
                    ]
                ],
                'alignment' => [
                    'wrapText' => true
                ]
            ],
            'A'.(7 + $this->count).':E'.(8 + $this->count +  $this->counteval) => [
                'borders' => [
                    'allBorders' => [
                        'borderStyle' =>  \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                    ]
                ],
                'alignment' => [
                    'wrapText' => true
                ]
            ],
        ];
    }

    public function columnWidths(): array
    {
        return [
            'A' => 40,
            'B' => 40,
            'C' => 40,
            'D' => 40,
            'E' => 40,
            'F' => 40,
            'G' => 40,
            'H' => 40,
            'I' => 40,
        ];
    }
}
