<?php

namespace App\Exports;

use App\Models\ActividadAcademica;
use App\Models\DiModulo;
use Illuminate\Contracts\Support\Responsable;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithProperties;

class DisenioInstruccionalExport implements FromCollection, Responsable, WithMultipleSheets, WithProperties
{
    use Exportable;

    protected $id;

    function __construct($id) {
        $this->id = $id;
    }

    public function collection() {
        return DiModulo::export($this->id);
    }

    public function sheets(): array
    {
        $sheets = [];
        $modulos = ActividadAcademica::listarModulos($this->id);
        $actividad = ActividadAcademica::findOrFail($this->id);
        foreach ($modulos as $modulo) {
            $sheets[] = new DiModuloSheet($modulo->id, $modulo->descripcion, $actividad);
        }
        return $sheets;
    }

    public function properties(): array
    {
        return [
            'creator'        => 'EMP',
            'lastModifiedBy' => 'EMP',
            'title'          => 'DISEÑO INSTRUCCIONAL',
            'description'    => 'DISEÑO INSTRUCCIONAL',
            'subject'        => 'DISEÑO INSTRUCCIONAL',
            'keywords'       => 'DISEÑO INSTRUCCIONAL',
            'category'       => 'FORMATOS',
            'manager'        => 'EMP',
            'company'        => 'EMP',
        ];
    }
}
