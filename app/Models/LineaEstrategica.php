<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class LineaEstrategica extends Model
{
    use HasFactory;
    protected $table = "linea_estrategica";

    public function bloqueacademico(){
        return $this->hasMany('App\Models\BloqueAcademico','linea_estrategica_id','id');
    }

    public static function listarByEstado($estado){
        $datos = DB::table('linea_estrategica')
            ->select('id','descripcion','nombre_corto','abreviatura')
            ->where('estado',$estado)
            ->orderby('descripcion','asc')
            ->get();

        return $datos;
    }

    public static function listarDatos($id){
        $datos = DB::table('linea_estrategica')
            ->select('id','descripcion','nombre_corto','abreviatura')
            ->where('id',$id)
            ->first();

        return $datos;
    }
}
