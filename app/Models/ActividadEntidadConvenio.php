<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ActividadEntidadConvenio extends Model
{
    use HasFactory;
    protected $table = "actividad_entidad_convenio";

}
