<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DocenteFormacion extends Model
{
    use HasFactory;
    protected $table = "docente_formacion";

    public static function listarByDocente($docente){
        $data = DB::table('docente_formacion')
            ->select('docente_formacion.id',
                        DB::Raw('grado_academico.descripcion as gradodes, institucion.descripcion as instituciondes, profesion.descripcion as profesiondes'),
                        'ingreso', 'egreso','sustento')
            ->join('grado_academico','docente_formacion.grado_academico_id','=','grado_academico.id')
            ->join('institucion','docente_formacion.institucion_id','=','institucion.id')
            ->join('profesion','docente_formacion.profesion_id','=','profesion.id')
            ->where('docente_formacion.docente_id', $docente)
            ->orderBy('docente_formacion.egreso','desc')
            ->get();

        return $data;
    }
}
