<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class DistritoFiscal extends Model
{
    use HasFactory;

    protected $table = "distrito_fiscal";
    protected $fillable = ['id','descripcion','abreviatura','latitud','longitud','estado','cod_usuc','cod_usum'];

    public static function listarByEstado($estado){
        $datos = DB::table('distrito_fiscal')
            ->select('id','descripcion','abreviatura')
            ->where('estado',$estado)
            ->orderby('descripcion','asc')
            ->get();

        return $datos;
    }
}
