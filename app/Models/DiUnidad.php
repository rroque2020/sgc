<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DiUnidad extends Model
{
    use HasFactory;
    protected $table = "di_unidad";

    public static function listarDatosWord($sesion){
        $data = DB::table('di_unidad')
            ->selectRaw("CONCAT('Unidad ',di_unidad.orden) AS ut_orden,
                                di_unidad.nombre AS ut_nombre,
                                di_unidad.descripcion AS ut_descripcion")
           ->where('di_unidad.di_sesion_id',$sesion)
            ->orderBy('di_unidad.orden','asc')
            ->get();

        return $data;
    }

    public static function listarBibliografiasWord($actividad){
        $data = DB::table('di_unidad')
            ->selectRaw(" DISTINCT di_unidad.bibliografia AS und_bibliografia")
            ->join('di_sesion','di_unidad.di_sesion_id','=','di_sesion.id')
            ->join('di_capacidad','di_sesion.di_capacidad_id','=','di_capacidad.id')
            ->join('di_competencia','di_capacidad.di_competencia_id','=','di_competencia.id')
            ->join('di_modulo','di_competencia.di_modulo_id','=','di_modulo.id')
            ->where('di_modulo.actividad_academica_id',$actividad)
            ->orderBy('di_unidad.bibliografia','ASC')
            ->get();

        return $data;
    }
}
