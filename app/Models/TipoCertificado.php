<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TipoCertificado extends Model
{
    use HasFactory;

    protected $table = "tipo_certificado";
    protected $fillable = ['id','descripcion','abreviatura','estado','cod_usuc','cod_usum'];

    public static function listarByEstado($estado){
        $datos = DB::table('tipo_certificado')
            ->select('id','descripcion','abreviatura')
            ->where('estado',$estado)
            ->orderby('descripcion','asc')
            ->get();

        return $datos;
    }
}
