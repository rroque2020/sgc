<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DocenteCapacitacion extends Model
{
    use HasFactory;
    protected $table = "docente_capacitacion";

    public static function listarByDocente($docente){
        $data = DB::table('docente_capacitacion')
            ->where('docente_id', $docente)
            ->orderBy('docente_capacitacion.egreso','desc')
            ->get();

        return $data;
    }
}
