<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActividadRequisito extends Model
{
    use HasFactory;
    protected $table = "actividad_requisito";
}
