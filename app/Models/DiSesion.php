<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DiSesion extends Model
{
    use HasFactory;
    protected $table = "di_sesion";

    public static function listarUnidades($sesion){
        $data = DB::table('di_unidad')
            ->selectRaw('di_unidad.id, di_unidad.di_sesion_id, di_unidad.orden, di_unidad.nombre,
                                di_unidad.descripcion, di_unidad.bibliografia')
            ->where('di_unidad.di_sesion_id',$sesion)
            ->orderBy('di_unidad.orden','asc')
            ->get();

        return $data;
    }

    public static function sumaDuracion($modulo)
    {
        $data = DB::table('di_modulo')
            ->select(DB::Raw('di_modulo.duracion, CASE WHEN SUM(di_sesion.duracion) IS NULL THEN 0 ELSE SUM(di_sesion.duracion) END AS suma'))
            ->leftJoin('di_competencia','di_modulo.id','=','di_competencia.di_modulo_id')
            ->leftJoin('di_capacidad','di_competencia.id','=','di_capacidad.di_competencia_id')
            ->leftJoin('di_sesion','di_capacidad.id','=','di_sesion.di_capacidad_id')
            ->where('di_modulo.id', $modulo)
            ->groupBy('di_modulo.duracion')
            ->first();

        return $data;
    }

    public static function listarDatosWord($modulo){
        $data = DB::table('di_sesion')
            ->selectRaw("di_sesion.id as ses_id, CONCAT('Sesión ',di_sesion.orden) AS ses_orden, di_sesion.descripcion AS ses_descripcion,
                                di_sesion.duracion AS ses_duracion,tipo_actividad_estrategica.descripcion AS ses_tipo,
                                di_sesion.des_actividad_estrategica_id AS ses_tipodescripcion,
	                            di_sesion.fecha_hora_inicio AS ses_fhi, di_sesion.fecha_hora_cierre AS ses_fhc")
            ->join('tipo_actividad_estrategica','di_sesion.tipo_actividad_estrategica_id','=','tipo_actividad_estrategica.id')
            ->join('di_capacidad','di_sesion.di_capacidad_id','=','di_capacidad.id')
            ->join('di_competencia','di_capacidad.di_competencia_id','=','di_competencia.id')
            ->where('di_competencia.di_modulo_id',$modulo)
            ->orderBy('di_sesion.orden','asc')
            ->get();

        return $data;
    }

    public static function listarRecursosObligatoriosWord($actividad){
        $data = DB::table('di_modulo')
                ->selectRaw("DISTINCT di_sesion.recursos_obligatorios AS ses_recobl")
                ->leftJoin('di_competencia','di_modulo.id','=','di_competencia.di_modulo_id')
                ->leftJoin('di_capacidad','di_competencia.id','=','di_capacidad.di_competencia_id')
                ->leftJoin('di_sesion','di_capacidad.id','=','di_sesion.di_capacidad_id')
                ->where('di_modulo.actividad_academica_id',$actividad)
                ->whereNotNull('di_sesion.recursos_obligatorios')
                ->orderBy('di_sesion.recursos_obligatorios','asc')
                ->get();

        return $data;
    }

    public static function listarRecursosComplementariosWord($actividad){
        $data = DB::table('di_modulo')
            ->selectRaw("DISTINCT di_sesion.recursos_complementarios AS ses_recomp")
            ->leftJoin('di_competencia','di_modulo.id','=','di_competencia.di_modulo_id')
            ->leftJoin('di_capacidad','di_competencia.id','=','di_capacidad.di_competencia_id')
            ->leftJoin('di_sesion','di_capacidad.id','=','di_sesion.di_capacidad_id')
            ->where('di_modulo.actividad_academica_id',$actividad)
            ->whereNotNull('di_sesion.recursos_complementarios')
            ->orderBy('di_sesion.recursos_complementarios','asc')
            ->get();

        return $data;
    }
}
