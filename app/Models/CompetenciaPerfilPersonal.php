<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CompetenciaPerfilPersonal extends Model
{
    use HasFactory;
    protected $table = "competencia_perfil_personal";

    public static function listarTipos(){
        $data = DB::table('competencia_perfil_personal')
            ->select('competencia_perfil_personal.tipo')
            ->distinct()
            ->orderBy('tipo','asc')
            ->get();

        return $data;
    }

    public static function listar(){
        $data = DB::table('competencia_perfil_personal')
            ->orderBy('codigo','asc')
            ->orderBy('tipo','asc')
            ->orderBy('descripcion','asc')
            ->get();

        return $data;
    }
}
