<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Resolucion extends Model
{
    use HasFactory;
    protected $table="resolucion";

    protected $fillable = ['id','descripcion','archivo','cod_usuc','cod_usum'];

    public static function listar(){
        $datos = DB::table('resolucion')
            ->select('id','descripcion','archivo')
            ->orderby('descripcion','asc')
            ->get();

        return $datos;
    }

    public static function listarByCodigo($codigo){
        $datos = DB::table('resolucion')
            ->select('id','descripcion','archivo')
            ->where('id',$codigo)
            ->first();

        return $datos;
    }

    public function actividades(){
        return $this
            ->belongsToMany(ActividadAcademica::class,'actividad_resolucion','resolucion_id','actividad_academica_id')
            ->withPivot('cod_usu')
            ->withTimestamps();
    }
}
