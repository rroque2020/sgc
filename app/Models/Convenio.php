<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Convenio extends Model
{
    use HasFactory;
    protected $table = "convenio";

    public static function listarByEstado($estado){
        $datos = DB::table('convenio')
            ->select('id','descripcion')
            ->where('estado',$estado)
            ->orderby('descripcion','asc')
            ->get();

        return $datos;
    }
}
