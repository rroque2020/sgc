<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PreInscrito extends Model
{
    use HasFactory;
    protected $table="pre_inscrito";

    public static function listarDatosByDNI($dni){
        $data = DB::table('pre_inscrito')
            ->where('pre_inscrito.dni', $dni)
            ->first();

        return $data;
    }

    public static function listarForCoordinador($actividad){
        $data = DB::table('pre_inscrito')
            ->selectRaw("id, CASE tipo WHEN 1 THEN 'INTERNO' ELSE 'EXTERNO' END AS tipo,
	                               dni, CONCAT(apellidos, ' ', nombres) AS preinscrito,
	                               celular, email, estado_correo")
            ->where('actividad_academica_id', $actividad)
            ->get();

        return $data;
    }

    public static function listarDatosByActividadAndDNI($actividad, $dni){
        $data = DB::table('pre_inscrito')
            ->where('actividad_academica_id', $actividad)
            ->where('dni', $dni)
            ->where('estado_correo', 0)
            ->first();

        return $data;
    }

}
