<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CriterioDiscenteCargo extends Model
{
    use HasFactory;
    protected $table = "criterio_discente_cargo";

    public static function listarCargosSeleccionados($criterio){
        $data = DB::table('tcargos')
            ->select('tcargos.codi_carg_tca', 'tcargos.desc_carg_tca')
            ->join('criterio_discente_cargo','tcargos.codi_carg_tca','=','criterio_discente_cargo.cargo_id')
            ->join('criterio_discente','criterio_discente_cargo.criterio_discente_id','=','criterio_discente.id')
            ->where('criterio_discente.actividad_academica_id',$criterio)
            ->orderBy('tcargos.codi_carg_tca','asc')
            ->get();

        return $data;
    }

}
