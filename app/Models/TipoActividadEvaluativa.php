<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TipoActividadEvaluativa extends Model
{
    use HasFactory;
    protected $table = 'tipo_actividad_evaluativa';

    public static function listarTipos(){
        $data = DB::table('tipo_actividad_evaluativa')
            ->select('tipo_actividad_evaluativa.tipo')
            ->distinct()
            ->orderBy('tipo','asc')
            ->get();

        return $data;
    }

    public static function listarByEstado($estado){
        $datos = DB::table('tipo_actividad_evaluativa')
            ->select('id','tipo','descripcion','abreviatura')
            ->where('estado',$estado)
            ->orderby('id','asc')
            ->get();

        return $datos;
    }
}
