<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class BloqueAcademico extends Model
{
    use HasFactory;
    protected $table = "bloque_academico";

    protected $fillable = ['id','linea_estrategica_id','descripcion','abreviatura','estado','cod_usuc','cod_usum'];

    public function lineaestrategica(){
        return $this->belongsTo('App\Models\LineaEstrategica','id','id');
    }

    public static function listarByEstado($estado){
        $datos = DB::table('bloque_academico')
            ->select('id','descripcion','abreviatura')
            ->where('estado',$estado)
            ->orderby('descripcion','asc')
            ->get();

        return $datos;
    }

    public static function listarByLinea($linea){
        $datos = DB::table('bloque_academico')
            ->select('id','descripcion','abreviatura')
            ->where('estado',1)
            ->where('linea_estrategica_id',$linea)
            ->orderby('descripcion','asc')
            ->get();

        return $datos;
    }

    public static function listarDatos($id){
        $datos = DB::table('bloque_academico')
            ->where('id',$id)
            ->first();

        return $datos;
    }

    /*public function listarGroupLinea($estado){
        $datos = $this::query()
                    ->select('id','descripcion')
                    ->where('estado',1)
                    ->orderBy('descripcion','asc')
                    ->with('lineaestrategica:id,descripcion')
                    ->get()
                    ->groupBy(function($bloque){
                        return $bloque->lineaestrategica->descripcion;
                    });

        $datos = DB::table('bloque_academico')
            ->select(DB::Raw('linea_estrategica.descripcion as linea, bloque_academico.id, bloque_academico.descripcion, bloque_academico.abreviatura'))
            ->join('linea_estrategica','bloque_academico.linea_estrategica_id','=','linea_estrategica.id')
            ->where('bloque_academico.estado',$estado)
            ->where('linea_estrategica.estado',$estado)
            ->orderby('linea_estrategica.descripcion','asc')
            ->orderby('bloque_academico.descripcion','asc')
            ->get();

        return $datos;
    }*/
}
