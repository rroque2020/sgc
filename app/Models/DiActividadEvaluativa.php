<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DiActividadEvaluativa extends Model
{
    use HasFactory;
    protected $table = "di_actividad_evaluativa";

    public static function sumaDuracion($modulo)
    {
        $data = DB::table('di_actividad_evaluativa')
            ->select(DB::Raw('CASE WHEN SUM(di_actividad_evaluativa.porcentaje) IS NULL THEN 0 ELSE SUM(di_actividad_evaluativa.porcentaje) END AS suma'))
            ->where('di_actividad_evaluativa.di_modulo_id', $modulo)
            ->first();

        return $data;
    }

    public static function listarDatosWord($actividad){
        $data = DB::table('di_actividad_evaluativa')
            ->selectRaw("di_modulo.descripcion AS mod_descripcion,
                                CONCAT('Actividad Evaluativa ',di_actividad_evaluativa.orden) AS dae_orden,
                                tipo_actividad_evaluativa.descripcion AS dae_tipo,
                                di_actividad_evaluativa.descripcion AS dae_descripcion,
                                di_actividad_evaluativa.porcentaje AS dae_porcentaje,
                                instrumento_evaluativo.descripcion AS dae_instrumento,
                                di_actividad_evaluativa.fecha_hora_inicio AS dae_fhi,
                                di_actividad_evaluativa.fecha_hora_cierre AS dae_fhc")
            ->join('tipo_actividad_evaluativa','di_actividad_evaluativa.tipo_actividad_evaluativa_id','=','tipo_actividad_evaluativa.id')
            ->join('instrumento_evaluativo','di_actividad_evaluativa.instrumento_evaluativo_id','=','instrumento_evaluativo.id')
            ->join('di_modulo','di_actividad_evaluativa.di_modulo_id','=','di_modulo.id')
            ->where('di_modulo.actividad_academica_id',$actividad)
            ->orderBy('di_modulo.item','ASC')
            ->orderBy('di_actividad_evaluativa.orden','ASC')
            ->get();

        return $data;
    }
}
