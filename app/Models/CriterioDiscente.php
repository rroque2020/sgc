<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CriterioDiscente extends Model
{
    use HasFactory;
    protected $table = "criterio_discente";

    public static function listarDatos($actividad){
        $data = DB::table('criterio_discente')
            ->where('criterio_discente.actividad_academica_id', $actividad)
            ->first();

        return $data;
    }

    public static function verificarAprobadoPreRequisito($actividad, $dni){
        $data = DB::table('actividad_discente')
            ->selectRaw('COUNT(actividad_discente.id) AS cont')
            ->join('discente','actividad_discente.discente_id','=','discente.id')
            ->join('persona','discente.persona_id','=','persona.id')
            ->join('actividad_requisito','actividad_discente.actividad_academica_id','=','actividad_requisito.id')
            ->where('actividad_requisito.actividad_academica_id', $actividad)
            ->where('persona.dni', $dni)
            ->whereIn('actividad_discente.estado', [3,6])
            ->first();

        return $data;
    }

    public static function verificaCruceHorarios($dni){
        $data = DB::table('actividad_discente')
            ->selectRaw('COUNT(actividad_discente.id) AS cont')
            ->join('discente','actividad_discente.discente_id','=','discente.id')
            ->join('persona','discente.persona_id','=','persona.id')
            ->join('actividad_academica','actividad_discente.actividad_academica_id','=','actividad_academica.id')
            ->where('persona.dni', $dni)
            ->where('actividad_academica.estado', 6)
            ->first();

        return $data;
    }

    public static function verificarParticipacionActividad($especialidad ,$dni){
        $data = DB::table('actividad_discente')
            ->selectRaw('COUNT(actividad_discente.id) AS cont')
            ->join('discente','actividad_discente.discente_id','=','discente.id')
            ->join('persona','discente.persona_id','=','persona.id')
            ->join('actividad_academica','actividad_discente.actividad_academica_id','=','actividad_academica.id')
            ->join('tematica','actividad_academica.tematica_id','=','tematica.id')
            ->where('persona.dni', $dni)
            ->whereRaw("tematica.descripcion LIKE '%".$especialidad."%'")
            ->whereIn('actividad_discente.estado', [3,6])
            ->first();

        return $data;
    }

    public static function verificarEspecialidad($especialidad ,$dni){
        $data = DB::table('tdependencias')
            ->join('tmaestro_personal','tdependencias.CODI_DEPE_TDE','=','tmaestro_personal.CODI_DEPE_TDE')
            ->whereRaw("tdependencias.desc_depe_tde LIKE '%".$especialidad."%'")
            ->where("tmaestro_personal.CODI_EMPL_PER",$dni)
            ->get();

        return $data;
    }

    public static function listarPertenenciaPerfil($perfil, $dni){
        if($perfil == 1){
            $data = DB::table('tmaestro_personal')
                ->selectRaw('tmaestro_personal.CODI_EMPL_PER, tmaestro_personal.APE_PAT_PER, tmaestro_personal.APE_MAT_PER, tmaestro_personal.NOM_EMP_PER')
                ->where("tmaestro_personal.CODI_EMPL_PER", $dni)
                ->whereIn("tmaestro_personal.CODI_CARG_TCA",['00000001','00000002','00000003','00000006','00000007','00000008','00000009'])
                ->get();
        }

        if($perfil == 2){
            $data = DB::table('tmaestro_personal')
                ->selectRaw('tmaestro_personal.CODI_EMPL_PER, tmaestro_personal.APE_PAT_PER, tmaestro_personal.APE_MAT_PER, tmaestro_personal.NOM_EMP_PER')
                ->where("tmaestro_personal.CODI_EMPL_PER", $dni)
                ->whereIn("tmaestro_personal.CODI_CARG_TCA",['00002121','00000199','00000372','00000391','00002119','00002145','00002118','00000374','00000378','00002174','00002106','00000116','00000367','00000368','00000369','00000389','00000151','00002120','00002123','00000390','00000200','00000190','00000198','00002114','00002124','00002166','00002233','00002234','00002235','00002236','00002237'])
                ->get();
        }

        if($perfil == 3){
            $data = DB::table('tmaestro_personal')
                ->selectRaw('tmaestro_personal.CODI_EMPL_PER, tmaestro_personal.APE_PAT_PER, tmaestro_personal.APE_MAT_PER, tmaestro_personal.NOM_EMP_PER')
                ->where("tmaestro_personal.CODI_EMPL_PER", $dni)
                ->whereNotIn("tmaestro_personal.CODI_CARG_TCA",['00000001','00000002','00000003','00000006','00000007','00000008','00000009','00002121','00000199','00000372','00000391','00002119','00002145','00002118','00000374','00000378','00002174','00002106','00000116','00000367','00000368','00000369','00000389','00000151','00002120','00002123','00000390','00000200','00000190','00000198','00002114','00002124','00002166','00002233','00002234','00002235','00002236','00002237'])
                ->get();
        }

        return $data;
    }

    public static function listarPertenenciaDistritoFiscal($distritos, $dni){
        $data = DB::table('tmaestro_personal')
            ->join('tdependencias','tmaestro_personal.CODI_DEPE_TDE','=','tdependencias.CODI_DEPE_TDE')
            ->where("tmaestro_personal.CODI_EMPL_PER",$dni)
            ->where("tdependencias.CODI_DIJU_DJU",$distritos)
            ->get();

        return $data;
    }

    public static function listarPertenenciaCargo($cargos, $dni){
        $data = DB::table('tmaestro_personal')
            ->where("tmaestro_personal.CODI_EMPL_PER",$dni)
            ->whereIn("tmaestro_personal.CODI_CARG_TCA",$cargos)
            ->get();

        return $data;
    }

    public function criterios_perfiles()
    {
        return $this->hasMany(CriterioDiscentePerfil::class,'id','criterio_discente_id');
    }
}
