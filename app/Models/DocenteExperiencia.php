<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DocenteExperiencia extends Model
{
    use HasFactory;

    protected $table = "docente_experiencia";

    public static function listarByDocente($docente){
        $data = DB::table('docente_experiencia')
            ->where('docente_id', $docente)
            ->orderBy('egreso','desc')
            ->get();

        return $data;
    }
}
