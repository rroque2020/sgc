<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Persona extends Model
{
    use HasFactory;
    protected $table = "persona";

    public static function listarDatos($usuario){
        $data = DB::table('persona')
            ->select('persona.id', 'persona.tipo', DB::Raw("CASE persona.tipo WHEN 0 then 'EXTERNO' ELSE 'INTERNO' END as tipodet"),
                'persona.dni', 'persona.apellidos', 'persona.nombres', 'persona.celular', 'persona.fecha', 'persona.email',
                'persona.genero','persona.idcargo', 'persona.cargo', 'persona.iddependencia', 'persona.institucion', 'persona.foto')
            ->where('persona.id', $usuario)
            ->first();

        return $data;
    }

    public static function listarDatosByDNI($dni){
        $data = DB::table('persona')
            ->select('persona.id', 'persona.tipo', DB::Raw("CASE persona.tipo WHEN 0 then 'EXTERNO' ELSE 'INTERNO' END as tipodet"),
                'persona.dni', 'persona.apellidos', 'persona.nombres', 'persona.celular', 'persona.fecha', 'persona.email',
                'persona.genero','persona.idcargo', 'persona.cargo', 'persona.iddependencia', 'persona.institucion', 'persona.foto')
            ->where('persona.dni', $dni)
            ->first();

        return $data;
    }
}
