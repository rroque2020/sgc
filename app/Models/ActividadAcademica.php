<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ActividadAcademica extends Model
{
    use HasFactory;
    protected $table = "actividad_academica";
    public $incrementing = false;

    public static function siguiente()
    {
        return static::max('id') + 1;
    }

    public static function maximo($filtro)
    {
        $datos = DB::table('actividad_academica')
                    ->select(DB::Raw('IF(MAX(SUBSTR(codigo,8)) IS NULL,0,MAX(SUBSTR(codigo,8))) AS maximo'))
                    ->whereRaw("codigo LIKE '".$filtro."%' ")
                    ->first();

        return $datos;
    }

    public function buscar($criterio){
        $datos = $this
            ->select('actividad_academica.id','actividad_academica.codigo','actividad_academica.titulo','actividad_academica.programado',
                DB::Raw("CASE actividad_academica.origen WHEN 'EMP' THEN 'EMP' WHEN 'DISTRITO FISCAL' THEN
                            (SELECT distrito_fiscal.descripcion FROM distrito_fiscal WHERE distrito_fiscal.id = organizador_id LIMIT 1) ELSE
                            (SELECT entidad_externa.descripcion FROM entidad_externa WHERE entidad_externa.id = organizador_id  LIMIT 1) END AS origen"),
                'actividad_academica.fecha_inicio','actividad_academica.estado')
            ->join('bloque_academico','actividad_academica.bloque_academico_id','bloque_academico.id')
            ->join('linea_estrategica','bloque_academico.linea_estrategica_id','linea_estrategica.id')
            //->whereRaw('2>1 '.$criterio)
            ->whereRaw($criterio)
            ->get();

        return $datos;
    }

    public static function listar($anio){
        $datos = DB::table('actividad_academica')
            ->select('actividad_academica.id','actividad_academica.codigo','actividad_academica.titulo','actividad_academica.programado',
                DB::Raw("CASE actividad_academica.origen WHEN 'EMP' THEN 'EMP' WHEN 'DISTRITO FISCAL' THEN
                            (SELECT distrito_fiscal.descripcion FROM distrito_fiscal WHERE distrito_fiscal.id = organizador_id LIMIT 1) ELSE
                            (SELECT entidad_externa.descripcion FROM entidad_externa WHERE entidad_externa.id = organizador_id  LIMIT 1) END AS origen"),
                'actividad_academica.fecha_inicio','actividad_academica.estado')
            ->whereRaw("actividad_academica.fecha_inicio between '".$anio."-01-01' and '".$anio."-12-31'")
            ->orderBy('actividad_academica.updated_at','desc')
            ->get();

        return $datos;
    }

    public static function listarByAnio($anio){
        $datos = DB::table('actividad_academica')
            ->select('id','titulo')
            ->whereRaw("fecha_inicio between '".$anio."-01-01' and '".$anio."-12-31'")
            ->whereRaw("estado not in (8)")
            ->get();

        return $datos;
    }

    public static function listarDatos($id){
        $datos = DB::table('actividad_academica')
            ->select('actividad_academica.id','actividad_academica.codigo','actividad_academica.titulo','actividad_academica.programado',
                            'actividad_academica.descripcion',
                DB::Raw("sub_gerencia.descripcion as subgerencia, tipo_actividad.descripcion AS tipo, CASE actividad_academica.modalidad_id WHEN 3 THEN
                modalidad.descripcion ELSE CONCAT(modalidad.descripcion, ' - ', sede.descripcion) END AS modalidad,
                sistema.descripcion as sistema, tematica.descripcion as tematica,
                CONCAT(linea_estrategica.descripcion, ' - ', bloque_academico.descripcion) as linea,
                tipo_certificado.descripcion as tipocertificado, coordinacion_nacional.descripcion as coordinacionnacional,
                CASE actividad_academica.origen WHEN 'EMP' THEN actividad_academica.origen WHEN 'DISTRITO FISCAL' THEN
                origendf.descripcion ELSE coop.descripcion END AS organizador, convenio.descripcion as convenio,
                (SELECT GROUP_CONCAT(entidad_externa.descripcion) FROM actividad_entidad_convenio aec JOIN entidad_externa
                ON aec.entidad_externa_id = entidad_externa.id AND aec.actividad_academica_id = ".$id.") as entidadconvenio,
                (SELECT GROUP_CONCAT(perfil_personal.descripcion) FROM actividad_perfil_personal app JOIN perfil_personal
                ON app.perfil_personal_id = perfil_personal.id AND app.actividad_academica_id = ".$id.") as perfil,
                actividad_academica.duracion, actividad_academica.fecha_inicio,
                actividad_academica.fecha_final, actividad_academica.horario_detallado, actividad_academica.nro_aulas,
                actividad_academica.nro_vacantes, actividad_academica.nro_docentes,
                actividad_academica.fecha_inicioe, actividad_academica.fecha_finale, actividad_academica.nro_aulase,
                actividad_academica.nro_vacantese, actividad_academica.nro_docentese,actividad_academica.duracione,
                actividad_academica.convocatoria, actividad_academica.compromiso, actividad_academica.autoformativo,
                actividad_academica.publico, actividad_academica.estado, actividad_academica.tipo_modulo,
                 tipo_modulo.descripcion as destipo_modulo"))
            ->join('sub_gerencia','actividad_academica.sub_gerencia_id','=','sub_gerencia.id')
            ->join('tipo_actividad','actividad_academica.tipo_actividad_id','=','tipo_actividad.id')
            ->join('modalidad','actividad_academica.modalidad_id','=','modalidad.id')
            ->leftJoin('distrito_fiscal AS sede','actividad_academica.sede','=','sede.id')
            ->join('sistema','actividad_academica.sistema_id','=','sistema.id')
            ->join('tematica','actividad_academica.tematica_id','=','tematica.id')
            ->join('bloque_academico','actividad_academica.bloque_academico_id','=','bloque_academico.id')
            ->join('linea_estrategica','bloque_academico.linea_estrategica_id','=','linea_estrategica.id')
            ->join('tipo_certificado','actividad_academica.tipo_certificado_id','=','tipo_certificado.id')
            ->leftJoin('coordinacion_nacional','actividad_academica.coordinacion_nacional_id','=','coordinacion_nacional.id')
            ->leftJoin('distrito_fiscal AS origendf','actividad_academica.organizador_id','=','origendf.id')
            ->leftJoin('entidad_externa AS coop','actividad_academica.organizador_id','=','coop.id')
            ->leftJoin('convenio','actividad_academica.convenio_id','=','convenio.id')
            ->leftJoin('tipo_modulo','actividad_academica.tipo_modulo','=','tipo_modulo.id')
            ->where('actividad_academica.id',$id)
            ->first();

        return $datos;
    }

    public static function validarEjecucion($id){
        $datos = DB::table('actividad_academica')
            ->select(DB::Raw('COUNT(actividad_academica.id) as cont'))
            ->where('actividad_academica.id',$id)
            ->whereNotNull('actividad_academica.fecha_inicioe')
            ->first();

        return $datos;
    }


    public static function listarRequisitos($id){
        $datos = DB::table('actividad_requisito')
            ->select('actividad_academica.id','actividad_academica.codigo','actividad_academica.titulo',DB::Raw('actividad_requisito.id as idar'),'actividad_requisito.actividad_academica_id')
            ->join('actividad_academica','actividad_requisito.requisito','=','actividad_academica.id')
            ->where('actividad_requisito.actividad_academica_id',$id)
            ->orderBy('actividad_requisito.requisito','asc')
            ->get();

        return $datos;
    }

    public static function listarResoluciones($id){
        $datos = DB::table('actividad_resolucion')
            ->select('resolucion.id','resolucion.descripcion', 'resolucion.archivo',DB::Raw('actividad_resolucion.id as idar'),'actividad_resolucion.actividad_academica_id')
            ->join('resolucion','actividad_resolucion.resolucion_id','=','resolucion.id')
            ->where('actividad_resolucion.actividad_academica_id',$id)
            ->orderBy('actividad_resolucion.resolucion_id','asc')
            ->get();

        return $datos;
    }

    public static function listarPlanes($id){
        $datos = DB::table('actividad_plan')
            ->select('plan.id','plan.descripcion','plan.entidad', 'plan.archivo',DB::Raw('actividad_plan.id as idap'),'actividad_plan.actividad_academica_id')
            ->join('plan','actividad_plan.plan_id','=','plan.id')
            ->where('actividad_plan.actividad_academica_id',$id)
            ->orderBy('actividad_plan.plan_id','asc')
            ->get();

        return $datos;
    }

    public static function listarPoliticas($id){
        $datos = DB::table('actividad_politica')
            ->select('politica.id','politica.descripcion','politica.entidad', DB::Raw('actividad_politica.id as idap'),'actividad_politica.actividad_academica_id')
            ->join('politica','actividad_politica.politica_id','=','politica.id')
            ->where('actividad_politica.actividad_academica_id',$id)
            ->orderBy('actividad_politica.politica_id','asc')
            ->get();

        return $datos;
    }

    public static function listarModulos($id){
        $datos = DB::table('di_modulo')
            ->selectRaw('di_modulo.id, di_modulo.tipo_capacitacion_id, di_modulo.actividad_academica_id,
                                di_modulo.item, di_modulo.descripcion, di_modulo.abreviatura,di_modulo.nombre,
                                di_modulo.duracion, di_modulo.fecha_inicio, di_modulo.fecha_final, tipo_capacitacion.descripcion as tipo')
            ->join('tipo_capacitacion','di_modulo.tipo_capacitacion_id','=','tipo_capacitacion.id')
            ->where('di_modulo.actividad_academica_id',$id)
            ->orderBy('di_modulo.item','asc')
            ->get();

        return $datos;
    }

    public static function listarModulosWord($id){
        $datos = DB::table('di_modulo')
            ->selectRaw('descripcion as mod_descripcion')
            ->where('actividad_academica_id',$id)
            ->orderBy('item','asc')
            ->get();

        return $datos;
    }

    public static function listarDatosWord($id){
        $datos = DB::table('actividad_academica')
            ->select('actividad_academica.id','actividad_academica.codigo','actividad_academica.titulo',
                'actividad_academica.descripcion',
                DB::Raw("tipo_actividad.descripcion as tipo, tipo_certificado.descripcion as certificado,
                CASE actividad_academica.modalidad_id WHEN 3 THEN modalidad.descripcion ELSE CONCAT(modalidad.descripcion, ' - ', sede.descripcion) END AS modalidad,
                (SELECT GROUP_CONCAT(perfil_personal.descripcion) FROM actividad_perfil_personal app JOIN perfil_personal
                ON app.perfil_personal_id = perfil_personal.id AND app.actividad_academica_id = ".$id.") as perfil,
                actividad_academica.fecha_inicioe, actividad_academica.fecha_finale, actividad_academica.nro_aulase,
                actividad_academica.nro_vacantese, actividad_academica.nro_docentese,actividad_academica.duracione"))
            ->join('modalidad','actividad_academica.modalidad_id','=','modalidad.id')
            ->join('tipo_actividad','actividad_academica.tipo_actividad_id','=','tipo_actividad.id')
            ->leftJoin('distrito_fiscal AS sede','actividad_academica.sede','=','sede.id')
            ->join('tipo_certificado','actividad_academica.tipo_certificado_id','=','tipo_certificado.id')
            ->where('actividad_academica.id',$id)
            ->first();

        return $datos;
    }

    public function entidadesconvenio(){
        return $this
            ->belongsToMany(EntidadConvenio::class,'actividad_entidad_convenio','actividad_academica_id','entidad_externa_id')
            ->withPivot('cod_usu')
            ->withTimestamps();
    }

    public function perfiles(){
        return $this
            ->belongsToMany(PerfilPersonal::class,'actividad_perfil_personal','actividad_academica_id','perfil_personal_id')
            ->withPivot('cod_usu')
            ->withTimestamps();
    }


    public function resoluciones(){
        return $this
            ->belongsToMany(Resolucion::class,'actividad_resolucion','actividad_academica_id','resolucion_id')
            ->withPivot('cod_usu')
            ->withTimestamps();
    }

    public function planes(){
        return $this
            ->belongsToMany(Plan::class,'actividad_plan','actividad_academica_id','plan_id')
            ->withPivot('cod_usu')
            ->withTimestamps();
    }

    public function politicas(){
        return $this
            ->belongsToMany(Politica::class,'actividad_politica','actividad_academica_id','politica_id')
            ->withPivot('cod_usu')
            ->withTimestamps();
    }

    public function modulos(){
        return $this
            ->hasMany(DiModulo::class,'actividad_academica_id','id');
    }
}
