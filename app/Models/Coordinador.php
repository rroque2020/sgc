<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Coordinador extends Model
{
    use HasFactory;
    protected $table = "px_maestro_personal";

    public static function listar(){
        $datos = DB::table('px_maestro_personal')
                        ->select('CODI_EMPL_PER','APE_PAT_PER','APE_MAT_PER','NOM_EMP_PER','NOMB_CORT_PER')
                        ->orderBy('NOMB_CORT_PER','asc')
                        ->get();

        return $datos;
    }
}
