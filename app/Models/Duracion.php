<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Duracion extends Model
{
    use HasFactory;

    protected $table = "duracion";
    protected $fillable = ['id','descripcion','abreviatura','estado','cod_usuc','cod_usum'];

    public static function listarByEstado($estado){
        $datos = DB::table('duracion')
            ->select('id','descripcion','abreviatura')
            ->where('estado',$estado)
            ->get();

        return $datos;
    }
}
