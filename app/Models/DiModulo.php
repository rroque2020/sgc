<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DiModulo extends Model
{
    use HasFactory;

    protected $table = "di_modulo";

    public static function siguienteModulo($actividad)
    {
        DB::statement("CALL sp_generar_codigo_modulo(" . $actividad . ",@codigo)");
        $resultado = DB::selectOne("select @codigo as codigo");

        return $resultado;
    }

    public static function sumaDuracion($actividad)
    {
        $data = DB::table('actividad_academica')
            ->select(DB::Raw('actividad_academica.duracione, CASE WHEN SUM(di_modulo.duracion) IS NULL THEN 0 ELSE SUM(di_modulo.duracion) END AS suma'))
            ->leftJoin('di_modulo','actividad_academica.id','=','di_modulo.actividad_academica_id')
            ->where('actividad_academica.id', $actividad)
            ->groupBy('actividad_academica.duracione')
            ->first();

        return $data;
    }

    public static function listarCompetencias($modulo)
    {
        $data = DB::table('di_competencia')
            ->select('di_competencia.id', 'di_competencia.di_modulo_id', 'di_competencia.orden',
                'competencia_perfil_personal.codigo', 'competencia_perfil_personal.perfil_personal_id',
                'competencia_perfil_personal.tipo', 'competencia_perfil_personal.descripcion')
            ->join('competencia_perfil_personal', 'di_competencia.competencia_perfil_personal_id', '=', 'competencia_perfil_personal.id')
            ->where('di_competencia.di_modulo_id', $modulo)
            ->orderBy('di_competencia.orden', 'asc')
            ->get();

        return $data;
    }

    public static function listarEstructuraPrueba($modulo){
        $data = DB::table('di_modulo')
            ->select(DB::Raw("       di_modulo.id AS mod_id,
                                           di_modulo.nombre AS mod_nombre,
                                           di_competencia.id AS comp_id,
                                           di_competencia.orden AS comp_orden,
                                           competencia_perfil_personal.descripcion AS comp_descripcion,
                                           di_capacidad.orden AS cap_orden,
                                           di_capacidad.descripcion AS cap_descripcion,
                                           di_sesion.orden AS ses_orden,
                                           di_sesion.descripcion AS ses_descripcion,
                                           di_unidad.orden AS und_orden,
                                           di_unidad.nombre AS und_nombre,
                                           di_unidad.descripcion AS und_descripcion,
                                           di_unidad.bibliografia AS und_bibliografia,
                                           di_sesion.duracion AS ses_duracion,
                                           tipo_actividad_estrategica.descripcion AS ses_tae,
                                           di_sesion.des_actividad_estrategica_id AS ses_destae,
                                           di_sesion.recursos_obligatorios AS ses_ro,
                                           di_sesion.recursos_complementarios AS ses_rc,
                                           di_sesion.fecha_hora AS ses_fh"))
            ->leftJoin('di_competencia','di_modulo.id','=','di_competencia.di_modulo_id')
            ->leftJoin('competencia_perfil_personal','di_competencia.competencia_perfil_personal_id','=','competencia_perfil_personal.id')
            ->leftJoin('di_capacidad','di_competencia.id','=','di_capacidad.di_competencia_id')
            ->leftJoin('di_sesion','di_capacidad.id','=','di_sesion.di_capacidad_id')
            ->leftJoin('tipo_actividad_estrategica','di_sesion.tipo_actividad_estrategica_id','=','tipo_actividad_estrategica.id')
            ->leftJoin('di_unidad','di_sesion.id','=','di_unidad.di_sesion_id')
            ->where('di_modulo.id',$modulo)
            ->orderBy('di_modulo.item','asc')
            ->orderBy('di_competencia.competencia_perfil_personal_id','asc')
            ->orderBy('di_capacidad.orden','asc')
            ->orderBy('di_sesion.orden','asc')
            ->get();

        return $data;
    }

    public static function listarEstructuraComps($modulo){
        $data = DB::table('di_competencia')
            ->select(DB::Raw("   di_competencia.di_modulo_id AS mod_id,
                                       di_competencia.id AS comp_id,
                                       di_competencia.orden AS comp_orden,
                                       competencia_perfil_personal.descripcion AS comp_descripcion,
                                       count(di_unidad.di_sesion_id) AS cont_unidad"))
            ->leftJoin('competencia_perfil_personal','di_competencia.competencia_perfil_personal_id','=','competencia_perfil_personal.id')
            ->leftJoin('di_capacidad','di_competencia.id','=','di_capacidad.di_competencia_id')
            ->leftJoin('di_sesion','di_capacidad.id','=','di_sesion.di_capacidad_id')
            ->leftJoin('di_unidad','di_sesion.id','=','di_unidad.di_sesion_id')
            ->where('di_competencia.di_modulo_id',$modulo)
            ->groupBy('di_competencia.di_modulo_id',
                                       'di_competencia.id',
                                       'di_competencia.orden',
                                       'competencia_perfil_personal.descripcion')
            ->orderBy('di_competencia.orden','asc')
            ->get();

        return $data;
    }

    public static function listarEstructuraCaps($modulo){
        $data = DB::table('di_capacidad')
            ->select(DB::Raw("       di_capacidad.di_competencia_id AS comp_id,
                                           di_capacidad.orden AS cap_orden,
                                           di_capacidad.descripcion AS cap_descripcion"))
            ->leftJoin('di_competencia','di_capacidad.di_competencia_id','=','di_competencia.id')
            ->where('di_competencia.di_modulo_id',$modulo)
            ->orderBy('di_capacidad.orden','asc')
            ->get();

        return $data;
    }

    public static function listarEstructuraSes($modulo){
        $data = DB::table('di_sesion')
            ->select(DB::Raw("       di_sesion.orden AS ses_orden,
                                           di_sesion.descripcion AS ses_descripcion,
                                           di_sesion.duracion AS ses_duracion,
                                           tipo_actividad_estrategica.descripcion AS ses_tae,
                                           di_sesion.des_actividad_estrategica_id AS ses_destae,
                                           di_sesion.recursos_obligatorios AS ses_ro,
                                           di_sesion.recursos_complementarios AS ses_rc,
                                           di_sesion.fecha_hora AS ses_fh"))
            ->leftJoin('tipo_actividad_estrategica','di_sesion.tipo_actividad_estrategica_id','=','tipo_actividad_estrategica.id')
            ->leftJoin('di_capacidad','di_sesion.di_capacidad_id','=','di_capacidad.id')
            ->leftJoin('di_competencia','di_capacidad.di_competencia_id','=','di_competencia.id')
            ->where('di_competencia.di_modulo_id',$modulo)
            ->orderBy('di_sesion.orden','asc')
            ->get();

        return $data;
    }

    public static function listarEstructuraUnd($modulo){
        $data = DB::table('di_unidad')
            ->select(DB::Raw("       di_unidad.orden AS und_orden,
                                           di_unidad.nombre AS und_nombre,
                                           di_unidad.descripcion AS und_descripcion,
                                           di_unidad.bibliografia AS und_bibliografia"))
            ->leftJoin('di_sesion','di_unidad.di_sesion_id','=','di_sesion.id')
            ->leftJoin('di_capacidad','di_sesion.di_capacidad_id','=','di_capacidad.id')
            ->leftJoin('di_competencia','di_capacidad.di_competencia_id','=','di_competencia.id')
            ->where('di_competencia.di_modulo_id',$modulo)
            ->orderBy('di_unidad.orden','asc')
            ->get();

        return $data;
    }

    public static function listarEstructuraCompetencias($modulo){
        $data = DB::table('di_modulo')
                    ->select(DB::Raw("di_modulo.nombre AS modulo,
                                           di_competencia.orden AS comp_orden,
                                           competencia_perfil_personal.descripcion AS comp_descripcion,
                                           di_capacidad.orden AS cap_orden,
                                           di_capacidad.descripcion AS cap_descripcion,
                                           di_sesion.orden AS ses_orden,
                                           di_sesion.descripcion AS ses_descripcion,
                                           di_unidad.orden AS und_orden,
                                           di_unidad.nombre AS und_nombre,
                                           di_unidad.descripcion AS und_descripcion,
                                           di_unidad.bibliografia AS und_bibliografia,
                                           di_sesion.duracion AS ses_duracion,
                                           tipo_actividad_estrategica.descripcion AS ses_tae,
                                           di_sesion.des_actividad_estrategica_id AS ses_destae,
                                           di_sesion.recursos_obligatorios AS ses_ro,
                                           di_sesion.recursos_complementarios AS ses_rc,
                                           di_sesion.fecha_hora_inicio AS ses_fhi,
                                           di_sesion.fecha_hora_cierre AS ses_fhc"))
                    ->leftJoin('di_competencia','di_modulo.id','=','di_competencia.di_modulo_id')
                    ->leftJoin('competencia_perfil_personal','di_competencia.competencia_perfil_personal_id','=','competencia_perfil_personal.id')
                    ->leftJoin('di_capacidad','di_competencia.id','=','di_capacidad.di_competencia_id')
                    ->leftJoin('di_sesion','di_capacidad.id','=','di_sesion.di_capacidad_id')
                    ->leftJoin('tipo_actividad_estrategica','di_sesion.tipo_actividad_estrategica_id','=','tipo_actividad_estrategica.id')
                    ->leftJoin('di_unidad','di_sesion.id','=','di_unidad.di_sesion_id')
                    ->where('di_modulo.id',$modulo)
                    ->orderBy('di_modulo.item','asc')
                    ->orderBy('di_competencia.orden','asc')
                    ->orderBy('di_capacidad.orden','asc')
                    ->orderBy('di_sesion.orden','asc')
                    ->orderBy('di_unidad.orden','asc')
                    ->get();

        return $data;
    }

    public static function listarEstructuraActividadesEvaluativas($modulo){
        $data = DB::table('di_modulo')
            ->select(DB::Raw("di_modulo.nombre AS modulo,
                                   di_actividad_evaluativa.orden AS ae_orden,
                                   tipo_actividad_evaluativa.descripcion AS ae_tae,
                                   di_actividad_evaluativa.descripcion AS ae_descripcion,
                                   di_actividad_evaluativa.porcentaje AS ae_porcentaje,
                                   instrumento_evaluativo.descripcion AS ae_instrumento,
                                   di_actividad_evaluativa.nota_aprobacion AS ae_nota"))
            ->leftJoin('di_actividad_evaluativa','di_modulo.id','=','di_actividad_evaluativa.di_modulo_id')
            ->leftJoin('tipo_actividad_evaluativa','di_actividad_evaluativa.tipo_actividad_evaluativa_id','=','tipo_actividad_evaluativa.id')
            ->leftJoin('instrumento_evaluativo','di_actividad_evaluativa.instrumento_evaluativo_id','=','instrumento_evaluativo.id')
            ->where('di_modulo.id',$modulo)
            ->orderBy('di_modulo.item','asc')
            ->orderBy('di_actividad_evaluativa.orden','asc')
            ->get();

        return $data;
    }

    public static function export($actividad){
        $data = DB::table('di_modulo')
            ->select(DB::Raw("di_modulo.descripcion, di_competencia.di_modulo_id,
                    di_competencia.id as comp_id,
                    di_capacidad.id as cap_id, di_capacidad.descripcion as cap_descripcion"))
            ->leftJoin('di_competencia','di_modulo.id','=','di_competencia.di_modulo_id')
            ->leftJoin('di_capacidad','di_competencia.id','=','di_capacidad.di_competencia_id')
            ->leftJoin('di_sesion','di_capacidad.id','=','di_sesion.di_capacidad_id')
            ->where('di_modulo.actividad_academica_id',$actividad)
            ->orderBy('di_competencia.id','asc')
            ->orderBy('di_capacidad.id','asc')
            ->orderBy('di_sesion.id','asc')
            ->get();

        return $data;
    }

    public static function listarActividadesEvaluativas($modulo){
        $data = DB::table('di_actividad_evaluativa')
            ->selectRaw('di_actividad_evaluativa.id, di_actividad_evaluativa.di_modulo_id,
                                di_actividad_evaluativa.tipo_actividad_evaluativa_id, tipo_actividad_evaluativa.descripcion as tipo,
                                di_actividad_evaluativa.orden, di_actividad_evaluativa.descripcion, di_actividad_evaluativa.porcentaje,
                                di_actividad_evaluativa.instrumento_evaluativo_id, instrumento_evaluativo.descripcion as instrumento,
                                di_actividad_evaluativa.nota_aprobacion,di_actividad_evaluativa.fecha_hora_inicio,
                                 di_actividad_evaluativa.fecha_hora_cierre')
            ->join('tipo_actividad_evaluativa','di_actividad_evaluativa.tipo_actividad_evaluativa_id','=','tipo_actividad_evaluativa.id')
            ->join('instrumento_evaluativo','di_actividad_evaluativa.instrumento_evaluativo_id','=','instrumento_evaluativo.id')
            ->where('di_actividad_evaluativa.di_modulo_id',$modulo)
            ->orderBy('di_actividad_evaluativa.orden','asc')
            ->get();

        return $data;
    }

    public static function listarByCapacitacionGeneral($actividad){
        $data = DB::table('di_modulo')
            ->where('tipo_capacitacion_id',1)
            ->where('actividad_academica_id',$actividad)
            ->orderBy('item','asc')
            ->get();

        return $data;
    }

    public static function listarByCapacitacionEspecifica($actividad){
        $data = DB::table('di_modulo')
            ->where('tipo_capacitacion_id',2)
            ->where('actividad_academica_id',$actividad)
            ->orderBy('item','asc')
            ->get();

        return $data;
    }

    public function actividades(){
        return $this
            ->belongsTo(ActividadAcademica::class,'actividad_academica_id','actividad_academica_id');
    }

    public function competencias(){
        return $this
            ->hasMany(DiCompetencia::class,'modulo_id','id');
    }
}
