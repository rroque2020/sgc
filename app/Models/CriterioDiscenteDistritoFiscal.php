<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CriterioDiscenteDistritoFiscal extends Model
{
    use HasFactory;
    protected $table = "criterio_discente_distrito_fiscal";

    public static function listarDistritosSeleccionados($criterio){
        $data = DB::table('distrito_fiscal')
            ->select('distrito_fiscal.id', 'distrito_fiscal.descripcion')
            ->join('criterio_discente_distrito_fiscal','distrito_fiscal.id','=','criterio_discente_distrito_fiscal.distrito_fiscal_id')
            ->join('criterio_discente','criterio_discente_distrito_fiscal.criterio_discente_id','=','criterio_discente.id')
            ->where('criterio_discente.actividad_academica_id',$criterio)
            ->orderBy('distrito_fiscal.id','asc')
            ->get();

        return $data;
    }
}
