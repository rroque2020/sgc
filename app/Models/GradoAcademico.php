<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class GradoAcademico extends Model
{
    use HasFactory;
    protected $table="sistema";

    protected $fillable = ['id','descripcion','estado','cod_usuc','cod_usum'];

    public static function listarByEstado($estado){
        $datos = DB::table('grado_academico')
            ->select('id','descripcion')
            ->where('estado',$estado)
            ->orderby('descripcion','asc')
            ->get();

        return $datos;
    }
}
