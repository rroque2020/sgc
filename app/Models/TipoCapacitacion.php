<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TipoCapacitacion extends Model
{
    use HasFactory;
    protected $table = "tipo_capacitacion";

    public static function listarByEstado($estado){
        $datos = DB::table('tipo_capacitacion')
            ->select('id','descripcion','abreviatura')
            ->where('estado',$estado)
            ->orderby('descripcion','asc')
            ->get();

        return $datos;
    }
}
