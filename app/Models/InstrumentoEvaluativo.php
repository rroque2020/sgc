<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class InstrumentoEvaluativo extends Model
{
    use HasFactory;
    protected $table = 'instrumento_evaluativo';

    public static function listarByEstado($estado){
        $datos = DB::table('instrumento_evaluativo')
            ->select('id','descripcion','abreviatura')
            ->where('estado',$estado)
            ->orderby('descripcion','asc')
            ->get();

        return $datos;
    }
}
