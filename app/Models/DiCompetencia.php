<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DiCompetencia extends Model
{
    use HasFactory;
    protected $table = "di_competencia";

    public function modulos(){
        return $this
            ->belongsTo(DiModulo::class,'modulo_id','modulo_id');
    }

    public static function listarCapacidades($competencia){
        $data = DB::table('di_capacidad')
            ->where('di_competencia_id',$competencia)
            ->orderBy('orden','asc')
            ->get();

        return $data;
    }

    public static function listarDatos($competencia){
        $data = DB::table('di_competencia')
            ->select('di_competencia.id','di_competencia.orden','competencia_perfil_personal.codigo',
                            'competencia_perfil_personal.tipo','competencia_perfil_personal.descripcion')
            ->join('competencia_perfil_personal','di_competencia.competencia_perfil_personal_id','=','competencia_perfil_personal.id')
            ->where('di_competencia.id',$competencia)
            ->first();

        return $data;
    }

    public function dicapacidad(){
        return $this->hasMany('App\Models\DiCapacidad','di_competencia_id','id');
    }
//
//    public function competenciaperfilpersonal(){
//        return $this->belongsTo('App\Models\CompetenciaPerfilPersonal','competencia_perfil_personal_id','id');
//    }
}
