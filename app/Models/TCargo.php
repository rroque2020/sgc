<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TCargo extends Model
{
    use HasFactory;
    protected $table = "tcargos";

    public static function listar(){
        $datos = DB::table('tcargos')
            ->select('CODI_CARG_TCA','DESC_CARG_TCA')
            ->where('DESC_CARG_TCA','<>','[NULL]')
            ->orderby('DESC_CARG_TCA','asc')
            ->get();

        return $datos;
    }
}
