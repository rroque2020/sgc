<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ActividadDocente extends Model
{
    use HasFactory;
    protected $table = "actividad_docente";

    public static function existe($actividad, $docente){
        $datos = DB::table('actividad_docente')
            ->where('actividad_academica_id',$actividad)
            ->where('docente_id',$docente)
            ->get();

        return $datos;
    }

    public static function confirmo($actividad, $docente){
        $datos = DB::table('actividad_docente')
            ->where('actividad_academica_id',$actividad)
            ->where('docente_id',$docente)
            ->where('estado',3)
            ->get();

        return $datos;
    }

    public static function listardocentes($actividad){
        $datos = DB::table('actividad_docente')
            ->selectRaw("actividad_docente.id, actividad_docente.actividad_academica_id as act_id,
                                  actividad_docente.docente_id, CONCAT(persona.apellidos, ' ', persona.nombres) as docente,
                                  actividad_docente.estado")
            ->join('docente','actividad_docente.docente_id','=','docente.id')
            ->join('persona','docente.persona_id','=','persona.id')
            ->where('actividad_academica_id',$actividad)
            ->get();

        return $datos;
    }

    public static function listarByDocente($docente){
        $data = DB::table('actividad_docente')
            ->select('actividad_academica.codigo','actividad_academica.titulo',
                'actividad_academica.fecha_inicio','actividad_academica.fecha_final','actividad_academica.estado',
                'actividad_docente.certificado')
            ->join('actividad_academica','actividad_docente.actividad_academica_id','=','actividad_academica.id')
            ->where('actividad_docente.docente_id', $docente)
            ->orderBy('actividad_academica.fecha_final','desc')
            ->get();

        return $data;
    }
}
