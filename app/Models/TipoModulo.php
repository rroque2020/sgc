<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TipoModulo extends Model
{
    use HasFactory;
    protected $table = "tipo_modulo";

    public static function listarByEstado($estado){
        $datos = DB::table('tipo_modulo')
            ->select('id','descripcion','abreviatura')
            ->where('estado',$estado)
            ->orderby('descripcion','asc')
            ->get();

        return $datos;
    }
}
