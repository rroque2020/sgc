<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Docente extends Model
{
    use HasFactory;

    protected $table = "docente";

    public static function listar(){
        $data = DB::table("docente")
                    ->selectRaw("docente.id, persona.dni, CASE persona.tipo WHEN 0 then 'EXTERNO' ELSE 'INTERNO' END as tipo,
                                        CONCAT(persona.apellidos,' ', persona.nombres) as docente,
                                        persona.cargo, persona.institucion, docente.estado")
                    ->join('persona','docente.persona_id','=','persona.id')
                    ->get();

        return $data;
    }

    public static function listarDatos($docente){
        $data = DB::table('docente')
                    ->select('docente.id', 'persona.tipo', DB::Raw("CASE persona.tipo WHEN 0 then 'EXTERNO' ELSE 'INTERNO' END as tipodet"),
                                    'persona.dni', 'persona.apellidos', 'persona.nombres', 'persona.celular', 'persona.fecha', 'persona.email',
                                    'persona.genero','persona.idcargo', 'persona.cargo', 'persona.iddependencia', 'persona.institucion', 'persona.foto',
                                    'docente.persona_id','docente.estado')
                    ->join('persona','docente.persona_id','=','persona.id')
                    ->where('docente.id', $docente)
                    ->first();

        return $data;
    }

    public static function totales($docente){
        $data = DB::table('docente')
            ->select(
                DB::Raw("(SELECT COUNT(actividad_docente.id) FROM actividad_docente WHERE actividad_docente.docente_id = ".$docente.") AS totactividad"),
                DB::Raw("(SELECT COUNT(docente_capacitacion.id) FROM docente_capacitacion WHERE docente_capacitacion.docente_id = ".$docente.") AS totcapacitacion"),
                DB::Raw("(SELECT COUNT(docente_experiencia.id) FROM docente_experiencia WHERE docente_experiencia.docente_id = ".$docente.") AS totexperiencia")
            )
            ->first();

        return $data;
    }

    public static function filtro($docente){
        $data = DB::table('actividad_docente')
                    ->selectRaw("CONCAT(persona.apellidos,' ',persona.nombres) AS docente, actividad_academica.titulo, docente.id as doc_id, actividad_academica.id as actividad_id")
                    ->join('docente','actividad_docente.docente_id','=','docente.id')
                    ->join('persona','docente.persona_id','=','persona.id')
                    ->join('actividad_academica','actividad_docente.actividad_academica_id','=','actividad_academica.id')
                    ->leftJoin('di_modulo','actividad_academica.id','=','di_modulo.actividad_academica_id')
                    ->whereRaw("(actividad_academica.titulo LIKE '%".$docente."%' OR di_modulo.nombre LIKE '%".$docente."%') AND actividad_docente.certificado = 1 ")
                    ->get();

        return $data;
    }
}
