<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TDependencia extends Model
{
    use HasFactory;

    protected $table = "tdependencias";

    public static function listarByDistrito($distrito){
        $datos = DB::table('tdependencias')
            ->select('CODI_DEPE_TDE','DESC_DEPE_TDE')
            ->where('CODI_DIJU_DJU',$distrito)
            ->where('DESC_DEPE_TDE','<>','[NULL]')
            ->orderby('DESC_DEPE_TDE','asc')
            ->get();

        return $datos;
    }

    public static function listarDatos($id){
        $datos = DB::table('tdependencias')
            ->where('CODI_DEPE_TDE',$id)
            ->first();

        return $datos;
    }
}
