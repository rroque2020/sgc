<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TExpositor extends Model
{
    use HasFactory;
    protected $table = "texpositor";

    public static function listar($dni){
        $datos = DB::table('texpositor')
            ->where('DES_DNI',$dni)
            ->first();

        return $datos;
    }
}
