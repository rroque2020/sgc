<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TMaestroPersonal extends Model
{
    use HasFactory;
    protected $table = "tmaestro_personal";

    public static function listarDatosbyDNI($dni){
        $datos = DB::table('tmaestro_personal')
            ->select('CODI_EMPL_PER','APE_PAT_PER','APE_MAT_PER','NOM_EMP_PER','NOMB_CORT_PER', 'TELF_CELU_PER','DIRE_EMAI_PER')
            ->where('CODI_EMPL_PER','=',$dni)
            ->first();

        return $datos;
    }

    public static function listarBusqueda($especialidad, $cargos, $perfiless, $distritos){
        $data = DB::table('tmaestro_personal')
            ->selectRaw(" 1 AS tipo, tmaestro_personal.codi_empl_per AS dni,
                                CONCAT(tmaestro_personal.APE_PAT_PER, ' ',tmaestro_personal.APE_MAT_PER, ' ', tmaestro_personal.NOM_EMP_PER) AS postulante,
	                            tmaestro_personal.TELF_CELU_PER AS celular, tmaestro_personal.DIRE_EMAI_PER AS email")
            ->whereIn("tmaestro_personal.CODI_CARG_TCA", explode(',',$cargos));

        $perfiles = explode(",", $perfiless);

        $fis = false;
        $for = false;
        $adm = false;
        $fiscal = "00000001,00000002,00000003,00000006,00000007,00000008,00000009";
        $forense = "00002121,00000199,00000372,00000391,00002119,00002145,00002118,00000374,00000378,00002174,00002106,00000116,00000367,00000368,00000369,00000389,00000151,00002120,00002123,00000390,00000200,00000190,00000198,00002114,00002124,00002166,00002233,00002234,00002235,00002236,00002237";

        for($i = 0; $i < count($perfiles); $i++){
            if($perfiles[$i] == 1){
                $fis = true;
            }
            if($perfiles[$i] == 2){
                $for = true;
            }
            if($perfiles[$i] == 3){
                $adm = true;
            }
        }
        if($fis && $for && $adm){
            $data1 =  DB::table('tmaestro_personal')
                ->selectRaw(" 1 AS tipo, tmaestro_personal.codi_empl_per AS dni,
                                CONCAT(tmaestro_personal.APE_PAT_PER, ' ',tmaestro_personal.APE_MAT_PER, ' ', tmaestro_personal.NOM_EMP_PER) AS postulante,
	                            tmaestro_personal.TELF_CELU_PER AS celular, tmaestro_personal.DIRE_EMAI_PER AS email");
        }
        if($fis && !$for && !$adm){
            $cargos1 = $fiscal;
            $data1 =  DB::table('tmaestro_personal')
                ->selectRaw(" 1 AS tipo, tmaestro_personal.codi_empl_per AS dni,
                                CONCAT(tmaestro_personal.APE_PAT_PER, ' ',tmaestro_personal.APE_MAT_PER, ' ', tmaestro_personal.NOM_EMP_PER) AS postulante,
	                            tmaestro_personal.TELF_CELU_PER AS celular, tmaestro_personal.DIRE_EMAI_PER AS email")
                ->whereIn("tmaestro_personal.CODI_CARG_TCA", explode(',',$cargos1));

        }
        if(!$fis && $for && !$adm){
            $cargos1 = $forense;
            $data1 =  DB::table('tmaestro_personal')
                ->selectRaw(" 1 AS tipo, tmaestro_personal.codi_empl_per AS dni,
                                CONCAT(tmaestro_personal.APE_PAT_PER, ' ',tmaestro_personal.APE_MAT_PER, ' ', tmaestro_personal.NOM_EMP_PER) AS postulante,
	                            tmaestro_personal.TELF_CELU_PER AS celular, tmaestro_personal.DIRE_EMAI_PER AS email")
                ->whereIn("tmaestro_personal.CODI_CARG_TCA", explode(',',$cargos1));
        }
        if($fis && $for && !$adm){
            $cargos1 = $fiscal.','.$forense;
            $data1 =  DB::table('tmaestro_personal')
                ->selectRaw(" 1 AS tipo, tmaestro_personal.codi_empl_per AS dni,
                                CONCAT(tmaestro_personal.APE_PAT_PER, ' ',tmaestro_personal.APE_MAT_PER, ' ', tmaestro_personal.NOM_EMP_PER) AS postulante,
	                            tmaestro_personal.TELF_CELU_PER AS celular, tmaestro_personal.DIRE_EMAI_PER AS email")
                ->whereIn("tmaestro_personal.CODI_CARG_TCA", explode(',',$cargos1));
        }
        if(!$fis && !$for && $adm){
            $cargos1 = $fiscal.','.$forense;;
            $data1 =  DB::table('tmaestro_personal')
                ->selectRaw(" 1 AS tipo, tmaestro_personal.codi_empl_per AS dni,
                                CONCAT(tmaestro_personal.APE_PAT_PER, ' ',tmaestro_personal.APE_MAT_PER, ' ', tmaestro_personal.NOM_EMP_PER) AS postulante,
	                            tmaestro_personal.TELF_CELU_PER AS celular, tmaestro_personal.DIRE_EMAI_PER AS email")
                ->whereNotIn("tmaestro_personal.CODI_CARG_TCA", explode(',',$cargos1));
        }
        if($fis && !$for && $adm){
            $cargos1 = $forense;
            $data1 =  DB::table('tmaestro_personal')
                ->selectRaw(" 1 AS tipo, tmaestro_personal.codi_empl_per AS dni,
                                CONCAT(tmaestro_personal.APE_PAT_PER, ' ',tmaestro_personal.APE_MAT_PER, ' ', tmaestro_personal.NOM_EMP_PER) AS postulante,
	                            tmaestro_personal.TELF_CELU_PER AS celular, tmaestro_personal.DIRE_EMAI_PER AS email")
                ->whereNotIn("tmaestro_personal.CODI_CARG_TCA", explode(',',$cargos1));
        }
        if(!$fis && $for && $adm){
            $cargos1 = $fiscal;
            $data1 =  DB::table('tmaestro_personal')
                ->selectRaw(" 1 AS tipo, tmaestro_personal.codi_empl_per AS dni,
                                CONCAT(tmaestro_personal.APE_PAT_PER, ' ',tmaestro_personal.APE_MAT_PER, ' ', tmaestro_personal.NOM_EMP_PER) AS postulante,
	                            tmaestro_personal.TELF_CELU_PER AS celular, tmaestro_personal.DIRE_EMAI_PER AS email")
                ->whereNotIn("tmaestro_personal.CODI_CARG_TCA", explode(',',$cargos1));
        }

        $data2 = $data->union($data1);
        $data3 =  DB::table('tmaestro_personal')
            ->selectRaw(" 1 AS tipo, tmaestro_personal.codi_empl_per AS dni,
                                CONCAT(tmaestro_personal.APE_PAT_PER, ' ',tmaestro_personal.APE_MAT_PER, ' ', tmaestro_personal.NOM_EMP_PER) AS postulante,
	                            tmaestro_personal.TELF_CELU_PER AS celular, tmaestro_personal.DIRE_EMAI_PER AS email")
            ->join("tdependencias","tmaestro_personal.CODI_DEPE_TDE","=", "tdependencias.CODI_DEPE_TDE")
            ->whereIn("tdependencias.CODI_DIJU_DJU", explode(',',$distritos));

        $data4 = $data2
                ->union($data3);

        $data5 =  DB::table('tmaestro_personal')
            ->selectRaw(" 1 AS tipo, tmaestro_personal.codi_empl_per AS dni,
                                CONCAT(tmaestro_personal.APE_PAT_PER, ' ',tmaestro_personal.APE_MAT_PER, ' ', tmaestro_personal.NOM_EMP_PER) AS postulante,
	                            tmaestro_personal.TELF_CELU_PER AS celular, tmaestro_personal.DIRE_EMAI_PER AS email")
            ->join("tdependencias","tmaestro_personal.CODI_DEPE_TDE","=", "tdependencias.CODI_DEPE_TDE")
            ->whereRaw("tdependencias.DESC_DEPE_TDE LIKE '%".$especialidad."%'");

        $datos = $data4->union($data5)->get();
        return $datos;
    }
}
