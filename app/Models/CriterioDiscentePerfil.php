<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CriterioDiscentePerfil extends Model
{
    use HasFactory;
    protected $table = "criterio_discente_perfil";

    public static function listarPerfilesSeleccionados($criterio){
        $data = DB::table('perfil_personal')
            ->select('perfil_personal.id','perfil_personal.descripcion')
            ->join('criterio_discente_perfil','perfil_personal.id','=','criterio_discente_perfil.perfil_personal_id')
            ->join('criterio_discente','criterio_discente_perfil.criterio_discente_id','=','criterio_discente.id')
            ->where('criterio_discente.actividad_academica_id',$criterio)
            ->orderBy('perfil_personal.id','asc')
            ->get();

        return $data;
    }

    public static function cargosFiscales(){
        return "'00000001','00000002','00000003','00000006','00000007','00000008','00000009'";
    }

    public static function cargosForenses(){
        return "'00002121','00000199','00000372','00000391','00002119','00002145','00002118','00000374','00000378','00002174','00002106','00000116','00000367','00000368','00000369','00000389','00000151','00002120','00002123','00000390','00000200','00000190','00000198','00002114','00002124','00002166','00002233','00002234','00002235','00002236','00002237'";
    }

    public function criterios()
    {
        return $this->belongsTo(CriterioDiscente::class,'criterio_discente_id','criterio_discente_id');
    }
}
