<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Discente extends Model
{
    use HasFactory;
    protected $table = "discente";

    public static function listarDatos($discente){
        $data = DB::table('discente')
            ->select('discente.id', 'discente.dni', 'discente.apellidos', 'discente.nombres', 'discente.celular', 'discente.fecha',
                'discente.email','discente.genero', 'discente.cargo', 'tcargos.desc_carg_tca', 'discente.dependencia', 'discente.foto','discente.estado')
            ->join('tcargos','tcargos.codi_carg_tca','=','discente.cargo')
            ->where('discente.id', $discente)
            ->first();

        return $data;
    }

    public static function totales($discente){
        $data = DB::table('discente')
            ->select(
                DB::Raw("(SELECT COUNT(actividad_discente.id) FROM actividad_discente WHERE actividad_discente.discente_id = ".$discente.") AS totactividad")
            )
            ->first();

        return $data;
    }

    public static function listarDatosByDNI($dni){
        $data = DB::table('discente')
            ->select('discente.id', 'discente.persona_id')
            ->join('persona','discente.persona_id','=','persona.id')
            ->where('persona.dni', $dni)
            ->first();

        return $data;
    }
}
