<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TipoActividadEstrategica extends Model
{
    use HasFactory;
    protected $table = 'tipo_actividad_estrategica';

    public static function listarByEstado($estado){
        $datos = DB::table('tipo_actividad_estrategica')
            ->select('id','descripcion','abreviatura')
            ->where('estado',$estado)
            ->orderby('descripcion','asc')
            ->get();

        return $datos;
    }

    public static function listarDatosWord($actividad){
        $data = DB::table('di_modulo')
            ->selectRaw("DISTINCT tipo_actividad_estrategica.descripcion AS tae_descripcion ")
            ->leftJoin('di_competencia','di_modulo.id','=','di_competencia.di_modulo_id')
            ->leftJoin('di_capacidad','di_competencia.id','=','di_capacidad.di_competencia_id')
            ->leftJoin('di_sesion','di_capacidad.id','=','di_sesion.di_capacidad_id')
            ->leftJoin('tipo_actividad_estrategica','di_sesion.tipo_actividad_estrategica_id','=','tipo_actividad_estrategica.id')
            ->where('di_modulo.actividad_academica_id',$actividad)
            ->whereNotNull('tipo_actividad_estrategica.descripcion')
            ->orderBy('tipo_actividad_estrategica.descripcion','ASC')
            ->get();

        return $data;
    }
}
