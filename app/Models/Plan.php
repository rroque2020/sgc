<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Plan extends Model
{
    use HasFactory;
    protected $table = "plan";
    protected $fillable = ['id','descripcion','abreviatura','entidad','archivo','estado','cod_usuc','cod_usum'];

    public static function listarByEstado($estado){
        $datos = DB::table('plan')
            ->select('id','descripcion','abreviatura','entidad','archivo')
            ->where('estado',$estado)
            ->orderby('descripcion','asc')
            ->get();

        return $datos;
    }

    public static function listarByCodigo($codigo){
        $datos = DB::table('plan')
            ->select('id','descripcion','abreviatura','entidad','archivo')
            ->where('id',$codigo)
            ->where('estado','1')
            ->first();

        return $datos;
    }

    public function actividades(){
        return $this
            ->belongsToMany(ActividadAcademica::class,'actividad_plan','plan_id','actividad_academica_id')
            ->withPivot('cod_usu')
            ->withTimestamps();
    }
}
