<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActividadResolucion extends Model
{
    use HasFactory;
    protected $table = "actividad_resolucion";
}
