<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class EntidadConvenio extends Model
{
    use HasFactory;
    protected $table = "entidad_externa";

    public function actividades(){
        return $this
            ->belongsToMany(ActividadAcademica::class,'actividad_entidad_convenio','entidad_externa_id','actividad_academica_id')
            ->withPivot('cod_usu')
            ->withTimestamps();;
    }
}
