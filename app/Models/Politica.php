<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Politica extends Model
{
    use HasFactory;
    protected $table = "politica";

    public static function listarByEstado($estado){
        $datos = DB::table('politica')
            ->select('id','descripcion','abreviatura','entidad')
            ->where('estado',$estado)
            ->orderby('descripcion','asc')
            ->get();

        return $datos;
    }

    public static function listarByCodigo($codigo){
        $datos = DB::table('politica')
            ->select('id','descripcion','abreviatura','entidad')
            ->where('id',$codigo)
            ->where('estado','1')
            ->first();

        return $datos;
    }

    public function actividades(){
        return $this
            ->belongsToMany(ActividadAcademica::class,'actividad_politica','politica_id','actividad_academica_id')
            ->withPivot('cod_usu')
            ->withTimestamps();
    }

}
