<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PerfilPersonal extends Model
{
    use HasFactory;
    protected $table="perfil_personal";

    protected $fillable = ['id','descripcion','abreviatura','estado','cod_usuc','cod_usum'];

    public static function listarByEstado($estado){
        $datos = DB::table('perfil_personal')
            ->select('id','descripcion','abreviatura')
            ->where('estado',$estado)
            ->orderby('descripcion','asc')
            ->get();

        return $datos;
    }

    public function actividades(){
        return $this
            ->belongsToMany(ActividadAcademica::class,'actividad_perfil_personal','perfil_personal_id','actividad_academica_id')
            ->withPivot('cod_usu')
            ->withTimestamps();;
    }
}
