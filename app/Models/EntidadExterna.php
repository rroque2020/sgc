<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class EntidadExterna extends Model
{
    use HasFactory;

    protected $table = "entidad_externa";
    protected $fillable = ['id','descripcion','abreviatura','estado','cod_usuc','cod_usum'];

    public static function listarByEstado($estado){
        $datos = DB::table('entidad_externa')
            ->select('id','descripcion','abreviatura')
            ->where('estado',$estado)
            ->orderby('descripcion','asc')
            ->get();

        return $datos;
    }
}
