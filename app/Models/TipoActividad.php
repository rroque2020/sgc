<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class TipoActividad extends Model
{
    use HasFactory;
    protected $table = 'tipo_actividad';
    protected $fillable = ['id','descripcion','abreviatura','estado','cod_usuc','cod_usum'];

    public static function listarByEstado($estado){
        $datos = DB::table('tipo_actividad')
                        ->select('id','descripcion','abreviatura','grupo')
                        ->where('estado',$estado)
                        ->orderby('descripcion','asc')
                        ->get();

        return $datos;
    }
}
