<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ActividadDiscente extends Model
{
    use HasFactory;
    protected $table = "actividad_discente";

    public static function listarByDiscente($discente){
        $data = DB::table('actividad_discente')
            ->select('actividad_academica.codigo','actividad_academica.titulo',
                'actividad_academica.fecha_inicio','actividad_academica.fecha_final','actividad_academica.estado',
                'actividad_discente.certificado, actividad_discente.nota')
            ->join('actividad_academica','actividad_discente.actividad_academica_id','=','actividad_academica.id')
            ->where('actividad_discente.discente_id', $discente)
            ->orderBy('actividad_academica.fecha_final','desc')
            ->get();

        return $data;
    }

//    public static function listarActividadesDesaprobadasDiscente($dni){
//        $data = DB::table('actividad_discente')
//            ->join('discente','actividad_discente.discente_id','=','discente.id')
//            ->join('persona','discente.persona_id','=','persona.id')
//            ->where('persona.dni', $dni)
//            ->whereIn('actividad_discente.estado', [4,5])
//            ->get();
//
//        return $data;
//    }
//
//    public static function listarActividadesParalelas($dni){
//        $data = DB::table('actividad_discente')
//            ->join('discente','actividad_discente.discente_id','=','discente.id')
//            ->join('persona','discente.persona_id','=','persona.id')
//            ->where('persona.dni', $dni)
//            ->where('actividad_discente.estado', 2)
//            ->get();
//
//        return $data;
//    }


}
