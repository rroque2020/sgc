<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DiCapacidad extends Model
{
    use HasFactory;

    protected $table = "di_capacidad";

    public static function listarSesiones($capacidad){
        $data = DB::table('di_sesion')
            ->selectRaw('di_sesion.id, di_sesion.di_capacidad_id, di_sesion.orden, di_sesion.descripcion,
                                di_sesion.duracion, di_sesion.tipo_actividad_estrategica_id,
                                tipo_actividad_estrategica.descripcion as des_tipo_actividad_estrategica, di_sesion.des_actividad_estrategica_id,
                                 di_sesion.recursos_obligatorios, di_sesion.recursos_complementarios,
                                 di_sesion.fecha_hora_inicio, di_sesion.fecha_hora_cierre')
            ->join('tipo_actividad_estrategica','di_sesion.tipo_actividad_estrategica_id','=','tipo_actividad_estrategica.id')
            ->where('di_sesion.di_capacidad_id',$capacidad)
            ->orderBy('di_sesion.orden','asc')
            ->get();

        return $data;
    }

    public static function leerModulo($capacidad){
        $data = DB::table('di_capacidad')
            ->selectRaw('di_modulo.id as modulo_id')
            ->join('di_competencia','di_capacidad.di_competencia_id','=','di_competencia.id')
            ->join('di_modulo','di_competencia.di_modulo_id','=','di_modulo.id')
            ->where('di_capacidad.id',$capacidad)
            ->first();

        return $data;
    }

    public static function listarDatosWord($actividad){
        $data = DB::table('di_modulo')
            ->selectRaw("di_modulo.descripcion AS mod_descripcion,
                                CONCAT('Competencia ', di_competencia.orden) AS comp_orden,
                                competencia_perfil_personal.descripcion AS comp_descripcion,
                                CONCAT('Resultado de aprendizaje ', di_capacidad.orden) AS cap_orden,
                                di_capacidad.descripcion AS cap_descripcion")
            ->leftJoin('di_competencia','di_modulo.id','=','di_competencia.di_modulo_id')
            ->leftJoin('competencia_perfil_personal','di_competencia.competencia_perfil_personal_id','=','competencia_perfil_personal.id')
            ->leftJoin('di_capacidad','di_competencia.id','=','di_capacidad.di_competencia_id')
            ->where('di_modulo.actividad_academica_id',$actividad)
            ->orderBy('di_modulo.item','asc')
            ->orderBy('di_competencia.orden','asc')
            ->orderBy('di_capacidad.orden','asc')
            ->get();

        return $data;
    }

    public function dicompetencia(){
        return $this->belongsTo('App\Models\DiCompetencia','di_competencia_id','id');
    }
}
