
$('#sustentoform').change(function(e){
    var fileName = e.target.files[0].name;
    $('#sustentoforml').html(fileName);
});

$('#sustentocap').change(function(e){
    var fileName = e.target.files[0].name;
    $('#sustentocapl').html(fileName);
});

$('#sustentoexp').change(function(e){
    var fileName = e.target.files[0].name;
    $('#sustentoexpl').html(fileName);
});

$(document).on("submit", ".frmformacion", function (e) {
    e.preventDefault();
    /////////limpiar errores
    var elem = document.getElementById('frmformacion').elements;
    for(var i = 0; i < elem.length; i++)
    {
        $('#' + elem[i].name + '_error').html('');
    }
    ////////////////////////////////////////////////////////////////////////
    var vid = $(this).attr("id");
    var vform = new FormData($("#"+vid+"")[0]);
    vform.append('coddocente', $('#id').val());
    var vtoken = $('meta[name="csrf-token"]').attr('content');
    var vurl = '/perfil/docentes/agregarformacion';
    $.ajax({
        url: vurl,
        type: 'POST',
        headers:{'X-CSRF-TOKEN':vtoken},
        data: vform,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function() {
            $('#mensaje').html($('#cargador1').html());
        },
        complete: function() {
            $('#mensaje').html('');
        },
        success: function (data) {
            $('#listadoformacion').html(data);
            limpiarFormacion();
            msgExito('Se agregó la Formación Académica correctamente...',2500)
        },
        error:function(data) {
            if(data.status == 422){
                msgError('Error, verifique y corrija la información registrada ...',2500)
                var resp_c = data.responseJSON;
                var i = 0;
                var enfocar;
                $.each(resp_c.errors,function(index,value) {
                    i++;
                    if (value.length != 0) {
                        if (i === 1) {
                            enfocar = index;
                        }
                        $('#' + index + '_error').html(value);
                    }

                    $('#' + enfocar).focus();
                });
            }else{
                console.log(data);
                msgError('Error ' + data.status + ': Verifique la información registrada...', 2500);
            }
        }
    });
});

function limpiarFormacion(){
    $('#grado').selectpicker('val', '-1');
    $('#institucion').selectpicker('val', '-1');
    $('#profesion').selectpicker('val', '-1');
    $('#ingreso').val('');
    $('#egreso').val('');
    $('#sustentoform').val('');
    $('.custom-file-label').html('');
}


function eliminarFormacion(codigo) {
    var vform = '_token=' + $('meta[name="csrf-token"]').attr('content') + '&codformacion=' + codigo;

    var vtipo = 'POST';
    var vurl = '/perfil/docentes/eliminarformacion';
    $.ajax({
        type: vtipo,
        url: vurl,
        data: vform ,
        beforeSend: function() {
            $('#mensaje').html($('#cargador1').html());
        },
        complete: function() {
            $('#mensaje').html('');
        },success: function(data){
            $("#listadoformacion").html(data);
            msgExito('Registro eliminado...',2500)
        },error: function(){
            msgError('Ocurrió en error, verifique...',2500)
        }
    });
}

$(document).on("submit", ".frmcapacitacion", function (e) {
    e.preventDefault();
    /////////limpiar errores
    var elem = document.getElementById('frmcapacitacion').elements;
    for(var i = 0; i < elem.length; i++)
    {
        $('#' + elem[i].name + '_error').html('');
    }
    ////////////////////////////////////////////////////////////////////////
    var vid = $(this).attr("id");
    var vform = new FormData($("#"+vid+"")[0]);
    vform.append('coddocente', $('#id').val());
    var vtoken = $('meta[name="csrf-token"]').attr('content');
    var vurl = '/perfil/docentes/agregarcapacitacion';

    $.ajax({
        url: vurl,
        type: 'POST',
        headers:{'X-CSRF-TOKEN':vtoken},
        data: vform,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function() {
            $('#mensaje').html($('#cargador1').html());
        },
        complete: function() {
            $('#mensaje').html('');
        },
        success: function (data) {
            msgExito('Se agrego la capacitación correctamente...',2500)
            $('#listadocapacitacion').html(data);
            limpiarCapacitacion();
            actualizarResumen();
        },
        error:function(data) {
            if(data.status == 422){
                msgError('Error, verifique y corrija la información registrada ...',2500)
                var resp_c = data.responseJSON;
                var i = 0;
                var enfocar;
                $.each(resp_c.errors,function(index,value) {
                    i++;
                    if (value.length != 0) {
                        if (i === 1) {
                            enfocar = index;
                        }
                        $('#' + index + '_error').html(value);
                    }

                    $('#' + enfocar).focus();
                });
            }else{
                msgError('Error ' + data.status + ': Verifique la información registrada...', 2500);
            }
        }
    });
});

function limpiarCapacitacion(){
    $('#tipocap').val('');
    $('#institucioncap').val('');
    $('#especialidadcap').val('');
    $('#ingresocap').val('');
    $('#egresocap').val('');
    $('#horascap').val('');
    $('#sustentocap').val('');
    $('.custom-file-label').html('');
}

function eliminarCapacitacion(codigo) {
    $("#listadocapacitacion").html('');
    var vform = '_token=' + $('meta[name="csrf-token"]').attr('content') + '&codcapacitacion=' + codigo;

    var vtipo = 'POST';
    var vurl = '/perfil/docentes/eliminarcapacitacion';
    $.ajax({
        type: vtipo,
        url: vurl,
        data: vform ,
        beforeSend: function() {
            $('#mensaje').html($('#cargador1').html());
        },
        complete: function() {
            $('#mensaje').html('');
        },
        success: function(data){
            msgExito('Registro eliminado...',2500);
            actualizarResumen();
            $("#listadocapacitacion").html(data);
        },
        error: function(data){
            msgError('Ocurrió en error, verifique...',2500);
        }
    });
}

$(document).on("submit", ".frmexperiencia", function (e) {
    e.preventDefault();
    /////////limpiar errores
    var elem = document.getElementById('frmexperiencia').elements;
    for(var i = 0; i < elem.length; i++)
    {
        $('#' + elem[i].name + '_error').html('');
    }
    ////////////////////////////////////////////////////////////////////////
    var vid = $(this).attr("id");
    var vform = new FormData($("#"+vid+"")[0]);
    vform.append('coddocente', $('#id').val());
    var vtoken = $('meta[name="csrf-token"]').attr('content');
    var vurl = '/perfil/docentes/agregarexperiencia';

    $.ajax({
        url: vurl,
        type: 'POST',
        headers:{'X-CSRF-TOKEN':vtoken},
        data: vform,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function() {
            $('#mensaje').html($('#cargador1').html());
        },
        complete: function() {
            $('#mensaje').html('');
        },
        success: function (data) {
            $('#listadoexperiencia').html(data);
            limpiarExperiencia();
            actualizarResumen();
            msgExito('Se agrego la experiencia correctamente...',2500);
        },
        error:function(data) {
            if(data.status == 422){
                msgError('Error, verifique y corrija la información registrada ...',2500)
                var resp_c = data.responseJSON;
                var i = 0;
                var enfocar;
                $.each(resp_c.errors,function(index,value) {
                    i++;
                    if (value.length != 0) {
                        if (i === 1) {
                            enfocar = index;
                        }
                        $('#' + index + '_error').html(value);
                    }

                    $('#' + enfocar).focus();
                });
            }else{
                msgError('Error ' + data.status + ': Verifique la información registrada...', 2500);
            }
        }
    });
});

function limpiarExperiencia(){
    $('#entidadexp').val('');
    $('#cargoexp').val('');
    $('#areaexp').val('');
    $('#funcionesexp').val('');
    $('#ingresoexp').val('');
    $('#egresoexp').val('');
    $('#sustentoexp').val('');
    $('.custom-file-label').html('');
}

function eliminarExperiencia(codigo) {
    var vform = '_token=' + $('meta[name="csrf-token"]').attr('content') + '&codexperiencia=' + codigo;

    var vtipo = 'POST';
    var vurl = '/perfil/docentes/eliminarexperiencia';
    $.ajax({
        type: vtipo,
        url: vurl,
        data: vform ,
        beforeSend: function() {
            $('#mensaje').html($('#cargador1').html());
        },
        complete: function() {
            $('#mensaje').html('');
        },
        success: function (data){
            msgExito('Registro eliminado...',2500);
            $("#listadoexperiencia").html(data);
            actualizarResumen();
        },
        error: function (data){
            msgError('Ocurrió en error, verifique...',2500);
        }
    });
}

function actualizarResumen(){
    var vurl = "/perfil/resumen";
    $("#resumen").html($("#cargador1").html());
    $.get(vurl, function (resul) {
        $("#resumen").html(resul);
    });
}
