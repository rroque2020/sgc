function frmRegistroPreInscripcionWeb(actividad){
    var vurl = "/registropreinscripcionweb/" + actividad;

    $("#myModal").html($("#cargador1").html());
    $.get(vurl, function (resul) {
        $("#myModal").html(resul);
        $('#myModal').modal({ backdrop: 'static', }).on('shown.bs.modal', function () {
            $('#dni').focus();
        });
    });
}


function listarDatosDNIWeb(cinvitado){
    var dni = $('#dni').val();

    if(dni.length < 8){
        return;
    }
    $("#mensaje").html($("#cargador1").html());
    $('#datosdiscente').html('');
    var vurl = "/listardatosdniweb/" + cinvitado + "/" + dni;
    $.get(vurl, function (data) {
        if(isEmpty(data.apellidos)){
            msgError('No se puede pre-inscribir a la Actividad Académica, consulte con el Coordinador...',2500);
        }else{
            $('#datosdiscente').html(data.apellidos + ' ' + data.nombres);
            $('#apellidos').val(data.apellidos);
            $('#nombres').val(data.nombres);
            $('#invitado').val(data.invitado);
        }
        $("#mensaje").html('');
    });
}

$(document).on("submit", ".frmregistroweb", function (e) {
    e.preventDefault();

    if(isEmpty($('#apellidos').val())){
        msgError('Registre su DNI y espera que el sistema valide sus datos, para que pueda pre-inscribirse...',2500);
        return;
    }

    Swal.fire({
        title: "Pre-Inscripción",
        text: "¿Esta seguro de pre-inscribirse, verifique su Celular y Correo Electrónico correctamente antes de continuar?",
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: "#28A745",
        confirmButtonText: "Si",
        cancelButtonColor: "#DC3545",
        cancelButtonText: "No",
    })
        .then(resultado => {
            if (resultado.value) {
                var vform = $(this).serialize();
                var vtipo = $(this).attr("method");
                var vtoken = $("input[name=_token]").val();
                var vurl = "/preinscribirweb";

                $.ajax({
                    dataType: "json",
                    type: vtipo,
                    url: vurl,
                    headers: {'X-CSRF-TOKEN': vtoken},
                    data: vform,
                    beforeSend: function () {
                        $('#mensaje').html($('#cargador1').html());
                    },
                    complete: function () {
                        $('#mensaje').html('');
                    },
                    success: function (data) {
                        console.log(data);
                        if(!data.valor) {
                            msgError(data.mensaje, 2500);
                        }else{
                            msgExito(data.mensaje,2500);
                            $("#myModal").modal('hide');
                        }
                    },
                    error: function (data) {
                        if (data.status == 422) {
                            msgError('Error, verifique y corrija la información registrada ...', 2500)
                            var resp_c = data.responseJSON;
                            var i = 0;
                            var enfocar;
                            $.each(resp_c.errors, function (index, value) {
                                i++;
                                if (value.length != 0) {
                                    if (i === 1) {
                                        enfocar = index;
                                    }
                                    $('#' + index + '_error').html(value);
                                }

                                $('#' + enfocar).focus();
                            });
                        } else {
                            msgError('Error ' + data.status + ': Verifique la información registrada ...', 2500);
                        }
                    }
                });
            }
        });
});


function actualizarListaPreinscritos(actividad){
    var vurl = "/actividadacademica/listapreinscrito/" + actividad;
    $("#listapreinscritos").html($("#cargador1").html());
    $.get(vurl, function (resul) {
        $("#listapreinscritos").html(resul);
        $('#tblpreinscritos').DataTable({
            "language": {
                "url": "/assets/vendor/datatables/js/dtspanish.json"
            },
            'columnDefs': [{
                'targets': 0,
                'checkboxes': {
                    'selectRow': true
                }
            }],
            'select': {
                'style': 'multi'
            },
            'order': [
                [3, 'asc']
            ],
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todo"]],
            dom: '<"row col-lg-12 col-md-12 col-sm-12 arriba1"B><"arriba2"lrf><"abajo"tip><"clear">',
            //dom: 'Blfrtip',
            text: 'Export',
            buttons: [
                { extend: 'copyHtml5',
                    text: '<i class="fa fa-copy"></i>&nbsp;Copiar',
                    "className": 'btn btn-info btn-sm'},
                { extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>&nbsp;Excel',
                    "className": 'btn btn-success btn-sm'},
                { extend: 'csvHtml5',
                    text: '<i class="fa fa-file-text-o"></i>&nbsp;CSV',
                    "className": 'btn btn-warning btn-sm'},
            ],
        });
    });
}


function enviarCorreoPreinscrito(actividad) {
    var table = $('#tblpreinscritos').DataTable();

    var rows = $(table.rows({
        selected: true
    }).$('input[type="checkbox"]').map(function () {
        return $(this).prop("checked") ? $(this).closest('tr').attr('data-id') : null;
    }));

    var ids = [];
    $.each(rows, function (index, rowId) {
        ids.push(rowId);
    });

    if (ids.length > 0) {
        Swal.fire({
            title: "Pre-Inscritos",
            text: "¿Esta seguro de enviar el correo?",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: "#28A745",
            confirmButtonText: "Si",
            cancelButtonColor: "#DC3545",
            cancelButtonText: "No",
        })
            .then(resultado => {
                if (resultado.value) {
                    var vurl = "/actividadacademica/enviarpreinscritos";
                    var vform = '_token=' + $('meta[name="csrf-token"]').attr('content') + '&ids[]=' + ids + '&codactividad=' + actividad;
                    $.ajax({
                        type: 'POST',
                        url: vurl,
                        data: vform,
                        beforeSend: function () {
                            $('#mensaje').html($('#cargador1').html());
                        },
                        complete: function () {
                            $('#mensaje').html('');
                        },
                        success: function (data) {
                            console.log(data);
                            if (data === 'error') {
                                msgError('Ocurrió un error inesperado, consulte con el Administrador del Sistema...', 2500)
                            } else {
                                msgExito('Se ha enviado correctamente los correos...', 2500);
                                actualizarListaPreinscritos(actividad);
                            }
                        },
                        error: function (data) {
                            console.log(data);
                            msgError('Ocurrió un error inesperado, consulte con el Administrador del Sistema...', 2500)
                        }
                    });
                }
            });
    } else {
        msgInformativo('No hay registros seleccionados...', 2500);
    }
}

function frmRegistroPreInscripcionCoordinador(actividad){
    var vurl = "/registropreinscripcioncoordinador/" + actividad;

    $("#myModal").html($("#cargador1").html());
    $.get(vurl, function (resul) {
        $("#myModal").html(resul);
        $('#myModal').modal({ backdrop: 'static', }).on('shown.bs.modal', function () {
            $('#dni').focus();
        });
    });
}

$(document).on("submit", ".frmregistrocoordinador", function (e) {
    e.preventDefault();

    if(isEmpty($('#apellidos').val())){
        msgError('Registre su DNI y espera que el sistema valide sus datos, para que pueda pre-inscribirse...',2500);
        return;
    }

    Swal.fire({
        title: "Pre-Inscripción",
        text: "¿Esta seguro de pre-inscribirse, verifique su Celular y Correo Electrónico correctamente antes de continuar?",
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: "#28A745",
        confirmButtonText: "Si",
        cancelButtonColor: "#DC3545",
        cancelButtonText: "No",
    })
        .then(resultado => {
            if (resultado.value) {
                var vform = $(this).serialize();
                var vtipo = $(this).attr("method");
                var vtoken = $("input[name=_token]").val();
                var vurl = "/preinscribircoordinador";

                $.ajax({
                    dataType: "json",
                    type: vtipo,
                    url: vurl,
                    headers: {'X-CSRF-TOKEN': vtoken},
                    data: vform,
                    beforeSend: function () {
                        $('#mensaje').html($('#cargador1').html());
                    },
                    complete: function () {
                        $('#mensaje').html('');
                    },
                    success: function (data) {
                        console.log(data);
                        if(!data.valor) {
                            msgError(data.mensaje, 2500);
                        }else{
                            actualizarListaPreinscritos($('#codactividad').val());
                            msgExito(data.mensaje,2500);
                            $("#myModal").modal('hide');
                        }
                    },
                    error: function (data) {
                        if (data.status == 422) {
                            msgError('Error, verifique y corrija la información registrada ...', 2500)
                            var resp_c = data.responseJSON;
                            var i = 0;
                            var enfocar;
                            $.each(resp_c.errors, function (index, value) {
                                i++;
                                if (value.length != 0) {
                                    if (i === 1) {
                                        enfocar = index;
                                    }
                                    $('#' + index + '_error').html(value);
                                }

                                $('#' + enfocar).focus();
                            });
                        } else {
                            msgError('Error ' + data.status + ': Verifique la información registrada ...', 2500);
                        }
                    }
                });
            }
        });
});
