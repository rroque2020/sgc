function onSelectDistritoChange(){
    var parametro = $("#distrito").val();
    var url = '/listarDependencia/' + parametro;
    $.get(url, function (data) {
        var old = $('#dependencia').data('old') !== '' ? $('#dependencia').data('old') : '';
        $('#dependencia').empty();
        $('#dependencia').append('<option value="-1"><< SELECCIONE >></option>');

        $.each(data.dependencias, function(fetch, objeto){
            if(old == objeto.CODI_DEPE_TDE){
                $("#dependencia").append("<option value='"+ objeto.CODI_DEPE_TDE + "' selected>"+ objeto.DESC_DEPE_TDE + "</option>");
            }else{
                $("#dependencia").append("<option value='"+ objeto.CODI_DEPE_TDE + "'>"+ objeto.DESC_DEPE_TDE + "</option>");
            }
        });

        $('#dependencia').selectpicker("refresh");
    });
}

$('#selfoto').on('click', function() {
    $('#foto').trigger('click');
});

$("#foto").change(function(){
    var vfoto = $(this).val();
    if(vfoto != ''){
        $('#frmfoto').closest("form").submit();
    }
});

$(document).on("submit", ".frmfoto", function (e) {
    e.preventDefault();
    var vid = $(this).attr("id");
    var vform = new FormData($("#"+vid+"")[0]);
    vform.append('id', $('#id').val());
    var vtoken = $('meta[name="csrf-token"]').attr('content');
    var vurl = '/perfil/persona/actualizarfoto';
    $.ajax({
        url: vurl,
        type: 'POST',
        headers:{'X-CSRF-TOKEN':vtoken},
        data: vform,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function() {
            $('#mensaje').html($('#cargador1').html());
        },
        complete: function() {
            $('#mensaje').html('');
        },
        success: function (data) {
            $('#perfil-foto').html(data);
            msgExito('Se actualizó la foto correctamente...',2500)
        },
        error:function(data) {
            msgError('Ocurrió un error, verifique si slegiste la foto ...',2500)
        }
    });
});

$(document).on("submit", ".frmdatospersonales", function (e) {
    e.preventDefault();
    /////////limpiar errores
    var elem = document.getElementById('frmdatospersonales').elements;
    for (var i = 0; i < elem.length; i++) {
        $('#' + elem[i].name + '_error').html('');
    }
    ////////////////////////////////////////////////////////////////////////
    var vform = $(this).serialize() ;
    var vtoken = $("input[name=_token]").val();
    var vurl = '/perfil/persona/datospersonales';
    $.ajax({
        type: 'POST',
        url: vurl,
        headers: {'X-CSRF-TOKEN': vtoken},
        data: vform,
        beforeSend: function () {
            $('#mensaje').html($('#cargador1').html());
        },
        complete: function () {
            $('#mensaje').html('');
        },
        success: function (data) {
            $('#info').html(data);
            msgExito('Datos Personales actualizados correctamente ...', 2500);
        },
        error: function (data) {
            if(data.status == 422){
                msgError('Error, verifique y corrija la información registrada ...',2500)
                var resp_c = data.responseJSON;
                var i = 0;
                var enfocar;
                $.each(resp_c.errors,function(index,value) {
                    i++;
                    if (value.length != 0) {
                        if (i === 1) {
                            enfocar = index;
                        }
                        $('#' + index + '_error').html(value);
                    }

                    // $('#' + enfocar).focus();
                });
            }else{
                msgError('Error ' + data.status + ': Verifique la información registrada...', 2500);
            }
        }
    });
});

$("#dependencia").on('changed.bs.select',function(){
    $("#dependenciadet").val($('#distrito :selected').text().trim() + '/' +$('#dependencia :selected').text().trim());
});

$("#cargo").on('changed.bs.select',function(){
    $("#cargodet").val($('#cargo :selected').text().trim());
});
