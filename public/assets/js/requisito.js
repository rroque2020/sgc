function limpiaraa(){
    $('#actividad').selectpicker('val', '-1');
}

function buscardetalleaa(codigo){
    var res = 0;
    $('#tblrequisito td input.codaa').each(function() {
        var cod = $(this).val();
        if(codigo === cod){
            res = 1;
            return false;
        }
    });
    return res;
}

function agregaraa(){
    var codaa = $('#actividad').val();
    var titaa = $('#actividad option:selected').text();

    if(codaa === '-1'){
        msgError('Elija una Actividad Académica...',2500);
        return;
    }

    var res = buscardetalleaa(codaa);
    if(res === 1){
        msgInformativo('Ya ingresó la Actividad Académica, elija otro ...',2500);
        limpiar();
        $('#actividad').focus();
        return;
    }

    if(codaa !== 0 && titaa !== '') {
        var fila = '<tr class="item" id="fila' + codaa + '">';
        fila += '<td style="width:10%;" class="text-center"><input type="hidden" class="codaa" name="codaa[]" value="' + codaa + '">' + codaa + '</td>';
        fila += '<td style="width:80%;" class="text-justify"><input type="hidden" name="titaa[]" value="' + titaa + '">'+titaa+'</td>';
        fila += '<td style="width:10%;"><a class="btn btn-danger btn-xs" onclick="eliminaraa(' + codaa + ')"><i class="la la-trash"></i></a></td>';
        fila += '</tr>';

        $('#tblrequisito').append(fila);

        limpiaraa();
    }else{
        msgError('Error al agregar la Actividad Académica...',2500);
    }
    $('#actividad').focus();
}

function eliminaraa(fila){
    $('#fila'+fila).remove();
}

$(document).on("submit", ".frmrequisito", function (e) {
    e.preventDefault();

    var codaa = $('#actividad').val();
    var titaa = $('#actividad option:selected').text();
    var codact = $('#codactividad').val();

    if(codaa === '-1'){
        msgError('Elija una Actividad Académica...',2500);
        return;
    }

    var res = buscardetalleaa(codaa);
    if(res === 1){
        msgInformativo('Ya ingresó la Actividad Académica, elija otro ...',2500);
        limpiaraa();
        $('#actividad').focus();
        return;
    }

    $("#listadorequisito").html('');
    var vform = $(this).serialize() + '&codactividad=' + codact;
    var vtipo = $(this).attr("method");
    var vurl = '/actividadacademica/agregarrequisito';
    $.ajax({
        type: vtipo,
        url: vurl,
        data: vform ,
        beforeSend: function() {
            $('#mensaje').html($('#cargador1').html());
        },
        complete: function() {
            $('#mensaje').html('');
        }
    })
        .done(function (data) {
            $("#listadorequisito").html(data);
            limpiaraa();
        })
        .fail(function(data){
            console.log(data);
        });
});

function eliminarRequisito(codigo, codaa) {
    $("#listadorequisito").html('');
    var vform = '_token=' + $('meta[name="csrf-token"]').attr('content') + '&actividad=' + codigo + '&codactividad=' + codaa;
    var vtipo = 'POST';
    var vurl = '/actividadacademica/eliminarrequisito';
    $.ajax({
        type: vtipo,
        url: vurl,
        data: vform ,
        beforeSend: function() {
            $('#mensaje').html($('#cargador1').html());
        },
        complete: function() {
            $('#mensaje').html('');
        }
    })
        .done(function (data) {
            $("#listadorequisito").html(data);
        })
        .fail(function(data){
            console.log(data);
        });
}
