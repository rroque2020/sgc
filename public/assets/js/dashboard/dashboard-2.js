(function($) {
   var dlabSettingsOptions = {
		typography: "helvetica",
		version: "light",
		layout: "Vertical",
		headerBg: "color_2",
		navheaderBg: "color_2",
		sidebarBg: "color_13",
		sidebarStyle: "full",
		sidebarPosition: "static",
		headerPosition: "static",
		containerLayout: "full",
		direction: "ltr"
	};

	jQuery(document).ready(function(){
		new dlabSettings(dlabSettingsOptions);

	});

})(jQuery);
