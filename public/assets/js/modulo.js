function listarDetalle(){
    $('#detalle').html($('#cargador1').html());
    var actividad = $("#codactividad").val();
    var url = '/disenioinstruccional/detalle/' + actividad;

    $.get(url, function (data) {
        $('#detalle').html(data);

        $(".modulos").on('show.bs.collapse', function () {
            var id = $(this).attr('id');
            actualizarTablaCompetencia(id.substring(1));
        });
    });
}

function validarEjecucion(codigo) {
    var vurl = "/disenioinstruccional/validarejecucion/" + codigo;
    $("#mensajeo").html($("#cargador1").html());
    let total = 0;
    $.get(vurl, function (resul) {
        total = resul.cont;
    });
    console.log(total);
    return total;
}

function frmModulo(actividad, codigo){
    var vurl = "/disenioinstruccional/modulo/" + actividad;

    if(codigo != 0){
        vurl = "/disenioinstruccional/modulo/" + actividad + "/" + codigo;
    }

    $("#mensaje").html($("#cargador1").html());
    $.get(vurl, function (resul) {
        $("#mensaje").html('');

        if(resul !== 'nova'){
            $("#myModal").html(resul);
            $('#myModal').modal({ backdrop: 'static', }).on('shown.bs.modal', function () {
                $('.mdate').bootstrapMaterialDatePicker({
                    time:false,
                    lang: 'es',
                    format: 'DD/MM/YYYY',
                    //minDate: new Date(),
                    cancelText: 'Cancelar',
                    okText: 'Aceptar',
                    clearText: 'Limpiar',
                    nowText: 'Hoy',
                    clearButton: true
                });
                $('#tipo').focus();
            });

        }else{
            msgError('Primero debe registrar los datos de ejecución',2500);
        }
    });
}

$(document).on("submit", ".frmregistromodulo", function (e) {
    e.preventDefault();

    /////////limpiar errores
    var elem = document.getElementById('frmregistromodulo').elements;
    for(var i = 0; i < elem.length; i++)
    {
        $('#' + elem[i].name + '_error').html('');
    }
    ////////////////////////////////////////////////////////////////////////
    var vform = $(this).serialize();
    var vtipo = $(this).attr("method");
    var vurl = "/disenioinstruccional/modulo/store";
    if(vtipo == 'put') {
        vurl = "/disenioinstruccional/modulo/update/" + $('#id').val();
    }

    $.ajax({
        type: vtipo,
        url: vurl,
        data: vform,
        beforeSend: function() {
            $('#mensaje').html($('#cargador1').html());
        },
        complete: function() {
            $('#mensaje').html('');
        }
    })
    .done(function (data) {
        if(vtipo == 'post'){
            msgExito('Se agregó el módulo correctamente...',2500)
        }else{
            msgExito('Se actualizó el módulo correctamente...',2500)
        }
        // $('#detalle').html(data);
        $("#myModal").modal('hide');
        listarDetalle();
    })
    .fail(function(data){
        if(data.status == 422){
            msgError('Error, verifique y corrija la información registrada ...',2500)
            var resp_c = data.responseJSON;
            var i = 0;
            var enfocar;
            $.each(resp_c.errors,function(index,value) {
                i++;
                if (value.length != 0) {
                    if (i === 1) {
                        enfocar = index;
                    }
                    $('#' + index + '_error').html(value);
                }

                $('#' + enfocar).focus();
            });
        }else{
            msgError('Error ' + data.status + ': Verifique la información registrada...', 2500);
        }
    });
});

function eliminarModulo(codigo) {
   Swal.fire({
        title: "Módulo",
        text: "¿Esta seguro de eliminar el Módulo?",
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: "#28A745",
        confirmButtonText: "Si",
        cancelButtonColor: "#DC3545",
        cancelButtonText: "No",
    })
    .then(resultado => {
        if (resultado.value) {
            $("#detalle").html('');
            var codact = $('#codactividad').val();
            var vform = '_token=' + $('meta[name="csrf-token"]').attr('content');
            var vurl = '/disenioinstruccional/modulo/destroy/' + codigo;

            $.ajax({
                type: 'DELETE',
                url: vurl,
                data: vform ,
                beforeSend: function() {
                    $('#mensaje').html($('#cargador1').html());
                },
                complete: function() {
                    $('#mensaje').html('');
                }
            })
                .done(function (data) {
                    msgExito('Se ha eliminado correctamente...',2500)
                    // $("#detalle").html(data);
                    listarDetalle();
                })
                .fail(function(data){
                    console.log(data);
                });
        }
    });
}

function frmVerCompetencia(codigo) {
    var vurl = "/competencias/" + codigo + "/ver";

    $("#myModal").html($("#cargador1").html());
    $.get(vurl, function (resul) {
        $("#myModal").html(resul);
        $('#myModal').modal().on('shown.bs.modal', function () {
        });
    });
}

function frmCompetencia(modulo, codigo) {
    if(codigo == 0){
        var vurl = "/competencias/" + modulo + "/create";
    }else{
        var vurl = "/competencias/" + codigo + "/edit";
    }

    $("#myModal").html($("#cargador1").html());
    $.get(vurl, function (resul) {

        $("#myModal").html(resul);
        $('#myModal').modal({ backdrop: 'static', }).on('shown.bs.modal', function () {
            $('.selectpicker').selectpicker();
            $('#descripcion').focus();
        });
    });
}

$(document).on("submit", ".frmcompetencia", function (e) {
    e.preventDefault();
    // $('html, body').animate({scrollTop: '0px'}, 200);

    /////////limpiar errores
    var elem = document.getElementById('frmcompetencia').elements;
    for (var i = 0; i < elem.length; i++) {
        $('#' + elem[i].name + '_error').html('');
    }
    ////////////////////////////////////////////////////////////////////////
    var vform = $(this).serialize();
    var vtipo = $(this).attr("method");
    var vtoken = $("input[name=_token]").val();
    if (vtipo == "post") {
        var vurl = "/competencias";
    } else {
        var vurl = "/competencias/" + $("input[name=codcompetencia]").val();
    }

    $.ajax({
        dataType: "json",
        type: vtipo,
        url: vurl,
        headers: {'X-CSRF-TOKEN': vtoken},
        data: vform,
        beforeSend: function () {
            $('#mensaje').html($('#cargador1').html());
        },
        complete: function () {
            $('#mensaje').html('');
        },
        success: function (data) {
            actualizarTablaCompetencia(data.modulo);
            if (vtipo == 'post') {
                msgExito('Se agregó la competencia correctamente...', 2500)
            } else {
                msgExito('Se actualizó la competencia correctamente...', 2500)
            }
            $("#myModal").modal('hide');
        },
        error: function (data) {
            if (data.status == 422) {
                msgError('Error, verifique y corrija la información registrada ...', 2500)
                var resp_c = data.responseJSON;
                var i = 0;
                var enfocar;
                $.each(resp_c.errors, function (index, value) {
                    i++;
                    if (value.length != 0) {
                        if (i === 1) {
                            enfocar = index;
                        }
                        $('#' + index + '_error').html(value);
                    }

                    $('#' + enfocar).focus();
                });
            } else {
                msgError('Error ' + data.status + ': Verifique la información registrada, si ya la registró seleccione otra Competencia ...', 2500);
            }
        }
    });
});

function actualizarTablaCompetencia(modulo) {
    $("#competencia"+modulo).html($('#cargador1').html());

    var vurl = "/competencias/" + modulo ;
    $.get(vurl, function (resul) {
        $("#competencia"+modulo).html(resul);
    });
}

function eliminarCompetencia(modulo,codigo) {
    Swal.fire({
        title: "Competencia",
        text: "¿Esta seguro de eliminar la Competencia?, recuerde que eliminará toda la información relacionada a él",
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: "#28A745",
        confirmButtonText: "Si",
        cancelButtonColor: "#DC3545",
        cancelButtonText: "No",
    })
    .then(resultado => {
        if (resultado.value) {
            var vform = '_token=' + $('meta[name="csrf-token"]').attr('content');
            var vurl = '/competencias/' + codigo;
            $.ajax({
                type: 'DELETE',
                url: vurl,
                data: vform ,
                beforeSend: function() {
                    $('#mensaje').html($('#cargador1').html());
                },
                complete: function() {
                    $('#mensaje').html('');
                }
            })
                .done(function (data) {
                    msgExito('Se ha eliminado correctamente...',2500)
                    actualizarTablaCompetencia(modulo);
                })
                .fail(function(data){
                    console.log(data);
                });
        }
    });
}

function frmCapacidad(competencia, codigo) {
    var vurl = '';
    if(codigo == 0){
        vurl = "/capacidades/" + competencia + "/create";
    }else{
        vurl = "/capacidades/" + codigo + "/edit";
    }

    $("#myModal").html($("#cargador1").html());
    $.get(vurl, function (resul) {

        $("#myModal").html(resul);
        $('#myModal').modal({ backdrop: 'static', }).on('shown.bs.modal', function () {
            $('#orden').focus();
        });
    });
}

$(document).on("submit", ".frmcapacidad", function (e) {
    e.preventDefault();
    // $('html, body').animate({scrollTop: '0px'}, 200);

    /////////limpiar errores
    var elem = document.getElementById('frmcapacidad').elements;
    for(var i = 0; i < elem.length; i++)
    {
        $('#' + elem[i].name + '_error').html('');
    }
    ////////////////////////////////////////////////////////////////////////
    var vform = $(this).serialize();
    var vtipo = $(this).attr("method");
    var vtoken = $("input[name=_token]").val();
    var vurl = '';
    if (vtipo === "post") {
        vurl = "/capacidades";
    }else{
        vurl = "/capacidades/" + $("input[name=codcapacidad]").val();
    }

    $.ajax({
        dataType: "json",
        type: vtipo,
        url: vurl,
        headers:{'X-CSRF-TOKEN':vtoken},
        data: vform,
        beforeSend: function() {
            $('#mensaje').html($('#cargador1').html());
        },
        complete: function() {
            $('#mensaje').html('');
        },
        success: function (data) {
            actualizarCapacidades(data.modulo, data.competencia);
            if (vtipo == 'post') {
                msgExito('Se agregó la capacidad correctamente...', 2500)
            } else {
                msgExito('Se actualizó la capacidad correctamente...', 2500)
            }
            $("#myModal").modal('hide');
        },
        error: function (data) {
            if (data.status == 422) {
                msgError('Error, verifique y corrija la información registrada ...', 2500)
                var resp_c = data.responseJSON;
                var i = 0;
                var enfocar;
                $.each(resp_c.errors, function (index, value) {
                    i++;
                    if (value.length != 0) {
                        if (i === 1) {
                            enfocar = index;
                        }
                        $('#' + index + '_error').html(value);
                    }

                    $('#' + enfocar).focus();
                });
            } else {
                console.log(data);
                msgError('Error ' + data.status + ': Verifique la información registrada, si ya la registró seleccione otra Capacidad ...', 2500);
            }
        }
    })
});

function actualizarCapacidades(modulo, competencia) {
    $('#detcapacidad' + modulo).html($('#cargador1').html());

    var vurl = "/capacidades/" + competencia ;
    $.get(vurl, function (resul) {
        $('#detcapacidad' + modulo).html(resul);
    });
}

function eliminarCapacidad(modulo,codigo) {
    Swal.fire({
        title: "Capacidad",
        text: "¿Esta seguro de eliminar la Capacidad?, recuerde que eliminará toda la información relacionada a él",
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: "#28A745",
        confirmButtonText: "Si",
        cancelButtonColor: "#DC3545",
        cancelButtonText: "No",
    })
    .then(resultado => {
        if (resultado.value) {
            var vform = '_token=' + $('meta[name="csrf-token"]').attr('content');
            var vurl = '/capacidades/' + codigo;

            $.ajax({
                type: 'DELETE',
                url: vurl,
                data: vform ,
                beforeSend: function() {
                    $('#mensaje').html($('#cargador1').html());
                },
                complete: function() {
                    $('#mensaje').html('');
                },
                success: function (data) {
                    msgExito('Se ha eliminado correctamente...',2500)
                    actualizarCapacidades(data.modulo, data.competencia);
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }
    });
}

function frmSesion(capacidad, codigo) {
    var vurl = '';
    if(codigo == 0){
        vurl = "/sesiones/" + capacidad + "/create";
    }else{
        vurl = "/sesiones/" + codigo + "/edit";
    }

    $("#myModal").html($("#cargador1").html());
    $.get(vurl, function (resul) {
        $("#myModal").html(resul);
        $('#myModal').modal({ backdrop: 'static', }).on('shown.bs.modal', function () {
            $('.date-time').bootstrapMaterialDatePicker({
                lang: 'es',
                format: 'DD/MM/YYYY HH:mm   ',
                //minDate: new Date(),
                cancelText: 'Cancelar',
                okText: 'Aceptar',
                clearText: 'Limpiar',
                nowText: 'Hoy'
            });
            $('#orden').focus();
        });
    });
}

$(document).on("submit", ".frmsesion", function (e) {
    e.preventDefault();
    // $('html, body').animate({scrollTop: '0px'}, 200);

    /////////limpiar errores
    var elem = document.getElementById('frmsesion').elements;
    for(var i = 0; i < elem.length; i++)
    {
        $('#' + elem[i].name + '_error').html('');
    }
    ////////////////////////////////////////////////////////////////////////
    var vform = $(this).serialize();
    var vtipo = $(this).attr("method");
    var vtoken = $("input[name=_token]").val();
    var vurl = '';
    if (vtipo === "post") {
        vurl = "/sesiones";
    }else{
        vurl = "/sesiones/" + $("input[name=codsesion]").val();
    }

    $.ajax({
        dataType: "json",
        type: vtipo,
        url: vurl,
        headers:{'X-CSRF-TOKEN':vtoken},
        data: vform,
        beforeSend: function() {
            $('#mensaje').html($('#cargador1').html());
        },
        complete: function() {
            $('#mensaje').html('');
        },
        success: function (data) {
            actualizarSesiones(data.capacidad);
            if (vtipo == 'post') {
                msgExito('Se agregó la sesión correctamente...', 2500)
            } else {
                msgExito('Se actualizó la sesión correctamente...', 2500)
            }
            $("#myModal").modal('hide');
        },
        error: function (data) {
            if (data.status == 422) {
                msgError('Error, verifique y corrija la información registrada ...', 2500)
                var resp_c = data.responseJSON;
                var i = 0;
                var enfocar;
                $.each(resp_c.errors, function (index, value) {
                    i++;
                    if (value.length != 0) {
                        if (i === 1) {
                            enfocar = index;
                        }
                        $('#' + index + '_error').html(value);
                    }

                    $('#' + enfocar).focus();
                });
            } else {
                console.log(data);
                msgError('Error ' + data.status + ': Verifique la información registrada, si ya la registró seleccione otra Sesión ...', 2500);
            }
        }
    })
});

function actualizarSesiones(capacidad) {
    $("#sesiondiv" + capacidad).html($('#cargador1').html());

    var vurl = "/sesiones/" + capacidad ;
    $.get(vurl, function (resul) {
        $("#sesiondiv"+ capacidad).html(resul);
    });
}

function eliminarSesion(capacidad,codigo) {
    Swal.fire({
        title: "Sesión",
        text: "¿Esta seguro de eliminar la Sesión?, recuerde que eliminará toda la información relacionada a él",
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: "#28A745",
        confirmButtonText: "Si",
        cancelButtonColor: "#DC3545",
        cancelButtonText: "No",
    })
    .then(resultado => {
        if (resultado.value) {
            var vform = '_token=' + $('meta[name="csrf-token"]').attr('content');
            var vurl = '/sesiones/' + codigo;

            $.ajax({
                type: 'DELETE',
                url: vurl,
                data: vform ,
                beforeSend: function() {
                    $('#mensaje').html($('#cargador1').html());
                },
                complete: function() {
                    $('#mensaje').html('');
                },
                success: function (data) {
                    msgExito('Se ha eliminado correctamente...',2500)
                    actualizarSesiones(data.capacidad);
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }
    });
}

function frmUnidad(sesion, codigo) {
    var vurl = '';
    if(codigo == 0){
        vurl = "/unidades/" + sesion + "/create";
    }else{
        vurl = "/unidades/" + codigo + "/edit";
    }

    $("#myModal").html($("#cargador1").html());
    $.get(vurl, function (resul) {
        $("#myModal").html(resul);
        $('#myModal').modal({ backdrop: 'static', }).on('shown.bs.modal', function () {
            $('#orden').focus();
        });
    });
}

$(document).on("submit", ".frmunidad", function (e) {
    e.preventDefault();
    // $('html, body').animate({scrollTop: '0px'}, 200);

    /////////limpiar errores
    var elem = document.getElementById('frmunidad').elements;
    for(var i = 0; i < elem.length; i++)
    {
        $('#' + elem[i].name + '_error').html('');
    }
    ////////////////////////////////////////////////////////////////////////
    var vform = $(this).serialize();
    var vtipo = $(this).attr("method");
    var vtoken = $("input[name=_token]").val();
    var vurl = '';
    if (vtipo === "post") {
        vurl = "/unidades";
    }else{
        vurl = "/unidades/" + $("input[name=codunidad]").val();
    }

    $.ajax({
        dataType: "json",
        type: vtipo,
        url: vurl,
        headers:{'X-CSRF-TOKEN':vtoken},
        data: vform,
        beforeSend: function() {
            $('#mensaje').html($('#cargador1').html());
        },
        complete: function() {
            $('#mensaje').html('');
        },
        success: function (data) {
            actualizarUnidades(data.sesion);
            if (vtipo == 'post') {
                msgExito('Se agregó la unidad correctamente...', 2500)
            } else {
                msgExito('Se actualizó la unidad correctamente...', 2500)
            }
            $("#myModal").modal('hide');
        },
        error: function (data) {
            if (data.status == 422) {
                msgError('Error, verifique y corrija la información registrada ...', 2500)
                var resp_c = data.responseJSON;
                var i = 0;
                var enfocar;
                $.each(resp_c.errors, function (index, value) {
                    i++;
                    if (value.length != 0) {
                        if (i === 1) {
                            enfocar = index;
                        }
                        $('#' + index + '_error').html(value);
                    }

                    $('#' + enfocar).focus();
                });
            } else {
                console.log(data);
                msgError('Error ' + data.status + ': Verifique la información registrada, si ya la registró seleccione otra Unidad ...', 2500);
            }
        }
    })
});

function actualizarUnidades(sesion) {
    $("#unidades"+sesion).html($('#cargador1').html());

    var vurl = "/unidades/" + sesion ;
    $.get(vurl, function (resul) {
        $("#unidades"+sesion).html(resul);
    });
}

function eliminarUnidad(sesion, codigo) {
    Swal.fire({
        title: "Unidad",
        text: "¿Esta seguro de eliminar la Unidad?, recuerde que eliminará toda la información relacionada a él",
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: "#28A745",
        confirmButtonText: "Si",
        cancelButtonColor: "#DC3545",
        cancelButtonText: "No",
    })
        .then(resultado => {
            if (resultado.value) {
                var vform = '_token=' + $('meta[name="csrf-token"]').attr('content');
                var vurl = '/unidades/' + codigo;

                $.ajax({
                    type: 'DELETE',
                    url: vurl,
                    data: vform ,
                    beforeSend: function() {
                        $('#mensaje').html($('#cargador1').html());
                    },
                    complete: function() {
                        $('#mensaje').html('');
                    },
                    success: function (data) {
                        msgExito('Se ha eliminado correctamente...',2500)
                        actualizarUnidades(data.sesion);
                    },
                    error: function (data) {
                        console.log(data);
                    }
                });
            }
        });
}

function frmActividadEvaluativa(modulo, codigo) {
    var vurl = '';
    if(codigo == 0){
        vurl = "/actividadesevaluativas/" + modulo + "/create";
    }else{
        vurl = "/actividadesevaluativas/" + codigo + "/edit";
    }

    $("#myModal").html($("#cargador1").html());
    $.get(vurl, function (resul) {
        $("#myModal").html(resul);
        $('#myModal').modal({ backdrop: 'static', }).on('shown.bs.modal', function () {
            $('.date-time').bootstrapMaterialDatePicker({
                lang: 'es',
                format: 'DD/MM/YYYY HH:mm   ',
                //minDate: new Date(),
                cancelText: 'Cancelar',
                okText: 'Aceptar',
                clearText: 'Limpiar',
                nowText: 'Hoy'
            });
            $('#orden').focus();
        });
    });
}

$(document).on("submit", ".frmactividadevaluativa", function (e) {
    e.preventDefault();
    // $('html, body').animate({scrollTop: '0px'}, 200);

    /////////limpiar errores
    var elem = document.getElementById('frmactividadevaluativa').elements;
    for(var i = 0; i < elem.length; i++)
    {
        $('#' + elem[i].name + '_error').html('');
    }
    ////////////////////////////////////////////////////////////////////////
    var vform = $(this).serialize();
    var vtipo = $(this).attr("method");
    var vtoken = $("input[name=_token]").val();
    var vurl = '';
    if (vtipo === "post") {
        vurl = "/actividadesevaluativas";
    }else{
        vurl = "/actividadesevaluativas/" + $("input[name=codactividadevaluativa]").val();
    }

    $.ajax({
        dataType: "json",
        type: vtipo,
        url: vurl,
        headers:{'X-CSRF-TOKEN':vtoken},
        data: vform,
        beforeSend: function() {
            $('#mensaje').html($('#cargador1').html());
        },
        complete: function() {
            $('#mensaje').html('');
        },
        success: function (data) {
            actualizarActividadesEvaluativas(data.modulo);
            if (vtipo == 'post') {
                msgExito('Se agregó la Actividad Evaluativa correctamente...', 2500)
            } else {
                msgExito('Se actualizó la  Actividad Evaluativa correctamente...', 2500)
            }
            $("#myModal").modal('hide');
        },
        error: function (data) {
            if (data.status == 422) {
                msgError('Error, verifique y corrija la información registrada ...', 2500)
                var resp_c = data.responseJSON;
                var i = 0;
                var enfocar;
                $.each(resp_c.errors, function (index, value) {
                    i++;
                    if (value.length != 0) {
                        if (i === 1) {
                            enfocar = index;
                        }
                        $('#' + index + '_error').html(value);
                    }

                    $('#' + enfocar).focus();
                });
            } else {
                console.log(data);
                msgError('Error ' + data.status + ': Verifique la información registrada, si ya la registró seleccione otra Actividad Evaluativa ...', 2500);
            }
        }
    })
});

function actualizarActividadesEvaluativas(modulo) {
    $("#listaactividadesevaluativas"+modulo).html($('#cargador1').html());

    var vurl = "/actividadesevaluativas/" + modulo ;
    $.get(vurl, function (resul) {
        $("#listaactividadesevaluativas"+modulo).html(resul);
    });
}

function eliminarActividadEvaluativa(modulo, codigo) {
    Swal.fire({
        title: "Actividad Evaluativa",
        text: "¿Esta seguro de eliminar la Actividad Evaluativa?, recuerde que eliminará toda la información relacionada a él",
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: "#28A745",
        confirmButtonText: "Si",
        cancelButtonColor: "#DC3545",
        cancelButtonText: "No",
    })
        .then(resultado => {
            if (resultado.value) {
                var vform = '_token=' + $('meta[name="csrf-token"]').attr('content');
                var vurl = '/actividadesevaluativas/' + codigo;

                $.ajax({
                    type: 'DELETE',
                    url: vurl,
                    data: vform ,
                    beforeSend: function() {
                        $('#mensaje').html($('#cargador1').html());
                    },
                    complete: function() {
                        $('#mensaje').html('');
                    },
                    success: function (data) {
                        msgExito('Se ha eliminado correctamente...',2500)
                        actualizarActividadesEvaluativas(data.modulo);
                    },
                    error: function (data) {
                        console.log(data);
                    }
                });
            }
        });
}

function aprobarDisenio(codigo, estado){
    var vform = '_token=' + $('meta[name="csrf-token"]').attr('content') + '&codactividad=' + codigo + '&estado=' + estado;
    var vurl = '/actividadacademica/aprobardisenio'
    $.ajax({
        dataType: "json",
        type: 'POST',
        url: vurl,
        data: vform,
        beforeSend: function() {
            $('#mensaje').html($('#cargador1').html());
        },
        complete: function() {
            $('#mensaje').html('');
        }
    })
        .done(function (data) {
            msgExito('Diseño Instruccional aprobado correctamente ...', 2500);
            window.location.href = "/actividadacademica/opciones/" + codigo;
            // $('#estado').html(data.htmlestado);
        })
        .fail(function(error){
            console.log(error);
            msgError('Verifique los datos ingresados  ...', 2500);
        });
}
