function esFloat(evt)
{
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31
        && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function esNumerico(evt)
{
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode < 48 || charCode > 57)
        return false;

    return true;
}

function obtenerAnio(fecha)
{
    //var anio = (new Date(fecha)).getFullYear();
    var anio = fecha.substring(6);

    return anio;
}

function nombreDia(dia){
    var nombre;
    switch(dia) {
        case '1':
            nombre = "Lunes";
            break;
        case '2':
            nombre = "Martes";
            break;
        case '3':
            nombre = "Miércoles";
            break;
        case '4':
            nombre = "Jueves";
            break;
        case '5':
            nombre = "Viernes";
            break;
        case '6':
            nombre = "Sábado";
            break;
        default:
            nombre = "Domingo";
    }

    return nombre;
}

function listarDatos(){
    var dni = $('#dni').val();

    if(dni.length < 8){
        return;
    }
    $("#mensaje").html($("#cargador1").html());
    var vurldni = "/listarDatos/" + dni;
    $.get(vurldni, function (data) {
        $('#apellidos').val(data.apellidoPaterno + ' ' + data.apellidoMaterno);
        $('#nombres').val(data.nombres);
    });

    var vurlexpositor = "/listarDatosExpositor/" + dni;
    $.get(vurlexpositor, function (data) {
        $('#correo').val(data.DES_EMAIL);
        $("#mensaje").html('');
    });
}

function isEmpty(str) {
    return (!str || str.length === 0 );
}
