$('#politica').on('change', function(){
    var url = '/politica/' + $(this).val();
    $.get(url, function (data) {
        $('#hentpolitica').val(data.politica.entidad);
    });
});

function limpiarpolitica(){
    $('#politica').selectpicker('val', '-1');
}

function buscardetallepolitica(codigo){
    var res = 0;
    $('#tblpolitica td input.codpolitica').each(function() {
        var cod = $(this).val();
        if(codigo === cod){
            res = 1;
            return false;
        }
    });
    return res;
}

function agregarpolitica(){
    var codpolitica = $('#politica').val();
    var despolitica = $('#politica option:selected').text();

    if(codpolitica === '-1'){
        msgError('Elija una Política...',2500);
        return;
    }

    var res = buscardetallepolitica(codpolitica);
    if(res === 1){
        msgInformativo('Ya ingresó la Política, elija otro ...',2500);
        limpiarpolitica();
        $('#politica').focus();
        return;
    }

    var hentpolitica = $('#hentpolitica').val();
    if(codpolitica !== 0 && despolitica !== '') {
        var fila = '<tr class="item" id="filapolitica' + codpolitica + '">';
        fila += '<td style="width:10%;" class="text-center"><input type="hidden" class="codpolitica" name="codpolitica[]" value="' + codpolitica + '">' + codpolitica + '</td>';
        fila += '<td style="width:35%;" class="text-justify"><input type="hidden" name="despolitica[]" value="' + despolitica + '">'+ despolitica +'</td>';
        fila += '<td style="width:35%;" class="text-justify"><input type="hidden" name="entpolitica[]" value="' + hentpolitica + '">' + hentpolitica + '</td>';
        fila += '<td style="width:10%;" class="text-center"><a class="btn btn-danger btn-xs" onclick="eliminarpolitica(' + codpolitica + ')"><i class="la la-trash"></i></a></td>';
        fila += '</tr>';

        $('#tblpolitica').append(fila);

        limpiarpolitica();
    }else{
        msgError('Error al agregar la Política...',2500);
    }
    $('#politica').focus();
}

function eliminarpolitica(fila){
    $('#filapolitica'+fila).remove();
}

$(document).on("submit", ".frmpolitica", function (e) {
    e.preventDefault();

    var codpolitica = $('#politica').val();
    var despolitica = $('#politica option:selected').text();
    var codact = $('#codactividad').val();

    if(codpolitica === '-1'){
        msgError('Elija una Política...',2500);
        return;
    }

    var res = buscardetallepolitica(codpolitica);
    if(res === 1){
        msgInformativo('Ya ingresó la Política, elija otro ...',2500);
        limpiarpolitica();
        $('#politica').focus();
        return;
    }

    $("#listadopolitica").html('');
    var vform = $(this).serialize() + '&codactividad=' + codact;
    var vtipo = $(this).attr("method");
    var vurl = '/actividadacademica/agregarpolitica';
    $.ajax({
        type: vtipo,
        url: vurl,
        data: vform ,
        beforeSend: function() {
            $('#mensaje').html($('#cargador1').html());
        },
        complete: function() {
            $('#mensaje').html('');
        }
    })
        .done(function (data) {
            $("#listadopolitica").html(data);
            limpiarpolitica();
        })
        .fail(function(data){
            console.log(data);
        });
});

function eliminarpoliticas(codigo, codaa) {
    $("#listadopolitica").html('');
    var vform = '_token=' + $('meta[name="csrf-token"]').attr('content') + '&politica=' + codigo + '&codactividad=' + codaa;

    var vtipo = 'POST';
    var vurl = '/actividadacademica/eliminarpolitica';
    $.ajax({
        type: vtipo,
        url: vurl,
        data: vform ,
        beforeSend: function() {
            $('#mensaje').html($('#cargador1').html());
        },
        complete: function() {
            $('#mensaje').html('');
        }
    })
        .done(function (data) {
            $("#listadopolitica").html(data);
        })
        .fail(function(data){
            console.log(data);
        });
}
