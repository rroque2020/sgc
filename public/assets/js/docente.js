function actualizardocente() {
    var vurl = "/listarDocente";
    $("#mensaje").html($("#cargador1").html());
    $.get(vurl, function (resul) {
        $("#mensaje").html('');
        $("#listado").html(resul);

        $('#tbldocente').DataTable({
            "language": {
                "url": "/assets/vendor/datatables/js/dtspanish.json"
            },
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todo"]],
            dom: '<"row col-lg-12 col-md-12 col-sm-12 arriba1"B><"arriba2"lrf><"abajo"tip><"clear">',
            //dom: 'Blfrtip',
            text: 'Export',
            buttons: [
                { extend: 'copyHtml5',
                    text: '<i class="fa fa-copy"></i>&nbsp;Copiar',
                    "className": 'btn btn-info btn-sm'},
                { extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>&nbsp;Excel',
                    "className": 'btn btn-success btn-sm'},
                { extend: 'csvHtml5',
                    text: '<i class="fa fa-file-text-o"></i>&nbsp;CSV',
                    "className": 'btn btn-warning btn-sm'},
            ],
        });
    });
}

$(function(){
    $("#distritoi").on('change',onSelectDistritoChange);
    $('#tipo').on('change',onToggleTipoChange);
});

function onSelectDistritoChange(){
    var parametro = $("#distritoi").val();
    var url = '/listarDependencia/' + parametro;
    $.get(url, function (data) {
        var old = $('#dependenciai').data('old') !== '' ? $('#dependenciai').data('old') : '';
        $('#dependenciai').empty();
        $('#dependenciai').append('<option value="-1"><< SELECCIONE >></option>');

        $.each(data.dependencias, function(fetch, objeto){
            if(old == objeto.CODI_DEPE_TDE){
                $("#dependenciai").append("<option value='"+ objeto.CODI_DEPE_TDE + "' selected>"+ objeto.DESC_DEPE_TDE + "</option>");
            }else{
                $("#dependenciai").append("<option value='"+ objeto.CODI_DEPE_TDE + "'>"+ objeto.DESC_DEPE_TDE + "</option>");
            }
        });

        $('#dependenciai').selectpicker("refresh");
    });
}

function onToggleTipoChange(){
    var parametro = $('#tipo').prop('checked');

    if(parametro === true){
        $('#interno').show();
        $('#externo').hide();
    }else{
        $('#interno').hide();
        $('#externo').show();
    }
}

$("#dependenciai").on('changed.bs.select',function(){
    $("#dependenciadet").val($('#distritoi :selected').text().trim() + '/' +$('#dependenciai :selected').text().trim());
});

$("#cargoi").on('changed.bs.select',function(){
    $("#cargodet").val($('#cargoi :selected').text().trim());
});
