$(document).on("submit", ".frmbusqueda", function (e) {
    e.preventDefault();
    $("#listado").html('');
    var vfini = $('#busquedarango-btn').data('daterangepicker').startDate.format('YYYY-MM-DD');
    var vffin = $('#busquedarango-btn').data('daterangepicker').endDate.format('YYYY-MM-DD');
    var vtoken = $("input[name=_token]").val();
    var vform = $(this).serialize() + '&inicio=' + vfini + '&final=' + vffin + '&' + vtoken;
    var vtipo = $(this).attr("method");
    var vurl = '/buscarActividadAcademica';
    $.ajax({
        type: vtipo,
        url: vurl,
        data: vform ,
        beforeSend: function() {
            $('#mensaje').html($('#cargador1').html());
        },
        complete: function() {
            $('#mensaje').html('');
        }
    })
    .done(function (data) {
        $("#listado").html(data);
        $('#tblactividad').DataTable({
            "language": {
                "url": "/assets/vendor/datatables/js/dtspanish.json"
            },
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todo"]],
            dom: '<"row col-lg-12 col-md-12 col-sm-12 arriba1"B><"arriba2"lrf><"abajo"tip><"clear">',
            //dom: 'Blfrtip',
            text: 'Export',
            buttons: [
                { extend: 'copyHtml5',
                    text: '<i class="fa fa-copy"></i>&nbsp;Copiar',
                    "className": 'btn btn-info btn-sm'},
                { extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>&nbsp;Excel',
                    "className": 'btn btn-success btn-sm'},
                { extend: 'csvHtml5',
                    text: '<i class="fa fa-file-text-o"></i>&nbsp;CSV',
                    "className": 'btn btn-warning btn-sm'},
            ],
        });
    })
    .fail(function(data){
        console.log(data);
    });
});

function limpiarBusqueda(){
    $('#busquedarango-btn span').html(moment().startOf('year').format('DD/MM/YYYY') + ' - ' + moment().endOf('year').format('DD/MM/YYYY'));

    $('#busquedarango-btn').data('daterangepicker').setStartDate(moment().startOf('year'));
    $('#busquedarango-btn').data('daterangepicker').setEndDate(moment().endOf('year'));

    $('#programado').selectpicker('val', '-1');
    $('#subgerencia').selectpicker('val', '-1');
    $('#tipo').selectpicker('val', '-1');
    $('#modalidad').selectpicker('val', '-1');
    $('#sistema').selectpicker('val', '-1');
    $('#tematica').selectpicker('val', '-1');
    $('#origen').selectpicker('val', '-1');
    $('#linea').selectpicker('val', '-1');
    $('#bloque').selectpicker('val', '-1');
    $('#estado').selectpicker('val', '-1');
    $("#listado").html('');
}

$(function(){
    $("#modalidad").on('change',onSelectModalidadChange);
    $("#linea").on('change',onSelectLineaChange);
    $("#origen").on('change',onSelectOrigenChange);
    $('#convenio').on('change',onToggleConvenioChange);
});

function onSelectModalidadChange(){
    var parametro = $("#modalidad").val();

    if(parametro !== '3' && parametro !== '-1'){
        $('#distrito-sede').show();
    }else{
        $('#distrito-sede').hide();
    }
}

function onSelectLineaChange(){
    var parametro = $("#linea").val();
    var url = '/listarBloque/' + parametro;

    $.get(url, function (data) {
        var old = $('#bloque').data('old') !== '' ? $('#bloque').data('old') : '';

        $('#bloque').empty();
        $('#bloque').append('<option value="-1"><< SELECCIONE >></option>');

        $.each(data.bloques, function(fetch, objeto){
            $("#bloque").append("<option value='"+ objeto.id +"'" + (old === objeto.id ? 'selected' : '') + ">"+ objeto.descripcion + "</option>");
        })

        $('#bloque').selectpicker("refresh");
    });
}

function onSelectOrigenChange(){
    var parametro = $("#origen").val();
    if(parametro === 'EMP' || parametro === '-1'){
        $('#cooperanteorg').hide();
        $('#distritoorg').hide();
        $('#cooperante').selectpicker('val', '-1');
        $('#distritofiscal').selectpicker('val', '-1');
    }

    if(parametro === 'DISTRITO FISCAL'){
        $('#cooperanteorg').hide();
        $('#distritoorg').show();
        $('#cooperante').selectpicker('val', '-1');
    }

    if(parametro === 'COOPERANTE'){
        $('#cooperanteorg').show();
        $('#distritoorg').hide();
        $('#distritofiscal').selectpicker('val', '-1');
    }
}



function onToggleConvenioChange(){
    var parametro = $('#convenio').prop('checked');

    if(parametro === true){
        $('#ent-convenio').show();
        $('#cod-convenio').show();
    }else{
        $('#ent-convenio').hide();
        $('#cod-convenio').hide();
    }
}

function actualizaraa(){
    limpiarBusqueda();
    $('#frmbusqueda').submit();
}
