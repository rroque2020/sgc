function frmRegistroEVA(codigo, eva) {
    if(eva !== 0){
        Swal.fire({
            title: "Registro EVA",
            text: 'Ya fué registrado la Actividad Académica, con el código: ' + eva + ", ¿Desea eliminar el Registro EVA?",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: "#28A745",
            confirmButtonText: "Si",
            cancelButtonColor: "#DC3545",
            cancelButtonText: "No",
        })
            .then(resultado => {
                if (resultado.value) {
                    deshacerRegistro(codigo);
                }
            });
        // msgInformativo('Curso ya fué registrado, con el código: ' + eva, 2500);
        return;
    }

    var vurl = "/eva/" + codigo;
    $("#mensajeo").html($("#cargador1").html());
    $.get(vurl, function (resul) {
        $("#myModal").html(resul);
        $("#mensajeo").html('');
        $('#myModal').modal({ backdrop: 'static', }).on('shown.bs.modal', function () {
            $('#categoria').focus();
        });
    });
}

function deshacerRegistro(codigo){
    var vform = '_token=' + $('meta[name="csrf-token"]').attr('content') + '&codactividad=' + codigo;
    var vurl = '/eva/deshacer'
    $.ajax({
        dataType: "json",
        type: 'POST',
        url: vurl,
        data: vform,
        beforeSend: function() {
            $('#mensaje').html($('#cargador1').html());
        },
        complete: function() {
            $('#mensaje').html('');
        }
    })
    .done(function (data) {
        msgExito('Se realizó correctamente la operación ...', 2500);
        window.location.href = "/actividadacademica/opciones/" + codigo;
    })
    .fail(function(error){
        console.log(error);
    });
}

$(document).on("submit", ".frmregistroeva", function (e) {
    e.preventDefault();
    // $('html, body').animate({scrollTop: '0px'}, 200);

    var codigo = $('#codactividad').val();

    /////////limpiar errores
    var elem = document.getElementById('frmregistroeva').elements;
    for(var i = 0; i < elem.length; i++)
    {
        $('#' + elem[i].name + '_error').html('');
    }
    ////////////////////////////////////////////////////////////////////////
    var vform = $(this).serialize();
    var vtipo = $(this).attr("method");
    var vurl = "/eva/registrarcurso";
    $.ajax({
        dataType: "json",
        type: vtipo,
        url: vurl,
        data: vform,
        beforeSend: function() {
            $('#mensaje').html($('#cargador1').html());
        },
        complete: function() {
            $('#mensaje').html('');
        }
    })
    .done(function (data) {
        console.log(data);
        msgExito('Curso registrado correctamente ...', 2500);

        $("#myModal").modal('hide');
        window.location.href = "/actividadacademica/opciones/" + codigo;
    })
    .fail(function(error){
        console.log(error);
        msgError('Verifique los datos ingresados  ...', 2500);
    });
});
