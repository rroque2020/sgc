function buscarIdTableHorario(id){
    var retorno = 0;
    $("#tblhorario tbody tr").each(function() {
        if(this.id === id){
            retorno = 1;
        }
    });
    return retorno;
}

function eliminarFilaTabla(id){
    $('#' + id).remove();
}

function sortTable(){
    var rows = $('#tblhorario tbody  tr').get();

    rows.sort(function(a, b) {

        var A = $(a).children('td').eq(0).text().toUpperCase();
        var B = $(b).children('td').eq(0).text().toUpperCase();

        if(A < B) {
            return -1;
        }

        if(A > B) {
            return 1;
        }

        return 0;

    });

    $.each(rows, function(index, row) {
        $('#tblhorario').children('tbody').append(row);
    });
}

function generarTablaHorario(){
    if($("#dia :selected").length <= 0){
        msgError("Seleccione al menos 1 días",2500);
        return 0;
    }

    if ($('#horaini').val()=== '') {
        msgError("Seleccione la hora de inicio",2500);
        return 0;
    }

    if ($('#horafin').val() === '') {
        msgError("Seleccione la hora final",2500);
        return 0;
    }

    if ($('#horafin').val() <= $('#horaini').val()) {
        msgError("La hora final debe ser mayor, que la hora inicial",2500);
        return 0;
    }

    var dias = $('#dia').val();
    var fila = "";
    for(var i=0; i < dias.length; i++){
        var busqueda = buscarIdTableHorario(dias[i]);

        if( busqueda === 0){
            fila +="<tr id='" + dias[i] + "'>";
            fila +="<td>" +dias[i] + "</td>";
            fila +="<td>" + nombreDia(dias[i]) + "</td>";
            fila +="<td>" + $('#horaini').val() + "</td>";
            fila +="<td>" + $('#horafin').val() + "</td>";
            fila +="<td><a class='btn btn-danger btn-xs' onclick='eliminarFilaTabla(" + dias[i] + ")'><i class='la la-trash'></i></a></td>";
            fila +="</tr>";
        }
    }
    $('#tblhorario tbody').append(fila);

    $('#dia').selectpicker('deselectAll');

    $('#horaini').val('');
    $('#horafin').val('');

    sortTable();
}

function genHorario(){
    var horarios = [];

    var horariosj = [];
    $("#tblhorario tbody tr").each(function(i,n) {
        var $row = $(n);
        horarios[i] = $row.find('td:eq(1)').text() + ", de: " + $row.find('td:eq(2)').text() + " a: " + $row.find('td:eq(3)').text();

        horariosj.push({
            codigo: $row.find('td:eq(0)').text(),
            dia: $row.find('td:eq(1)').text(),
            horaini: $row.find('td:eq(2)').text(),
            horafin: $row.find('td:eq(3)').text(),
            accion: $row.find('td:eq(4)').text()
        });
    });

    $('#horariodetallado').val(horarios.join("; "));
    $('#horariojdetallado').val(JSON.stringify(horariosj));

    $("#frmhorario").modal('hide');
}
