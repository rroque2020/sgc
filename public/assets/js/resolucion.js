$('#resolucion').on('change', function(){
    var url = '/resolucion/' + $(this).val();
    $.get(url, function (data) {
        $('#harcresolucion').val(data.resolucion.archivo);
    });
});

function limpiarresolucion(){
    $('#resolucion').selectpicker('val', '-1');
}

function buscardetalleresolucion(codigo){
    var res = 0;
    $('#tblresolucion td input.codresolucion').each(function() {
        var cod = $(this).val();
        if(codigo === cod){
            res = 1;
            return false;
        }
    });
    return res;
}

function agregarresolucion(){
    var codresolucion = $('#resolucion').val();
    var desresolucion = $('#resolucion option:selected').text();

    if(codresolucion === '-1'){
        msgError('Elija una Resolución ...',2500);
        return;
    }

    var res = buscardetalleresolucion(codresolucion);
    if(res === 1){
        msgInformativo('Ya ingresó la Resolución, elija otro ...',2500);
        limpiarresolucion();
        $('#resolucion').focus();
        return;
    }

    var harcresolucion = $('#harcresolucion').val();
    if(codresolucion !== 0 && desresolucion !== '') {
        var fila = '<tr class="item" id="filaresolucion' + codresolucion + '">';
        fila += '<td style="width:10%;" class="text-center"><input type="hidden" class="codresolucion" name="codresolucion[]" value="' + codresolucion + '">' + codresolucion + '</td>';
        fila += '<td style="width:70%;" class="text-justify"><input type="hidden" name="desresolucion[]" value="' + desresolucion + '">'+ desresolucion +'</td>';
        fila += '<td style="width:10%;" class="text-center"><input type="hidden" name="arcresolucion[]" value="' + harcresolucion + '">' + harcresolucion + '</td>';
        fila += '<td style="width:10%;" class="text-center"><a class="btn btn-danger btn-xs" onclick="eliminarresolucion(' + codresolucion + ')"><i class="la la-trash"></i></a></td>';
        fila += '</tr>';

        $('#tblresolucion').append(fila);

        limpiarresolucion();
    }else{
        msgError('Error al agregar la Resolución ...',2500);
    }
    $('#resolucion').focus();
}

function eliminarresolucion(fila){
    $('#filaresolucion'+fila).remove();
}

$(document).on("submit", ".frmresolucion", function (e) {
    e.preventDefault();

    var codresolucion = $('#resolucion').val();
    var desresolucion = $('#resolucion option:selected').text();
    var codact = $('#codactividad').val();

    if(codresolucion === '-1'){
        msgError('Elija una Resolución ...',2500);
        return;
    }

    var res = buscardetalleresolucion(codresolucion);
    if(res === 1){
        msgInformativo('Ya ingresó la Resolución, elija otro ...',2500);
        limpiarresolucion();
        $('#resolucion').focus();
        return;
    }

    $("#listadoresolucion").html('');
    var vform = $(this).serialize() + '&codactividad=' + codact;
    var vtipo = $(this).attr("method");
    var vurl = '/actividadacademica/agregarresolucion';
    $.ajax({
        type: vtipo,
        url: vurl,
        data: vform ,
        beforeSend: function() {
            $('#mensaje').html($('#cargador1').html());
        },
        complete: function() {
            $('#mensaje').html('');
        }
    })
        .done(function (data) {
            $("#listadoresolucion").html(data);
            limpiarresolucion();
        })
        .fail(function(data){
            console.log(data);
        });
});

function eliminarResol(codigo, codaa) {
    $("#listadoresolucion").html('');
    var vform = '_token=' + $('meta[name="csrf-token"]').attr('content') + '&resolucion=' + codigo + '&codactividad=' + codaa;

    var vtipo = 'POST';
    var vurl = '/actividadacademica/eliminarresolucion';
    $.ajax({
        type: vtipo,
        url: vurl,
        data: vform ,
        beforeSend: function() {
            $('#mensaje').html($('#cargador1').html());
        },
        complete: function() {
            $('#mensaje').html('');
        }
    })
        .done(function (data) {
            $("#listadoresolucion").html(data);
        })
        .fail(function(data){
            console.log(data);
        });
}
