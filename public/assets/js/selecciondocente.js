$(document).on("submit", ".frmbusquedadocente", function (e) {
    e.preventDefault();

    var vform = $(this).serialize();
    var vtipo = $(this).attr("method");
    var vtoken = $("input[name=_token]").val();
    var vurl = "/buscardocente";
    $.ajax({
        type: vtipo,
        url: vurl,
        headers:{'X-CSRF-TOKEN':vtoken},
        data: vform,
        beforeSend: function() {
            $('#mensaje').html($('#cargador1').html());
        },
        complete: function() {
            $('#mensaje').html('');
        },
        success: function (data) {
            $('#listabuscada').html(data);
        },
        error: function (data) {
            console.log(data);
        }
    })
});

function selecciondocente(){
    var docentes=[];
    $("#tbldocentelista input[type=checkbox]:checked").each(function () {
        var fila = $(this).closest("tr")[0];
         docentes.push(fila.cells[4].innerHTML);
    });

    if(docentes.length == 0){
        msgError('Debe seleccionar al menos 1 docente...',2500);
        return;
    }

    let unique = [...new Set(docentes)];
    var vform = '_token=' + $('meta[name="csrf-token"]').attr('content') + '&codactividad=' + $('#codactividad').val() + '&coddocente[]=' + unique;
    var vurl = "/guardarseleccion";
    $.ajax({
        type: 'POST',
        url: vurl,
        data: vform,
        beforeSend: function() {
            $('#mensaje').html($('#cargador1').html());
        },
        complete: function() {
            $('#mensaje').html('');
        },
        success: function (data) {
            if(data == 'nada'){
                msgError('Ya fué seleccionado el Docente',2500);
            }else{
                msgExito('Docente seleccionado correctamente',2500);
                $('#listaseleccionada').html(data);
            }
        },
        error: function (data) {
            console.log(data);
        }
    });
}


function eliminardocente(estado, codigo, actividad){
    if(estado == 2){
        msgInformativo('El correo ya fué enviado, se espera confirmación, no puede ser eliminado ...',2500);
        return;
    }else {
        if (estado == 3) {
            msgInformativo('El docente ya ha confirmado, no puede ser eliminado...', 2500);
            return;
        } else {
            Swal.fire({
                title: "Selección Docente",
                text: "¿Esta seguro de eliminar el Docente?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: "#28A745",
                confirmButtonText: "Si",
                cancelButtonColor: "#DC3545",
                cancelButtonText: "No",
            })
            .then(resultado => {
                if (resultado.value) {
                    var vurl = "/eliminarseleccion";
                    var vform = '_token=' + $('meta[name="csrf-token"]').attr('content') + '&id=' + codigo + '&codactividad=' + actividad;
                    $.ajax({
                        type: 'POST',
                        url: vurl,
                        data: vform,
                        beforeSend: function() {
                            $('#mensaje').html($('#cargador1').html());
                        },
                        complete: function() {
                            $('#mensaje').html('');
                        },
                        success: function (data) {
                            msgExito('Docente eliminado correctamente',2500);
                            $('#listaseleccionada').html(data);
                        },
                        error: function (data) {
                            console.log(data);
                        }
                    });
                }
            });
        }
    }
}

function enviarcorreo(id, estado, actividad, docente){
    if(estado == 2){
        msgInformativo('El correo ya fué enviado, se espera confirmación...',2500);
        return;
    }else{
        if(estado == 3){
            msgInformativo('El docente ya ha confirmado y se le ha enviado el Diseño Instruccional ...',2500);
            return;
        }else{
            Swal.fire({
                title: "Selección Docente",
                text: "¿Esta seguro de enviar el correo, solicitando confirmación?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: "#28A745",
                confirmButtonText: "Si",
                cancelButtonColor: "#DC3545",
                cancelButtonText: "No",
            })
            .then(resultado => {
                if (resultado.value) {
                    var vurl = "/enviarcorreodocente";
                    var vform = '_token=' + $('meta[name="csrf-token"]').attr('content') + '&id=' + id + '&coddocente=' + docente + '&codactividad=' + actividad;
                    $.ajax({
                        type: 'POST',
                        url: vurl,
                        data: vform,
                        beforeSend: function() {
                            $('#mensaje').html($('#cargador1').html());
                        },
                        complete: function() {
                            $('#mensaje').html('');
                        },
                        success: function (data) {
                            if(data == 'error'){
                                msgError('Ocurrió un error inesperado, consulte con el Administrador del Sistema...',2500)
                            }else{
                                msgExito('Correo enviado correctamente',2500);
                                $('#listaseleccionada').html(data);
                            }
                        },
                        error: function (data) {
                            console.log(data);
                            msgError('Ocurrió un error inesperado, consulte con el Administrador del Sistema...',2500)
                        }
                    });
                }
            });
        }
    }
}
