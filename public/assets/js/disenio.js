
function frmRegistroEjecucion(codigo) {
    var vurl = "/disenioinstruccional/registroejecucion/" + codigo;
    $("#mensajeo").html($("#cargador1").html());
    $.get(vurl, function (resul) {
        $("#myModal").html(resul);
        $("#mensajeo").html('');
        $('#myModal').modal({ backdrop: 'static', }).on('shown.bs.modal', function () {
            $('.mdate').bootstrapMaterialDatePicker({
                time:false,
                lang: 'es',
                format: 'DD/MM/YYYY',
                //minDate: new Date(),
                cancelText: 'Cancelar',
                okText: 'Aceptar',
                clearText: 'Limpiar',
                nowText: 'Hoy',
                clearButton: true
            });
            // $('#fechainicioe').focus();
        });
    });
}


$(document).on("submit", ".frmregistroejecucion", function (e) {
    e.preventDefault();

    /////////limpiar errores
    var elem = document.getElementById('frmregistroejecucion').elements;
    for(var i = 0; i < elem.length; i++)
    {
        $('#' + elem[i].name + '_error').html('');
    }
    ////////////////////////////////////////////////////////////////////////
    var vform = $(this).serialize();
    var vtipo = $(this).attr("method");
    var vurl = "/disenioinstruccional/registroejecucion";
    $.ajax({
        type: vtipo,
        url: vurl,
        data: vform,
        beforeSend: function() {
            $('#mensaje').html($('#cargador1').html());
        },
        complete: function() {
            $('#mensaje').html('');
        },
        success: function (data) {
            $('#ejecucion').html(data);
            msgExito('Se actualizó los datos de ejecución correctamente...',2500)
            $("#myModal").modal('hide');
        },
        error:function(data) {
            if(data.status == 422){
                msgError('Error, verifique y corrija la información registrada ...',2500)
                var resp_c = data.responseJSON;
                var i = 0;
                var enfocar;
                $.each(resp_c.errors,function(index,value) {
                    i++;
                    if (value.length != 0) {
                        if (i === 1) {
                            enfocar = index;
                        }
                        $('#' + index + '_error').html(value);
                    }

                    $('#' + enfocar).focus();
                });
            }else{
                console.log(data);
                msgError('Error ' + data.status + ': Verifique la información registrada...', 2500);
            }
        }
    });
});

function frmRegistroCriterioDiscente(codigo, opcion) {
    var vurl = "/disenioinstruccional/registrocriteriodiscente/" + codigo + "/create";
    if(opcion == 1){
        vurl = "/disenioinstruccional/registrocriteriodiscente/" + codigo + "/edit";
    }

    $("#mensajeo").html($("#cargador1").html());
    $.get(vurl, function (resul) {
        $("#myModal").html(resul);
        $("#mensajeo").html('');
        $('#myModal').modal({ backdrop: 'static', }).on('shown.bs.modal', function () {
            $('.selectpicker').selectpicker();
            $('#dni').focus();
        });
    });
}


$(document).on("submit", ".frmregistrocriterio", function (e) {
    e.preventDefault();

    /////////limpiar errores
    var elem = document.getElementById('frmregistrocriterio').elements;
    for(var i = 0; i < elem.length; i++)
    {
        $('#' + elem[i].id + '_error').html('');
    }
    ////////////////////////////////////////////////////////////////////////
    var vform = $(this).serialize();
    var vtipo = $(this).attr("method");
    var vurl = "/disenioinstruccional/registrocriteriodiscente";
    if(vtipo == 'put'){
        vurl = "/disenioinstruccional/registrocriteriodiscente/" + $("input[name=id]").val();
    }
    $.ajax({
        type: vtipo,
        url: vurl,
        data: vform,
        beforeSend: function() {
            $('#mensaje').html($('#cargador1').html());
        },
        complete: function() {
            $('#mensaje').html('');
        },
        success: function (data) {
            $('#criteriodiscente').html(data);
            if(vtipo == 'post'){
                msgExito('Se agregó los datos de criterio correctamente...',2500)
            }else{
                msgExito('Se actualizó los datos de criterio correctamente...',2500)
            }
            $("#myModal").modal('hide');
        },
        error:function(data) {
            if(data.status == 422){
                msgError('Error, verifique y corrija la información registrada ...',2500)
                var resp_c = data.responseJSON;
                var i = 0;
                var enfocar;
                $.each(resp_c.errors,function(index,value) {
                    i++;
                    if (value.length != 0) {
                        if (i === 1) {
                            enfocar = index;
                        }
                        $('#' + index + '_error').html(value);
                    }

                    $('#' + enfocar).focus();
                });
            }else{
                console.log(data);
                msgError('Error ' + data.status + ': Verifique la información registrada...', 2500);
            }
        }
    });
});
