$('#plan').on('change', function(){
    var url = '/plan/' + $(this).val();
    $.get(url, function (data) {
        $('#hentplan').val(data.plan.entidad);
        $('#harcplan').val(data.plan.archivo);
    });
});

function limpiarplan(){
    $('#plan').selectpicker('val', '-1');
}

function buscardetalleplan(codigo){
    var res = 0;
    $('#tblplan td input.codplan').each(function() {
        var cod = $(this).val();
        if(codigo === cod){
            res = 1;
            return false;
        }
    });
    return res;
}

function agregarplan(){
    var codplan = $('#plan').val();
    var desplan = $('#plan option:selected').text();

    if(codplan === '-1'){
        msgError('Elija un Plan...',2500);
        return;
    }

    var res = buscardetalleplan(codplan);
    if(res === 1){
        msgInformativo('Ya ingresó el Plan, elija otro ...',2500);
        limpiarplan();
        $('#plan').focus();
        return;
    }

    var hentplan = $('#hentplan').val();
    var harcplan = $('#harcplan').val();
    if(codplan !== 0 && desplan !== '') {
        var fila = '<tr class="item" id="filaplan' + codplan + '">';
        fila += '<td style="width:10%;" class="text-center"><input type="hidden" class="codplan" name="codplan[]" value="' + codplan + '">' + codplan + '</td>';
        fila += '<td style="width:35%;" class="text-justify"><input type="hidden" name="desplan[]" value="' + desplan + '">'+ desplan +'</td>';
        fila += '<td style="width:35%;" class="text-justify"><input type="hidden" name="entplan[]" value="' + hentplan + '">' + hentplan + '</td>';
        fila += '<td style="width:10%;" class="text-center"><input type="hidden" name="arcplan[]" value="' + harcplan + '">' + harcplan + '</td>';
        fila += '<td style="width:10%;" class="text-center"><a class="btn btn-danger btn-xs" onclick="eliminarplan(' + codplan + ')"><i class="la la-trash"></i></a></td>';
        fila += '</tr>';

        $('#tblplan').append(fila);

        limpiarplan();
    }else{
        msgError('Error al agregar el Plan...',2500);
    }
    $('#plan').focus();
}

function eliminarplan(fila){
    $('#filaplan'+fila).remove();
}

$(document).on("submit", ".frmplan", function (e) {
    e.preventDefault();

    var codplan = $('#plan').val();
    var desplan = $('#plan option:selected').text();
    var codact = $('#codactividad').val();

    if(codplan === '-1'){
        msgError('Elija un Plan...',2500);
        return;
    }

    var res = buscardetalleplan(codplan);
    if(res === 1){
        msgInformativo('Ya ingresó el Plan, elija otro ...',2500);
        limpiarplan();
        $('#plan').focus();
        return;
    }

    $("#listadoplan").html('');
    var vform = $(this).serialize() + '&codactividad=' + codact;
    var vtipo = $(this).attr("method");
    var vurl = '/actividadacademica/agregarplan';
    $.ajax({
        type: vtipo,
        url: vurl,
        data: vform ,
        beforeSend: function() {
            $('#mensaje').html($('#cargador1').html());
        },
        complete: function() {
            $('#mensaje').html('');
        }
    })
        .done(function (data) {
            $("#listadoplan").html(data);
            limpiarplan();
        })
        .fail(function(data){
            console.log(data);
        });
});

function eliminarplanes(codigo, codaa) {
    $("#listadoplan").html('');
    var vform = '_token=' + $('meta[name="csrf-token"]').attr('content') + '&plan=' + codigo + '&codactividad=' + codaa;

    var vtipo = 'POST';
    var vurl = '/actividadacademica/eliminarplan';
    $.ajax({
        type: vtipo,
        url: vurl,
        data: vform ,
        beforeSend: function() {
            $('#mensaje').html($('#cargador1').html());
        },
        complete: function() {
            $('#mensaje').html('');
        }
    })
        .done(function (data) {
            $("#listadoplan").html(data);
        })
        .fail(function(data){
            console.log(data);
        });
}
