<?php

use App\Http\Controllers\ActividadAcademicaController;
use App\Http\Controllers\ActividadDocenteController;
use App\Http\Controllers\ApiDNIController;
use App\Http\Controllers\DiActividadEvaluativaController;
use App\Http\Controllers\DiSesionController;
use App\Http\Controllers\DiUnidadController;
use App\Http\Controllers\Perfil\PerfilDiscenteController;
use App\Http\Controllers\Perfil\PerfilDocenteController;
use App\Http\Controllers\Perfil\PerfilHomeController;
use App\Http\Controllers\Perfil\PerfilPersonaController;
use App\Http\Controllers\PreInscripcionController;
use App\Http\Controllers\PreInscritoController;
use App\Http\Controllers\RegistroDiscenteController;
use App\Http\Controllers\SeleccionDiscenteController;
use App\Http\Controllers\TDependenciaController;
use App\Http\Controllers\DiCapacidadController;
use App\Http\Controllers\DiCompetenciaController;
use App\Http\Controllers\DiModuloController;
use App\Http\Controllers\DisenioInstruccionalController;
use App\Http\Controllers\DocenteController;
use App\Http\Controllers\EvaController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\BloqueAcademicoController;
use App\Http\Controllers\PlanController;
use App\Http\Controllers\PoliticaController;
use App\Http\Controllers\ResolucionController;
use App\Http\Controllers\TExpositorController;
use Illuminate\Support\Facades\Route;


Route::group(['middleware' => ['guest']], function () {
    Route::get('/recepcionarconfirmacion/{hash}', [ActividadDocenteController::class,'confirmar']);

    Route::get('/registropreinscripcionweb/{codigo}', [PreInscripcionController::class,'registroweb']);
    Route::get('/listardatosdniweb/{invitado}/{codigo}', [PreInscripcionController::class,'listardatosdniweb']);
    Route::post('/preinscribirweb', [PreInscripcionController::class, 'preinscribirweb']);



});

Route::get('/', [HomeController::class, 'index']);
Route::get('/actividadacademica', [ActividadAcademicaController::class, 'index'])->name('actividad.index');

Route::get('/actividadacademica/opciones/{codigo}', [ActividadAcademicaController::class, 'option'])->name('actividad.option');
Route::get('/actividadacademica/editar/{codigo}', [ActividadAcademicaController::class, 'edit'])->name('actividad.edit');
Route::get('/actividadacademica/ver/{codigo}', [ActividadAcademicaController::class, 'show'])->name('actividad.show');
Route::get('/actividadacademica/nuevo', [ActividadAcademicaController::class, 'create']);
Route::post('/buscarActividadAcademica', [ActividadAcademicaController::class, 'buscar']);
Route::post('/actividadacademica', [ActividadAcademicaController::class, 'store'])->name('actividad.store');
Route::put('/actividadacademica/{codigo}', [ActividadAcademicaController::class, 'update'])->name('actividad.update');
Route::post('/actividadacademica/agregarrequisito', [ActividadAcademicaController::class, 'storerequisito']);
Route::post('/actividadacademica/eliminarrequisito', [ActividadAcademicaController::class, 'destroyrequisito']);
Route::post('/actividadacademica/agregarresolucion', [ActividadAcademicaController::class, 'storeresolucion']);
Route::post('/actividadacademica/eliminarresolucion', [ActividadAcademicaController::class, 'destroyresolucion']);
Route::post('/actividadacademica/agregarplan', [ActividadAcademicaController::class, 'storeplan']);
Route::post('/actividadacademica/eliminarplan', [ActividadAcademicaController::class, 'destroyplan']);
Route::post('/actividadacademica/agregarplan', [ActividadAcademicaController::class, 'storeplan']);
Route::post('/actividadacademica/eliminarpolitica', [ActividadAcademicaController::class, 'destroypolitica']);
Route::post('/actividadacademica/agregarpolitica', [ActividadAcademicaController::class, 'storepolitica']);
Route::post('/actividadacademica/asignarcoordinador', [ActividadAcademicaController::class, 'assigncoordinator']);
Route::post('/actividadacademica/aprobardisenio', [ActividadAcademicaController::class, 'aprobardisenio']);

Route::get('/actividadacademica/horario/{fechaini}/{fechafin}', [ActividadAcademicaController::class, 'schedule'])->name('actividad.horario');
Route::get('/actividadacademica/seleccionarDocente/{id}', [ActividadDocenteController::class, 'index'])->name('actividad.seleccionardocente');

Route::get('/actividadacademica/preinscrito/{id}', [PreInscripcionController::class, 'index'])->name('actividad.preinscrito');
Route::get('/actividadacademica/listapreinscrito/{id}', [PreInscripcionController::class, 'listar']);
Route::post('/actividadacademica/enviarpreinscritos', [PreInscripcionController::class, 'mailmasivo']);
Route::get('/registropreinscripcioncoordinador/{codigo}', [PreInscripcionController::class,'registrocoordinador']);
Route::post('/preinscribircoordinador', [PreInscripcionController::class, 'preinscribircoordinador']);

Route::get('/actividadacademica/selecciondiscentes/{id}', [SeleccionDiscenteController::class, 'index'])->name('actividad.seleccion');
Route::get('/actividadacademica/listaseleccionado/{id}', [SeleccionDiscenteController::class, 'listar']);
Route::post('/actividadacademica/convocarpostulante', [SeleccionDiscenteController::class,'convocar']);
Route::get('/registropreinscripcionconvocatoria/{codigo}', [SeleccionDiscenteController::class,'registroconvocatoria']);
Route::get('/listardatosdniconvocatoria/{actividad}/{codigo}', [SeleccionDiscenteController::class,'listardatosconvocatoria']);
Route::post('/preinscribirconvocatoria', [SeleccionDiscenteController::class, 'preinscribirconvocatoria']);

Route::get('/disenioinstruccional/{codigo}', [DisenioInstruccionalController::class, 'index'])->name('disenio.index');
Route::get('/disenioinstruccional/detalle/{codigo}', [DisenioInstruccionalController::class, 'detalle'])->name('disenio.detail');
Route::get('/disenioinstruccional/export/{codigo}', [DisenioInstruccionalController::class, 'export'])->name('disenio.export');
Route::get('/disenioinstruccional/exportword/{codigo}', [DisenioInstruccionalController::class, 'exportword'])->name('disenio.exportword');
Route::get('/disenioinstruccional/registroejecucion/{codigo}',[DisenioInstruccionalController::class,'registroejecucion']);
Route::post('/disenioinstruccional/registroejecucion',[DisenioInstruccionalController::class,'actualizarejecucion']);
Route::get('/disenioinstruccional/registrocriteriodiscente/{codigo}/create',[DisenioInstruccionalController::class,'createcriteriodiscente']);
Route::get('/disenioinstruccional/registrocriteriodiscente/{codigo}/edit',[DisenioInstruccionalController::class,'editcriteriodiscente']);
Route::post('/disenioinstruccional/registrocriteriodiscente',[DisenioInstruccionalController::class,'storecriteriodiscente']);
Route::put('/disenioinstruccional/registrocriteriodiscente/{codigo}',[DisenioInstruccionalController::class,'updatecriteriodiscente']);

Route::get('/disenioinstruccional/modulo/{actividad}',[DiModuloController::class,'create']);
Route::get('/disenioinstruccional/modulo/{actividad}/{codigo}',[DiModuloController::class,'edit']);
Route::post('/disenioinstruccional/modulo/store',[DiModuloController::class,'store']);
Route::put('/disenioinstruccional/modulo/update/{codigo}',[DiModuloController::class,'update']);
Route::delete('/disenioinstruccional/modulo/destroy/{codigo}',[DiModuloController::class,'destroy']);

Route::get('/competencias/{modulo}/create',[DiCompetenciaController::class,'create']);
Route::get('/competencias/{competencia}/ver',[DiCompetenciaController::class,'ver']);
Route::resource('competencias', DiCompetenciaController::class,['except'=>['index','create']]);
Route::get('/capacidades/{competencia}/create',[DiCapacidadController::class,'create']);
Route::resource('capacidades', DiCapacidadController::class,['except'=>['index','create']]);
Route::get('/sesiones/{capacidad}/create',[DiSesionController::class,'create']);
Route::resource('sesiones', DiSesionController::class,['except'=>['index','create']]);
Route::get('/unidades/{sesion}/create',[DiUnidadController::class,'create']);
Route::resource('unidades', DiUnidadController::class,['except'=>['index','create']]);
Route::get('/actividadesevaluativas/{modulo}/create',[DiActividadEvaluativaController::class,'create']);
Route::resource('actividadesevaluativas', DiActividadEvaluativaController::class,['except'=>['index','create']]);

Route::get('/listarBloque/{linea}', [BloqueAcademicoController::class, 'listarByLinea']);

Route::get('/plan/{codigo}', [PlanController::class, 'listarDatos']);
Route::get('/politica/{codigo}', [PoliticaController::class, 'listarDatos']);
Route::get('/resolucion/{codigo}', [ResolucionController::class, 'listarDatos']);

Route::get('/eva/{codigo}',[EvaController::class,'mostrarformulario']);
Route::post('/eva/registrarcurso',[EvaController::class,'storecurso']);
Route::post('/eva/deshacer',[EvaController::class,'deshacerRegistro']);

Route::post('/buscardocente', [DocenteController::class,'buscar']);
Route::post('/guardarseleccion', [ActividadDocenteController::class,'store']);
Route::post('/eliminarseleccion', [ActividadDocenteController::class,'destroy']);
Route::post('/enviarcorreodocente', [ActividadDocenteController::class,'mail']);
Route::resource('docentes', DocenteController::class,['except' => ['destroy']]);

Route::get('/listarDocente', [DocenteController::class, 'listarDocente']);
Route::get('/listarDatos/{dni}', [ApiDNIController::class, 'getDatos']);
Route::get('/listarDatosExpositor/{dni}', [TExpositorController::class, 'listarByDNI']);
Route::get('/listarDependencia/{distrito}', [TDependenciaController::class, 'listarByDistrito']);

Route::get('/competenciastest/{id}', [DiModuloController::class, 'estructura']);


/*PERFIL*/
Route::get('/perfil', [PerfilHomeController::class, 'index']);
Route::post('/perfil/persona/actualizarfoto',[PerfilPersonaController::class,'actualizarfoto']);
Route::post('/perfil/persona/datospersonales',[PerfilPersonaController::class,'actualizardatos']);

Route::get('/perfil/resumen', [PerfilHomeController::class, 'resumen']);
Route::post('/perfil/docentes/agregarformacion',[PerfilDocenteController::class,'storeformacion']);
Route::post('/perfil/docentes/eliminarformacion',[PerfilDocenteController::class,'destroyformacion']);
Route::post('/perfil/docentes/agregarcapacitacion',[PerfilDocenteController::class,'storecapacitacion']);
Route::post('/perfil/docentes/eliminarcapacitacion',[PerfilDocenteController::class,'destroycapacitacion']);
Route::post('/perfil/docentes/agregarexperiencia',[PerfilDocenteController::class,'storeexperiencia']);
Route::post('/perfil/docentes/eliminarexperiencia',[PerfilDocenteController::class,'destroyexperiencia']);

Route::get('/perfil/discentes/resumen', [PerfilDiscenteController::class, 'resumen']);

