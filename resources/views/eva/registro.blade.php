<div class="modal-dialog modal-dialog-centered" role="document" data-keyboard="false" data-backdrop="static">
    <div class="modal-content">
        <div class="modal-header head-modal">
            <h5 class="modal-title text-white">Registrar en el Entorno Virtual de Aprendizaje</h5>
            <button type="button" class="close" data-dismiss="modal"><span>x</span>
            </button>
        </div>
        <form id="frmregistroeva" class="frmregistroeva" method="post">
            <div class="modal-body">
                @csrf
                <input type="hidden" id="codactividad" name="codactividad" value="{{ $codactividad }}">
                <div id="mensaje"></div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="categoria">Categoría</label>
                            <select id="categoria" name="categoria" class="form-control" data-live-search="true" tabindex="1">
                                <option value="-1"><< SELECCIONE >></option>
                                @foreach($categorias as $categoria)
                                    <option value="{{ $categoria->id }}">{{ $categoria->name }}</option>
                                @endforeach
                            </select>
                            <div id="categoria_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-cancelar" data-dismiss="modal"><i class="la la-times-circle-o"></i> Cancelar</button>
                <button type="submit" class="btn btn-aceptar"><i class="fa fa-save"></i> Guardar</button>
            </div>
        </form>
    </div>
</div>
