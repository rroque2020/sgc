<div class="modal-dialog modal-dialog-centered" role="document" data-keyboard="false" data-backdrop="static">
    <div class="modal-content">
        <div class="modal-header head-modal">
            <h5 class="modal-title text-white">{{ isset($item) ? 'Editar Competencia : '.$item->id : 'Crear una Competencia'}}</h5>
            <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
            </button>
        </div>
        <form id="frmcompetencia" class="frmcompetencia" method="{{ isset($item)? 'put':'post' }}">
            <div class="modal-body">
                @csrf
                @if(isset($item))
                    @method('PUT')
                @endif
                <input type="hidden" id="codcompetencia" name="codcompetencia" value="{{ isset($item)? $item->id: '' }}">
                <input type="hidden" id="codmodulo" name="codmodulo" value="{{ isset($item) ? $item->di_modulo_id : $modulo }}">
                <div id="mensaje"></div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="orden">Seleccione el Nro. de Competencia</label>
                            <select id="orden" name="orden" class="form-control" data-live-search="true">
                                <option value="-1"><< SELECCIONE >></option>
                                @for ($i = 0; $i < 10; $i++)
                                    <option value="{{ $i + 1  }}" {{ isset($item) && $item->orden == ($i + 1) ? 'selected' : '' }}>{{ $i + 1 }}</option>
                                @endfor
                            </select>
                            <div id="orden_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="competencia">Seleccione competencia</label>
                            <select  style="width: 150px;" id="competencia" name="competencia" class="form-control selectpicker" data-live-search="true">
                                @foreach($tipos as $tipo)
                                    <optgroup label="{{ $tipo->tipo }}">
                                    @foreach($competencias as $competencia)
                                        @if($competencia->tipo == $tipo->tipo)
                                            <option {{ isset($item) && $item->competencia_perfil_personal_id == $competencia->id ? 'selected' : '' }}
                                                    title="{{ $competencia->codigo.' - '.$competencia->descripcion }}"
                                                    value="{{ $competencia->id }}">{{ $competencia->codigo.' - '.substr($competencia->descripcion, 0, 80).'...' }}</option>
                                    @endif
                                    @endforeach
                                @endforeach
                            </select>
                            <div id="competencia_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-cancelar" data-dismiss="modal"><i class="la la-times-circle-o"></i> Cancelar</button>
                <button type="submit" class="btn btn-aceptar"><i class="fa fa-save"></i> Guardar</button>
            </div>
        </form>
    </div>
</div>
