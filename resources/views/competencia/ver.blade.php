<div class="modal-dialog modal-dialog-centered" role="document" data-keyboard="false" data-backdrop="static">
    <div class="modal-content">
        <div class="modal-header head-modal">
            <h5 class="modal-title text-white">{{ 'Competencia N° '.$item->orden }}</h5>
            <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group">
                        <label class="form-label " for="orden"><strong>Código</strong></label>
                        <p class="text-justify subrayado">{{ $item->codigo }}
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group">
                        <label class="form-label" for="orden"><strong>Tipo</strong></label>
                        <p class="text-justify subrayado">{{ $item->tipo }}
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group">
                        <label class="form-label" for="orden"><strong>Competencia</strong></label>
                        <p class="text-justify subrayado">{{ $item->descripcion }}
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-cancelar" data-dismiss="modal"><i class="la la-times-circle-o"></i> Cerrar</button>
        </div>
    </div>
</div>
