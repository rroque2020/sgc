<div class="modal-dialog modal-dialog-centered modal-xl" role="document" data-keyboard="false" data-backdrop="static">
    <div class="modal-content">
        <div class="modal-header head-modal">
            <h5 class="modal-title text-white">{{ isset($item) ? 'Editar Sesión : '.$item->id : 'Crear Sesión'}}</h5>
            <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
            </button>
        </div>
        <form id="frmsesion" class="frmsesion" method="{{ isset($item)? 'put':'post' }}">
            <div class="modal-body">
                @csrf
                @if(isset($item))
                    @method('PUT')
                @endif
                <input type="hidden" id="codcapacidad" name="codcapacidad" value="{{ isset($item) ? $item->di_capacidad_id : $capacidad }}">
                <input type="hidden" id="codsesion" name="codsesion" value="{{ isset($item) ? $item->id : '' }}">
                <div id="mensaje"></div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="orden">Seleccione Nro. de Sesión</label>
                            <select id="orden" name="orden" class="form-control" data-live-search="true">
                                <option value="-1"><< SELECCIONE >></option>
                                @for ($i = 0; $i < 10; $i++)
                                    <option value="{{ $i + 1  }}" {{ isset($item) && $item->orden == ($i + 1) ? 'selected' : '' }}>{{ $i + 1 }}</option>
                                @endfor
                            </select>
                            <div id="orden_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="idestrategia">Tipo de Actividad Pedagógica</label>
                            .<select id="idestrategia" name="idestrategia" class="form-control" data-live-search="true">
                                <option value="-1"><< SELECCIONE >></option>
                                @foreach($estrategias as $estrategia)
                                    <option value="{{ $estrategia->id }}" {{ (isset($item) && $item->tipo_actividad_estrategica_id == $estrategia->id ? 'selected' : '') }} >{{ $estrategia->descripcion }}</option>
                                @endforeach
                            </select>
                            <div id="idestrategia_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="descripcion">Nombre de la Sesión</label>
                            <textarea id="descripcion" name="descripcion" class="form-control"  rows="4">{{ isset($item) ? $item->descripcion : '' }}</textarea>
                            <div id="descripcion_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="desestrategia">Actividad Pedagógica</label>
                            <textarea id="desestrategia" name="desestrategia" class="form-control"  rows="4">{{ isset($item) ? $item->des_actividad_estrategica_id : '' }}</textarea>
                            <div id="desestrategia_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="duracion">Duración</label>
                            <div class="input-group mb-3">
                                <input type="number" id="duracion" name="duracion" min="0" class="form-control" value="{{ isset($item) ? $item->duracion : '' }}">
                                <div class="input-group-append">
                                    <span class="input-group-text"> &nbsp;HORAS ACADÉMICAS</span>
                                </div>
                            </div>
                            <div id="duracion_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="fechahorainicio">Fecha y Hora de Inicio</label>
                            <input type="text" id="fechahorainicio" name="fechahorainicio" class="form-control date-time"
                                   value="{{ isset($item->fecha_hora_inicio) ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$item->fecha_hora_inicio)->format('d/m/Y H:i') : ''}}">
                            <div id="fechahorainicio_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="fechahoracierre">Fecha y Hora de Cierre</label>
                            <input type="text" id="fechahoracierre" name="fechahoracierre" class="form-control date-time"
                                   value="{{ isset($item->fecha_hora_cierre) ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$item->fecha_hora_cierre)->format('d/m/Y H:i') : ''}}">
                            <div id="fechahoracierre_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="recursosobligatorios">Recursos Obligatorios (Listar)</label>
                            <textarea id="recursosobligatorios" name="recursosobligatorios" class="form-control"  rows="4">{{ isset($item) ? $item->recursos_obligatorios : '' }}</textarea>
                            <div id="recursosobligatorios_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="recursoscomplementarios">Recursos Complementarios (Listar) - Opcional</label>
                            <textarea id="recursoscomplementarios" name="recursoscomplementarios" class="form-control"  rows="4">{{ isset($item) ? $item->recursos_complementarios : '' }}</textarea>
                            <div id="recursoscomplementarios_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-cancelar" data-dismiss="modal"><i class="la la-times-circle-o"></i> Cancelar</button>
                <button type="submit" class="btn btn-aceptar"><i class="fa fa-save"></i> Guardar</button>
            </div>
        </form>
    </div>
</div>
d7
