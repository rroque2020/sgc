@if(!isset($sesiones) || count($sesiones) == 0)
    <div class='informado'>
        <label style='color:#0055A0'>... No se encontraron registros ...</label>
    </div>
@else
    <div class="row mt-2">
        <div class="col-lg-12 col-md-12 col-xs-12">
            <div id="accordion-sesion" class="accordion">
                @foreach($sesiones as $sesion)
                    <div class="accordion__item">
                        <div class="accordion__header cabecera-warning-accordion" data-toggle="collapse" data-target="#ses{{ $sesion->id }}">
                            <span class="accordion__header--text">Sesión N° {{ $sesion->orden }}</span>
                            <span class="accordion__header--indicator"></span>
                            <a class="text-info" onclick="frmSesion({{ $sesion->di_capacidad_id }}, {{ $sesion->id }})"
                               data-toggle="tooltip" data-placement="top" title="Editar Sesión">
                                (<i class="la la-pencil"></i>)
                            </a>
                            <a class="text-info" onclick="eliminarSesion({{ $sesion->di_capacidad_id }}, {{ $sesion->id }})"
                               data-toggle="tooltip" data-placement="top" title="Eliminar Sesión">
                                (<i class="la la-trash"></i>)
                            </a>
                        </div>
                        <div id="ses{{ $sesion->id }}" class="sesiones collapse accordion__body" data-parent="#accordion-sesion">
                            <div class="accordion__body--text">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <label class="form-label font-weight-bold" for="descripcion">Nombre de la Sesión</label>
                                            <p class="text-justify subrayado">
                                                {{ $sesion->descripcion }}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="card mb-0" style="height: auto !important;">
                                            <div class="card-header bg-dark text-white text-center py-2" style="display: block !important;">
                                                <strong>CONTENIDO DE LA SESIÓN</strong>
                                            </div>
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 mb-2  ">
                                                        <div class="pull-right">
                                                            <a class="btn btn-rounded btn-dark btn-xs" onclick="actualizarUnidades({{ $sesion->id }})"
                                                               data-toggle="tooltip" data-placement="top" title="Actualizar Unidades Temáticas">
                                                            <span class="btn-icon-left text-dark">
                                                                <i class="fa fa-refresh color-primary"></i>
                                                            </span>Actualizar Unidades Temáticas
                                                            </a>
                                                            &nbsp;&nbsp;
                                                            <a class="btn btn-rounded btn-dark btn-xs" onclick="frmUnidad({{ $sesion->id }},0)"
                                                               data-toggle="tooltip" data-placement="top" title="Nueva Unidad Temática">
                                                            <span class="btn-icon-left text-dark">
                                                                <i class="fa fa-plus color-primary"></i>
                                                            </span>Nueva Unidad Temática
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div id="unidades{{ $sesion->id }}" class="col-lg-12 col-md-12 col-sm-12">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <label class="form-label font-weight-bold" for="duracion">Duración</label>
                                            <p class="text-justify subrayado">
                                                {{ $sesion->duracion }} Horas Académicas
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <label class="form-label font-weight-bold" for="estrategia">Tipo de Actividad Pedagógica</label><br>
                                            <p class="text-justify subrayado">
                                                {{ $sesion->des_tipo_actividad_estrategica }}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <label class="form-label font-weight-bold" for="estrategia">Actividad Pedagógica</label><br>
                                            <p class="text-justify subrayado">
                                                {{ $sesion->des_actividad_estrategica_id }}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <label class="form-label font-weight-bold" for="estrategia">Recursos Obligatorios</label>
                                            <p class="text-justify subrayado">
                                                {{ $sesion->recursos_obligatorios }}
                                            </p>
                                        </div>
                                    </div>
                                    @if(isset($sesion->recursos_complementarios))
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <div class="form-group">
                                                <label class="form-label font-weight-bold" for="estrategia">Recursos Complementarios</label>
                                                <p class="text-justify subrayado">
                                                    {{ $sesion->recursos_complementarios }}
                                                </p>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="form-label font-weight-bold" for="estrategia">Fecha y Hora de Inicio</label>
                                            <p class="text-justify subrayado">
                                                {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sesion->fecha_hora_inicio)->format('d/m/Y H:i') }}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="form-label font-weight-bold" for="estrategia">Fecha y Hora de Cierre</label>
                                            <p class="text-justify subrayado">
                                                {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sesion->fecha_hora_cierre)->format('d/m/Y H:i') }}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endif
