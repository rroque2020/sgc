<div class="modal-dialog modal-dialog-centered" role="document" data-keyboard="false" data-backdrop="static">
    <div class="modal-content">
        <div class="modal-header head-modal">
            <h5 class="modal-title text-white">{{ isset($item) ? 'Editar Actividad Evaluativa : '.$item->id : 'Crear Actividad Evaluativa'}}</h5>
            <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
            </button>
        </div>
        <form id="frmactividadevaluativa" class="frmactividadevaluativa" method="{{ isset($item)? 'put':'post' }}">
            <div class="modal-body">
                @csrf
                @if(isset($item))
                    @method('PUT')
                @endif
                <input type="hidden" id="codmodulo" name="codmodulo" value="{{ isset($item) ? $item->di_modulo_id : $modulo }}">
                <input type="hidden" id="codactividadevaluativa" name="codactividadevaluativa" value="{{ isset($item) ? $item->id : '' }}">
                <div id="mensaje"></div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="orden">Seleccione Nro. de Actividad Evaluativa</label>
                            <select id="orden" name="orden" class="form-control" data-live-search="true">
                                <option value="-1"><< SELECCIONE >></option>
                                @for ($i = 0; $i < 10; $i++)
                                    <option value="{{ $i + 1  }}" {{ isset($item) && $item->orden == ($i + 1) ? 'selected' : '' }}>{{ $i + 1 }}</option>
                                @endfor
                            </select>
                            <div id="orden_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="tipo">Tipo</label>
                            <select id="tipo" name="tipo" class="form-control" data-live-search="true">
                                <option value="-1"><< SELECCIONE >></option>
                                @foreach($tiposs as $tipog)
                                    <optgroup label="{{ $tipog->tipo }}">
                                        @foreach($tipos as $tipo)
                                            @if($tipog->tipo == $tipo->tipo)
                                                <option {{ isset($item) && $item->tipo_actividad_evaluativa_id == $tipo->id ? 'selected' : '' }} value="{{ $tipo->id }}">{{ $tipo->descripcion }}</option>
                                            @endif
                                        @endforeach
                                @endforeach
                            </select>
                            <div id="tipo_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="form-group">
                            <label class="form-label" for="descripcion">Descripción</label>
                            <textarea id="descripcion" name="descripcion" class="form-control"  rows="4">{{ isset($item) ? $item->descripcion : '' }}</textarea>
                            <div id="descripcion_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="porcentaje">Porcentaje</label>
                            <div class="input-group mb-3">
                                <input type="number" id="porcentaje" name="porcentaje" min="0" max="100" class="form-control" value="{{ isset($item) ? $item->porcentaje : '' }}">
                                <div class="input-group-append">
                                    <span class="input-group-text"> &nbsp;%</span>
                                </div>
                            </div>
                            <div id="porcentaje_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="instrumento">Instrumento</label>
                            <select id="instrumento" name="instrumento" class="form-control" data-live-search="true">
                                <option value="-1"><< SELECCIONE >></option>
                                @foreach($instrumentos as $instrumento)
                                    <option {{ isset($item) && $item->instrumento_evaluativo_id == $instrumento->id ? 'selected': '' }} value="{{ $instrumento->id }}">{{ $instrumento->descripcion }}</option>
                                @endforeach
                            </select>
                            <div id="instrumento_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="nota">Nota de Aprobación</label>
                            <div class="input-group mb-3">
                                <input type="number" id="nota" name="nota" min="0" max="20" class="form-control" value="{{ isset($item) ? $item->nota_aprobacion : '' }}">
                                <div class="input-group-append">
                                    <span class="input-group-text"> &nbsp;nota</span>
                                </div>
                            </div>
                            <div id="nota_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="fechahorainicio">Fecha y Hora de Inicio</label>
                            <input type="text" id="fechahorainicio" name="fechahorainicio" class="form-control date-time"
                                   value="{{ isset($item->fecha_hora_inicio) ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$item->fecha_hora_inicio)->format('d/m/Y H:i') : ''}}">
                            <div id="fechahorainicio_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="fechahoracierre">Fecha y Hora de Cierre</label>
                            <input type="text" id="fechahoracierre" name="fechahoracierre" class="form-control date-time"
                                   value="{{ isset($item->fecha_hora_cierre) ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$item->fecha_hora_cierre)->format('d/m/Y H:i') : ''}}">
                            <div id="fechahoracierre_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-cancelar" data-dismiss="modal"><i class="la la-times-circle-o"></i> Cancelar</button>
                <button type="submit" class="btn btn-aceptar"><i class="fa fa-save"></i> Guardar</button>
            </div>
        </form>
    </div>
</div>

