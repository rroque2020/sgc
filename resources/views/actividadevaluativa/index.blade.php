@if(isset($actividadesevaluativas) && count($actividadesevaluativas) > 0)
    <div class="row mt-2">
        <div class="col-lg-12 col-md-12 col-xs-12">
            <div id="tab-actividadevaluativa" class="default-tab">
                <ul class="nav nav-tabs" role="tablist">
                    @foreach($actividadesevaluativas as $actividadevaluativa)
                        <li class="nav-item">
                            <a class="tabaes nav-link" data-toggle="pill" href="#ae{{ $actividadevaluativa->id }}">
                                Actividad Evaluativa N° {{ $actividadevaluativa->orden }}
                            </a>
                            <div class="text-right" >
                                <a class="text-info" onclick="frmActividadEvaluativa({{ $actividadevaluativa->di_modulo_id }}, {{ $actividadevaluativa->id }})"
                                   data-toggle="tooltip" data-placement="top" title="Editar Actividad Evaluativa ">
                                    (<i class="la la-pencil"></i>)
                                </a>
                                <a class="text-info" onclick="eliminarActividadEvaluativa({{ $actividadevaluativa->di_modulo_id }}, {{ $actividadevaluativa->id }})"
                                   data-toggle="tooltip" data-placement="top" title="Eliminar Actividad Evaluativa ">
                                    (<i class="la la-trash"></i>)
                                </a>
                            </div>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach($actividadesevaluativas as $actividadevaluativa)
                        <div class="tab-pane fade pt-3" id="ae{{ $actividadevaluativa->id }}" role="tabpanel">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <label class="form-label font-weight-bold" for="tipo">Tipo</label>
                                        <p class="text-justify subrayado">
                                            {{ $actividadevaluativa->tipo }}
                                        </p>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <label class="form-label font-weight-bold" for="descripcion">Descripción</label>
                                        <p class="text-justify subrayado">
                                            {{ $actividadevaluativa->descripcion }}
                                        </p>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <label class="form-label font-weight-bold" for="porcentaje">Porcentaje</label>
                                        <p class="text-justify subrayado">
                                            {{ $actividadevaluativa->porcentaje }} %
                                        </p>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <label class="form-label font-weight-bold" for="tipo">Instrumento</label>
                                        <p class="text-justify subrayado">
                                            {{ $actividadevaluativa->instrumento }}
                                        </p>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <label class="form-label font-weight-bold" for="nota">Nota de Aprobación</label>
                                        <p class="text-justify subrayado">
                                            {{ $actividadevaluativa->nota_aprobacion }}
                                        </p>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="form-label font-weight-bold" for="estrategia">Fecha y Hora de Inicio</label>
                                        <p class="text-justify subrayado">
                                            {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$actividadevaluativa->fecha_hora_inicio)->format('d/m/Y H:i') }}
                                        </p>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="form-label font-weight-bold" for="estrategia">Fecha y Hora de Cierre</label>
                                        <p class="text-justify subrayado">
                                            {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$actividadevaluativa->fecha_hora_cierre)->format('d/m/Y H:i') }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@else
    <div class='informado'>
        <label style='color:#0055A0'>... No se encontraron registros ...</label>
    </div>
@endif
