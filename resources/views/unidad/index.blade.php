@if(!isset($unidades) || count($unidades) == 0)
    <div class='informado'>
        <label style='color:#0055A0'>... No se encontraron registros ...</label>
    </div>
@else
    <div class="row mt-2">
        <div class="col-lg-12 col-md-12 col-xs-12">
            <div class="default-tab">
                <ul class="nav nav-tabs" role="tablist">
                    @foreach($unidades as $unidad)
                        <li class="nav-item">
                            <a class="nav-link bg-dark text-white" data-toggle="tab" href="#und{{ $unidad->id }}">
                                Unidad Temática N° {{ $unidad->orden }}
                            </a>
                            <div class="text-right" >
                                <a class="text-info" onclick="frmUnidad({{ $unidad->di_sesion_id }}, {{ $unidad->id }})"
                                   data-toggle="tooltip" data-placement="top" title="Editar Unidad">
                                    (<i class="la la-pencil"></i>)
                                </a>
                                <a class="text-info" onclick="eliminarUnidad({{ $unidad->di_sesion_id }}, {{ $unidad->id }})"
                                   data-toggle="tooltip" data-placement="top" title="Eliminar Unidad">
                                    (<i class="la la-trash"></i>)
                                </a>
                            </div>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach($unidades as $unidad)
                        <div class="tab-pane fade pt-3" id="und{{ $unidad->id }}" role="tabpanel">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <label class="form-label font-weight-bold" for="Nombre">Nombre</label>
                                        <p class="text-justify subrayado">
                                            {{ $unidad->nombre }}
                                        </p>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <label class="form-label font-weight-bold" for="descripcion">Descripción</label>
                                        <p class="text-justify subrayado">
                                            {{ $unidad->descripcion }}
                                        </p>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <label class="form-label font-weight-bold" for="bibliografia">Bibliografia</label>
                                        <p class="text-justify subrayado">
                                            {{ $unidad->bibliografia }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endif
