<div class="modal-dialog modal-dialog-centered" role="document" data-keyboard="false" data-backdrop="static">
    <div class="modal-content">
        <div class="modal-header head-modal">
            <h5 class="modal-title text-white">{{ isset($item) ? 'Editar Unidad Temática : '.$item->id : 'Crear Unidad Temática'}}</h5>
            <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
            </button>
        </div>
        <form id="frmunidad" class="frmunidad" method="{{ isset($item)? 'put':'post' }}">
            <div class="modal-body">
                @csrf
                @if(isset($item))
                    @method('PUT')
                @endif
                <input type="hidden" id="codsesion" name="codsesion" value="{{ isset($item) ? $item->di_sesion_id : $sesion }}">
                <input type="hidden" id="codunidad" name="codunidad" value="{{ isset($item) ? $item->id : '' }}">
                <div id="mensaje"></div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="orden">Seleccione Nro. de Unidad Temática</label>
                            <select id="orden" name="orden" class="form-control" data-live-search="true">
                                <option value="-1"><< SELECCIONE >></option>
                                @for ($i = 0; $i < 10; $i++)
                                    <option value="{{ $i + 1  }}" {{ isset($item) && $item->orden == ($i + 1) ? 'selected' : '' }}>{{ $i + 1 }}</option>
                                @endfor
                            </select>
                            <div id="orden_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="form-group">
                            <label class="form-label" for="nombre">Título de la Unidad Temática</label>
                            <textarea id="nombre" name="nombre" class="form-control"  rows="4">{{ isset($item) ? $item->nombre : '' }}</textarea>
                            <div id="nombre_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="form-group">
                            <label class="form-label" for="descripcion">Contenido de la Unidad Temática</label>
                            <textarea id="descripcion" name="descripcion" class="form-control"  rows="4">{{ isset($item) ? $item->descripcion : '' }}</textarea>
                            <div id="descripcion_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="form-group">
                            <label class="form-label" for="bibliografia">Bibliografia</label>
                            <textarea id="bibliografia" name="bibliografia" class="form-control"  rows="4">{{ isset($item) ? $item->bibliografia : '' }}</textarea>
                            <div id="bibliografia_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-cancelar" data-dismiss="modal"><i class="la la-times-circle-o"></i> Cancelar</button>
                <button type="submit" class="btn btn-aceptar"><i class="fa fa-save"></i> Guardar</button>
            </div>
        </form>
    </div>
</div>
d7
