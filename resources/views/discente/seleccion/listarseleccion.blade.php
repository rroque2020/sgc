@if(!isset($postulantes) || count($postulantes) == 0)
    <div class='informado'>
        <label style='color:#0055A0'>... No se encontraron registros ...</label>
    </div>
@else
    <div id ="mensaje"></div>
    <div class="table-responsive">
        <table id="tblselecciondiscente" class="table text-center display table-striped hover" style="min-width: 850px">
            <thead class="thead-primary">
            <tr>
{{--                <th></th>--}}
                <th>Tipo</th>
                <th>DNI</th>
                <th>Postulante</th>
                <th>Celular</th>
                <th>Correo</th>
            </tr>
            </thead>
            <tbody>
            @foreach($postulantes as $item)
                <tr id="{{ $item->dni }}" data-id="{{ $item->dni }}">
{{--                    <td></td>--}}
                    <td>{{ $item->tipo == 1 ? 'INTERNO' : 'EXTERNO' }}</td>
                    <td>{{ $item->dni }}</td>
                    <td class="text-left">{{ $item->postulante }}</td>
                    <td>{{ $item->celular }}</td>
                    <td class="text-left">{{ $item->email }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endif
