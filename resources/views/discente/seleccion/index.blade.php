@extends('layouts.layout')
@section('title')
    <div class="col-sm-6 p-md-0">
        <div class="welcome-text">
            <h4>Actividades Académicas - Selección de Discentes</h4>
        </div>
    </div>
@endsection

@section('breadcrumb')
    <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Inicio</a></li>
            <li class="breadcrumb-item"><a href="{{ route('actividad.index') }}">Actividades Académicas</a></li>
            <li class="breadcrumb-item active"><a href="javascript:void(0);">Selección de Discentes</a></li>
        </ol>
    </div>
@endsection

@section('content')
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="container-fluid mb-1">
                        <div class="text-center mt-1">
                            <h4>CÓDIGO: {{ $actividad->codigo }}</h4>
                            <h2>{{ $actividad->titulo }}</h2>
                            <input type="hidden" id="codactividad" name="codactividad" value="{{ $actividad->id }}">
                            <p id="estado">{!! html_entity_decode(Helper::estadoAA($actividad->estado, false)) !!}</p>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div id="accordion-eleven" class="accordion accordion-header-bg accordion-rounded-stylish  accordion-header-shadow accordion-bordered">
                                    <div class="accordion__item">
                                        <div class="accordion__header collapsed cabecera-primary-accordion" data-toggle="collapse" data-target="#rounded-stylish_collapseOne">
                                            <span class="accordion__header--icon"><i class="fa fa-list"></i></span>
                                            <span class="accordion__header--text">Criterios para la Selección de Discentes</span>
                                            <span class="accordion__header--indicator"></span>
                                        </div>
                                        <div id="rounded-stylish_collapseOne" class="collapse accordion__body " data-parent="#accordion-eleven"  >
                                            <div class="accordion__body--text">
                                                <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="form-label font-weight-bold">Validar DNI</label>
                                                        <p class="text-justify subrayado">
                                                            {{ isset($item) && $item->dni == 1 ? 'SI' : 'NO' }}
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="form-label font-weight-bold">Solo Aprobados</label>
                                                        <p class="text-justify subrayado">
                                                            {{ isset($item) && $item->aprobado == 1 ? 'SI' : 'NO' }}
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="form-label font-weight-bold">No Cruce de Horarios</label>
                                                        <p class="text-justify subrayado">
                                                            {{ isset($item) && $item->temporalidad == 1 ? 'SI' : 'NO' }}
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="form-label font-weight-bold">No Sancionado</label>
                                                        <p class="text-justify subrayado">
                                                            {{ isset($item) && $item->sancion == 1 ? 'SI' : 'NO' }}
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="form-label font-weight-bold">Invitados</label>
                                                        <p class="text-justify subrayado">
                                                            {{ isset($item) && $item->invitados == 1 ? 'SI' : 'NO' }}
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="form-label font-weight-bold">Brecha</label>
                                                        <p class="text-justify subrayado">
                                                            {{ isset($item) && $item->brecha == 1 ? 'SI' : 'NO' }}
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="form-label font-weight-bold">Sistema de Personal</label>
                                                        <p class="text-justify subrayado">
                                                            {{ isset($item) ? $perfilesseleccionados : '' }}
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="form-label font-weight-bold">Especialidad</label>
                                                        <p class="text-justify subrayado">
                                                            {{ isset($item) ? $item->especialidad : '' }}
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="form-label font-weight-bold">Cargo</label>
                                                        <p class="text-justify subrayado">
                                                            {{ isset($item) ? $cargosseleccionados : '' }}
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="form-label font-weight-bold">Distrito Fiscal</label>
                                                        <p class="text-justify subrayado">
                                                            {{ isset($item) ? $distritosseleccionados : '' }}
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="pull-right mb-3 mr-0">
                                            <a onclick="actualizarListaSeleccionDiscente({{ $actividad->id }})" class="btn btn-rounded btn-primary" id="btncorreomasivo"
                                               data-toggle="tooltip" data-placement="top" title="Buscar">
                                                <span class="btn-icon-left text-tumblr">
                                                    <i class="la la-search color-primary"></i>
                                                 </span>Buscar
                                            </a>
                                            &nbsp;
                                            <a onclick="convocardiscente({{ $actividad->id }})" class="btn btn-rounded btn-success" id="btnaprobardisenio"
                                               data-toggle="tooltip" data-placement="top" title="Convocar">
                                               <span class="btn-icon-left text-primary">
                                                   <i class="la la-list-ul color-success"></i>
                                               </span>Convocar
                                            </a>
                                        </div>
                                    </div>
                                    <div id="listarseleccionados" class="col-lg-12 col-md-12 col-sm-12">
                                        @include('discente.seleccion.listarseleccion')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
@push('scriptsapp')
    <script src="{{ asset('assets/js/selecciondiscente.js') }}"></script>
@endpush
