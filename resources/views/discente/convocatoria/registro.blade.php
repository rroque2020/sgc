<div class="modal-dialog modal-dialog-centered" role="document" data-keyboard="false" data-backdrop="static">
    <div class="modal-content">
        <div class="modal-header head-modal">
            <h5 class="modal-title text-white">Registro de Pre-Inscripción</h5>
            <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
            </button>
        </div>
        <form id="frmregconvocatoria" class="frmregconvocatoria" method="post">
            <div class="modal-body">
                @csrf
                <input type="hidden" id="codactividad" name="codactividad" value="{{ $actividad }}">
                <div id="mensaje"></div>

                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="dni">DNI</label>
                            <input type="text" id="dni" name="dni" class="form-control"  onkeypress="return esNumerico(event)"
                                   onkeyup="listarDatosConvocatoria({{$actividad}})" maxlength="8" autocomplete="off">
                            <div id="dni_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="apellidos">Apellidos y Nombres</label>
                            <p id="datosdiscente" class="text-justify subrayado"></p>
                            <input type="hidden" id="apellidos" name="apellidos">
                            <input type="hidden" id="nombres" name="nombres">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="celular">Celular</label>
                            <input type="text" id="celular" name="celular" class="form-control" onkeypress="return esNumerico(event)" maxlength="9">
                            <div id="celular_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="correo">Correo</label>
                            <input type="email" id="correo" name="correo" class="form-control">
                            <div id="correo_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-cancelar" data-dismiss="modal"><i class="la la-times-circle-o"></i> Cancelar</button>
                <button type="submit" class="btn btn-aceptar"><i class="fa fa-save"></i> Pre-Inscribirse</button>
            </div>
        </form>
    </div>
</div>
