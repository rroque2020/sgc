@extends('layouts.layout')
@section('title')
    <div class="col-sm-6 p-md-0">
        <div class="welcome-text">
            <h4>Actividades Académicas - PreInscritos</h4>
        </div>
    </div>
@endsection

@section('breadcrumb')
    <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Inicio</a></li>
            <li class="breadcrumb-item"><a href="{{ route('actividad.index') }}">Actividades Académicas</a></li>
            <li class="breadcrumb-item active"><a href="javascript:void(0);">PreInscritos</a></li>
        </ol>
    </div>
@endsection

@section('content')
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="container-fluid mb-1">
                        <div class="text-center mt-1">
                            <h4>CÓDIGO: {{ $actividad->codigo }}</h4>
                            <h2>{{ $actividad->titulo }}</h2>
                            <input type="hidden" id="codactividad" name="codactividad" value="{{ $actividad->id }}">
                            <p id="estado">{!! html_entity_decode(Helper::estadoAA($actividad->estado, false)) !!}</p>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="card" style="height: auto !important;">
                                    <div class="card-header bg-primary text-white text-center py-2" style="display: block !important;">
                                        <strong>LISTA DE PRE-INSCRITOS</strong>
                                    </div>
                                    <div class="card-body">
                                        <form id="frm-preinscrito" class="frm-preinscrito" name="frm-preinscrito">
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <div class="pull-right mb-3 mr-0">
                                                        <a onclick="actualizarListaPreinscritos({{ $actividad->id }})" class="btn btn-rounded btn-tumblr"
                                                           data-toggle="tooltip" data-placement="top" title="Actualizar">
                                                            <span class="btn-icon-left text-tumblr">
                                                                <i class="fa fa-refresh color-primary"></i>
                                                            </span>Actualizar
                                                        </a>
                                                        &nbsp;
                                                        <a onclick="frmRegistroPreInscripcionCoordinador({{ $actividad->id }})" class="btn btn-rounded btn-tumblr" id="btnnuevopreinscrito"
                                                           data-toggle="tooltip" data-placement="top" title="Nuevo">
                                                            <span class="btn-icon-left text-tumblr">
                                                                <i class="la la-plus color-primary"></i>
                                                             </span>Nuevo
                                                        </a>
                                                        &nbsp;
                                                        <a onclick="enviarCorreoPreinscrito({{ $actividad->id }})" class="btn btn-rounded btn-primary" id="btncorreomasivo"
                                                           data-toggle="tooltip" data-placement="top" title="Enviar Correo Masivo">
                                                            <span class="btn-icon-left text-tumblr">
                                                                <i class="la la-envelope color-primary"></i>
                                                             </span>Enviar Correo Masivo
                                                        </a>
                                                        &nbsp;
                                                        <button type="submit" class="btn btn-rounded btn-success" id="btnaprobardisenio"
                                                           data-toggle="tooltip" data-placement="top" title="Matricular">
                                                            <span class="btn-icon-left text-primary">
                                                                <i class="la la-list-ul color-success"></i>
                                                            </span>Matricular
                                                        </button>
                                                    </div>
                                                </div>
                                                <div id="listapreinscritos" class="col-lg-12 col-md-12 col-sm-12">
                                                    @include('discente.preinscripcion.listarpreinscritos')
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scriptsapp')
    <script src="{{ asset('assets/js/preinscripcion.js') }}"></script>
    <script type="text/javascript">
       $('#tblpreinscritos').DataTable({
            "language": {
                "url": "/assets/vendor/datatables/js/dtspanish.json"
            },
            'columnDefs': [{
                'targets': 0,
                'checkboxes': {
                    'selectRow': true
                }
            }],
            'select': {
                'style': 'multi'
            },
            'order': [
                [3, 'asc']
            ],
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todo"]],
            dom: '<"row col-lg-12 col-md-12 col-sm-12 arriba1"B><"arriba2"lrf><"abajo"tip><"clear">',
            //dom: 'Blfrtip',
            text: 'Export',
            buttons: [
                { extend: 'copyHtml5',
                    text: '<i class="fa fa-copy"></i>&nbsp;Copiar',
                    "className": 'btn btn-info btn-sm'},
                { extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>&nbsp;Excel',
                    "className": 'btn btn-success btn-sm'},
                { extend: 'csvHtml5',
                    text: '<i class="fa fa-file-text-o"></i>&nbsp;CSV',
                    "className": 'btn btn-warning btn-sm'},
            ],
        });
    </script>
@endpush
