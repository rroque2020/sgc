@if(!isset($preinscritos) || count($preinscritos) == 0)
    <div class='informado'>
        <label style='color:#0055A0'>... No se encontraron registros ...</label>
    </div>
@else
    <div id ="mensaje"></div>
    <div class="table-responsive">
        <table id="tblpreinscritos" class="table text-center display table-striped hover" style="min-width: 850px">
            <thead class="thead-primary">
            <tr>
                <th></th>
                <th>Tipo</th>
                <th>DNI</th>
                <th>Pre-Inscrito</th>
                <th>Celular</th>
                <th>E-mail</th>
                <th>Correo</th>
            </tr>
            </thead>
            <tbody>
            @foreach($preinscritos as $item)
                <tr id="{{ $item->id }}" data-id="{{ $item->id }}">
                    <td></td>
                    <td>{{ $item->tipo }}</td>
                    <td>{{ $item->dni }}</td>
                    <td class="text-left">{{ $item->preinscrito }}</td>
                    <td>{{ $item->celular }}</td>
                    <td class="text-left">{{ $item->email }}</td>
                    <td class="text-center">
                        @switch($item->estado_correo)
                            @case(1)
                                <span class='badge-est badge-estado1'>NO ENVIADO</span>
                            @break
                            @case(2)
                                <span class='badge-est badge-estado2'>ENVIADO</span>
                            @break
                            @default
                                <span class='badge-est badge-estado3'>CONVOCADO</span>
                        @endswitch
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endif
