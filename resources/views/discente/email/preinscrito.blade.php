@component('mail::message')
# {{ $detalles['titulo'] }}

Hola, {{ $detalles['nombres'] }}<br>
Se ha Pre-Inscrito correctamente a la Actividad Académica, se le comunicará por este medio la confirmación de su inscripción.

Gracias,<br>
<strong>ESCUELA DEL MINISTERIO PÚBLICO</strong>
@endcomponent
