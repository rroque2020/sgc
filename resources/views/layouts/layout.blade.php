<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>.:: Sistema de Gestión de la Capacitación ::.</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/images/favicon.png') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/skin-3.css') }}">

    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset('assets/vendor/daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/select2/css/select2.min.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/vendor/datatables/css/jquery.dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/datatables/css/buttons.dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <link href="{{ asset('assets/vendor/clockpicker/css/bootstrap-clockpicker.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/vendor/toastr/css/toastr.min.css') }}">

    <link href="{{ asset('assets/vendor/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/button-nice.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/fullcalendar/css/fullcalendar.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/bootstrap-datepicker/bootstrap-datepicker3.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/bootstrap-treeview/css/bootstrap-treeview.min.css') }}" rel="stylesheet">
{{--    <link rel="stylesheet" href="{{ asset('assets/vendor/pace/center-radar.css') }}">--}}
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>
<body>
<div id="preloader">
    <div class="sk-three-bounce">
        <div class="sk-child sk-bounce1"></div>
        <div class="sk-child sk-bounce2"></div>
        <div class="sk-child sk-bounce3"></div>
    </div>
</div>
<div id="main-wrapper">
    <div class="nav-header">
        <a href="/" class="brand-logo">
            <img class="logo-abbr" src="{{ asset('assets/images/logo-white-2.png') }}" alt="">
            <img class="logo-compact" src="{{ asset('assets/images/logo-text-white.png') }}" alt="">
            <img class="brand-title" src="{{ asset('assets/images/logo-text-white.png') }}" alt="">
        </a>

        <div class="nav-control">
            <div class="hamburger">
                <span class="line"></span><span class="line"></span><span class="line"></span>
            </div>
        </div>
    </div>

    @include('partials.header')
    @include('partials.aside')
    <section>
        <div id="myModal" class="modal fade" role="dialog">
        </div>
    </section>
    <div class="content-body">
        <div class="container-fluid">
            <div class="row page-titles mx-0">
                @yield('title')
                @yield('breadcrumb')
            </div>

            @if (session()->has('success'))
                <input type="hidden" id="exito">
            @endif
            <div class="row">
                <!-- cargador empresa  -->
                <div style="display: none;" id="cargador1" align="center">
                    <img src="{{ asset('assets/images/cargando.gif') }}" align="middle" alt="cargador">
                    &nbsp;<label style="color:#3C8DBC">Realizando tarea solicitada ...</label>
                </div>
                <!-- cargador 2 -->
                <div style="display: none;" id="cargador2" align="center">
                    <img src="{{ asset('assets/images/load.gif') }}" align="middle" alt="cargador">
                    &nbsp;<label style="color:#B9260E">Espere ...</label>
                </div>
                @yield('content')
            </div>
        </div>
    </div>

    @include('partials.footer')
</div>

<script src="{{ asset('assets/vendor/global/global.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('assets/js/custom.min.js') }}"></script>
<script src="{{ asset('assets/js/dashboard/dashboard-2.js') }}"></script>

<script src="{{ asset('assets/vendor/svganimation/vivus.min.js') }}"></script>
<script src="{{ asset('assets/vendor/svganimation/svg.animation.js') }}"></script>

<!-- date-range-picker -->
<script src="{{ asset('assets/vendor/moment/moment.min.js') }}"></script>
<script src="{{ asset('assets/vendor/moment/es.min.js') }}"></script>
<script src="{{ asset('assets/vendor/daterangepicker/daterangepicker.js') }}"></script>

<script src="{{ asset('assets/vendor/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>

<script src="{{ asset('assets/vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/js/jszip.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/js/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/js/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/js/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/js/dataTables.checkboxes.min.js') }}"></script>

<script src="{{ asset('assets/vendor/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
<script src="{{ asset('assets/vendor/clockpicker/js/bootstrap-clockpicker.min.js') }}"></script>
<script src="{{ asset('assets/vendor/toastr/js/toastr.min.js') }}"></script>
<script src="{{ asset('assets/vendor/sweetalert2/dist/sweetalert2.min.js') }}"></script>

<script src="{{ asset('assets/vendor/fullcalendar/js/fullcalendar.min.js') }}"></script>
<script src="{{ asset('assets/vendor/fullcalendar/js/es.js') }}"></script>

<script src="{{ asset('assets/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-datepicker/bootstrap-datepicker.es.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-treeview/js/bootstrap-treeview.min.js') }}"></script>

<script src="{{ asset('assets/js/notificacion.js') }}"></script>
<script src="{{ asset('assets/js/helper.js') }}"></script>
{{--<script src="{{ asset('assets/vendor/pace/pace.min.js') }}"></script>--}}
<script type="text/javascript">
    $(function() {
        if ($('#exito').length) {
            msgExito('{!! session()->get('success') !!}', 5000);
        }

        $('[data-toggle="tooltip"]').tooltip({animation:true});
    });
</script>
@stack('scriptsapp')
</body>
</html>
