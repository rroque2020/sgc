@if(!isset($requisitos) || count($requisitos) == 0)
    <div class='informado'>
        <label style='color:#0055A0'>... No se encontraron registros ...</label>
    </div>
@else
    <div class="table-responsive mt-4 text-center">
        <table id="tblrequisito" class="table table-bordered table-hover table-striped verticle-middle table-responsive-sm" style="min-width: 845px">
            <thead class="thead-encabezado">
            <tr>
                <th style="width:10%">Código</th>
                <th style="width:80%">Actividad</th>
                <th style="width:10%">Acciones</th>
            </tr>
            </thead>
            <tbody>
            @foreach($requisitos as $requisito)
                <tr class="item" id="fila{{ $requisito->id }}">
                    <td class="text-center">
                        <input type="hidden" class="codaa" name="codaa[]" value="{{ $requisito->id }}">
                        {{ $requisito->id }}
                    </td>
                    <td class="text-justify">
                        {{ $requisito->titulo }}</td>
                    <td class="text-center">
                        <a type="submit" class="btn btn-danger btn-xs" onclick="eliminarRequisito({{ $requisito->idar }},{{ $requisito->actividad_academica_id }})">
                            <i class="la la-trash"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
 @endif
