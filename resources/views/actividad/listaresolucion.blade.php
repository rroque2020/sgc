@if(!isset($resoluciones) || count($resoluciones) == 0)
    <div class='informado'>
        <label style='color:#0055A0'>... No se encontraron registros ...</label>
    </div>
@else
    <div class="table-responsive mt-4 text-center">
        <table id="tblresolucion" class="table table-bordered table-hover table-striped verticle-middle table-responsive-sm" style="min-width: 845px">
            <thead class="thead-encabezado">
            <tr>
                <th style="width:10%;">Código</th>
                <th style="width:70%;">Resolución</th>
                <th style="width:10%;">Archivo</th>
                <th style="width:10%;">Acciones</th>
            </tr>
            </thead>
            <tbody>
            @foreach($resoluciones as $resolucion)
                <tr class="item" id="filaresolucion{{ $resolucion->id }}">
                    <td class="text-center">
                        <input type="hidden" class="codresolucion" name="codresolucion[]" value="{{ $resolucion->id }}">
                        {{ $resolucion->id }}
                    </td>
                    <td class="text-justify">
                        {{ $resolucion->descripcion }}
                    </td>
                    <td class="text-justify">
                        {{ $resolucion->archivo }}
                    </td>
                    <td class="text-center">
                        <a class="btn btn-danger btn-xs" onclick="eliminarResol({{ $resolucion->idar }},{{ $resolucion->actividad_academica_id }})">
                            <i class="la la-trash"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endif
