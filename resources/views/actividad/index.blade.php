@extends('layouts.layout')
@section('title')
    <div class="col-sm-6 p-md-0">
        <div class="welcome-text">
            <h4>Actividades Académicas</h4>
        </div>
    </div>
@endsection

@section('breadcrumb')
    <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Inicio</a></li>
            <li class="breadcrumb-item active"><a href="javascript:void(0);">Actividades Académicas</a></li>
        </ol>
    </div>
@endsection

@section('content')
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card">
            <div class="card-header">
                <h4></h4>

                <div class="pull-right">
                    <a onclick="actualizaraa()" class="btn btn-rounded btn-tumblr" data-toggle="tooltip" data-placement="top" title="Actualizar">
                    <span class="btn-icon-left text-tumblr">
                        <i class="fa fa-refresh color-primary"></i>
                    </span>Actualizar
                    </a>
                    &nbsp;
                    <a href="/actividadacademica/nuevo" class="btn btn-rounded btn-primary"  data-toggle="tooltip" data-placement="top" title="Nuevo">
                        <span class="btn-icon-left text-primary">
                            <i class="fa fa-plus color-primary"></i>
                        </span>Nuevo
                    </a>
                </div>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div id="accordion-eleven" class="accordion accordion-header-bg accordion-rounded-stylish  accordion-header-shadow accordion-bordered">
                            <div class="accordion__item">
                                <div class="accordion__header collapsed cabecera-primary-accordion" data-toggle="collapse" data-target="#rounded-stylish_collapseOne">
                                    <span class="accordion__header--icon"><i class="fa fa-search"></i></span>
                                    <span class="accordion__header--text">Buscar Actividades</span>
                                    <span class="accordion__header--indicator"></span>
                                </div>
                                <div id="rounded-stylish_collapseOne" class="collapse accordion__body " data-parent="#accordion-eleven"  >
                                    <div class="accordion__body--text">
                                        <form id="frmbusqueda" class="frmbusqueda" method="POST">
                                            @csrf
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-12">
                                                    <div class="form-group row">
                                                        <label for="inputEmail3" class="col-sm-4">Fecha :</label>
                                                        <div class="col-sm-8">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    <i class="fa fa-calendar"></i>
                                                                </span>
                                                                <button type="button" name="fecha" id="busquedarango-btn" class="btn btn-default form-control" style="text-align: left;"  tabindex="1" autofocus>
                                                                    <span>
                                                                        <i class="fa fa-calendar"></i>&nbsp;Seleccione Rango de Fechas
                                                                    </span>
                                                                        <i class="fa fa-caret-down"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12">
                                                    <div class="form-group row">
                                                        <label for="inputEmail3" class="col-sm-4 col-form-label" for="programado">Planificado :</label>
                                                        <div class="col-sm-8">
                                                            <select id="programado" name="programado" class="form-control" >
                                                                <option value="-1"><< SELECCIONE >></option>
                                                                <option value="1">PROGRAMADO</option>
                                                                <option value="0">NO PROGRAMADO</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12">
                                                    <div class="form-group row">
                                                        <label for="subgerencia" class="col-sm-4 col-form-label">Sub Gerencia :</label>
                                                        <div class="col-sm-8">
                                                            <select id="subgerencia" name="subgerencia" class="form-control" data-live-search="true">
                                                                <option value="-1"><< SELECCIONE >></option>
                                                                @foreach($subgerencias as $subgerencia)
                                                                    <option value="{{ $subgerencia->id }}">{{ $subgerencia->descripcion }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12">
                                                    <div class="form-group row">
                                                        <label for="inputEmail3" class="col-sm-4 col-form-label">Tipo :</label>
                                                        <div class="col-sm-8">
                                                            <select id="tipo" name="tipo" class="form-control" data-live-search="true">
                                                                <option value="-1"><< SELECCIONE >></option>
                                                                @foreach($tipos as $tipo)
                                                                    <option value="{{ $tipo->id }}">{{ $tipo->descripcion }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12">
                                                    <div class="form-group row">
                                                        <label for="inputEmail3" class="col-sm-4 col-form-label">Modalidad :</label>
                                                        <div class="col-sm-8">
                                                            <select id="modalidad" name="modalidad" class="form-control" data-live-search="true">
                                                                <option value = "-1"><< SELECCIONE >></option>
                                                                @foreach($modalidades as $modalidad)
                                                                    <option value="{{ $modalidad->id }}">{{ $modalidad->descripcion }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12">
                                                    <div class="form-group row">
                                                        <label for="inputEmail3" class="col-sm-4 col-form-label">Sistema :</label>
                                                        <div class="col-sm-8">
                                                            <select id="sistema" name="sistema" class="form-control" data-live-search="true">
                                                                <option value = "-1"><< SELECCIONE >></option>
                                                                @foreach($sistemas as $sistema)
                                                                    <option value="{{ $sistema->id }}">{{ $sistema->descripcion }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12">
                                                    <div class="form-group row">
                                                        <label for="inputEmail3" class="col-sm-4 col-form-label">Temática :</label>
                                                        <div class="col-sm-8">
                                                            <select id="tematica" name="tematica" class="form-control" data-live-search="true">
                                                                <option value = "-1"><< SELECCIONE >></option>
                                                                @foreach($tematicas as $tematica)
                                                                    <option value="{{ $tematica->id }}">{{ $tematica->descripcion }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12">
                                                    <div class="form-group row">
                                                        <label for="inputEmail3" class="col-sm-4 col-form-label">Origen :</label>
                                                        <div class="col-sm-8">
                                                            <select id="origen" name="origen" class="form-control" data-live-search="true">
                                                                <option value = "-1"><< SELECCIONE >></option>
                                                                <option value="EMP">EMP</option>
                                                                <option value="DISTRITO FISCAL">DISTRITO FISCAL</option>
                                                                <option value="COOPERANTE">COOPERANTE</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12">
                                                    <div class="form-group row">
                                                        <label for="inputEmail3" class="col-sm-4 col-form-label">Línea Estratégica :</label>
                                                        <div class="col-sm-8">
                                                            <select id="linea" name="linea" class="form-control" data-live-search="true">
                                                                <option value = "-1"><< SELECCIONE >></option>
                                                                @foreach($lineas as $linea)
                                                                    <option value="{{ $linea->id }}">{{ $linea->descripcion }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12">
                                                    <div class="form-group row">
                                                        <label for="inputEmail3" class="col-sm-4 col-form-label">Bloque Académico :</label>
                                                        <div class="col-sm-8">
                                                            <select id="bloque" name="bloque" class="form-control" data-live-search="true">
                                                                <option value = "-1"><< SELECCIONE >></option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12">
                                                    <div class="form-group row">
                                                        <label for="inputEmail3" class="col-sm-4 col-form-label">Estado :</label>
                                                        <div class="col-sm-8">
                                                            <select id="estado" name="estado" class="form-control" data-live-search="true">
                                                                <option value="-1"><< SELECCIONE >></option>
                                                                {!!html_entity_decode(Helper::estado())!!}
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <div class="pull-right">
                                                        <a  class="btn btn-light " onclick="limpiarBusqueda()"><i class="fa fa-chain-broken"></i> Limpiar</a>
                                                        <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Buscar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="mensaje"></div>

    <div class="col-lg-12 col-md-12 col-sm-12">
        <div id="listado">
            @if(isset($items))
                @include('actividad.lista')
            @endif
        </div>
    </div>
@endsection
@push('scriptsapp')
    <script type="application/javascript">
        $(function(){
            if($('#exito').length){
                $('#tblactividad').DataTable().page('last').draw('page');
            }

            $('#busquedarango-btn span').html(moment().startOf('year').format('DD/MM/YYYY') + ' - ' + moment().endOf('year').format('DD/MM/YYYY'));

            $('#busquedarango-btn').data('daterangepicker').setStartDate(moment().startOf('year'));
            $('#busquedarango-btn').data('daterangepicker').setEndDate(moment().endOf('year'));
        });

        $('#busquedarango-btn').daterangepicker(
            {
                format: 'DD/MM/YYYY',
                "locale": {
                    "format": "DD/MM/YYYY",
                    "separator": " - ",
                    "applyLabel": "Filtrar",
                    "cancelLabel": "Cancelar",
                    "fromLabel": "De",
                    "toLabel": "A",
                    "customRangeLabel": "Rango",
                    "weekLabel": "W",
                    "daysOfWeek": [
                        "Do",
                        "Lu",
                        "Ma",
                        "Mi",
                        "Ju",
                        "Vi",
                        "Sa"
                    ],
                    "monthNames": [
                        "Enero",
                        "Febrero",
                        "Marzo",
                        "Abril",
                        "Mayo",
                        "Junio",
                        "Julio",
                        "Agosto",
                        "Setiembre",
                        "Octubre",
                        "Noviembre",
                        "Diciembre"
                    ]
                },
                ranges: {
                    'Hoy': [moment(), moment()],
                    'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Esta Semana': [moment().startOf('isoweek'), moment().endOf('isoweek')],
                    'Este Mes': [moment().startOf('month'), moment().endOf('month')],
                    'Ultimos 3 Meses': [moment().subtract(2, "month").startOf("month"), moment().endOf("month")],
                    'Este Semestre': [moment().subtract(5, "month").startOf("month"), moment().endOf("month")],
                    'Este Año': [moment().startOf('year'), moment().endOf('year')]
                }
            },
            cb);

        function cb(start, end) {
            $('#busquedarango-btn span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
        }

        $('#tblactividad').DataTable({
            "language": {
                "url": "/assets/vendor/datatables/js/dtspanish.json"
            },
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todo"]],
            dom: '<"row col-lg-12 col-md-12 col-sm-12 arriba1"B><"arriba2"lrf><"abajo"tip><"clear">',
            //dom: 'Blfrtip',
            text: 'Export',
            buttons: [
                { extend: 'copyHtml5',
                    text: '<i class="fa fa-copy"></i>&nbsp;Copiar',
                    "className": 'btn btn-info btn-sm'},
                { extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>&nbsp;Excel',
                    "className": 'btn btn-success btn-sm'},
                { extend: 'csvHtml5',
                    text: '<i class="fa fa-file-text-o"></i>&nbsp;CSV',
                    "className": 'btn btn-warning btn-sm'},
            ],
        });
    </script>
    <script src="{{ asset('assets/js/actividadacademica.js') }}"></script>

@endpush
