@if(!isset($items) || count($items) == 0)
    <div class='informado'>
        <label style='color:#0055A0'>... No se encontraron registros ...</label>
    </div>
@else
    <div class="card">
        <div class="card-header">
            <h4>Lista de Actividades Académicas</h4>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tblactividad" class="table text-center display table-striped hover" style="min-width: 850px">
                    <thead class="thead-primary">
                    <tr>
                        <th>ID</th>
                        <th>Codigo</th>
                        <th>Actividad</th>
                        <th>Programado</th>
                        <th>Organizador</th>
                        <th>F. Inicio </th>
                        <th>Estado </th>
                        <th>Acciones </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->codigo }}</td>
                            <td class="text-justify">{{ $item->titulo }}</td>
                            <td>
                                @if($item->programado == 1)
                                    PROGRAMADO
                                @else
                                    NO PROGRAMADO
                                @endif
                            </td>
                            <td class="text-justify">{{ $item->origen }}</td>
                            <td style="min-width:80px;">{{ \Carbon\Carbon::createFromFormat('Y-m-d',$item->fecha_inicio)->format('d/m/Y') }}</td>
                            <td>
                                {!!html_entity_decode(Helper::estadoAA($item->estado, false))!!}
                            </td>
                            <td>
                                <a href="{{ route('actividad.option',['codigo' => $item->id]) }}" class="btn btn-xs btn-facebook" alt="Ver Detalles"
                                   data-toggle="tooltip" data-placement="top" title="Ver Opciones">
                                    <i class="la la-search"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endif
