@extends('layouts.layout')
@section('title')
    <div class="col-sm-6 p-md-0">
        <div class="welcome-text">
            <h4>Actividades Académicas - Opciones</h4>
        </div>
    </div>
@endsection

@section('breadcrumb')
    <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Inicio</a></li>
            <li class="breadcrumb-item"><a href="{{ route('actividad.index') }}">Actividades Académicas</a></li>
            <li class="breadcrumb-item active"><a href="javascript:void(0);">Opciones</a></li>
        </ol>
    </div>
@endsection

@section('content')
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="container-fluid mb-1">
{{--                        <div style="position: absolute; right: 10px; top:10px;">--}}
{{--                            {!!html_entity_decode(Helper::estadoAA($actividad->estado, false))!!}--}}
{{--                        </div>--}}
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div id="mensaje"></div>
                        </div>
                        <div class="text-center mt-1">
                            <h4>CÓDIGO: {{ $actividad->codigo }}</h4>
                            <h2>{{ $actividad->titulo }}</h2>
                            <input type="hidden" id="codactividad" name="codactividad" value="{{ $actividad->id }}">
                            <p id="estado">{!! html_entity_decode(Helper::estadoAA($actividad->estado, false)) !!}</p>
                        </div>
                        <div class="row">
                            <div id="ver" class="col-md-4">
                                <a href="{{ route('actividad.show',['codigo' => $actividad->id]) }}">
                                    <div class="box">
                                        <div class="our-services settings">
                                            <div class="icon"> <img src="{{ asset('assets/images/icon-ver.png') }}"> </div>
                                            <h4>Detalle</h4>
                                            <p>Permite Ver el detalle de la información de la Actividad Académica</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
{{--                            @if($actividad->estado == 1)--}}
                                <div id="editar" class="col-md-4">
                                    <a href="{{ route('actividad.edit',['codigo' => $actividad->id]) }}">
                                        <div class="box">
                                            <div class="our-services speedup">
                                                <div class="icon"> <img src="{{ asset('assets/images/icon-editar.png') }}"> </div>
                                                <h4>Editar</h4>
                                                <p>Permite Modificar o Actualizar los datos de la Actividad Académica</p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
{{--                            @endif--}}
                            <div id="asignarcoordinador" class="col-md-4">
                                <a data-toggle="modal" data-target="#frmcoordinador">
                                    <div class="box">
                                        <div class="our-services privacy">
                                            <div class="icon"> <img src="{{ asset('assets/images/icon-seleccionarcoordinador.png') }}"> </div>
                                            <h4>Coordinador</h4>
                                            <p>Permite designar el Coordinador de la Actividad Académica</p>
                                        </div>
                                    </div>
                                </a>
                                <div class="modal fade" id="frmcoordinador">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header head-modal">
                                                <h5 class="modal-title text-white">Seleccionar Coordinador</h5>
                                                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="form-group">
                                                            <select id="codcoordinador" name="codcoordinador" class="form-control" data-live-search="true">
                                                                <option value = "-1"><< SELECCIONE >></option>
                                                                @foreach($coordinadores as $coordinador)
                                                                    <option value = "{{ $coordinador->CODI_EMPL_PER }}">
                                                                        {{ $coordinador->NOMB_CORT_PER }}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-cancelar" data-dismiss="modal">Cancelar</button>
                                                <button type="button" class="btn btn-aceptar" onclick="registrarCoordinador()">Aceptar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="mensajeo" class="col-md-12">

                            </div>
                            <div id="disenio" class="col-md-4">
                                <a href="{{ route('disenio.index',['codigo' => $actividad->id]) }}">
                                    <div class="box">
                                        <div class="our-services backups">
                                            <div class="icon"> <img src="{{ asset('assets/images/icon-silabo.png') }}"> </div>
                                            <h4>Diseño Instruccional</h4>
                                            <p>Permite registrar la Estructura del Diseño Instruccional de la Actividad Académica </p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div id="eva" class="col-md-4">
                                <a href="{{ route('actividad.seleccionardocente',['id' => $actividad->id]) }}">
                                    <div class="box">
                                        <div class="our-services ssl">
                                            <div class="icon"> <img src="{{ asset('assets/images/icon-DOCENTE.png') }}"> </div>
                                            <h4>Selección de Docente</h4>
                                            <p>Permite seleccionar el(los) Docente(s) para la Actividad Académica</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div id="eva" class="col-md-4">
                                <a onclick="frmRegistroEVA({{ $actividad->id }},{{ $actividad->eva }})">
                                    <div class="box">
                                        <div class="our-services ssl">
                                            <div class="icon"> <img src="{{ asset('assets/images/icon-EVA.png') }}"> </div>
                                            <h4>Registrar en EVA</h4>
                                            <p>Permite registrar la Actividad Académica en el Entorno Virtual de Aprendizaje</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-4">
                                <a href="{{ route('actividad.preinscrito',['id' => $actividad->id]) }}">
                                    <div class="box">
                                        <div class="our-services database">
                                            <div class="icon"> <img src="{{ asset('assets/images/icon-tutor.png') }}"> </div>
                                            <h4>Pre-Inscripción</h4>
                                            <p>Permite registrar y validar los Pre-Inscritos de la Actividad Académica</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-4">
                                <a href="{{ route('actividad.seleccion',['id' => $actividad->id]) }}">
                                    <div class="box">
                                        <div class="our-services database">
                                            <div class="icon"> <img src="{{ asset('assets/images/icon-tutor.png') }}"> </div>
                                            <h4>Seleccionar Discentes</h4>
                                            <p>Permite buscar y seleccionar Discentes para la Actividad Académica</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-4">
                                <a onclick="frmRegistroPreInscripcionWeb({{ $actividad->id }})">
                                    <div class="box">
                                        <div class="our-services database">
                                            <div class="icon"> <img src="{{ asset('assets/images/icon-tutor.png') }}"> </div>
                                            <h4>Asignar Tutor</h4>
                                            <p>Permite registrar el Tutor para la Actividad Académica</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-4">
                                <a onclick="frmRegistroPreInscripcionConvocatoria({{ $actividad->id }})">
                                    <div class="box">
                                        <div class="our-services database">
                                            <div class="icon"> <img src="{{ asset('assets/images/icon-tutor.png') }}"> </div>
                                            <h4>Asignar Tutor</h4>
                                            <p>Permite registrar el Tutor para la Actividad Académica</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scriptsapp')
    <script src="{{ asset('assets/js/eva.js') }}"></script>
    <script src="{{ asset('assets/js/preinscripcion.js') }}"></script>
    <script src="{{ asset('assets/js/selecciondiscente.js') }}"></script>
    <script type="application/javascript">
       function registrarCoordinador(){
           var codact = $('#codactividad').val();
           var codcoor = $('#codcoordinador').val();

           if(codcoor === '-1'){
               msgError('Elija un Coordinador ...',2500);
               return;
           }

           var vform = '_token=' + $('meta[name="csrf-token"]').attr('content') + '&codactividad=' + codact + '&codcoordinador=' + codcoor;
           var vurl = '/actividadacademica/asignarcoordinador';

           $.ajax({
               type: 'POST',
               url: vurl,
               data: vform ,
               beforeSend: function() {
                   $('#mensaje').html($('#cargador1').html());
               },
               complete: function() {
                   $('#mensaje').html('');
               }
           })
           .done(function (data) {
               $('#estado').html(data.htmlestado);
               if(data.estado > 1){
                   $('#editar').hide();
               }

               msgExito('Coordinador asignado correctamente...', 2500);
               $("#frmcoordinador").modal('hide');
           })
           .fail(function(data){
               console.log(data);
               msgError('Ocurrió un error, intentelo otra vez, en caso persista el error, consulte con el Administrador del Sistema...', 2500);
           });
       }
    </script>
@endpush
