@extends('layouts.layout')
@section('title')
    <div class="col-sm-6 p-md-0">
        <div class="welcome-text">
            <h4>Actividades Académicas - Editar</h4>
        </div>
    </div>
@endsection

@section('breadcrumb')
    <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Inicio</a></li>
            <li class="breadcrumb-item"><a href="{{ route('actividad.index') }}">Actividades Académicas</a></li>
            <li class="breadcrumb-item active"><a href="javascript:void(0);">Editar</a></li>
        </ol>
    </div>
@endsection
@section('content')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div id="mensaje"></div>
                        <div class="aa">
                            <ul class="nav nav-tabs">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#datos">
                                        <i class="fa fa-list"></i>
                                        Datos Generales
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#requisitos">
                                        <i class="fa fa-table"></i>
                                        Pre-Requisitos
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#resoluciones">
                                        <i class="fa fa-table"></i>
                                        Resoluciones
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#planes">
                                        <i class="fa fa-table"></i>
                                        Planes Alineados
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#politicas">
                                        <i class="fa fa-table"></i>
                                        Políticas Alineadas
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade show active" id="datos" role="tabpanel">
                                    <form id="frmaa" action="{{ route('actividad.update', ['codigo' => $actividad->id]) }}" method="post">
                                        @csrf
                                        @method('PUT')
                                        <div class="pt-4">
                                            @error('unknown')
                                                <div class="col-lg-12 col-md-12 rechazado">
                                                    * {{ $message }}
                                                </div>
                                            @enderror
                                            @if (isset($errors) && $errors->any())
                                                <div class="col-lg-12 col-md-12 rechazado">
                                                    * Existen errores, verifique e intentelo de nuevo...
                                                </div>
                                            @endif
                                            <div class="row">
                                                <div class="col-lg-3 col-md-3 col-sm-12">
                                                    <div class="form-group" style>
                                                        <label class="form-label font-weight-bold" for="titulo">Código</label>
                                                        <input type="hidden" id="codactividad" name="codactividad" value="{{ $actividad->id }}">
                                                        <p class="text-justify subrayado">{{ $actividad->codigo }}</p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12">
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-12">
                                                    <div class="form-group" style>
                                                        <input type="hidden" name="estado" value="{{ $actividad->estado }}">
                                                        <label class="form-label font-weight-bold" for="estado">Estado</label><br>
                                                        {!!html_entity_decode(Helper::estadoAA($actividad->estado, false))!!}
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12">
                                                    <div class="form-group prog">
                                                        <label class="form-label" for="programado">¿La Actividad es ?</label>
                                                        <input type="checkbox" id="programado" name="programado" class="form-control"  data-on="PROGRAMADO" data-off="NO PROGRAMADO" data-toggle="toggle"
                                                            {{ old('programado') == 'on' ? 'checked' : ($actividad->programado == 1 ? 'checked' : '' ) }}>
                                                        @error('programado')
                                                        <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="form-label" for="titulo">Titulo de la Actividad Académica</label>
                                                        <textarea id="titulo" name="titulo" class="form-control"  rows="2">{{ old('titulo') ?? $actividad->titulo }}</textarea>
                                                        @error('titulo')
                                                            <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="form-label" for="descripcion">Descripción/Sumilla</label>
                                                        <textarea id="descripcion" name="descripcion" class="form-control" rows="4">{{ old('descripcion') ?? $actividad->descripcion }}</textarea>
                                                        @error('descripcion')
                                                            <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="form-label" for="subgerencia">Sub Gerencia</label>
                                                        <select id="subgerencia" name="subgerencia" class="form-control" data-live-search="true">
                                                            <option value="-1"><< SELECCIONE >></option>
                                                            @foreach($subgerencias as $subgerencia)
                                                                <option {{ old('subgerencia') == $subgerencia->id ? 'selected' : ( $subgerencia->id == $actividad->sub_gerencia_id ? 'selected': '' ) }} value="{{ $subgerencia->id }}">{{ $subgerencia->descripcion }}</option>
                                                            @endforeach
                                                        </select>
                                                        @error('subgerencia')
                                                        <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="form-label" for="tipo">Tipo</label>
                                                        <select id="tipo" name="tipo" class="form-control" data-live-search="true">
                                                            <option value="-1"><< SELECCIONE >></option>
                                                            @foreach($tipos as $tipo)
                                                                <option {{ old('tipo') == $tipo->id ? 'selected' : ( $tipo->id == $actividad->tipo_actividad_id ? 'selected' : '') }} value="{{ $tipo->id }}">{{ $tipo->descripcion }}</option>
                                                            @endforeach
                                                        </select>
                                                        @error('tipo')
                                                            <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="form-label" for="modalidad">Modalidad</label>
                                                        <select id="modalidad" name="modalidad" class="form-control" data-live-search="true">
                                                            <option value = "-1"><< SELECCIONE >></option>
                                                            @foreach($modalidades as $modalidad)
                                                                <option {{ old('modalidad') == $modalidad->id ? 'selected' : ( $modalidad->id == $actividad->modalidad_id ? 'selected' : '' ) }} value="{{ $modalidad->id }}">{{ $modalidad->descripcion }}</option>
                                                            @endforeach
                                                        </select>
                                                        @error('modalidad')
                                                        <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3col-sm-12">
                                                    <div id="distrito-sede">
                                                        <div class="form-group">
                                                            <label class="form-label" for="distritosede">Sede</label>
                                                            <select id="distritosede" name="distritosede" class="form-control" data-live-search="true">
                                                                <option value = "-1"><< SELECCIONE >></option>
                                                                @foreach($distritos as $distrito)
                                                                    <option {{ old('distritosede') == $distrito->id ? 'selected' : ($distrito->id == $actividad->sede ? 'selected' : '') }} value="{{ $distrito->id }}">{{ $distrito->descripcion }}</option>
                                                                @endforeach
                                                            </select>
                                                            @error('distritosede')
                                                                <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="form-label" for="sistema">Sistema</label>
                                                        <select id="sistema" name="sistema" class="form-control" data-live-search="true">
                                                            <option value = "-1"><< SELECCIONE >></option>
                                                            @foreach($sistemas as $sistema)
                                                                <option {{ old('sistema') == $sistema->id ? 'selected' : ($sistema->id == $actividad->sistema_id ? 'selected' : '') }} value="{{ $sistema->id }}">{{ $sistema->descripcion }}</option>
                                                            @endforeach
                                                        </select>
                                                        @error('sistema')
                                                            <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="form-label" for="tematica">Temática</label>
                                                        <select id="tematica" name="tematica" class="form-control" data-live-search="true">
                                                            <option value = "-1"><< SELECCIONE >></option>
                                                            @foreach($tematicas as $tematica)
                                                                <option {{ old('tematica') == $tematica->id ? 'selected' : ($tematica->id == $actividad->tematica_id ? 'selected' : '') }} value="{{ $tematica->id }}">{{ $tematica->descripcion }}</option>
                                                            @endforeach
                                                        </select>
                                                        @error('tematica')
                                                        <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="form-label" for="linea">Línea Estratégica</label>
                                                        <select id="linea" name="linea" class="form-control" data-live-search="true">
                                                            <option disabled value = "-1"><< SELECCIONE >></option>
                                                            @foreach($lineas as $linea)
                                                                <option {{ old('linea') == $linea->id ? 'selected' : ($linea->id == $lineaid ? 'selected' : '') }} {{ $linea->id != $lineaid ? 'disabled' : '' }} value="{{ $linea->id }}">{{ $linea->descripcion }}</option>
                                                            @endforeach
                                                        </select>
                                                        @error('linea')
                                                        <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="form-label" for="bloque">Bloque Académico</label>
                                                        <select id="bloque" name="bloque" class="form-control" data-old="{{ old('bloque') ?? $actividad->bloque_academico_id }}" data-live-search="true">
                                                            <option value = "-1"><< SELECCIONE >></option>
                                                        </select>
                                                        @error('bloque')
                                                            <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="form-label" for="tipocertificado">Tipo de Certificado</label>
                                                        <select id="tipocertificado" name="tipocertificado" class="form-control" data-live-search="true">
                                                            <option value = "-1"><< SELECCIONE >></option>
                                                            @foreach($tiposcertificado as $tipocertificado)
                                                                <option {{ old('tipocertificado') == $tipocertificado->id ? 'selected' : ($tipocertificado->id == $actividad->tipo_certificado_id ? 'selected' : '') }} value="{{ $tipocertificado->id }}">{{ $tipocertificado->descripcion }}</option>
                                                            @endforeach
                                                        </select>
                                                        @error('tipocertificado')
                                                        <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-lg-9 col-md-9 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="form-label" for="coordinacion">Coordinación Nacional</label>
                                                        <select id="coordinacion" name="coordinacion" class="form-control" data-live-search="true">
                                                            <option value = "-1"><< SELECCIONE >></option>
                                                            @foreach($coordinaciones as $coordinacion)
                                                                <option {{ old('coordinacion') == $coordinacion->id ? 'selected' : ($coordinacion->id == $actividad->coordinacion_nacional_id ? 'selected' : '') }} value="{{ $coordinacion->id }}">{{ $coordinacion->descripcion }}</option>
                                                            @endforeach
                                                        </select>
                                                        @error('coordinacion')
                                                        <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="form-label" for="origen">Organizador</label>
                                                        <select id="origen" name="origen" class="form-control" data-live-search="true">
                                                            <option value = "-1"><< SELECCIONE >></option>
                                                            <option {{ old('origen') == 'EMP' ? 'selected' : ($actividad->origen == 'EMP' ? 'selected' : '') }} value="EMP">EMP</option>
                                                            <option {{ old('origen') == 'DISTRITO FISCAL' ? 'selected' : ($actividad->origen == 'DISTRITO FISCAL' ? 'selected' : '') }} value="DISTRITO FISCAL">DISTRITO FISCAL</option>
                                                            <option {{ old('origen') == 'COOPERANTE' ? 'selected' : ($actividad->origen == 'COOPERANTE' ? 'selected' : '') }} value="COOPERANTE">COOPERANTE</option>
                                                        </select>
                                                        @error('origen')
                                                        <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-lg-9 col-md-9 col-sm-12">
                                                    <div id="cooperanteorg">
                                                        <div class="form-group">
                                                            <label class="form-label" for="cooperante">Cooperante</label>
                                                            <select id="cooperante" name="cooperante" class="form-control" data-live-search="true">
                                                                <option value = "-1"><< SELECCIONE >></option>
                                                                @foreach($entidades as $entidad)
                                                                    <option {{ old('cooperante') == $entidad->id ? 'selected' : ($entidad->id == $actividad->organizador_id ? 'selected' : '') }} value="{{ $entidad->id }}">{{ $entidad->descripcion }}</option>
                                                                @endforeach
                                                            </select>
                                                            @error('cooperante')
                                                            <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div id="distritoorg">
                                                        <div class="form-group">
                                                            <label class="form-label" for="distritofiscal">Distrito Fiscal</label>
                                                            <select id="distritofiscal" name="distritofiscal" class="form-control" data-live-search="true">
                                                                <option value = "-1"><< SELECCIONE >></option>
                                                                @foreach($distritos as $distrito)
                                                                    <option {{ old('distritofiscal') == $distrito->id ? 'selected' : ($distrito->id == $actividad->organizador_id ? 'selected' : '' ) }} value="{{ $distrito->id }}">{{ $distrito->descripcion }}</option>
                                                                @endforeach
                                                            </select>
                                                            @error('distritofiscal')
                                                            <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-12">
                                                    <div class="form-group">
                                                        <div class="form-group">
                                                            <label class="form-label" for="convenio">¿ Tiene Convenio ?</label>
                                                            <input type="checkbox" id="convenio" name="convenio" data-toggle="toggle" data-on="SI" data-off="NO" data-onstyle="success" data-offstyle="danger"
                                                                {{ old('convenio') == 'on' ? 'checked' : ($actividad->convenio == 1 ? 'checked' : '' ) }}>
                                                            @error('convenio')
                                                                <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-9 col-md-9 col-sm-12">
                                                    <div id="cod-convenio">
                                                        <div class="form-group">
                                                            <label class="form-label" for="codconvenio">Convenio</label>
                                                            <select id="codconvenio" name="codconvenio" class="form-control" data-live-search="true">
                                                                <option value = "-1"><< SELECCIONE >></option>
                                                                @foreach($convenios as $convenio)
                                                                    <option {{ old('codconvenio') == $convenio->id ? 'selected' : ($convenio->id == $actividad->convenio_id ? 'selected' : '') }} value="{{ $convenio->id }}">{{ $convenio->descripcion }}</option>
                                                                @endforeach
                                                            </select>
                                                            @error('codconvenio')
                                                            <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <div id="ent-convenio">
                                                        <div class="form-group">
                                                            <label class="form-label" for="entconvenio">Entidad - Convenio</label>
                                                            <select id="entconvenio" name="entconvenio[]" class="form-control" data-live-search="true" title="<< SELECCIONE >>" multiple>
                                                                @foreach($entidades as $entidad)
                                                                    <option {{ (collect(old('entconvenio'))->contains($entidad->id)) ? 'selected': ((collect($entidadesconvenio)->contains($entidad->id)) ? 'selected' : '') }} value="{{ $entidad->id }}">{{ $entidad->descripcion }}</option>
                                                                @endforeach
                                                            </select>
                                                            @error('entconvenio')
                                                            <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="form-label" for="perfilpersonal">Dirigido A</label>
                                                        <select id="perfilpersonal" name="perfilpersonal[]" class="form-control" data-live-search="true" title="<< SELECCIONE >>" multiple>
                                                            @foreach($perfilespersonales as $perfilpersonal)
                                                                <option {{ (collect(old('perfilpersonal'))->contains($perfilpersonal->id)) ? 'selected' : ((collect($perfilesseleccionados)->contains($perfilpersonal->id)) ? 'selected' : '') }} value="{{ $perfilpersonal->id }}">{{ $perfilpersonal->descripcion }}</option>
                                                            @endforeach
                                                        </select>
                                                        @error('perfilpersonal')
                                                            <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="form-label" for="fechainicio">Fecha Inicio</label>
                                                        <input type="text" id="fechainicio" name="fechainicio" class="form-control mdate" value="{{ old('fechainicio') ?? \Carbon\Carbon::createFromFormat('Y-m-d',$actividad->fecha_inicio)->format('d/m/Y') }}">
                                                        @error('fechainicio')
                                                        <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="form-label" for="fechafin">Fecha Fin</label>
                                                        <input type="text" id="fechafin" name="fechafin" class="form-control mdate" value="{{ old('fechafin') ?? \Carbon\Carbon::createFromFormat('Y-m-d',$actividad->fecha_final)->format('d/m/Y') }}">
                                                        @error('fechafin')
                                                        <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="duracion">Duración</label>
                                                        <div class="input-group mb-3">
                                                            <input type="number" id="duracion" name="duracion" min="0" class="form-control" value="{{ old('duracion') ?? $actividad->duracion }}">-
                                                            <div class="input-group-append">
                                                                <span class="input-group-text"> &nbsp;HORAS ACADÉMICAS</span>
                                                            </div>
                                                        </div>
                                                        @error('duracion')
                                                        <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="form-label" for="horariodetallado">Horario Programado</label>
                                                        <div class="input-group mb-3">
                                                            <textarea id="horariodetallado" name="horariodetallado" class="form-control" rows="2" readonly>{{ old('horariodetallado') ?? $actividad->horario_detallado }}</textarea>
                                                            <input type="hidden" id="horariojdetallado" name="horariojdetallado" class="form-control" readonly value="{{ old('horariojdetallado') ?? $actividad->horarioj_detallado }}">
                                                            <div class="input-group-append">
                                                                <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#frmhorario"><i class="la la-hourglass"></i></button>
                                                                <div class="modal fade" id="frmhorario">
                                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header head-modal">
                                                                                <h5 class="modal-title text-white">Horario Programado</h5>
                                                                                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <div class="row">
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                                                        <div class="form-group">
                                                                                            <label class="form-label" for="dia">Día(s)</label>
                                                                                            <select id="dia" name="dia[]" class="form-control" title="<< SELECCIONE >>" multiple>
                                                                                                <option value = "1">LUNES</option>
                                                                                                <option value = "2">MARTES</option>
                                                                                                <option value = "3">MIÉRCOLES</option>
                                                                                                <option value = "4">JUEVES</option>
                                                                                                <option value = "5">VIERNES</option>
                                                                                                <option value = "6">SÁBADO</option>
                                                                                                <option value = "7">DOMINGO</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                                                                        <div class="form-group">
                                                                                            <label class="form-label" for="horaini">Hora Inicio</label>
                                                                                            <div class="input-group clockpicker" data-placement="bottom" data-align="top" data-autoclose="true">
                                                                                                <input type="text" id="horaini" name="horaini"  class="form-control" value="" readonly>
                                                                                                <span class="input-group-append">
                                                                                                    <span class="input-group-text">
                                                                                                        <i class="fa fa-clock-o"></i>
                                                                                                    </span>
                                                                                                </span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                                                                        <div class="form-group">
                                                                                            <label class="form-label" for="horafin">Hora Fin</label>
                                                                                            <div class="input-group clockpicker" data-placement="bottom" data-align="top" data-autoclose="true">
                                                                                                <input type="text" id="horafin" name="horafin"  class="form-control" value="" readonly>
                                                                                                <span class="input-group-append">
                                                                                                    <span class="input-group-text">
                                                                                                        <i class="fa fa-clock-o"></i>
                                                                                                    </span>
                                                                                                </span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                                                        <div class="form-group pull-right">
                                                                                            <a id="btnGenerarHorario" class="btn btn-exito" onclick="generarTablaHorario()">Agregar</a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                                                        <div class="table-responsive  text-center">
                                                                                            <table id="tblhorario" class="table table-bordered table-hover table-striped table-responsive-sm">
                                                                                                <thead class="thead-encabezado">
                                                                                                <tr>
                                                                                                    <th>ID</th>
                                                                                                    <th>DÍA</th>
                                                                                                    <th>HORA INICIO</th>
                                                                                                    <th>HORA FIN</th>
                                                                                                    <th></th>
                                                                                                </tr>
                                                                                                </thead>
                                                                                                <tbody>

                                                                                                </tbody>
                                                                                            </table>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-cancelar" data-dismiss="modal">Cancelar</button>
                                                                                <button type="button" class="btn btn-aceptar" onclick="genHorario()">Aceptar</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @error('horariodetallado')
                                                        <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="form-label" for="nroaulas">Nro. Aulas</label>
                                                        <input type="number" id="nroaulas" name="nroaulas" min="0" class="form-control" value="{{ old('nroaulas') ?? $actividad->nro_aulas }}">
                                                        @error('nroaulas')
                                                        <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="form-label" for="nrovacantes">Nro. Vacantes</label>
                                                        <input type="number" id="nrovacantes" name="nrovacantes" min="0" class="form-control" value="{{ old('nrovacantes') ?? $actividad->nro_vacantes }}">
                                                        @error('nrovacantes')
                                                        <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="form-label" for="nrodocentes">Nro. Docentes</label>
                                                        <input type="number" id="nrodocentes" name="nrodocentes" min="0" class="form-control" value="{{ old('nrodocentes') ?? $actividad->nro_docentes }}">
                                                        @error('nrodocentes')
                                                        <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-12">

                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-12">
                                                    <div class="form-group prog">
                                                        <label class="form-label" for="convocatoria">Tipo de Convocatoria</label>
                                                        <input type="checkbox" id="convocatoria" name="convocatoria" class="form-control"  data-on="ABIERTA" data-off="CERRADA" data-toggle="toggle"
                                                            {{ old('convocatoria') == 'on' ? 'checked' : ($actividad->convocatoria == 1 ? 'checked' : '' ) }}>
                                                        @error('convocatoria')
                                                        <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="form-label" for="compromiso">¿ Requiere Carta de Compromiso ?</label>
                                                        <input type="checkbox" id="compromiso" name="compromiso" class="w-50" data-toggle="toggle" data-on="SI" data-off="NO" data-onstyle="success" data-offstyle="danger"
                                                            {{ old('compromiso') == 'on' ? 'checked' : ($actividad->compromiso == 1 ? 'checked' : '' ) }}>
                                                        @error('compromiso')
                                                        <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="form-label" for="autoformativo">¿ Autoformativo ?</label>
                                                        <input type="checkbox" id="autoformativo" name="autoformativo" class="w-50" data-toggle="toggle" data-on="SI" data-off="NO" data-onstyle="success" data-offstyle="danger"
                                                            {{ old('autoformativo') == 'on' ? 'checked' : ($actividad->autoformativo == 1 ? 'checked' : '' ) }}>
                                                        @error('autoformativo')
                                                        <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="form-label" for="publico">¿ Es Público ?</label>
                                                        <input type="checkbox" id="publico" name="publico" class="w-50" data-toggle="toggle" data-on="SI" data-off="NO" data-onstyle="success" data-offstyle="danger"
                                                            {{ old('publico') == 'on' ? 'checked' : ($actividad->publico == 1 ? 'checked' : '' ) }}>
                                                        @error('publico')
                                                        <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <div class="pull-right mb-4">
                                                    <a href="{{ route('actividad.option',['codigo' => $actividad->id]) }}" class="btn btn-light "><i class="la la-times-circle-o"></i> Cancelar</a>
                                                    @if($actividad->estado == 1)
                                                        <button id="btnguardaraa" class="btn btn-primary" ><i class="fa fa-save"></i> Guardar</button>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane fade" id="requisitos" role="tabpanel">
                                    <div class="pt-4">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <div class="card card-form ">
                                                    <div class="card-header bg-primary pt-3">
                                                        <h5 style="color: #fff;">Pre-Requisitos</h5>
                                                    </div>
                                                    <div class="card-body">
                                                        <form id="frmrequisito" class="frmrequisito" method="post">
                                                            @csrf
                                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                                <div class="form-group">
                                                                    <label class="form-label" for="actividad">Seleccione la Actividad Académica</label>
                                                                    <select id="actividad" name="actividad" class="form-control" data-live-search="true">
                                                                        <option value="-1"><< SELECCIONE >></option>
                                                                        @foreach($actividades as $actividad)
                                                                            <option value="{{ $actividad->id }}">{{ $actividad->titulo }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                                <div class=" form-group pull-right">
                                                                    <button type="submit" class="btn btn-primary" >
                                                                        <i class="la la-arrow-down"></i>&nbsp;Agregar</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <div id="listadorequisito">
                                                    @include('actividad.listarequisito')
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <div class="pull-right mb-4">
                                                    <a href="{{ route('actividad.option',['codigo' => $actividad->id]) }}" class="btn btn-light "><i class="la la-times-circle-o"></i> Volver a Opciones</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="resoluciones" role="tabpanel">
                                    <div class="pt-4">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <div class="card card-form ">
                                                    <div class="card-header bg-primary pt-3">
                                                        <h5 style="color: #fff;">Resoluciones</h5>
                                                    </div>
                                                    <div class="card-body">
                                                        <form id="frmresolucion" class="frmresolucion" method="post">
                                                            @csrf
                                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                                <div class="form-group">
                                                                    <label class="form-label" for="plan">Seleccione la Resolución</label>
                                                                    <select id="resolucion" name="resolucion" class="form-control" data-live-search="true">
                                                                        <option value="-1"><< SELECCIONE >></option>
                                                                        @foreach($resolucionesall as $resolucion)
                                                                            <option value="{{ $resolucion->id }}">{{ $resolucion->descripcion }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                                <div class=" form-group pull-right">
                                                                    <button type="submit" class="btn btn-primary" >
                                                                        <i class="la la-arrow-down"></i>&nbsp;Agregar</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <div id="listadoresolucion">
                                                    @include('actividad.listaresolucion')
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <div class="pull-right mb-4">
                                                    <a href="{{ route('actividad.option',['codigo' => $actividad->id]) }}" class="btn btn-light "><i class="la la-times-circle-o"></i> Volver a Opciones</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="planes">
                                    <div class="pt-4">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <div class="card card-form ">
                                                    <div class="card-header bg-primary pt-3">
                                                        <h5 style="color: #fff;">Planes</h5>
                                                    </div>
                                                    <div class="card-body">
                                                        <form id="frmplan" class="frmplan" method="post">
                                                            @csrf
                                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                                <div class="form-group">
                                                                    <label class="form-label" for="plan">Seleccione el Plan</label>
                                                                    <select id="plan" name="plan" class="form-control" data-live-search="true">
                                                                        <option value="-1"><< SELECCIONE >></option>
                                                                        @foreach($planesall as $plan)
                                                                            <option value="{{ $plan->id }}">{{ $plan->descripcion }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                                <div class=" form-group pull-right">
                                                                    <button type="submit" class="btn btn-primary" >
                                                                        <i class="la la-arrow-down"></i>&nbsp;Agregar</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <div id="listadoplan">
                                                    @include('actividad.listaplan')
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <div class="pull-right mb-4">
                                                    <a href="{{ route('actividad.option',['codigo' => $actividad->id]) }}" class="btn btn-light "><i class="la la-times-circle-o"></i> Volver a Opciones</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="politicas">
                                    <div class="pt-4">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <div class="card card-form ">
                                                    <div class="card-header bg-primary pt-3">
                                                        <h5 style="color: #fff;">Políticas</h5>
                                                    </div>
                                                    <div class="card-body">
                                                        <form id="frmpolitica" class="frmpolitica" method="post">
                                                            @csrf
                                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                                <div class="form-group">
                                                                    <label class="form-label" for="politica">Seleccione la Política</label>
                                                                    <select id="politica" name="politica" class="form-control" data-live-search="true">
                                                                        <option value="-1"><< SELECCIONE >></option>
                                                                        @foreach($politicasall as $politica)
                                                                            <option value="{{ $politica->id }}">{{ $politica->descripcion }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                                <div class=" form-group pull-right">
                                                                    <button type="submit" class="btn btn-primary" >
                                                                        <i class="la la-arrow-down"></i>&nbsp;Agregar</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <div id="listadopolitica">
                                                    @include('actividad.listapolitica')
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <div class="pull-right mb-4">
                                                    <a href="{{ route('actividad.option',['codigo' => $actividad->id]) }}" class="btn btn-light "><i class="la la-times-circle-o"></i> Volver a Opciones</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scriptsapp')
    <script type="application/javascript">
        $('#btnguardaraa').click(function(e) {
            var form =  $(this).closest("form");
            e.preventDefault();

            Swal.fire({
                title: "Actividad Académica",
                text: "¿Esta seguro de continuar la edición?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: "#28A745",
                confirmButtonText: "Si",
                cancelButtonColor: "#DC3545",
                cancelButtonText: "No",
            })
                .then(resultado => {
                    if (resultado.value) {
                        form.submit();
                    }
                });
        });

        $(function(){
            $('#distrito-sede').hide();
            $('#distritoorg').hide();
            $('#cooperanteorg').hide();
            // $('#ent-convenio').hide();

            var modalidad = $('#modalidad').val();
            if(modalidad !== '3' && modalidad !== '-1'){
                $('#distrito-sede').show();
            }

            onSelectModalidadChange();
            onSelectLineaChange();
            onSelectOrigenChange();
            onToggleConvenioChange();

            $('.mdate').bootstrapMaterialDatePicker({
                time:false,
                lang: 'es',
                format: 'DD/MM/YYYY',
                //minDate: new Date(),
                cancelText: 'Cancelar',
                okText: 'Aceptar',
                clearText: 'Limpiar',
                nowText: 'Hoy'
            });

            $('.clockpicker').clockpicker({
                donetext: 'Elegir',
            }).find('input').change(function () {
                //console.log(this.value);
            });
        });
    </script>
    <script src="{{ asset('assets/js/actividadacademica.js') }}"></script>
    <script src="{{ asset('assets/js/horario.js') }}"></script>
    <script src="{{ asset('assets/js/requisito.js') }}"></script>
    <script src="{{ asset('assets/js/resolucion.js') }}"></script>
    <script src="{{ asset('assets/js/plan.js') }}"></script>
    <script src="{{ asset('assets/js/politica.js') }}"></script>
@endpush
