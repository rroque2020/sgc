@extends('layouts.layout')
@section('title')
    <div class="col-sm-6 p-md-0">
        <div class="welcome-text">
            <h4>Actividades Académicas - Seleccionar Docente</h4>
        </div>
    </div>
@endsection

@section('breadcrumb')
    <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Inicio</a></li>
            <li class="breadcrumb-item"><a href="{{ route('actividad.index') }}">Actividades Académicas</a></li>
            <li class="breadcrumb-item active"><a href="javascript:void(0);">Seleccionar Docente</a></li>
        </ol>
    </div>
@endsection

@section('content')
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="container-fluid mb-1">
                        {{--                        <div style="position: absolute; right: 10px; top:10px;">--}}
                        {{--                            {!!html_entity_decode(Helper::estadoAA($actividad->estado, false))!!}--}}
                        {{--                        </div>--}}

                        <div class="text-center mt-1">
                            <h4>CÓDIGO: {{ $actividad->codigo }}</h4>
                            <h2>{{ $actividad->titulo }}</h2>
                            <input type="hidden" id="codactividad" name="codactividad" value="{{ $actividad->id }}">
                            <p id="estado">{!! html_entity_decode(Helper::estadoAA($actividad->estado, false)) !!}</p>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="card" style="height: auto !important;">
                                    <div class="card-header bg-primary text-white text-center py-2" style="display: block !important;">
                                        <strong>LISTA DE DOCENTES SELECCIONADOS</strong>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div id="listaseleccionada" class="col-lg-12 col-md-12 col-sm-12">
                                                @include('docente.listarseleccionados')
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div id="mensaje"></div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="card" style="height: auto !important;">
                                    <div class="card-header bg-primary text-white text-center py-2" style="display: block !important;">
                                        <strong>BÚSQUEDA DE DOCENTES</strong>
                                    </div>
                                    <div class="card-body">
                                        <form id="frmbusquedadocente" class="frmbusquedadocente" method="POST">
                                            @csrf
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">Ingresar Filtro : </span>
                                                        </div>
                                                        <input type="text" id="filtro" name="filtro" class="form-control input-rounded" tabindex="1" autofocus>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <div class="pull-right">
                                                        <button type="submit" name="submit" class="btn btn-rounded btn-primary">
                                                            <i class="la la-search"></i> Buscar
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div id="listabuscada" class="col-lg-12 col-md-12 col-sm-12">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scriptsapp')
    <script src="{{ asset('assets/js/selecciondocente.js') }}"></script>
@endpush
