@extends('layouts.layout')
@section('title')
    <div class="col-sm-6 p-md-0">
        <div class="welcome-text">
            <h4>Actividades Académicas - Detalle</h4>
        </div>
    </div>
@endsection

@section('breadcrumb')
    <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Inicio</a></li>
            <li class="breadcrumb-item"><a href="{{ route('actividad.index') }}">Actividades Académicas</a></li>
            <li class="breadcrumb-item active"><a href="javascript:void(0);">Detalle</a></li>
        </ol>
    </div>
@endsection
@section('content')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="aa">
                            <ul class="nav nav-tabs">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#datos">
                                        <i class="fa fa-list"></i>
                                        Datos Generales
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#requisitos">
                                        <i class="fa fa-table"></i>
                                        Pre-Requisitos
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#resoluciones">
                                        <i class="fa fa-table"></i>
                                        Resoluciones
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#planes">
                                        <i class="fa fa-table"></i>
                                        Planes Alineados
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#politicas">
                                        <i class="fa fa-table"></i>
                                        Políticas Alineadas
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade show active" id="datos" role="tabpanel">
                                    <div class="pt-4">
                                        <div class="row pl-2">
                                            <div class="col-lg-3 col-md-3 col-sm-12">
                                                <div class="form-group" style>
                                                    <label class="form-label font-weight-bold" for="titulo">Código</label>
                                                    <p class="text-justify subrayado">{{ $actividad->codigo }}</p>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12">
                                                <div class="form-group" style>
                                                    <label class="form-label font-weight-bold" for="estado">Estado</label><br>
                                                    {!!html_entity_decode(Helper::estadoAA($actividad->estado, false))!!}
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-12">
                                                <div class="form-group prog">
                                                    <label class="form-label font-weight-bold" for="programado">¿La Actividad es ?</label>
                                                    <p class="text-justify subrayado">{{ $actividad->programado == 1 ? 'PROGRAMADO' : 'NO PROGRAMADO'}}</p>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <div class="form-group" style>
                                                    <label class="form-label font-weight-bold" for="titulo">Titulo de la Actividad Académica</label>
                                                    <p class="text-justify subrayado">{{ $actividad->titulo }}</p>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <div class="form-group">
                                                    <label class="form-label font-weight-bold" for="descripcion">Descripción/Sumilla</label>
                                                    <p class="text-justify subrayado">{{ $actividad->descripcion }}</p>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <label class="form-label font-weight-bold" for="subgerencia">Sub Gerencia</label>
                                                    <p class="text-justify subrayado">{{ $actividad->subgerencia }}</p>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <label class="form-label font-weight-bold" for="tipo">Tipo</label>
                                                    <p class="text-justify subrayado">{{ $actividad->tipo }}</p>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <label class="form-label font-weight-bold" for="modalidad">Modalidad - Sede</label>
                                                    <p class="text-justify subrayado">{{ $actividad->modalidad }}</p>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12">
                                                <div class="form-group">
                                                    <label class="form-label font-weight-bold" for="sistema">Sistema</label>
                                                    <p class="text-justify subrayado">{{ $actividad->sistema }}</p>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12">
                                                <div class="form-group">
                                                    <label class="form-label font-weight-bold" for="tematica">Temática</label>
                                                    <p class="text-justify subrayado">{{ $actividad->tematica }}</p>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <div class="form-group">
                                                    <label class="form-label font-weight-bold" for="linea">Línea Estratégica - Bloque Académico</label>
                                                    <p class="text-justify subrayado">{{ $actividad->linea }}</p>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-12">
                                                <div class="form-group">
                                                    <label class="form-label font-weight-bold" for="tipocertificado">Tipo de Certificado</label>
                                                    <p class="text-justify subrayado">{{ $actividad->tipocertificado }}</p>
                                                </div>
                                            </div>
                                            @if(isset($actividad->coordinacionnacional))
                                                <div class="col-lg-8 col-md-8 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="form-label font-weight-bold" for="coordinacionnacional">Coordinación Nacional</label>
                                                        <p class="text-justify subrayado">{{ $actividad->coordinacionnacional }}</p>
                                                    </div>
                                                </div>
                                            @endif
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <div class="form-group">
                                                    <label class="form-label font-weight-bold" for="organizador">Organizador</label>
                                                    <p class="text-justify subrayado">{{ $actividad->organizador }}</p>
                                                </div>
                                            </div>
                                            @if(isset($actividad->convenio))
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="form-label font-weight-bold" for="convenio">Convenio</label>
                                                        <p class="text-justify subrayado">{{ $actividad->convenio }}</p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="form-label font-weight-bold" for="convenio">Entidad - Convenio</label>
                                                        <p class="text-justify subrayado">{{ $actividad->entidadconvenio }}</p>
                                                    </div>
                                                </div>
                                            @endif
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <div class="form-group">
                                                    <label class="form-label font-weight-bold" for="perfil">Dirigido A</label>
                                                    <p class="text-justify subrayado">{{ $actividad->perfil }}</p>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-12">
                                                <div class="form-group">
                                                    <label class="form-label font-weight-bold" for="fechainicio">Fecha de Inicio</label>
                                                    <p class="text-justify subrayado">{{ \Carbon\Carbon::createFromFormat('Y-m-d',$actividad->fecha_inicio)->format('d/m/Y') }}</p>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-12">
                                                <div class="form-group">
                                                    <label class="form-label font-weight-bold" for="fechafinal">Fecha Final</label>
                                                    <p class="text-justify subrayado">{{ \Carbon\Carbon::createFromFormat('Y-m-d',$actividad->fecha_final)->format('d/m/Y') }}</p>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-12">
                                                <div class="form-group">
                                                    <label class="form-label font-weight-bold" for="duracion">Duración</label>
                                                    <p class="text-justify subrayado">{{ $actividad->duracion }} HORAS ACADÉMICAS</p>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <div class="form-group">
                                                    <label class="form-label font-weight-bold" for="horario">Horario</label>
                                                    <p class="text-justify subrayado">{{ $actividad->horario_detallado }}</p>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-12">
                                                <div class="form-group">
                                                    <label class="form-label font-weight-bold" for="nroaulas">Nro de Aulas</label>
                                                    <p class="text-justify subrayado">{{ $actividad->nro_aulas }}</p>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-12">
                                                <div class="form-group">
                                                    <label class="form-label font-weight-bold" for="nrovacantes">Nro de Vacantes</label>
                                                    <p class="text-justify subrayado">{{ $actividad->nro_vacantes }}</p>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-12">
                                                <div class="form-group">
                                                    <label class="form-label font-weight-bold" for="nrodocentes">Nro de Docentes</label>
                                                    <p class="text-justify subrayado">{{ $actividad->nro_docentes }}</p>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12">
                                                <div class="form-group">
                                                    <label class="form-label font-weight-bold" for="tipoconvocatoria">Tipo de Convocatoria</label>
                                                    <p class="text-justify subrayado">
                                                        {{ $actividad->convocatoria == 1 ? 'ABIERTA' : 'CERRADA' }}
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12">
                                                <div class="form-group">
                                                    <label class="form-label font-weight-bold" for="compromiso">¿Requiere Carta de Compromiso?</label>
                                                    <p class="text-justify subrayado">
                                                        {{ $actividad->compromiso == 1 ? 'SI' : 'NO' }}
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12">
                                                <div class="form-group">
                                                    <label class="form-label font-weight-bold" for="compromiso">¿Autoformativo?</label>
                                                    <p class="text-justify subrayado">
                                                        {{ $actividad->autoformativo == 1 ? 'SI' : 'NO' }}
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12">
                                                <div class="form-group">
                                                    <label class="form-label font-weight-bold" for="compromiso">¿Público?</label>
                                                    <p class="text-justify subrayado">
                                                        {{ $actividad->publico == 1 ? 'SI' : 'NO' }}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="requisitos" role="tabpanel">
                                    <div class="pt-4">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                @if(!isset($requisitos) || count($requisitos) == 0)
                                                    <div class='informado'>
                                                        <label style='color:#0055A0'>... No se encontraron registros ...</label>
                                                    </div>
                                                @else
                                                    <div class="table-responsive mt-4 text-center">
                                                        <table id="tblrequisito" class="table table-bordered table-hover table-striped verticle-middle table-responsive-sm" style="min-width: 845px">
                                                            <thead class="thead-encabezado">
                                                            <tr>
                                                                <th style="width:10%">Código</th>
                                                                <th style="width:90%">Actividad</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach($requisitos as $requisito)
                                                                <tr class="item" id="fila{{ $requisito->id }}">
                                                                    <td class="text-center">
                                                                        {{ $requisito->id }}
                                                                    </td>
                                                                    <td class="text-justify">
                                                                        {{ $requisito->titulo }}
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="resoluciones" role="tabpanel">
                                    <div class="pt-4">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                @if(!isset($resoluciones) || count($resoluciones) == 0)
                                                    <div class='informado'>
                                                        <label style='color:#0055A0'>... No se encontraron registros ...</label>
                                                    </div>
                                                @else
                                                    <div class="table-responsive mt-4 text-center">
                                                        <table id="tblresolucion" class="table table-bordered table-hover table-striped verticle-middle table-responsive-sm" style="min-width: 845px">
                                                            <thead class="thead-encabezado">
                                                            <tr>
                                                                <th style="width:10%;">Código</th>
                                                                <th style="width:80%;">Resolución</th>
                                                                <th style="width:10%;">Archivo</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach($resoluciones as $resolucion)
                                                                    <tr class="item" id="filaresolucion{{ $resolucion->id }}">
                                                                        <td class="text-center">
                                                                            {{ $resolucion->id }}
                                                                        </td>
                                                                        <td class="text-justify">
                                                                            {{ $resolucion->descripcion }}
                                                                        </td>
                                                                        <td class="text-justify">
                                                                            {{ $resolucion->archivo }}
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="planes">
                                    <div class="pt-4">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                @if(!isset($planes) || count($planes) == 0)
                                                    <div class='informado'>
                                                        <label style='color:#0055A0'>... No se encontraron registros ...</label>
                                                    </div>
                                                @else
                                                    <div class="table-responsive mt-4 text-center">
                                                        <table id="tblplan" class="table table-bordered table-striped verticle-middle table-responsive-sm" style="min-width: 845px">
                                                            <thead class="thead-encabezado">
                                                            <tr>
                                                                <th style="width:10%;">Código</th>
                                                                <th style="width:40%;">Plan</th>
                                                                <th style="width:40%;">Entidad</th>
                                                                <th style="width:10%;">Archivo</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach($planes as $plan)
                                                                    <tr class="item" id="filaplan{{ $plan->id }}">
                                                                        <td class="text-center">
                                                                            {{ $plan->id }}
                                                                        </td>
                                                                        <td class="text-justify">
                                                                            {{ $plan->descripcion }}
                                                                        </td>
                                                                        <td class="text-justify">
                                                                            {{ $plan->entidad }}
                                                                        </td>
                                                                        <td class="text-justify">
                                                                            {{ $plan->archivo }}
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="politicas">
                                    <div class="pt-4">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                @if(!isset($politicas) || count($politicas) == 0)
                                                    <div class='informado'>
                                                        <label style='color:#0055A0'>... No se encontraron registros ...</label>
                                                    </div>
                                                @else
                                                    <div class="table-responsive mt-4 text-center">
                                                        <table id="tblpolitica" class="table table-bordered table-striped verticle-middle table-responsive-sm" style="min-width: 845px">
                                                            <thead class="thead-encabezado">
                                                            <tr>
                                                                <th style="width:10%;">Código</th>
                                                                <th style="width:50%;">Política</th>
                                                                <th style="width:40%;">Entidad</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach($politicas as $politica)
                                                                <tr class="item" id="filapolitica{{ $politica->id }}">
                                                                    <td class="text-center">
                                                                        {{ $politica->id }}
                                                                    </td>
                                                                    <td class="text-justify">
                                                                        {{ $politica->descripcion }}
                                                                    </td>
                                                                    <td class="text-justify">
                                                                        {{ $politica->entidad }}
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="card-footer">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="pull-right mb-4">
                        <a href="{{ route('actividad.index') }}" class="btn btn-light" ><i class="fa fa-arrow-left"></i> Volver a Lista</a>
                        <a href="{{ route('actividad.option',['codigo' => $actividad->id]) }}" class="btn btn-primary" ><i class="fa fa-arrow-left"></i> Volver a Opciones</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scriptsapp')
    <script type="application/javascript">

        $('#btnguardaraa').click(function(e) {
            var form =  $(this).closest("form");
            e.preventDefault();

            Swal.fire({
                    title: "Actividad Académica",
                    text: "Revise la información de cada una de las pestañas, para grabar, ¿Esta seguro de continuar?",
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonColor: "#28A745",
                    confirmButtonText: "Si",
                    cancelButtonColor: "#DC3545",
                    cancelButtonText: "No",
                })
                .then(resultado => {
                    if (resultado.value) {
                        form.submit();
                    }
                });
        });


        $(function(){
            $('#distrito-sede').hide();
            $('#cooperanteorg').hide();
            $('#ent-convenio').hide();
            $('#distritoorg').hide();

            var modalidad = $('#modalidad').val();
            if(modalidad !== '3' && modalidad !== '-1'){
                $('#distrito-sede').show();
            }

            onSelectModalidadChange();
            onSelectLineaChange();
            onSelectOrigenChange();
            onToggleConvenioChange();

            $('.mdate').bootstrapMaterialDatePicker({
                time:false,
                lang: 'es',
                format: 'DD/MM/YYYY',
                //minDate: new Date(),
                cancelText: 'Cancelar',
                okText: 'Aceptar',
                clearText: 'Limpiar',
                nowText: 'Hoy'
            });

            $('.clockpicker').clockpicker({
                donetext: 'Elegir',
            }).find('input').change(function () {
                //console.log(this.value);
            });
        });
    </script>
    <script src="{{ asset('assets/js/actividadacademica.js') }}"></script>
    <script src="{{ asset('assets/js/horario.js') }}"></script>
    <script src="{{ asset('assets/js/requisito.js') }}"></script>
    <script src="{{ asset('assets/js/resolucion.js') }}"></script>
    <script src="{{ asset('assets/js/plan.js') }}"></script>
@endpush
