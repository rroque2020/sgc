@if(!isset($planes) || count($planes) == 0)
    <div class='informado'>
        <label style='color:#0055A0'>... No se encontraron registros ...</label>
    </div>
@else
    <div class="table-responsive mt-4 text-center">
        <table id="tblplan" class="table table-bordered table-hover table-striped verticle-middle table-responsive-sm" style="min-width: 845px">
            <thead class="thead-encabezado">
            <tr>
                <th style="width:10%;">Código</th>
                <th style="width:35%;">Plan</th>
                <th style="width:35%;">Entidad</th>
                <th style="width:10%;">Archivo</th>
                <th style="width:10%;">Acciones</th>
            </tr>
            </thead>
            <tbody>
            @foreach($planes as $plan)
                <tr class="item" id="filaplan{{ $plan->id }}">
                    <td class="text-center">
                        <input type="hidden" class="codplan" name="codplan[]" value="{{ $plan->id }}">
                        {{ $plan->id }}
                    </td>
                    <td class="text-justify">
                        {{ $plan->descripcion }}
                    </td>
                    <td class="text-justify">
                        {{ $plan->entidad }}
                    </td>
                    <td class="text-justify">
                        {{ $plan->archivo }}
                    </td>
                    <td class="text-center">
                        <a class="btn btn-danger btn-xs" onclick="eliminarplanes({{ $plan->idap }},{{ $plan->actividad_academica_id }})">
                            <i class="la la-trash"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endif
