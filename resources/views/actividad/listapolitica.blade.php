@if(!isset($politicas) || count($politicas) == 0)
    <div class='informado'>
        <label style='color:#0055A0'>... No se encontraron registros ...</label>
    </div>
@else
    <div class="table-responsive mt-4 text-center">
        <table id="tblpolitica" class="table table-bordered table-hover table-striped verticle-middle table-responsive-sm" style="min-width: 845px">
            <thead class="thead-encabezado">
            <tr>
                <th style="width:10%;">Código</th>
                <th style="width:40%;">Politica</th>
                <th style="width:40%;">Entidad</th>
                <th style="width:10%;">Acciones</th>
            </tr>
            </thead>
            <tbody>
            @foreach($politicas as $politica)
                <tr class="item" id="filapolitica{{ $politica->id }}">
                    <td class="text-center">
                        <input type="hidden" class="codpolitica" name="codpolitica[]" value="{{ $politica->id }}">
                        {{ $politica->id }}
                    </td>
                    <td class="text-justify">
                        {{ $politica->descripcion }}
                    </td>
                    <td class="text-justify">
                        {{ $politica->entidad }}
                    </td>
                    <td class="text-center">
                        <a class="btn btn-danger btn-xs" onclick="eliminarpoliticas({{ $politica->idap }},{{ $politica->actividad_academica_id }})">
                            <i class="la la-trash"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endif
