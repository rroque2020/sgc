<div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-header head-modal">
            <h5 class="modal-title text-white">{{ isset($item) ? 'Editar Capacidad : '.$item->id : 'Crear una Capacidad'}}</h5>
            <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
            </button>
        </div>
        <form id="frmcapacidad" class="frmcapacidad" method="{{ isset($item)? 'put':'post' }}">
            <div class="modal-body">
                @csrf
                @if(isset($item))
                    @method('PUT')
                @endif
                <input type="hidden" id="codmodulo" name="codmodulo" value="{{ isset($modulo)? $modulo: '' }}">
                <input type="hidden" id="codcapacidad" name="codcapacidad" value="{{ isset($item)? $item->id: '' }}">
                <input type="hidden" id="codcompetencia" name="codcompetencia" value="{{ isset($item) ? $item->di_competencia_id : $competencia }}">
                <div id="mensaje"></div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="orden">Seleccione el Nro. de Capacidad</label>
                            <select id="orden" name="orden" class="form-control" data-live-search="true">
                                <option value="-1"><< SELECCIONE >></option>
                                @for ($i = 0; $i < 10; $i++)
                                    <option value="{{ $i + 1  }}" {{ isset($item) && $item->orden == ($i + 1) ? 'selected' : '' }}>{{ $i + 1 }}</option>
                                @endfor
                            </select>
                            <div id="orden_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="form-group">
                            <label class="form-label" for="descripcion">Resultado de Aprendizaje</label>
                            <textarea id="descripcion" name="descripcion" class="form-control"  rows="4">{{ isset($item) ? $item->descripcion : '' }}</textarea>
                            <div id="descripcion_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-cancelar" data-dismiss="modal"><i class="la la-times-circle-o"></i> Cancelar</button>
                <button type="submit" class="btn btn-aceptar"><i class="fa fa-save"></i> Guardar</button>
            </div>
        </form>
    </div>
</div>
