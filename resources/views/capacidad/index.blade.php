@if(!isset($capacidades) || count($capacidades) == 0)
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 mb-2" style="margin-top: -40px;">
            <div class="pull-right">
                <a class="btn btn-rounded btn-success btn-xs" onclick="frmCapacidad({{ $competencia }},0)"
                   data-toggle="tooltip" data-placement="top" title="Nueva Capacidad">
                        <span class="btn-icon-left text-primary">
                            <i class="fa fa-plus color-primary"></i>
                        </span>Nueva Capacidad
                </a>
            </div>
        </div>
    </div>
    <div class='informado col-lg-12 col-md-12 col-sm-12 mt-1'>
        <label style='color:#0055A0'>... No se encontraron registros de capacidades ...</label>
    </div>
@else
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 mb-2" style="margin-top: -40px;">
            <div class="pull-right">
                <a class="btn btn-rounded btn-success btn-xs" onclick="frmCapacidad({{ $competencia }},0)"
                   data-toggle="tooltip" data-placement="top" title="Nueva Capacidad">
                        <span class="btn-icon-left text-primary">
                            <i class="fa fa-plus color-primary"></i>
                        </span>Nueva Capacidad
                </a>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-xs-12">
            <div id="accordion-capacidad" class="accordion">
                @foreach($capacidades as $capacidad)
                    <div class="accordion__item">
                        <div class="accordion__header cabecera-success-accordion" data-toggle="collapse" data-target="#cap{{ $capacidad->id }}">
                            <span class="accordion__header--text">Resultado de Aprendizaje {{ $capacidad->orden }}</span>
                            <span class="accordion__header--indicator"></span>
                            <a class="text-info" onclick="frmCapacidad({{ $capacidad->di_competencia_id }}, {{ $capacidad->id }})"
                               data-toggle="tooltip" data-placement="top" title="Editar Capacidad">
                                (<i class="la la-pencil"></i>)
                            </a>
                            <a class="text-info" onclick="eliminarCapacidad({{ $capacidad->di_competencia_id }},{{ $capacidad->id }})"
                               data-toggle="tooltip" data-placement="top" title="Eliminar Capacidad">
                                (<i class="la la-trash"></i>)
                            </a>
                        </div>
                        <div id="cap{{ $capacidad->id }}" class="capacidades collapse accordion__body" data-parent="#accordion-capacidad">
                            <div class="accordion__body--text">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 text-justify mt-1">
                                        {{ $capacidad->descripcion }}
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 my-2">
{{--                                        <div class="card my-2" style="height: auto !important;">--}}
{{--                                            <div class="card-header bg-warning text-white text-center py-2" style="display: block !important;">--}}
{{--                                                <strong>ORGANIZACIÓN DE LAS SESIONES</strong>--}}
{{--                                            </div>--}}
{{--                                            <div class="card-body">--}}
                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 my-2">
                                                        <div class="pull-right">
                                                            <a class="btn btn-rounded btn-warning btn-xs" onclick="frmSesion({{ $capacidad->id }},0)"
                                                               data-toggle="tooltip" data-placement="top" title="Nueva Sesión">
                                                <span class="btn-icon-left text-warning">
                                                    <i class="fa fa-plus color-primary"></i>
                                                </span>Nueva Sesión
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <hr/>
                                                    <div id="sesiondiv{{ $capacidad->id }}" class="col-lg-12 col-md-12 col-sm-12 mt-1">

                                                    </div>
                                                </div>
{{--                                            </div>--}}
{{--                                        </div>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endif
<script type="text/javascript">
    $('.collapse').on('show.bs.collapse', function(e) {
        if ($(this).is(e.target)) {
            if(this.id.substring(0,3) === 'cap'){
                var cap = this.id.substring(3); // activated tab
                actualizarSesiones(cap);
            }
        }
    });
</script>
