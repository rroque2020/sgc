<div class="header">
    <div class="header-content">
        <nav class="navbar navbar-expand">
            <div class="collapse navbar-collapse justify-content-between">
                <div class="header-left">
                </div>
                <ul class="navbar-nav header-right">
                    <li class="nav-item  header-profile">
                        <a class="nav-link text-white" href="#" role="button" >
                            <i class="ti-user"></i>
                            <span>Usuario</span>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>
