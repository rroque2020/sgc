<div class="dlabnav">
    <div class="dlabnav-scroll">
        <ul class="metismenu" id="menu">
            <li class="nav-label first">MENÚ PRINCIPAL</li>
            <li><a class="ai-icon" href="/" aria-expanded="false">
                    <i class="la la-home"></i>
                    <span class="nav-text">Inicio</span>
                </a>
            </li>
            <li><a class="ai-icon" href="/actividadacademica" aria-expanded="false">
                    <i class="la la-graduation-cap"></i>
                    <span class="nav-text">Actividades Académicas</span>
                </a>
            </li>
            <li><a class="ai-icon" href="/docentes" aria-expanded="false">
                    <i class="la la-user"></i>
                    <span class="nav-text">Docentes</span>
                </a>
            </li>
        </ul>
    </div>
    <div class="text-white bg-warning2" style="position: fixed; bottom: 0; left: 0; width: 17.1875rem;">
            <ul class="metismenu" id="menu">
                <li>
                    <a href="/admin/home" class="ai-icon">
                        <i class="la la-backward"></i>
                        <span class="ml-2">Regresar al Área Principal </span>
                    </a>
                </li>
                <li>
                    <a href="/logout" class="ai-icon">
                        <i class="la la-sign-out"></i>
                        <span class="ml-2">Cerrar Sesión </span>
                    </a>
                </li>
            </ul>
    </div>
</div>

