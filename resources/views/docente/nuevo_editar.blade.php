@extends('layouts.layout')
@section('title')
    <div class="col-sm-6 p-md-0">
        <div class="welcome-text">
            <h4>{{ isset($item) ? 'Editar Docente '.$item->id : 'Nuevo Docente'}}</h4>
        </div>
    </div>
@endsection

@section('breadcrumb')
    <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Inicio</a></li>
            <li class="breadcrumb-item active"><a href="{{ route('docentes.index') }}">Docentes</a></li>
            <li class="breadcrumb-item active">
                <a href="javascript:void(0);">
                    {{ isset($item) ? 'Editar Docente' : 'Nuevo Docente'}}
                </a>
            </li>
        </ol>
    </div>
@endsection

@section('content')
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card">
            <div class="card-header">
                <h4>Información Básica</h4>
            </div>
            <div id="mensaje"></div>
            @if(isset($item))
                <form action="{{ route('docentes.update', ['docente' => $item->id]) }}" id="frmdocente" class="frmdocente" method="post">
                    @method('PUT')
            @else
                <form action="{{ route('docentes.store') }}" id="frmdocente" class="frmdocente" method="post">
            @endif
                <div class="card-body">
                    @error('unknown')
                    <div class="col-lg-12 col-md-12 rechazado">
                        * {{ $message }}
                    </div>
                    @enderror
                    @csrf
                    <input type="hidden" name="codigo" value="{{ isset($item) ? $item->id : '' }}">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <div class="form-group prog">
                                <label class="form-label" for="tipo">Tipo</label>
                                <input type="checkbox" id="tipo" name="tipo" class="form-control toggleCheck" data-toggle="toggle" data-on="INTERNO" data-off="EXTERNO"
                                    {{ old('tipo') == 'on' ? 'checked' : (isset($item) && $item->tipo == 1 ? 'checked' : '' ) }}>
                                @error('tipo')
                                    <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-xs-12">
                        </div>
                        <div class="col-lg-4 col-md-4 col-xs-12">
                            @if(isset($item))
                                <div class="form-group">
                                    <label class="form-label" for="estado">Estado</label>
                                    <input type="checkbox" id="estado" name="estado" data-toggle="toggle" data-on="ACTIVO" data-off="INACTIVO" data-onstyle="success" data-offstyle="danger"
                                        {{ old('estado') == 'on' ? 'checked' : (isset($item) && $item->estado == 1 ? 'checked' : '' ) }}>
                                    @error('estado')
                                        <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                    @enderror
                                </div>
                               @endif
                        </div>

                        @if(isset($item))
                            <div class="col-lg-4 col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label class="form-label" for="dni">DNI</label>
                                    <p class="text-justify subrayado">{{ $item->dni }}</p>
                                    <input type="hidden" id="dni" name="dni" value="{{ $item->dni }}">
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-8 col-xs-12">
                                <div class="form-group">
                                    <label class="form-label" for="apellidos">Apellidos y Nombres</label>
                                    <p class="text-justify subrayado">{{ $item->apellidos.' '.$item->nombres }}</p>
                                    <input type="hidden" id="apellidos" name="apellidos" value="{{ $item->apellidos }}">
                                    <input type="hidden" id="nombres" name="nombres" value="{{ $item->nombres }}">
                                </div>
                            </div>
                        @else
                            <div class="col-lg-2 col-md-2 col-xs-12">
                                <div class="form-group">
                                    <label class="form-label" for="dni">DNI</label>
                                    <input type="text" id="dni" name="dni" class="form-control"  onkeypress="return esNumerico(event)"
                                           onkeyup="listarDatos()" maxlength="8" value="{{ old('dni') ? old('dni') : (isset($item) ? $item->dni : '')  }}">
                                    @error('dni')
                                    <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-5 col-md-5 col-xs-12">
                                <div class="form-group">
                                    <label class="form-label" for="apellidos">Apellidos</label>
                                    <input type="text" id="apellidos" name="apellidos" class="form-control "
                                           value="{{ old('apellidos') ? old('apellidos') : (isset($item) ? $item->apellidos : '')  }}">
                                    @error('apellidos')
                                    <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-5 col-md-5 col-xs-12">
                                <div class="form-group">
                                    <label class="form-label" for="nombres">Nombres</label>
                                    <input type="text" id="nombres" name="nombres" class="form-control"
                                           value="{{ old('nombres') ? old('nombres') : (isset($item) ? $item->nombres : '')  }}">
                                    @error('nombres')
                                    <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        @endif
                        <div class="col-lg-4 col-md-4 col-xs-12">
                            <div class="form-group">
                                <label class="form-label" for="celular">Celular</label>
                                <input type="text" id="celular" name="celular" class="form-control" onkeypress="return esNumerico(event)"
                                       maxlength="9" value="{{ old('celular') ? old('celular') : (isset($item) ? $item->celular : '')  }}">
                                @error('celular')
                                <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-xs-12">
                            <div class="form-group">
                                <label class="form-label" for="fecha">Fecha de Nacimiento</label>
                                <input type="text" id="fecha" name="fecha" class="form-control mdate"
                                       value="{{ old('fecha') ? old('fecha') : (isset($item->fecha)  ? \Carbon\Carbon::createFromFormat('Y-m-d',$item->fecha)->format('d/m/Y') : '')  }}">
                                @error('fecha')
                                <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="genero">Genero</label>
                                <select id="genero" name="genero" class="form-control">
                                    <option value="-1"><< SELECCIONE >></option>
                                    <option value="M" {{ old('genero') == 'M' ? 'selected' : (isset($item) && $item->genero == 'M' ? 'selected' : '') }}>MASCULINO</option>
                                    <option value="F" {{ old('genero') == 'F' ? 'selected' : (isset($item) && $item->genero == 'F' ? 'selected' : '') }}>FEMENINO</option>
                                </select>
                                @error('genero')
                                    <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <div class="form-group">
                                <label class="form-label" for="correo">Correo</label>
                                <input type="text" id="correo" name="correo" class="form-control"
                                       value="{{ old('correo') ? old('correo') : (isset($item) ? $item->email : '')  }}">
                                @error('correo')
                                    <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div id="interno" class="row">
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <label class="form-label" for="distritoi">Distrito Fiscal</label>
                                        <select id="distritoi" name="distritoi" class="form-control" data-live-search="true">
                                            <option value="-1"><< SELECCIONE >></option>
                                            @foreach($distritos as $distrito)
                                                <option value="{{ $distrito->id }}"
                                                    {{ old('distritoi') == $distrito->id ? 'selected' : (isset($item) && $distritosel == $distrito->id ? 'selected' : '') }}>
                                                    {{ $distrito->descripcion }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @error('distritoi')
                                            <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <label class="form-label" for="dependenciai">Dependencia</label>
                                        <select id="dependenciai" name="dependenciai" class="form-control" data-old="{{ old('dependenciai') ? old('dependenciai') : (isset($item) && $item->iddependencia ? $item->iddependencia : '') }}" data-live-search="true">
                                            <option value="-1"><< SELECCIONE >></option>
                                        </select>
                                        <input type="hidden" id="dependenciadet" name="dependenciadet"  value="{{ isset($item) ? $item->institucion : ''  }}">
                                        @error('dependenciai')
                                            <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <label class="form-label" for="cargoi">Cargo</label>
                                        <select id="cargoi" name="cargoi" class="form-control" data-live-search="true">
                                            <option value="-1"><< SELECCIONE >></option>
                                            @foreach($cargos as $cargo)
                                                <option value="{{ $cargo->CODI_CARG_TCA }}"
                                                    {{ old('cargoi') == $cargo->CODI_CARG_TCA ? 'selected' : (isset($item) && $item->idcargo == $cargo->CODI_CARG_TCA ? 'selected' : '') }}>
                                                    {{ $cargo->DESC_CARG_TCA }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <input type="hidden" id="cargodet" name="cargodet" value="{{ isset($item) ? $item->cargo : '' }}">
                                        @error('cargoi')
                                            <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div id="externo" class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="form-label" for="cargoe">Cargo</label>
                                        <input type="text" id="cargoe" name="cargoe" class="form-control"
                                               value="{{ old('cargoe') ? old('cargoe') : (isset($item) ? $item->cargo : '')  }}">
                                        @error('cargoe')
                                            <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="form-label" for="institucione">Institución</label>
                                        <input type="text" id="institucione" name="institucione" class="form-control"
                                               value="{{ old('institucione') ? old('institucione') : (isset($item) ? $item->institucion : '')  }}">
                                        @error('institucione')
                                            <div class="invalid-feedback animated fadeInUp">* {{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer mb-2">
                    <div class="pull-right mb-4">
                        <a href="{{ route('docentes.index') }}" class="btn btn-cancelar" data-dismiss="modal"><i class="la la-times-circle-o"></i> Cancelar</a>
                        <button type="submit" name="submit" class="btn btn-aceptar"><i class="fa fa-save"></i> Guardar</button>
                    </div>
            </div>
            </form>
        </div>
    </div>
@endsection
@push('scriptsapp')
    <script src="{{ asset('assets/js/docente.js') }}"></script>

    <script type="application/javascript">
        $(function(){
            if($('#exito').length){
                $('#tbldocente').DataTable().page('last').draw('page');
            }

            $('.mdate').bootstrapMaterialDatePicker({
                time:false,
                lang: 'es',
                format: 'DD/MM/YYYY',
                //minDate: new Date(),
                cancelText: 'Cancelar',
                okText: 'Aceptar',
                clearText: 'Limpiar',
                nowText: 'Hoy'
            });

            $('#interno').hide();
            $('#externo').show();

            onSelectDistritoChange();
            onToggleTipoChange();
        });
    </script>

@endpush
