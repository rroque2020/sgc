@if(!isset($items) || count($items) == 0)
    <div class='informado'>
        <label style='color:#0055A0'>... No se encontraron registros ...</label>
    </div>
@else
    <div class="card">
        <div class="card-body">
            <div class="row mb-2">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="pull-right">
                        <a onclick="selecciondocente()" class="btn btn-aceptar"><i class="la la-list-ul"></i> Seleccionar</a>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table id="tbldocentelista" class="table text-center display table-striped hover" style="min-width: 850px">
                    <thead class="thead-primary">
                    <tr>
                        <th>&nbsp;</th>
                        <th>Docente</th>
                        <th>Actividad Académica</th>
                        <th>Desempeño</th>
                        <th style="display: none;"></th>
                        <th style="display: none;"></th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $item)
                        <tr>
                            <td><input type="checkbox"/></td>
                            <td>{{ $item->docente }}</td>
                            <td>{{ $item->titulo }}</td>
                            <td></td>
                            <td style="display: none;">{{ $item->doc_id }}</td>
                            <td style="display: none;">{{ $item->actividad_id }}</td>
                            <td>
                                <a href="{{ route('docentes.show',['docente' => $item->doc_id]) }}" class="btn btn-xs btn-facebook" alt="Ver Detalles"
                                   data-toggle="tooltip" data-placement="top" title="Ver Detalle" target="_blank">
                                    <i class="la la-search"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endif
