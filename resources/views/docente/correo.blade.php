@component('mail::message')
# {{ $detalles['titulo'] }}

Se le invita a participar como DOCENTE, desde {{ $detalles['fecha_inicio'] }} a {{ $detalles['fecha_final'] }}, en el horario {{ $detalles['horario'] }}.

@component('mail::button', ['url' => $detalles['url'], 'color' => 'green'])
CONFIRMAR
@endcomponent

Gracias,<br>
<strong>ESCUELA DEL MINISTERIO PÚBLICO</strong>
@endcomponent
