@extends('layouts.layout')
@section('title')
    <div class="col-sm-6 p-md-0">
        <div class="welcome-text">
            <h4>Ver Docente</h4>
        </div>
    </div>
@endsection

@section('breadcrumb')
    <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Inicio</a></li>
            <li class="breadcrumb-item active"><a href="{{ route('docentes.index') }}">Docentes</a></li>
            <li class="breadcrumb-item active">
                <a href="javascript:void(0);">
                    Ver Docente
                </a>
            </li>
        </ol>
    </div>
@endsection

@section('content')
    <div class="col-xl-3 col-xxl-4 col-lg-4">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="text-center p-3 overlay-box" style="background-image: url(images/big/img1.jpg);">
                        <div class="profile-photo">
                            <img src="{{ isset($item->foto) ? asset('fotos/'.$item->foto) : asset('assets/images/profile/profile.png') }}" width="100" class="img-fluid rounded-circle" alt="">
                        </div>
                        <input type="hidden" id="id" name="id" value="{{ $item->id }}">
                        <h3 class="mt-3 mb-1 text-white">{{ $item->nombres }}</h3>
                        <p class="text-white mb-0">{{ $item->cargo }}</p>
                        <p class="text-white-50 mb-0">{{ $item->tipodet }}</p>
{{--                        <p class="text-white mb-0 text-warning">--}}
{{--                            <i class="fa fa-star"></i>--}}
{{--                            <i class="fa fa-star"></i>--}}
{{--                            <i class="fa fa-star"></i>--}}
{{--                            <i class="fa fa-star"></i>--}}
{{--                        </p>--}}
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item d-flex justify-content-between"><span class="mb-0">Actividades Académicas</span> <strong class="text-muted">{{ $totalesdoc->totactividad }}</strong></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body pb-0">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item d-flex px-0 justify-content-between">
                                <strong>Capacitaciones</strong>
                                <span class="mb-0">{{ $totalesdoc->totcapacitacion }}</span>
                            </li>
                            <li class="list-group-item d-flex px-0 justify-content-between">
                                <strong>Experiencia Laboral</strong>
                                <span class="mb-0">{{ $totalesdoc->totexperiencia }}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-9 col-xxl-8 col-lg-8">
        <div class="card">
            <div class="card-body">
                <div class="profile-tab">
                    <div class="custom-tab-1">
                        <ul class="nav nav-tabs">
                            <li class="nav-item"><a href="#about-me" data-toggle="tab" class="nav-link active show">Información Personal</a></li>
                            <li class="nav-item"><a href="#cv" data-toggle="tab" class="nav-link">Curriculum Vitae</a></li>
                            <li class="nav-item"><a href="#my-posts" data-toggle="tab" class="nav-link">Actividades Académicas</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="about-me" class="tab-pane fade active show">
                                <div class="profile-personal-info">
                                    <div class="row mt-4 mb-4">
                                        <div class="col-lg-3 col-md-4 col-sm-6 col-6">
                                            <h5 class="f-w-500">Documento <span class="pull-right">:</span>
                                            </h5>
                                        </div>
                                        <div class="col-lg-9 col-md-8 col-sm-6 col-6"><span>{{ $item->dni }}</span>
                                        </div>
                                    </div>
                                    <div class="row mt-4 mb-4">
                                        <div class="col-lg-3 col-md-4 col-sm-6 col-6">
                                            <h5 class="f-w-500">Nombres <span class="pull-right">:</span>
                                            </h5>
                                        </div>
                                        <div class="col-lg-9 col-md-8 col-sm-6 col-6"><span>{{ $item->nombres }}</span>
                                        </div>
                                    </div>
                                    <div class="row mt-4 mb-4">
                                        <div class="col-lg-3 col-md-4 col-sm-6 col-6">
                                            <h5 class="f-w-500">Apellidos <span class="pull-right">:</span>
                                            </h5>
                                        </div>
                                        <div class="col-lg-9 col-md-8 col-sm-6 col-6"><span>{{ $item->apellidos }}</span>
                                        </div>
                                    </div>
                                    <div class="row mb-4">
                                        <div class="col-lg-3 col-md-4 col-sm-6 col-6">
                                            <h5 class="f-w-500">Celular <span class="pull-right">:</span>
                                            </h5>
                                        </div>
                                        <div class="col-lg-9 col-md-8 col-sm-6 col-6"><span>{{ $item->celular }}</span>
                                        </div>
                                    </div>
                                    <div class="row mb-4">
                                        <div class="col-lg-3 col-md-4 col-sm-6 col-6">
                                            <h5 class="f-w-500">Correo <span class="pull-right">:</span>
                                            </h5>
                                        </div>
                                        <div class="col-lg-9 col-md-8 col-sm-6 col-6"><span>{{ $item->email }}</span>
                                        </div>
                                    </div>
                                    <div class="row mb-4">
                                        <div class="col-lg-3 col-md-4 col-sm-6 col-6">
                                            <h5 class="f-w-500">Distrito Fiscal/Institución / Dependencia <span class="pull-right">:</span>
                                            </h5>
                                        </div>
                                        <div class="col-lg-9 col-md-8 col-sm-6 col-6"><span>{{ $item->institucion }}</span>
                                        </div>
                                    </div>
                                    <div class="row mb-4">
                                        <div class="col-lg-3 col-md-4 col-sm-6 col-6">
                                            <h5 class="f-w-500">Cargo <span class="pull-right">:</span>
                                            </h5>
                                        </div>
                                        <div class="col-lg-9 col-md-8 col-sm-6 col-6"><span>{{ $item->cargo }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="cv" class="tab-pane fade">
                                <div class="my-post-content pt-3">
                                    <div id="accordion-three" class="accordion accordion-no-gutter accordion-header-bg">
                                        <div class="accordion__item">
                                            <div class="accordion__header collapsed" data-toggle="collapse" data-target="#formacion">
                                                <span class="accordion__header--text">Formación Académica</span>
                                                <span class="accordion__header--indicator"></span>
                                            </div>
                                            <div id="formacion" class="collapse accordion__body" data-parent="#accordion-three">
                                                <div class="accordion__body--text">
                                                    @if(!isset($formaciones) || count($formaciones) == 0)
                                                        <div class='mt-4 informado'>
                                                            <label style='color:#0055A0'>... No se encontraron registros ...</label>
                                                        </div>
                                                    @else
                                                        <div class="table-responsive mt-4 text-center">
                                                            <table id="tblformacion" class="table table-bordered table-hover table-striped verticle-middle table-responsive-sm">
                                                                <thead class="thead-encabezado">
                                                                <tr>
                                                                    <th>Grado Académico</th>
                                                                    <th>Institución</th>
                                                                    <th>Profesión</th>
                                                                    <th>A. Ingreso </th>
                                                                    <th>A. Egreso </th>
                                                                    <th>Sustento</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                @foreach($formaciones as $formacion)
                                                                    <tr>
                                                                        <td>{{ $formacion->gradodes }}</td>
                                                                        <td>{{ $formacion->instituciondes }}</td>
                                                                        <td>{{ $formacion->profesiondes }}</td>
                                                                        <td>{{ $formacion->ingreso }}</td>
                                                                        <td>{{ $formacion->egreso }}</td>
                                                                        <td>
                                                                            @if(isset($formacion->sustento) && $formacion->sustento <> '')
                                                                                <a href="{{ env('DIR_PERFIL').'archivos/'.$formacion->sustento }}" target="_blank">
                                                                                    <img src="{{ asset('assets/images/pdf.png') }}" alt="PDF"></a>
                                                                            @endif
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion__item">
                                            <div class="accordion__header collapsed" data-toggle="collapse" data-target="#capacitacion">
                                                <span class="accordion__header--text">Capacitaciones</span>
                                                <span class="accordion__header--indicator"></span>
                                            </div>
                                            <div id="capacitacion" class="collapse accordion__body" data-parent="#accordion-three">
                                                <div class="accordion__body--text">
                                                    @if(!isset($capacitaciones) || count($capacitaciones) == 0)
                                                        <div class='mt-4 informado'>
                                                            <label style='color:#0055A0'>... No se encontraron registros ...</label>
                                                        </div>
                                                    @else
                                                        <div class="table-responsive mt-4 text-center">
                                                            <table id="tblcapacitacion" class="table table-bordered table-hover table-striped verticle-middle table-responsive-sm">
                                                                <thead class="thead-encabezado">
                                                                <tr>
                                                                    <th>Tipo de Estudio</th>
                                                                    <th>Institución</th>
                                                                    <th>Especialidad</th>
                                                                    <th>Ingreso</th>
                                                                    <th>Egreso</th>
                                                                    <th>Horas</th>
                                                                    <th>Sustento</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                @foreach($capacitaciones as $capacitacion)
                                                                    <tr>
                                                                        <td>{{ $capacitacion->tipo }}</td>
                                                                        <td>{{ $capacitacion->institucion }}</td>
                                                                        <td>{{ $capacitacion->especialidad }}</td>
                                                                        <td style="min-width: 80px;">
                                                                            {{ \Carbon\Carbon::createFromFormat('Y-m-d',$capacitacion->ingreso)->format('d/m/Y') }}
                                                                        </td>
                                                                        <td style="min-width: 80px;">
                                                                            {{ \Carbon\Carbon::createFromFormat('Y-m-d',$capacitacion->egreso)->format('d/m/Y') }}
                                                                        </td>
                                                                        <td>{{ $capacitacion->horas }}</td>
                                                                        <td>
                                                                            @if(isset($capacitacion->sustento) && $capacitacion->sustento <> '')
                                                                                <a href="{{ env('DIR_PERFIL').'archivos/'.$capacitacion->sustento }}" target="_blank">
                                                                                    <img src="{{ asset('assets/images/pdf.png') }}" alt="PDF"></a>
                                                                            @endif
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion__item">
                                            <div class="accordion__header collapsed" data-toggle="collapse" data-target="#experiencia">
                                                <span class="accordion__header--text">Experiencia Laboral</span>
                                                <span class="accordion__header--indicator"></span>
                                            </div>
                                            <div id="experiencia" class="collapse accordion__body" data-parent="#accordion-three">
                                                <div class="accordion__body--text">
                                                    @if(!isset($experiencias) || count($experiencias) == 0)
                                                        <div class='mt-4 informado'>
                                                            <label style='color:#0055A0'>... No se encontraron registros ...</label>
                                                        </div>
                                                    @else
                                                        <div class="table-responsive mt-4 text-center">
                                                            <table id="tblexperiencia" class="table table-bordered table-hover table-striped verticle-middle table-responsive-sm">
                                                                <thead class="thead-encabezado">
                                                                <tr>
                                                                    <th>Entidad</th>
                                                                    <th>Cargo Ocupado</th>
                                                                    <th>Área</th>
                                                                    <th>F. Ingreso </th>
                                                                    <th>F. Egreso </th>
                                                                    <th>Sustento</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                @foreach($experiencias as $experiencia)
                                                                    <tr>
                                                                        <td>{{ $experiencia->entidad }}</td>
                                                                        <td>{{ $experiencia->cargo }}</td>
                                                                        <td>{{ $experiencia->area }}</td>
                                                                        <td style="min-width: 80px;">
                                                                            {{ \Carbon\Carbon::createFromFormat('Y-m-d',$experiencia->ingreso)->format('d/m/Y') }}
                                                                        </td>
                                                                        <td style="min-width: 80px;">
                                                                            {{ \Carbon\Carbon::createFromFormat('Y-m-d',$experiencia->egreso)->format('d/m/Y') }}
                                                                        </td>
                                                                        <td>
                                                                            @if(isset($experiencia->sustento) && $experiencia->sustento <> '')
                                                                                <a href="{{ env('DIR_PERFIL').'archivos/'.$experiencia->sustento }}" target="_blank">
                                                                                    <img src="{{ asset('assets/images/pdf.png') }}" alt="PDF"></a>
                                                                            @endif
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    @endif

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="my-posts" class="tab-pane fade">
                                <div class="my-post-content pt-3">
                                    <div class="profile-uoloaded-post border-bottom-1 pb-5">
                                        <a class="post-title" href="javascript:void()">
                                            <h4>Actividad 1 (Julio 2021)</h4>
                                        </a>
                                        <p>A wonderful serenity has take possession of my entire soul like these sweet morning of spare which enjoy whole heart.A wonderful serenity has take possession of my entire soul like these sweet morning
                                            of spare which enjoy whole heart.</p>
                                        <button class="btn btn-primary mr-3">
                                                    <span class="mr-3">
                                                        <i class="fa fa-certificate"></i>
                                                    </span>Ver Certificado
                                        </button>
                                        <button class="btn btn-google-plus">
                                                    <span class="mr-3">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </span>
                                        </button>
                                    </div>
                                    <div class="profile-uoloaded-post border-bottom-1 pb-5">
                                        <a class="post-title" href="javascript:void()">
                                            <h4>Actividad 2 (Mayo 2021)</h4>
                                        </a>
                                        <p>A wonderful serenity has take possession of my entire soul like these sweet morning of spare which enjoy whole heart.A wonderful serenity has take possession of my entire soul like these sweet morning
                                            of spare which enjoy whole heart.</p>
                                        <button class="btn btn-primary mr-3">
                                                    <span class="mr-3">
                                                        <i class="fa fa-certificate"></i>
                                                    </span>Ver Certificado
                                        </button>
                                        <button class="btn btn-google-plus">
                                                    <span class="mr-3">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </span>
                                        </button>
                                    </div>
                                    <div class="profile-uoloaded-post border-bottom-1 pb-5">
                                        {{--                                                    <img src="{{ asset('assets/images/profile/8.jpg') }}" alt="" class="img-fluid">--}}
                                        <a class="post-title" href="javascript:void()">
                                            <h4>Actividad 3 (Enero 2021)</h4>
                                        </a>
                                        <p>A wonderful serenity has take possession of my entire soul like these sweet morning of spare which enjoy whole heart.A wonderful serenity has take possession of my entire soul like these sweet morning
                                            of spare which enjoy whole heart.</p>
                                        <button class="btn btn-primary mr-3">
                                                    <span class="mr-3">
                                                        <i class="fa fa-certificate"></i>
                                                    </span>Ver Certificado
                                        </button>
                                        <button class="btn btn-google-plus">
                                                    <span class="mr-3">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </span>
                                        </button>
                                    </div>
                                    <div class="text-center mb-2">
                                        <a href="javascript:void()" class="btn btn-primary">
                                            <i class="la la-angle-double-down"></i>
                                            Cargar Más
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <a href="{{ route('docentes.index') }}" class="btn btn-primary pull-right" ><i class="fa fa-arrow-left"></i> Volver</a>
            </div>
        </div>
    </div>
@endsection

@push('scriptsapp')
    <script src="{{ asset('assets/js/docente.js') }}"></script>

    <script type="application/javascript">
        $(function(){
            if($('#exito').length){
                $('#tbldocente').DataTable().page('last').draw('page');
            }
        });
    </script>

@endpush
