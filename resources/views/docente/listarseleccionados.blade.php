@if(!isset($docentes) || count($docentes) == 0)
    <div class='informado'>
        <label style='color:#0055A0'>... No se encontraron registros ...</label>
    </div>
@else
    <div class="table-responsive">
        <table id="tbldocenteseleccion" class="table text-center display table-striped hover" style="min-width: 850px">
            <thead class="thead-primary">
            <tr>
                <th>Docente</th>
                <th>Estado</th>
                <th>Acciones</th>
            </tr>
            </thead>
            <tbody>
            @foreach($docentes as $item)
                <tr>
                    <td>{{ $item->docente }}</td>
                    <td class="text-center">
                        @switch($item->estado)
                            @case(1)
                                <span class='badge-est badge-estado1'>PROPUESTO</span>
                                @break
                            @case(2)
                                <span class='badge-est badge-estado2'>NO CONFIRMADO</span>
                                @break
                            @default
                                <span class='badge-est badge-estado3'>CONFIRMADO</span>
                        @endswitch
                    </td>
                    <td>
                        <a onclick="enviarcorreo({{ $item->id }}, {{ $item->estado }}, {{$item->act_id }}, {{$item->docente_id }})" class="btn btn-xs btn-success"
                           data-toggle="tooltip" data-placement="top" title="Enviar Correo">
                            <i class="fa fa-envelope"></i>
                        </a>
                        <a onclick="eliminardocente({{$item->estado }}, {{$item->id }}, {{$item->act_id }})" class="btn btn-xs btn-danger"
                           data-toggle="tooltip" data-placement="top" title="Eliminar">
                            <i class="la la-trash"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endif
