@if(!isset($items) || count($items) == 0)
    <div class='informado'>
        <label style='color:#0055A0'>... No se encontraron registros ...</label>
    </div>
@else
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbldocente" class="table text-center display table-striped hover" style="min-width: 850px">
                    <thead class="thead-primary">
                    <tr>
                        <th>ID</th>
                        <th>Tipo</th>
                        <th>Docente</th>
                        <th>Dependencia/Institución</th>
                        <th>Cargo</th>
                        <th>Estado</th>
                        <th width="70px">Acciones </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->tipo }}</td>
                            <td>{{ $item->docente }}</td>
                            <td>{{ $item->institucion }}</td>
                            <td>{{ $item->cargo }}</td>
                            <td>
                                @if($item->estado == 1)
                                    <span class='badge-est badge-estadoactivo'>ACTIVO</span>
                                @else
                                    <span class='badge-est badge-estadoinactivo'>INACTIVO</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('docentes.edit',['docente' => $item->id]) }}" class="btn btn-xs btn-facebook"
                                   data-toggle="tooltip" data-placement="top" title="Editar">
                                    <i class="la la-pencil"></i>
                                </a>
                                <a href="{{ route('docentes.show',['docente' => $item->id]) }}" class="btn btn-xs btn-facebook"
                                   data-toggle="tooltip" data-placement="top" title="Ver Detalle">
                                    <i class="la la-search"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endif
