@extends('layouts.layout')
@section('title')
    <div class="col-sm-6 p-md-0">
        <div class="welcome-text">
            <h4>Lista de Docentes</h4>
        </div>
    </div>
@endsection

@section('breadcrumb')
    <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Inicio</a></li>
            <li class="breadcrumb-item active"><a href="javascript:void(0);">Docentes</a></li>
        </ol>
    </div>
@endsection

@section('content')
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card">
            <div class="card-header">
                <h4></h4>

                <div class="pull-right">
                    <a onclick="actualizardocente()" class="btn btn-rounded btn-tumblr" data-toggle="tooltip" data-placement="top" title="Actualizar">
                    <span class="btn-icon-left text-tumblr">
                        <i class="fa fa-refresh color-primary"></i>
                    </span>Actualizar
                    </a>
                    &nbsp;
                    <a href="{{ route('docentes.create') }}" class="btn btn-rounded btn-primary"  data-toggle="tooltip" data-placement="top" title="Nuevo">
                        <span class="btn-icon-left text-primary">
                            <i class="fa fa-plus color-primary"></i>
                        </span>Nuevo
                    </a>
                </div>
            </div>
            <div id="mensaje"></div>
            <div class="card-body">
                <div id="listado">
                    @if(isset($items))
                        @include('docente.lista')
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scriptsapp')
    <script type="application/javascript">
        $(function(){
            if($('#exito').length){
                $('#tbldocente').DataTable().page('last').draw('page');
            }

        });

        $('#tbldocente').DataTable({
            "language": {
                "url": "/assets/vendor/datatables/js/dtspanish.json"
            },
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todo"]],
            dom: '<"row col-lg-12 col-md-12 col-sm-12 arriba1"B><"arriba2"lrf><"abajo"tip><"clear">',
            //dom: 'Blfrtip',
            text: 'Export',
            buttons: [
                { extend: 'copyHtml5',
                    text: '<i class="fa fa-copy"></i>&nbsp;Copiar',
                    "className": 'btn btn-info btn-sm'},
                { extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>&nbsp;Excel',
                    "className": 'btn btn-success btn-sm'},
                { extend: 'csvHtml5',
                    text: '<i class="fa fa-file-text-o"></i>&nbsp;CSV',
                    "className": 'btn btn-warning btn-sm'},
            ],
        });
    </script>
    <script src="{{ asset('assets/js/docente.js') }}"></script>

@endpush
