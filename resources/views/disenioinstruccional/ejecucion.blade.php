<div class="modal-dialog modal-dialog-centered" role="document" data-keyboard="false" data-backdrop="static">
    <div class="modal-content">
        <div class="modal-header head-modal">
            <h5 class="modal-title text-white">Registrar Datos de la Ejecución</h5>
            <button type="button" class="close" data-dismiss="modal"><span>x</span>
            </button>
        </div>
        <form id="frmregistroejecucion" class="frmregistroejecucion" method="post">
            <div class="modal-body">
                @csrf
                <input type="hidden" id="codactividad" name="codactividad" value="{{ $actividad->id }}">
                <div id="mensaje"></div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="fechainicioe">Fecha Inicio</label>
                            <input type="text" id="fechainicioe" name="fechainicioe" class="form-control mdate"
                                   value="{{ isset($actividad->fecha_inicioe) ? \Carbon\Carbon::createFromFormat('Y-m-d',$actividad->fecha_inicioe)->format('d/m/Y') :
                                        \Carbon\Carbon::createFromFormat('Y-m-d',$actividad->fecha_inicio)->format('d/m/Y')  }}">
                            <div id="fechainicioe_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="fechafine">Fecha Fin</label>
                            <input type="text" id="fechafine" name="fechafine" class="form-control mdate"
                                   value="{{ isset($actividad->fecha_finale) ? \Carbon\Carbon::createFromFormat('Y-m-d',$actividad->fecha_finale)->format('d/m/Y') :
                                        \Carbon\Carbon::createFromFormat('Y-m-d',$actividad->fecha_final)->format('d/m/Y') }}">
                            <div id="fechafine_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="nroaulase">Nro. Aulas</label>
                            <input type="number" id="nroaulase" name="nroaulase" min="0" class="form-control"
                                   value="{{ isset($actividad->nro_aulase) ? $actividad->nro_aulase : $actividad->nro_aulas }}">
                            <div id="nroaulase_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="nrovacantese">Nro. Vacantes</label>
                            <input type="number" id="nrovacantese" name="nrovacantese" min="0" class="form-control"
                                   value="{{ isset($actividad->nro_vacantese) ? $actividad->nro_vacantese : $actividad->nro_vacantes }}">
                            <div id="nrovacantese_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="nrodocentese">Nro. Docentes</label>
                            <input type="number" id="nrodocentese" name="nrodocentese" min="0" class="form-control"
                                   value="{{ isset($actividad->nro_docentese) ? $actividad->nro_docentese : $actividad->nro_docentes }}">
                            <div id="nrodocentese_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="duracione">Duración</label>
                            <div class="input-group mb-3">
                                <input type="number" id="duracione" name="duracione" min="0" class="form-control"
                                       value="{{ isset($actividad->duracione) ? $actividad->duracione : $actividad->duracion }}">
                                <div class="input-group-append">
                                    <span class="input-group-text"> &nbsp;HORAS ACADÉMICAS</span>
                                </div>
                            </div>
                            <div id="duracione_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="tipomodulo">Tipo de Estructura</label>
                            <select id="tipomodulo" name="tipomodulo" class="form-control selectpicker" style="display: block !important;">
                                <option value="-1"><< SELECCIONE >></option>
                                @foreach($tipos as $tipo)
                                    <option {{ isset($actividad) && $actividad->tipo_modulo == $tipo->id ?  'selected' : '' }} value="{{ $tipo->id }}">{{ $tipo->descripcion }}</option>
                                @endforeach
                            </select>
                            <div id="tipomodulo_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-cancelar" data-dismiss="modal"><i class="la la-times-circle-o"></i> Cancelar</button>
                <button type="submit" class="btn btn-aceptar"><i class="fa fa-save"></i> Guardar</button>
            </div>
        </form>
    </div>
</div>
