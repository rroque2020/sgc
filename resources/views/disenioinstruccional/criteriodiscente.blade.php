<div class="modal-dialog modal-dialog-centered" role="document" data-keyboard="false" data-backdrop="static">
    <div class="modal-content">
        <div class="modal-header head-modal">
            <h5 class="modal-title text-white">{{ isset($item) ? 'Editar' : 'Registrar' }} Criterios para Selección de Discente</h5>
            <button type="button" class="close" data-dismiss="modal"><span>x</span>
            </button>
        </div>
        <form id="frmregistrocriterio" class="frmregistrocriterio" method="{{ isset($item) ? 'put' : 'post' }}">
            <div class="modal-body">
                @csrf
                @if(isset($item))
                    @method('PUT')
                @endif
                <input type="hidden" id="codactividad" name="codactividad" value="{{ isset($item) ? $item->actividad_academica_id : $actividad }}">
                <input type="hidden" id="id" name="id" value="{{ isset($item) ? $item->id : '' }}">
                <div id="mensaje"></div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="form-check form-switch">
                            <input class="form-check-input" type="checkbox" role="switch" id="dni" name="dni" class="w-50" {{ isset($item) && $item->dni == 1 ? 'checked' : '' }}>
                            <label class="form-check-label" for="dni">¿ Validar DNI ?</label>
                        </div>
                        <div id="dni_error" class="invalid-feedback animated fadeInUp"></div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="form-check form-switch">
                            <input class="form-check-input" type="checkbox" role="switch" id="aprobado" name="aprobado" class="w-50" {{ isset($item) && $item->aprobado == 1 ? 'checked' : '' }}>
                            <label class="form-check-label" for="aprobado">¿ Solo Aprobados ?</label>
                        </div>
                        <div id="aprobado_error" class="invalid-feedback animated fadeInUp"></div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="form-check form-switch">
                            <input class="form-check-input" type="checkbox" role="switch" id="temporalidad" name="temporalidad" class="w-50" {{ isset($item) && $item->temporalidad == 1 ? 'checked' : '' }}>
                            <label class="form-check-label" for="temporalidad">¿ Sin Cruce de Horarios ?</label>
                        </div>
                        <div id="temporalidad_error" class="invalid-feedback animated fadeInUp"></div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="form-check form-switch">
                            <input class="form-check-input" type="checkbox" role="switch" id="sancion" name="sancion" class="w-50" {{ isset($item) && $item->sancion == 1 ? 'checked' : '' }}>
                            <label class="form-check-label" for="sancion">¿ No Sancionados ?</label>
                        </div>
                        <div id="sancion_error" class="invalid-feedback animated fadeInUp"></div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="form-check form-switch">
                            <input class="form-check-input" type="checkbox" role="switch" id="invitados" name="invitados" class="w-50" {{ isset($item) && $item->invitados == 1 ? 'checked' : '' }}>
                            <label class="form-check-label" for="invitados">¿ Invitados ?</label>
                        </div>
                        <div id="invitados_error" class="invalid-feedback animated fadeInUp"></div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="form-check form-switch">
                            <input class="form-check-input" type="checkbox" role="switch" id="brecha" name="brecha" class="w-50" {{ isset($item) && $item->brecha == 1 ? 'checked' : '' }}>
                            <label class="form-check-label" for="brecha">¿ Reducir Brecha ?</label>
                        </div>
                        <div id="brecha_error" class="invalid-feedback animated fadeInUp"></div>
                    </div>
                    <br><br>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="especialidad">Especialidad</label>
                            <input type="text" id="especialidad" name="especialidad" min="0" class="form-control"
                                   value="{{ isset($item) ? $item->especialidad : '' }}">
                            <div id="especialidad_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="perfilpersonal">Perfil de Personal</label>
                            <select id="perfilpersonal" name="perfilpersonal[]" class="form-control selectpicker" data-live-search="true" title="<< SELECCIONE >>" multiple>
                                @foreach($perfilespersonales as $perfilpersonal)
                                    <option {{ isset($item) ? ((collect($perfilesseleccionados)->contains($perfilpersonal->id)) ? 'selected' : '') : '' }} value="{{ $perfilpersonal->id }}">{{ $perfilpersonal->descripcion }}</option>
                                @endforeach
                            </select>
                            <div id="perfilpersonal_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="cargo">Cargos</label>
                            <select id="cargo" name="cargo[]" class="form-control selectpicker" data-live-search="true" title="<< SELECCIONE >>" multiple>
                                @foreach($cargos as $cargo)
                                    <option {{ isset($item) ? ((collect($cargosseleccionados)->contains($cargo->CODI_CARG_TCA)) ? 'selected' : '') : '' }} value="{{ $cargo->CODI_CARG_TCA }}">{{ $cargo->DESC_CARG_TCA }}</option>
                                @endforeach
                            </select>
                            <div id="cargo_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="distritofiscal">Distrito Fiscal</label>
                            <select id="distritofiscal" name="distritofiscal[]" class="form-control selectpicker" data-live-search="true" title="<< SELECCIONE >>" multiple>
                                <option value = "-1"><< SELECCIONE >></option>
                                @foreach($distritos as $distrito)
                                    <option {{ isset($item) ? ((collect($distritosseleccionados)->contains($distrito->id)) ? 'selected' : '') : '' }} value="{{ $distrito->id }}">{{ $distrito->descripcion }}</option>
                                @endforeach
                            </select>
                            <div id="distritofiscal_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-cancelar" data-dismiss="modal"><i class="la la-times-circle-o"></i> Cancelar</button>
                <button type="submit" class="btn btn-aceptar"><i class="fa fa-save"></i> Guardar</button>
            </div>
        </form>
    </div>
</div>
