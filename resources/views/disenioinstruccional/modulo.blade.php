<div class="modal-dialog modal-dialog-centered" role="document" data-keyboard="false" data-backdrop="static">
    <div class="modal-content">
        <div class="modal-header head-modal">
            <h5 class="modal-title text-white">{{ isset($modulo) ? 'Editar : '.$modulo->id : 'Registrar ' }}</h5>
            <button type="button" class="close" data-dismiss="modal"><span>x</span>
            </button>
        </div>
        <form id="frmregistromodulo" class="frmregistromodulo" method="{{ isset($modulo) ? 'put' : 'post' }}">
            <div class="modal-body">
                @csrf
                @if(isset($modulo))
                    @method('PUT')
                @endif
                <input type="hidden" id="codactividad" name="codactividad" value="{{ isset($modulo) ? $modulo->actividad_academica_id : $id}}">
                <input type="hidden" id="id" name="id" value="{{ isset($modulo) ? $modulo->id : ''}}">
                <div id="mensaje"></div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="tipo">Tipo de Capacitación</label>
                            <select id="tipo" name="tipo" class="form-control" data-live-search="true">
                                <option value="-1"><< SELECCIONE >></option>
                                @foreach($tipos as $tipo)
                                    <option {{ isset($modulo) && $modulo->tipo_capacitacion_id == $tipo->id ? 'selected': '' }} value="{{ $tipo->id }}">{{ $tipo->descripcion }}</option>
                                @endforeach
                            </select>
                            <div id="tipo_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="nombre">Nombre </label>
                            <input type="text" id="nombre" name="nombre" class="form-control" value="{{ isset($modulo) ? $modulo->nombre : '' }}">
                            <div id="nombre_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="fechainicio">Fecha Inicio</label>
                            <input type="text" id="fechainicio" name="fechainicio" class="form-control mdate" value="{{ isset($modulo) ? \Carbon\Carbon::createFromFormat('Y-m-d',$modulo->fecha_inicio)->format('d/m/Y') : '' }}">
                            <div id="fechainicio_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="fechafin">Fecha Fin</label>
                            <input type="text" id="fechafin" name="fechafin" class="form-control mdate" value="{{ isset($modulo) ? \Carbon\Carbon::createFromFormat('Y-m-d',$modulo->fecha_final)->format('d/m/Y') : '' }}">
                            <div id="fechafin_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="duracion">Duración</label>
                            <div class="input-group mb-3">
                                <input type="number" id="duracion" name="duracion" min="0" class="form-control" value="{{ isset($modulo) ? $modulo->duracion : '' }}">
                                <div class="input-group-append">
                                    <span class="input-group-text"> &nbsp;HORAS ACADÉMICAS</span>
                                </div>
                            </div>
                            <div id="duracion_error" class="invalid-feedback animated fadeInUp"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-cancelar" data-dismiss="modal"><i class="la la-times-circle-o"></i> Cancelar</button>
                <button type="submit" class="btn btn-aceptar"><i class="fa fa-save"></i> Guardar</button>
            </div>
        </form>
    </div>
</div>
