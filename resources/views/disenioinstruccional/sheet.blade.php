<table id="tblcompetencia" class="text-center">
    <thead>
    <tr>
        <td colspan="1">
            <img src="{{ public_path('/assets/images/logo-reporte.png') }}" height="40">
        </td>
        <td colspan="8" style=" text-align: center; vertical-align: middle;">
            <strong>DISEÑO INSTRUCCIONAL</strong><br>
            (MATRIZ CURRICULAR)
        </td>
    </tr>
    <tr>
        <td colspan="9"></td>
    </tr>
    <tr>
        <td colspan="1"><strong>ACTIVIDAD ({{ $actividad->codigo }}):</strong> </td>
        <td colspan="8">{{ $actividad->titulo }}</td>
    </tr>
    <tr>
        <td colspan="9"></td>
    </tr>
    <tr>
        <th>
            <strong>TIPO</strong>
        </th>
        <th>
            <strong>COMPETENCIA DEL CURSO</strong>
        </th>
        <th>
            <strong>CAPACIDAD</strong><br>
            (Habilidad/destreza/actitud)
        </th>
        <th>
            <strong>ORGANIZACIÓN</strong><br>
            (Sesión, lección, etc.)
        </th>
        <th>
            <strong>CONTENIDOS</strong>
        </th>
        <th>
            <strong>DURACIÓN</strong>
        </th>
        <th>
            <strong>ACTIVIDAD ESTRATÉGICA</strong><br>
            (Actividades Pedagógicas)
        </th>
        <th>
            <strong>RECURSOS DE APRENDIZAJE</strong><br>
        </th>
        <th>
            <strong>FECHA Y HORA</strong><br>
        </th>
    </tr>
    </thead>
    <tbody class="small">
    @if(isset($competencias) && count($competencias) > 0)
        @foreach($competencias as $competencia)
            <tr style=" min-height: 20px;" class="">
                <td class="text-center" style="word-wrap: break-word;">
                    {{ $competencia->modulo }}
                </td>
                <td style="max-width:300px; word-wrap: break-word;">
                    <span style="font-weight: bold;">{{ $competencia->comp_orden }}</span>
                    <br>
                    <span style="font-weight: normal;">{{ $competencia->comp_descripcion }}</span>
                </td>
                <td style="word-wrap: break-word;">
                    Resultado de Aprendizaje {{ $competencia->cap_orden }}<br>
                    {{ $competencia->cap_descripcion }}
                </td>
                <td style="word-wrap: break-word;">
                    Sesión N° {{ $competencia->ses_orden }}<br>
                    {{ $competencia->ses_descripcion }}
                </td>
                <td style="word-wrap: break-word;">
                    Unidad N° {{ $competencia->und_orden }} : &nbsp; {{ $competencia->und_nombre }}<br>
                    {{ $competencia->und_descripcion }}<br><br>
                    Bibliografía <br>
                    {{ $competencia->und_bibliografia }}
                </td>
                <td style="word-wrap: break-word;">
                    {{ $competencia->ses_duracion }} &nbsp;horas
                </td>
                <td style="word-wrap: break-word;">
                    Tipo <br>
                    {{ $competencia->ses_tae }} <br><br>
                    Descripción <br>
                    {{ $competencia->ses_destae }}
                </td>
                <td style="word-wrap: break-word;">
                    Recursos Obligatorios <br>
                    {{ $competencia->ses_ro }} <br><br>
                    @if(isset($competencia->ses_rc))
                        Recursos Complementarios <br>
                        {{ $competencia->ses_rc }}
                    @endif
                </td>
                <td class="text-center" style="word-wrap: break-word;">
                    De
                    @if(isset($competencia->ses_fhi))
                        {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$competencia->ses_fhi)->format('d/m/Y H:i') }}
                    @endif
                    A
                    @if(isset($competencia->ses_fhc))
                        {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$competencia->ses_fhc)->format('d/m/Y H:i') }}
                    @endif
                </td>
            </tr>
        @endforeach
    @endif
    @if(isset($evaluaciones) && count($evaluaciones) > 0)
        <tr>
            <td colspan="5"></td>
        </tr>
        <tr>
            <td colspan="5">
                <strong>ACTIVIDADES EVALUATIVAS</strong>
            </td>
        </tr>
        <tr>
            <td>
                <strong>ORDEN</strong>
            </td>
            <td>
                <strong>TIPO</strong>
            </td>
            <td>
                <strong>PORCENTAJE</strong>
            </td>
            <td>
                <strong>INSTRUMENTO</strong>
            </td>
            <td>
                <strong>NOTA DE APROBACIÓN</strong>
            </td>
        </tr>
        @foreach($evaluaciones as $evaluacion)
            <tr>
                <td>
                    {{ $evaluacion->ae_orden }}
                </td>
                <td>
                    Tipo <br>
                    {{ $evaluacion->ae_tae }} <br><br>
                    Descripción <br>
                    {{ $evaluacion->ae_descripcion }}
                </td>
                <td>
                    {{ $evaluacion->ae_porcentaje }} &nbsp;%
                </td>
                <td>
                    {{ $evaluacion->ae_instrumento }}
                </td>
                <td>
                    {{ $evaluacion->ae_nota }}
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
