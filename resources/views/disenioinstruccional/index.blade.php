@extends('layouts.layout')
@section('title')
    <div class="col-sm-6 p-md-0">
        <div class="welcome-text">
            <h4>Diseño Instruccional</h4>
        </div>
    </div>
@endsection

@section('breadcrumb')
    <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Inicio</a></li>
            <li class="breadcrumb-item"><a href="{{ route('actividad.index') }}">Actividades Académicas</a></li>
            <li class="breadcrumb-item active"><a href="javascript:void(0);">Diseño Instruccional</a></li>
        </ol>
    </div>
@endsection

@section('content')
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card">
            <div class="card-header">
                <h4></h4>
                <input id="codactividad" type="hidden" value="{{ $actividad->id }}">
                <div class="pull-right">
                    <a href="{{ route('disenio.index',['codigo' => $actividad->id]) }}" class="btn btn-rounded btn-tumblr"
                       data-toggle="tooltip" data-placement="top" title="Actualizar">
                        <span class="btn-icon-left text-tumblr">
                            <i class="fa fa-refresh color-primary"></i>
                        </span>Actualizar
                    </a>
                    &nbsp;
                    &nbsp;
                    <a href="{{ route('disenio.export',['codigo' => $actividad->id]) }}" class="btn btn-rounded btn-success" id="btnexportarexcel"
                       data-toggle="tooltip" data-placement="top" title="Exportar a Excel el Diseño Instruccional">
                        <span class="btn-icon-left text-primary">
                            <i class="fa fa-file-excel-o color-success"></i>
                         </span>Diseño Instruccional
                    </a>
                    &nbsp;
                    <a href="{{ route('disenio.exportword',['codigo' => $actividad->id]) }}" class="btn btn-rounded btn-facebook" id="btnexportarword"
                       data-toggle="tooltip" data-placement="top" title="Exportar a Word el Diseño Instruccional">
                        <span class="btn-icon-left text-primary">
                            <i class="fa fa-file-word-o"></i>
                        </span>Silabo
                    </a>
                    &nbsp;
                    <a class="btn btn-rounded btn-success" id="btnaprobardisenio"
                       data-toggle="tooltip" data-placement="top" title="Aprobar Diseño Instruccional">
                        <span class="btn-icon-left text-primary">
                            <i class="  fa fa-check color-success"></i>
                        </span>Aprobar
                    </a>
                </div>
            </div>

            <div class="card-body mt-4">
                <div id="mensaje"></div>
                <input type="hidden" id="codactividad" name="codactividad" value="{{ $actividad->id }}">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="text-center mt-1">
                            <h4>CÓDIGO: {{ $actividad->codigo }}</h4>
                            <h2>{{ $actividad->titulo }}</h2>
                            <p id="estado">{!! html_entity_decode(Helper::estadoAA($actividad->estado, false)) !!}</p>
                            <input type="hidden" id="estadoAA" name="estadoAA" value="{{ $actividad->estado }}">
                        </div>
                    </div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="aa">
                            <ul class="nav nav-tabs">
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#datosejec">
                                        <i class="fa fa-list"></i>
                                        Datos de Ejecución
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#criterioselec">
                                        <i class="fa fa-table"></i>
                                       Criterios para Selección de Discentes
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade" id="datosejec" role="tabpanel">
                                    <div id="ejecucion" class="row p-4">
                                        @include('disenioinstruccional.ejecuciondet')
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="criterioselec" role="tabpanel">
                                    <div id="criteriodiscente" class="row p-4">
                                        @include('disenioinstruccional.criteriodiscentedet')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr/>
                <div id="detalle1">
                    <div class="row mb-3 px-3">
{{--                        <div class="col-lg-12 col-md-12 col-sm-12 bg-dark text-white p-1 text-center mb-4 ">--}}
{{--                            <strong>ESTRUCTURA</strong>--}}
{{--                        </div>--}}
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="pull-right">
                                <a class="btn btn-rounded btn-primary" onclick="frmModulo({{ $actividad->id }}, 0)"
                                   data-toggle="tooltip" data-placement="top" title="Nuevo">
                                    <span class="btn-icon-left text-primary">
                                        <i class="fa fa-plus color-primary"></i>
                                    </span>Nuevo
                                </a>
                            </div>
                        </div>
                    </div>
                    <div id="detalle">
                        @include('disenioinstruccional.detalle')
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="pull-right mb-4">
                        <a href="{{ route('actividad.option',['codigo' => $actividad->id]) }}" class="btn btn-primary" ><i class="fa fa-arrow-left"></i> Volver a Opciones</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="mensaje"></div>
@endsection
@push('scriptsapp')
    <script src="{{ asset('assets/js/disenio.js') }}"></script>
    <script src="{{ asset('assets/js/modulo.js') }}"></script>
    <script type="text/javascript">
        $('#btnaprobardisenio').click(function(e) {
            var estado = $('#estadoAA').val();

            if(estado == 3){
                Swal.fire({
                    title: "Aprobar Diseño Instruccional",
                    text: "El Diseño ya fué aprobado, ¿Desea deshacer la Aprobación?",
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonColor: "#28A745",
                    confirmButtonText: "Si",
                    cancelButtonColor: "#DC3545",
                    cancelButtonText: "No",
                })
                    .then(resultado => {
                        if (resultado.value) {
                            aprobarDisenio($('#codactividad').val(),2);
                        }
                    });
            }else{
                Swal.fire({
                    title: "Aprobar Diseño Instruccional",
                    text: "¿Esta seguro de aprobar el Diseño Instruccional?",
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonColor: "#28A745",
                    confirmButtonText: "Si",
                    cancelButtonColor: "#DC3545",
                    cancelButtonText: "No",
                })
                .then(resultado => {
                    if (resultado.value) {
                        aprobarDisenio($('#codactividad').val(),3);
                    }
                });
            }
        });

        // $(".modulos").on('show.bs.collapse', function () {
        //     var id = $(this).attr('id');
        //
        //     console.log(id);
        //     actualizarTablaCompetencia(id.substring(1));
        //     actualizarActividadesEvaluativas(id.substring(1));
        // });

        $('.collapse').on('show.bs.collapse', function(e) {
            if ($(this).is(e.target)) {
                if(this.id.substring(0,1) === 'm'){
                    actualizarTablaCompetencia(this.id.substring(1));
                    actualizarActividadesEvaluativas(this.id.substring(1));
                }
            }
        });

        $('.date-time').bootstrapMaterialDatePicker({
            format: 'DD/MM/YYYY HH:mm',
            lang: 'es',
        });
    </script>
@endpush
