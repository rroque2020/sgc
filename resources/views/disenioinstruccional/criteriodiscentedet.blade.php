
<div class="col-lg-4 col-md-4 col-sm-12">
    <div class="form-group">
        <label class="form-label font-weight-bold">Validar DNI</label>
        <p class="text-justify subrayado">
            {{ isset($item) && $item->dni == 1 ? 'SI' : 'NO' }}
        </p>
    </div>
</div>
<div class="col-lg-4 col-md-4 col-sm-12">
    <div class="form-group">
        <label class="form-label font-weight-bold">Solo Aprobados</label>
        <p class="text-justify subrayado">
            {{ isset($item) && $item->aprobado == 1 ? 'SI' : 'NO' }}
        </p>
    </div>
</div>
<div class="col-lg-4 col-md-4 col-sm-12">
    <div class="form-group">
        <label class="form-label font-weight-bold">No Cruce de Horarios</label>
        <p class="text-justify subrayado">
            {{ isset($item) && $item->temporalidad == 1 ? 'SI' : 'NO' }}
        </p>
    </div>
</div>
<div class="col-lg-4 col-md-4 col-sm-12">
    <div class="form-group">
        <label class="form-label font-weight-bold">No Sancionado</label>
        <p class="text-justify subrayado">
            {{ isset($item) && $item->sancion == 1 ? 'SI' : 'NO' }}
        </p>
    </div>
</div>
<div class="col-lg-4 col-md-4 col-sm-12">
    <div class="form-group">
        <label class="form-label font-weight-bold">Invitados</label>
        <p class="text-justify subrayado">
            {{ isset($item) && $item->invitados == 1 ? 'SI' : 'NO' }}
        </p>
    </div>
</div>
<div class="col-lg-4 col-md-4 col-sm-12">
    <div class="form-group">
        <label class="form-label font-weight-bold">Reducir Brecha</label>
        <p class="text-justify subrayado">
            {{ isset($item) && $item->brecha == 1 ? 'SI' : 'NO' }}
        </p>
    </div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12">
    <div class="form-group">
        <label class="form-label font-weight-bold">Sistema de Personal</label>
        <p class="text-justify subrayado">
            {{ isset($item) ? $perfilesseleccionados : '' }}
        </p>
    </div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12">
    <div class="form-group">
        <label class="form-label font-weight-bold">Especialidad</label>
        <p class="text-justify subrayado">
            {{ isset($item) ? $item->especialidad : '' }}
        </p>
    </div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12">
    <div class="form-group">
        <label class="form-label font-weight-bold">Cargo</label>
        <p class="text-justify subrayado">
            {{ isset($item) ? $cargosseleccionados : '' }}
        </p>
    </div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12">
    <div class="form-group">
        <label class="form-label font-weight-bold">Distrito Fiscal</label>
        <p class="text-justify subrayado">
            {{ isset($item) ? $distritosseleccionados : '' }}
        </p>
    </div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12">
    <div class="pull-right">
        @if(isset($item))
            <a onclick="frmRegistroCriterioDiscente({{ $item->actividad_academica_id }},1)" class="btn btn-primary btn-rounded mt-3 pull-right" style="color: #fff !important;" data-toggle="tooltip" data-placement="top" title="Actualizar Datos">
                Establecer Criterios de Selección Discente
            </a>
        @else
            <a onclick="frmRegistroCriterioDiscente({{ $actividad->id }},0)" class="btn btn-primary btn-rounded mt-3 pull-right" style="color: #fff !important;" data-toggle="tooltip" data-placement="top" title="Actualizar Datos">
                Establecer Criterios de Selección Discente
            </a>
        @endif

    </div>
</div>
