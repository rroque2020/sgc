@if(isset($competencias) && count($competencias) > 0)
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="email-left-box px-0 mb-5">
                <div class="list-group" id="list-tab" role="tablist">
                    @foreach($competencias as $competencia)
                        <a class="list-group-item list-group-item-action text-justify" id="comp{{ $competencia->id }}"
                           onclick="actualizarCapacidades({{ $competencia->di_modulo_id  }},{{ $competencia->id }})" data-toggle="list" >
                            <strong>Competencia N° {{ $competencia->orden }}</strong><br>
                            <span class="text-justify">{{ substr($competencia->descripcion, 0, 150).'...' }}</span>
                        </a>
                    <div class="d-flex justify-content-between">
                        <div class="text-left">
                            <a class="text-info" onclick="frmCompetencia({{ $competencia->di_modulo_id }}, {{ $competencia->id }})"
                               data-toggle="tooltip" data-placement="top" title="Editar Competencia">
                                (<i class="la la-pencil"></i>)
                            </a>
                            <a class="text-info" onclick="eliminarCompetencia({{ $competencia->di_modulo_id }},{{ $competencia->id }})"
                               data-toggle="tooltip" data-placement="top" title="Eliminar Competencia">
                                (<i class="la la-trash"></i>)
                            </a>
                        </div>
                        <div class="text-right">
                            <a class="text-danger subrayado" onclick="frmVerCompetencia({{ $competencia->id }})"
                               data-toggle="tooltip" data-placement="top" title="Leer más">
                                Leer más
                            </a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="email-right-box ml-0 ml-sm-4 ml-sm-0">
                <div class="row mb-1">
                    <div id="detcapacidad{{ $modulo }}" class="col-lg-12 col-md-12 colsm-12">

                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    <div class='informado'>
        <label style='color:#0055A0'>... No se encontraron registros ...</label>
    </div>
@endif
