@if(!isset($modulos) || count($modulos) == 0)
    <div class='informado'>
        <label style='color:#0055A0'>... No se encontraron registros ...</label>
    </div>
@else
    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12">
            <div id="mod-acc" class="accordion accordion-header-bg accordion-rounded-stylish  accordion-header-shadow accordion-bordered">
            @foreach($modulos as $modulo)
                    <div class="accordion__item">
                        <div class="accordion__header collapsed cabecera-primary-accordion" data-toggle="collapse"  data-target="#m{{ $modulo->id }}">
                            <span class="accordion__header--text">{{ $modulo->descripcion.': '.$modulo->nombre }}</span>
                            <span class="accordion__header--indicator style_two"></span>&nbsp;&nbsp;
                            <a class="text-info" onclick="frmModulo({{ $modulo->actividad_academica_id }},{{ $modulo->id }})" data-toggle="tooltip" data-placement="top" title="Editar">
                                (<i class="la la-pencil"></i>)
                            </a>
                            <a class="text-info" onclick="eliminarModulo({{ $modulo->id }})" data-toggle="tooltip" data-placement="top" title="Eliminar">
                                (<i class="la la-trash"></i>)
                            </a>
                            <br>
                            <span class="text-sm-left text-white-50">
                                {{ $modulo->tipo }} : Desde el {{ \Carbon\Carbon::createFromFormat('Y-m-d',$modulo->fecha_inicio)->format('d/m/Y') }} al {{ \Carbon\Carbon::createFromFormat('Y-m-d',$modulo->fecha_final)->format('d/m/Y') }}, durante {{ $modulo->duracion }} horas académicas.
                            </span>
                        </div>
                        <div id="m{{ $modulo->id }}" class="modulos collapse accordion__body " data-parent="#mod-acc"  >
                            <div class="accordion__body--text">
                                <div class="card mb-0" style="height: auto !important;">
                                    <div class="card-header bg-primary text-white text-center py-2" style="display: block !important;">
                                        <strong>PLANIFICACIÓN</strong>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-xs-12">
                                                <div class="email-left-box">
                                                    <div class="w-100 text-right">
                                                        <a class="text-success text-bold" onclick="frmCompetencia({{ $modulo->id }}, 0)"
                                                           data-toggle="tooltip" data-placement="top" title="Nueva Competencia">
                                                            Nuevo (<i class="la la-plus"></i>)
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-xs-12">
                                                <div id="competencia{{ $modulo->id }}">
                                                    @include('disenioinstruccional.tablacompetencia',['modulo' => $modulo->id])
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card mb-0" style="height: auto !important;">
                                    <div class="card-header bg-primary text-white text-center py-2" style="display: block !important;">
                                        <strong>ACTIVIDAD EVALUATIVA</strong>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-xs-12">
                                                <div class="email-left-box">
                                                    <div class="w-100 text-right">
                                                        <a class="text-success text-bold" onclick="frmActividadEvaluativa({{ $modulo->id }}, 0)"
                                                           data-toggle="tooltip" data-placement="top" title="Nueva Actividad Evaluativa">
                                                            Nuevo (<i class="la la-plus"></i>)
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="listaactividadesevaluativas{{ $modulo->id }}" class="col-lg-12 col-md-12 col-xs-12">
                                                @include('actividadevaluativa.index',['modulo' => $modulo->id])
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            @endforeach
                </div>

        </div>
    </div>
@endif

