<div class="col-lg-3 col-md-3 col-sm-12">
    <div class="form-group">
        <label class="form-label font-weight-bold" for="fechainicio">Fecha de Inicio</label>
        <p class="text-justify subrayado">
            {{ isset($actividad->fecha_inicioe) ? \Carbon\Carbon::createFromFormat('Y-m-d',$actividad->fecha_inicioe)->format('d/m/Y') : '' }}
        </p>
    </div>
</div>
<div class="col-lg-3 col-md-3 col-sm-12">
    <div class="form-group">
        <label class="form-label font-weight-bold" for="fechafinal">Fecha Final</label>
        <p class="text-justify subrayado">
            {{ isset($actividad->fecha_finale) ? \Carbon\Carbon::createFromFormat('Y-m-d',$actividad->fecha_finale)->format('d/m/Y') : '' }}
        </p>
    </div>
</div>
<div class="col-lg-3 col-md-3 col-sm-12">
    <div class="form-group">
        <label class="form-label font-weight-bold" for="nroaulas">Nro de Aulas</label>
        <p class="text-justify subrayado">
            {{ isset($actividad->nro_aulase) ? $actividad->nro_aulase : '' }}
        </p>
    </div>
</div>
<div class="col-lg-3 col-md-3 col-sm-12">
    <div class="form-group">
        <label class="form-label font-weight-bold" for="nrovacantes">Nro de Vacantes</label>
        <p class="text-justify subrayado">
            {{ isset($actividad->nro_vacantese) ? $actividad->nro_vacantese : '' }}
        </p>
    </div>
</div>
<div class="col-lg-3 col-md-3 col-sm-12">
    <div class="form-group">
        <label class="form-label font-weight-bold" for="nrodocentes">Nro de Docentes</label>
        <p class="text-justify subrayado">
            {{ isset($actividad->nro_docentese) ? $actividad->nro_docentese : '' }}
        </p>
    </div>
</div>
<div class="col-lg-3 col-md-3 col-sm-12">
    <div class="form-group">
        <label class="form-label font-weight-bold" for="duracion">Duración</label>
        <p class="text-justify subrayado">
            {{ isset($actividad->duracione) ? $actividad->duracione : '' }} Horas Académicas
        </p>
    </div>
</div>
<div class="col-lg-6 col-md-6 col-sm-12">
    <div class="form-group">
        <label class="form-label font-weight-bold" for="tipomodulo">Tipo de Estructura</label>
        <p class="text-justify subrayado">
            {{ isset($actividad->destipo_modulo) ? $actividad->destipo_modulo : '' }}
        </p>
    </div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12">
    <div class="pull-right">
        <a onclick="frmRegistroEjecucion({{ $actividad->id }})" class="btn btn-primary btn-rounded mt-3 pull-right" style="color: #fff !important;" data-toggle="tooltip" data-placement="top" title="Actualizar Datos">
            Actualizar Datos de Ejecución
        </a>
    </div>
</div>
