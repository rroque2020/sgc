@if(isset($totalesdoc))
    <li class="list-group-item d-flex justify-content-between">
        <span class="mb-0">Actividades Académicas - Docente</span> <strong class="text-muted">{{ $totalesdoc->totactividad }}	</strong>
    </li>
    <li class="list-group-item d-flex justify-content-between">
        <span class="mb-0">Experiencia Laboral</span> <strong class="text-muted">{{ $totalesdoc->totexperiencia }}	</strong>
    </li>
    <li class="list-group-item d-flex justify-content-between">
        <span class="mb-0">Capacitaciones</span> <strong class="text-muted">{{ $totalesdoc->totcapacitacion }}	</strong>
    </li>
@endif
@if(isset($totalesdisc))
    <li class="list-group-item d-flex justify-content-between">
        <span class="mb-0">Actividades Académicas - Discente</span> <strong class="text-muted">{{ $totalesdisc->totactividad }}	</strong>
    </li>
@endif

