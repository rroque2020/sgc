<div class="dlabnav" style="width:0px !important;">
    <div class="dlabnav-scroll">
        <ul class="metismenu" id="menu">
            <li class="nav-label first">MENÚ PRINCIPAL</li>
            <li><a class="ai-icon" href="/" aria-expanded="false">
                    <i class="la la-home"></i>
                    <span class="nav-text">Inicio</span>
                </a>
            </li>
            <li><a class="ai-icon" href="/" aria-expanded="false">
                    <i class="la la-user"></i>
                    <span class="nav-text">Docente</span>
                </a>
            </li>
            <li><a class="ai-icon" href="/discentes" aria-expanded="false">
                    <i class="la la-users"></i>
                    <span class="nav-text">Discente</span>
                </a>
            </li>
        </ul>
    </div>
</div>
