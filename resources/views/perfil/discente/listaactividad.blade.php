@if(!isset($actividadesdisc) || count($actividadesdisc) == 0)
    <div class='mt-4 informado'>
        <label style='color:#0055A0'>... No se encontraron registros ...</label>
    </div>
@else
    <div class="table-responsive mt-4 text-center">
        <table id="tblactividad" class="table table-bordered table-hover table-striped verticle-middle table-responsive-sm">
            <thead class="thead-encabezado">
            <tr>
                <th>Código</th>
                <th>Actividad Académica</th>
                <th>F. Inicio</th>
                <th>F. Final</th>
                <th>Estado </th>
                <th>Certificado</th>
            </tr>
            </thead>
            <tbody>
            @foreach($actividadesdisc as $actividad)
                <tr>
                    <td>{{ $actividad->codigo }}</td>
                    <td>{{ $actividad->titulo }}</td>
                    <td style="min-width: 80px;">
                        {{ \Carbon\Carbon::createFromFormat('Y-m-d',$actividad->fecha_inicio)->format('d/m/Y') }}
                    </td>
                    <td style="min-width: 80px;">
                        {{ \Carbon\Carbon::createFromFormat('Y-m-d',$actividad->fecha_final)->format('d/m/Y') }}
                    </td>
                    <td> {!!html_entity_decode(Helper::estadoAA($actividad->estado, false))!!}</td>
                    <td>
                        @if(isset($actividad->certificado) && $actividad->certificado <> '')
                            <a href="{{ asset('archivos/'.$actividad->certificado) }}" target="_blank">
                                <img src="{{ asset('assets/images/pdf.png') }}" alt="PDF"></a>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endif
