<div class="profile-personal-info">
    <h4 class="text-primary mb-4">Información Personal</h4>
    <hr>
    <div class="row mb-3">
        <div class="col-lg-3 col-md-4 col-sm-6 col-6">
            <h5 class="f-w-500">Documento <span class="pull-right">:</span>
            </h5>
        </div>
        <div class="col-lg-9 col-md-8 col-sm-6 col-6"><span>{{ $persona->dni }}</span>
        </div>
    </div>
    <div class="row mb-3">
        <div class="col-lg-3 col-md-4 col-sm-6 col-6">
            <h5 class="f-w-500">Nombres y Apellidos <span class="pull-right">:</span>
            </h5>
        </div>
        <div class="col-lg-9 col-md-8 col-sm-6 col-6"><span>{{ $persona->nombres. ' '. $persona->apellidos }}</span>
        </div>
    </div>
    <div class="row mb-3">
        <div class="col-lg-3 col-md-4 col-sm-6 col-6">
            <h5 class="f-w-500">Fecha de Nacimiento<span class="pull-right">:</span>
            </h5>
        </div>
        <div class="col-lg-9 col-md-8 col-sm-6 col-6"><span>{{ \Carbon\Carbon::createFromFormat('Y-m-d',$persona->fecha)->format('d/m/Y') }}</span>
        </div>
    </div>
    <div class="row mb-3">
        <div class="col-lg-3 col-md-4 col-sm-6 col-6">
            <h5 class="f-w-500">Celular <span class="pull-right">:</span>
            </h5>
        </div>
        <div class="col-lg-9 col-md-8 col-sm-6 col-6"><span>{{ $persona->celular }}</span>
        </div>
    </div>
    <div class="row mb-3">
        <div class="col-lg-3 col-md-4 col-sm-6 col-6">
            <h5 class="f-w-500">Correo <span class="pull-right">:</span>
            </h5>
        </div>
        <div class="col-lg-9 col-md-8 col-sm-6 col-6"><span>{{ $persona->email }}</span>
        </div>
    </div>
    <div class="row mb-3">
        <div class="col-lg-3 col-md-4 col-sm-6 col-6">
            <h5 class="f-w-500">Distrito Fiscal/Institución / Dependencia<span class="pull-right">:</span></h5>
        </div>
        <div class="col-lg-9 col-md-8 col-sm-6 col-6"><span>{{ $persona->institucion }}</span>
        </div>
    </div>
    <div class="row mb-0">
        <div class="col-lg-3 col-md-4 col-sm-6 col-6">
            <h5 class="f-w-500">Cargo <span class="pull-right">:</span></h5>
        </div>
        <div class="col-lg-9 col-md-8 col-sm-6 col-6"><span>{{ $persona->cargo }}</span>
        </div>
    </div>
</div>
