@if(!isset($experiencias) || count($experiencias) == 0)
    <div class='mt-4 informado'>
        <label style='color:#0055A0'>... No se encontraron registros ...</label>
    </div>
@else
    <div class="table-responsive mt-4 text-center">
        <table id="tblexperiencia" class="table table-bordered table-hover table-striped verticle-middle table-responsive-sm">
            <thead class="thead-encabezado">
            <tr>
                <th>Entidad</th>
                <th>Cargo Ocupado</th>
                <th>Área</th>
                <th>F. Ingreso </th>
                <th>F. Egreso </th>
                <th>Sustento</th>
                <th>Acciones </th>
            </tr>
            </thead>
            <tbody>
            @foreach($experiencias as $experiencia)
                <tr>
                    <td>{{ $experiencia->entidad }}</td>
                    <td>{{ $experiencia->cargo }}</td>
                    <td>{{ $experiencia->area }}</td>
                    <td style="min-width: 80px;">
                        {{ \Carbon\Carbon::createFromFormat('Y-m-d',$experiencia->ingreso)->format('d/m/Y') }}
                    </td>
                    <td style="min-width: 80px;">
                        {{ \Carbon\Carbon::createFromFormat('Y-m-d',$experiencia->egreso)->format('d/m/Y') }}
                    </td>
                    <td>
                        @if(isset($experiencia->sustento) && $experiencia->sustento <> '')
                            <a href="{{ asset('archivos/'.$experiencia->sustento) }}" target="_blank">
                                <img src="{{ asset('assets/images/pdf.png') }}" alt="PDF"></a>
                        @endif
                    </td>
                    <td>
                        <a onclick="eliminarExperiencia({{ $experiencia->id }})" class="btn btn-sm btn-danger"><i class="la la-trash"></i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endif
