@if(!isset($formaciones) || count($formaciones) == 0)
    <div class='mt-4 informado'>
        <label style='color:#0055A0'>... No se encontraron registros ...</label>
    </div>
@else
    <div class="table-responsive mt-4 text-center">
        <table id="tblformacion" class="table table-bordered table-hover table-striped verticle-middle table-responsive-sm">
            <thead class="thead-encabezado">
            <tr>
                <th>Grado Académico</th>
                <th>Institución</th>
                <th>Profesión</th>
                <th>A. Ingreso </th>
                <th>A. Egreso </th>
                <th>Sustento</th>
                <th>Acciones </th>
            </tr>
            </thead>
            <tbody>
            @foreach($formaciones as $formacion)
                <tr>
                    <td>{{ $formacion->gradodes }}</td>
                    <td>{{ $formacion->instituciondes }}</td>
                    <td>{{ $formacion->profesiondes }}</td>
                    <td>{{ $formacion->ingreso }}</td>
                    <td>{{ $formacion->egreso }}</td>
                    <td>
                        @if(isset($formacion->sustento) && $formacion->sustento <> '')
                            <a href="{{ asset('archivos/'.$formacion->sustento) }}" target="_blank">
                                <img src="{{ asset('assets/images/pdf.png') }}" alt="PDF"></a>
                        @endif
                    </td>
                    <td>
                        <a onclick="eliminarFormacion({{ $formacion->id }})" class="btn btn-sm btn-danger"><i class="la la-trash"></i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endif
