@if(!isset($capacitaciones) || count($capacitaciones) == 0)
    <div class='mt-4 informado'>
        <label style='color:#0055A0'>... No se encontraron registros ...</label>
    </div>
@else
    <div class="table-responsive mt-4 text-center">
        <table id="tblcapacitacion" class="table table-bordered table-hover table-striped verticle-middle table-responsive-sm">
            <thead class="thead-encabezado">
            <tr>
                <th>Tipo de Estudio</th>
                <th>Institución</th>
                <th>Especialidad</th>
                <th>Ingreso</th>
                <th>Egreso</th>
                <th>Horas</th>
                <th>Sustento</th>
                <th>Acciones </th>
            </tr>
            </thead>
            <tbody>
            @foreach($capacitaciones as $capacitacion)
                <tr>
                    <td>{{ $capacitacion->tipo }}</td>
                    <td>{{ $capacitacion->institucion }}</td>
                    <td>{{ $capacitacion->especialidad }}</td>
                    <td style="min-width: 80px;">
                        {{ \Carbon\Carbon::createFromFormat('Y-m-d',$capacitacion->ingreso)->format('d/m/Y') }}
                    </td>
                    <td style="min-width: 80px;">
                        {{ \Carbon\Carbon::createFromFormat('Y-m-d',$capacitacion->egreso)->format('d/m/Y') }}
                    </td>
                    <td>{{ $capacitacion->horas }}</td>
                    <td>
                        @if(isset($capacitacion->sustento) && $capacitacion->sustento <> '')
                            <a href="{{ asset('archivos/'.$capacitacion->sustento) }}" target="_blank">
                                <img src="{{ asset('assets/images/pdf.png') }}" alt="PDF"></a>
                        @endif
                    </td>
                    <td>
                        <a onclick="eliminarCapacitacion({{ $capacitacion->id }})" class="btn btn-sm btn-danger"><i class="la la-trash"></i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endif
