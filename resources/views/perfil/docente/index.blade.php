@extends('perfil.layouts.layout')
@section('title')
    <div class="col-sm-6 p-md-0">
        <div class="welcome-text">
            <h4>Datos de Perfil del Docente</h4>
        </div>
    </div>
@endsection

@section('breadcrumb')
@endsection

@section('content')
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">PERFIL</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-3 col-xxl-4 col-lg-4">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card">
                                        <div class="text-center p-3 overlay-box" style="background-color: #143B64 !important;">
                                            <div class="profile-photo">
                                                <form id="frmfoto" class="frmfoto" enctype="multipart/form-data">
                                                    @csrf
                                                    <input type="file" id="foto" name="foto" accept="image/jpeg" style="display: none;" >

                                                    <div class="dropdown pull-right" >
                                                        <a id="selfoto"><i class="la la-pencil" style="color:#fff;" data-toggle="tooltip" data-placement="top" title="Modificar Foto"></i></a>
                                                    </div>
                                                </form>
                                                <div id="perfil-foto">
                                                    @include('perfil.foto')
                                                </div>
                                            </div>
                                            <h3 class="mt-3 mb-1 text-white">{{ $docente->nombres }}</h3>
                                            <p class="text-white mb-0">{{ $docente->cargo }}</p>
                                            <p class="text-white-50 mb-0">{{ $docente->tipodet }}</p>
                                        </div>
                                        <div id="resumen">
                                            @include('perfil.docente.resumen')
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-9 col-xxl-8 col-lg-8">
                            <div class="card">
                                <div class="card-body">
                                    <div class="profile-personal-info">
                                        <h4 class="text-primary mb-4">Información Personal</h4>
                                        <hr>
                                        <div class="row mb-3">
                                            <div class="col-lg-3 col-md-4 col-sm-6 col-6">
                                                <h5 class="f-w-500">Documento <span class="pull-right">:</span>
                                                </h5>
                                            </div>
                                            <div class="col-lg-9 col-md-8 col-sm-6 col-6"><span>{{ $docente->dni }}</span>
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <div class="col-lg-3 col-md-4 col-sm-6 col-6">
                                                <h5 class="f-w-500">Nombres y Apellidos <span class="pull-right">:</span>
                                                </h5>
                                            </div>
                                            <div class="col-lg-9 col-md-8 col-sm-6 col-6"><span>{{ $docente->nombres. ' '. $docente->apellidos }}</span>
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <div class="col-lg-3 col-md-4 col-sm-6 col-6">
                                                <h5 class="f-w-500">Celular <span class="pull-right">:</span>
                                                </h5>
                                            </div>
                                            <div class="col-lg-9 col-md-8 col-sm-6 col-6"><span>{{ $docente->celular }}</span>
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <div class="col-lg-3 col-md-4 col-sm-6 col-6">
                                                <h5 class="f-w-500">Correo <span class="pull-right">:</span>
                                                </h5>
                                            </div>
                                            <div class="col-lg-9 col-md-8 col-sm-6 col-6"><span>{{ $docente->email }}</span>
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <div class="col-lg-3 col-md-4 col-sm-6 col-6">
                                                <h5 class="f-w-500">Distrito Fiscal/Institución / Dependencia<span class="pull-right">:</span></h5>
                                            </div>
                                            <div class="col-lg-9 col-md-8 col-sm-6 col-6"><span>{{ $docente->institucion }}</span>
                                            </div>
                                        </div>
                                        <div class="row mb-0">
                                            <div class="col-lg-3 col-md-4 col-sm-6 col-6">
                                                <h5 class="f-w-500">Cargo <span class="pull-right">:</span></h5>
                                            </div>
                                            <div class="col-lg-9 col-md-8 col-sm-6 col-6"><span>{{ $docente->cargo }}</span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="mensaje"></div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="aa">
                                <ul class="nav nav-tabs">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#datos">DATOS PERSONALES</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#formacion">FORMACIÓN ACADÉMICA</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#capacitacion">CAPACITACIONES</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#experiencia">EXPERIENCIA LABORAL</a>
                                    </li>
                                    <li class="nav-item" style=" background-color: #00A2FF">
                                        <a class="nav-link" data-toggle="tab" href="#certificado">ACTIVIDADES ACADÉMICAS</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade show active" id="datos" role="tabpanel">
                                        <form id="frmdatospersonales" class="frmdatospersonales">
                                            <div class="pt-4">
                                                    <input type="hidden" id="id" name="id" value="{{ $docente->id }}">
                                                    <input type="hidden" id="tipo" name="tipo" value="{{ $docente->tipo }}" >
                                                    @csrf
                                                <div class="row pl-3">
                                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                                        <div class="form-group">
                                                            <label class="form-label" for="celular">Celular</label>
                                                            <input type="text" id="celular" name="celular" class="form-control" onkeypress="return esNumerico(event)"
                                                                   maxlength="9" value="{{ $docente->celular }}">
                                                            <div id="celular_error" class="invalid-feedback animated fadeInUp"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                                        <div class="form-group">
                                                            <label class="form-label">Fecha de Nacimiento</label>
                                                            <input type="text" id="fecha" name="fecha" class="form-control mdate"
                                                                   value="{{ isset($docente->fecha) ? \Carbon\Carbon::createFromFormat('Y-m-d',$docente->fecha)->format('d/m/Y') : '' }}">

                                                            <div id="fecha_error" class="invalid-feedback animated fadeInUp"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                                        <div class="form-group">
                                                            <label class="form-label" for="genero">Genero</label>
                                                            <select id="genero" name="genero" class="form-control">
                                                                <option value="-1"><< SELECCIONE >></option>
                                                                <option value="M" {{ $docente->genero == 'M' ? 'selected' : '' }}>MASCULINO</option>
                                                                <option value="F" {{ $docente->genero == 'F' ? 'selected' : '' }}>FEMENINO</option>
                                                            </select>
                                                            <div id="genero_error" class="invalid-feedback animated fadeInUp"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="form-group">
                                                            <label class="form-label" for="correo">Correo</label>
                                                            <input type="text" id="correo" name="correo" class="form-control"
                                                                   value="{{ $docente->email }}">
                                                            <div id="correo_error" class="invalid-feedback animated fadeInUp"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="pull-right">
                                                <button class="btn btn-success btn-rounded px-4" style="color: #fff !important;" data-toggle="tooltip" data-placement="top" title="Actualizar Datos">Actualizar Datos</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane fade" id="formacion">
                                        <form id="frmformacion" class="frmformacion" enctype="multipart/form-data">
                                            <div class="pt-4">
                                                <div class="row pl-3">
                                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                                        @csrf
                                                        <div class="form-group">
                                                            <label class="form-label" for="grado">Grado Académico</label>
                                                            <select id="grado" name="grado" class="form-control" data-live-search="true">
                                                                <option value = "-1"><< SELECCIONE >></option>
                                                                @foreach($grados as $grado)
                                                                    <option value="{{ $grado->id }}">{{ $grado->descripcion }}</option>
                                                                @endforeach
                                                            </select>
                                                            <div id="grado_error" class="invalid-feedback animated fadeInUp"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                                        <div class="form-group">
                                                            <label class="form-label" for="institucion">Institución</label>
                                                            <select id="institucion" name="institucion" class="form-control" data-live-search="true">
                                                                <option value = "-1"><< SELECCIONE >></option>
                                                                @foreach($instituciones as $institucion)
                                                                    <option value="{{ $institucion->id }}">{{ $institucion->descripcion }}</option>
                                                                @endforeach
                                                            </select>
                                                            <div id="institucion_error" class="invalid-feedback animated fadeInUp"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                                        <div class="form-group">
                                                            <label class="form-label" for="profesion">Profesión</label>
                                                            <select id="profesion" name="profesion" class="form-control" data-live-search="true">
                                                                <option value = "-1"><< SELECCIONE >></option>
                                                                @foreach($profesiones as $profesion)
                                                                    <option value="{{ $profesion->id }}">{{ $profesion->descripcion }}</option>
                                                                @endforeach
                                                            </select>
                                                            <div id="profesion_error" class="invalid-feedback animated fadeInUp"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-12">
                                                        <div class="form-group">
                                                            <label class="form-label">Año de Ingreso</label>
                                                            <div class="input-group mb-3">
                                                                <input type="text" id="ingreso" name="ingreso" class="form-control fecha" autocomplete="off">
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text"><i class="la la-calendar"></i></span>
                                                                </div>
                                                            </div>
                                                            <div id="ingreso_error" class="invalid-feedback animated fadeInUp"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-12">
                                                        <div class="form-group">
                                                            <label class="form-label">Año de Egreso</label>
                                                            <div class="input-group mb-3">
                                                                <input type="text" id="egreso" name="egreso" class="form-control fecha" autocomplete="off">
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text"><i class="la la-calendar"></i></span>
                                                                </div>
                                                            </div>
                                                            <div id="egreso_error" class="invalid-feedback animated fadeInUp"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                                        <div class="form-group fallback w-100">
                                                            <label class="form-label">Sustento</label>
                                                            <div class="input-group">
                                                                <div class="custom-file">
                                                                    <input type="file" class="custom-file-input" id="sustentoform" name="sustentoform" accept="application/pdf">
                                                                    <label id="sustentoforml" class="custom-file-label" for="sustentoform">Seleccionar</label>
                                                                </div>
                                                            </div>
                                                            <div id="sustentoform_error" class="invalid-feedback animated fadeInUp"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                                        <div class="form-group">
                                                            <br>
                                                            <button type="submit" class="btn btn-rounded btn-primary mb-0 pull-right" data-toggle="tooltip" data-placement="top" title="Agregar Formación">
                                                                <i class="la la-plus"></i>&nbsp;Agregar
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <hr>
                                        <div id="listadoformacion" class="col-lg-12 col-md-12 col-sm-12">
                                            @include('perfil.docente.listaformacion')
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="capacitacion">
                                        <form id="frmcapacitacion" class="frmcapacitacion" enctype="multipart/form-data">
                                            <div class="pt-4">
                                                <div class="row pl-3">
                                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                                        <div class="form-group">
                                                            <label class="form-label" for="tipocap">Tipo de Estudio o Capacitación</label>
                                                            <input type="text" id="tipocap" name="tipocap" class="form-control">
                                                            <div id="tipocap_error" class="invalid-feedback animated fadeInUp"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                                        <div class="form-group">
                                                            <label class="form-label" for="institucioncap">Institución</label>
                                                            <input type="text" id="institucioncap" name="institucioncap" class="form-control">
                                                            <div id="institucioncap_error" class="invalid-feedback animated fadeInUp"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                                        <div class="form-group">
                                                            <label class="form-label" for="especialidadcap">Especialidad</label>
                                                            <input type="text" id="especialidadcap" name="especialidadcap" class="form-control">
                                                            <div id="especialidadcap_error" class="invalid-feedback animated fadeInUp"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-xs-12">
                                                        <div class="form-group">
                                                            <label class="form-label" for="ingresocap">Fecha de Ingreso</label>
                                                            <input type="text" id="ingresocap" name="ingresocap" class="form-control mdate">
                                                            <div id="ingresocap_error" class="invalid-feedback animated fadeInUp"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-xs-12">
                                                        <div class="form-group">
                                                            <label class="form-label" for="egresocap">Fecha de Egreso</label>
                                                            <input type="text" id="egresocap" name="egresocap" class="form-control mdate">
                                                            <div id="egresocap_error" class="invalid-feedback animated fadeInUp"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-12">
                                                        <div class="form-group">
                                                            <label class="form-label" for="horascap">Horas</label>
                                                            <input type="number" id="horascap" name="horascap" min="0" max="100" class="form-control">
                                                            <div id="horascap_error" class="invalid-feedback animated fadeInUp"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                                        <div class="form-group fallback w-100">
                                                            <label class="form-label">Sustento</label>
                                                            <div class="input-group">
                                                                <div class="custom-file">
                                                                    <input type="file" class="custom-file-input" id="sustentocap" name="sustentocap" accept="application/pdf">
                                                                    <label id="sustentocapl" class="custom-file-label" for="sustentocap">Seleccionar</label>
                                                                </div>
                                                            </div>
                                                            <div id="sustentocap_error" class="invalid-feedback animated fadeInUp"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-12">
                                                        <div class="form-group">
                                                            <br>
                                                            <button type="submit" class="btn btn-rounded btn-primary mb-0 pull-right" data-toggle="tooltip" data-placement="top" title="Agregar Capacitación">
                                                                <i class="la la-plus"></i>&nbsp;Agregar
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <hr>
                                        <div id="listadocapacitacion" class="col-lg-12 col-md-12 col-sm-12">
                                            @include('perfil.docente.listacapacitacion')
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="experiencia">
                                        <form id="frmexperiencia" class="frmexperiencia" enctype="multipart/form-data">
                                            <div class="pt-4">
                                                <div class="row pl-3">
                                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                                        <div class="form-group">
                                                            <label class="form-label" for="entidadexp">Entidad</label>
                                                            <input type="text" id="entidadexp" name="entidadexp" class="form-control">
                                                            <div id="entidadexp_error" class="invalid-feedback animated fadeInUp"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                                        <div class="form-group">
                                                            <label class="form-label" for="cargoexp">Cargo</label>
                                                            <input type="text" id="cargoexp" name="cargoexp" class="form-control">
                                                            <div id="cargoexp_error" class="invalid-feedback animated fadeInUp"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                                        <div class="form-group">
                                                            <label class="form-label" for="areaexp">Área</label>
                                                            <input type="text" id="areaexp" name="areaexp" class="form-control">
                                                            <div id="areaexp_error" class="invalid-feedback animated fadeInUp"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-xs-12">
                                                        <div class="form-group">
                                                            <label class="form-label" for="ingresoexp">Fecha de Ingreso</label>
                                                            <input type="text" id="ingresoexp" name="ingresoexp" class="form-control mdate">
                                                            <div id="ingresoexp_error" class="invalid-feedback animated fadeInUp"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-xs-12">
                                                        <div class="form-group">
                                                            <label class="form-label" for="egresoexp">Fecha de Egreso</label>
                                                            <input type="text" id="egresoexp" name="egresoexp" class="form-control mdate">
                                                            <div id="egresoexp_error" class="invalid-feedback animated fadeInUp"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                                        <div class="form-group">
                                                            <label class="form-label" for="funcionesexp">Funciones Principales</label>
                                                            <textarea id="funcionesexp" name="funcionesexp" class="form-control" rows="5"></textarea>
                                                            <div id="funcionesexp_error" class="invalid-feedback animated fadeInUp"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                                        <div class="form-group fallback w-100">
                                                            <label class="form-label">Sustento</label>
                                                            <div class="input-group">
                                                                <div class="custom-file">
                                                                    <input type="file" class="custom-file-input" id="sustentoexp" name="sustentoexp" accept="application/pdf">
                                                                    <label id="sustentoexpl" class="custom-file-label" for="sustentoexp">Seleccionar</label>
                                                                </div>
                                                            </div>
                                                            <div id="sustentoexp_error" class="invalid-feedback animated fadeInUp"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="form-group">
                                                            <br>
                                                            <button type="submit" class="btn btn-rounded btn-primary mb-0 pull-right" data-toggle="tooltip" data-placement="top" title="Agregar Experiencia">
                                                                <i class="la la-plus"></i>&nbsp;Agregar
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <hr>
                                        <div id="listadoexperiencia" class="col-lg-12 col-md-12 col-sm-12">
                                            @include('perfil.docente.listaexperiencia')
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="certificado">
                                        <div class="pt-4">
                                            <div id="listadoactividad" class="col-lg-12 col-md-12 col-sm-12">
                                                @include('perfil.docente.listaactividad')
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
@push('scriptsapp')
    <script src="{{ asset('assets/js/docente-perfil.js') }}"></script>
@endpush
