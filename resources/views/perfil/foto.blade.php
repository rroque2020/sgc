@if(isset($persona->foto) && $persona->foto != '' && file_exists(public_path('fotos/'.$persona->foto)))
    <img src="{{ asset('fotos/'.$persona->foto)  }}" width="100" class="img-fluid rounded-circle" alt="">
@else
    <img src="{{ asset('assets/images/profile/profile.png')  }}" width="100" class="img-fluid rounded-circle" alt="">
@endif
